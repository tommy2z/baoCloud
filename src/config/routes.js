/**
 * 路由配置
 */

var routes = exports = module.exports = {};


routes.config = function ($stateProvider, $locationProvider, $urlRouterProvider) {
    //html5路由特性
    $locationProvider.html5Mode(false);
    //默认加载页
    $urlRouterProvider.otherwise('/VPCBox');

    $stateProvider.state('VPCBox',{
        url: '/VPCBox',
        title: 'VPCBox',
        templateUrl: 'VPCBox/VPCBox.html'
    })
    .state('VPCView',{
        url: '/VPCView',
        title: 'VPCView',
        templateUrl: 'VPCView/VPCView.html'
    });
}
