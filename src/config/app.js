'use strict'

angular
    .module('baocloud', [
        'baocloud.core',
        'baocloud.components',
        'baocloud.templates',
        'baocloud.IndexView',
        'baocloud.VPCBox',
        'baocloud.VPCView'
    ]);

require('./core');


//注册导入模块(手工)
require('../components');
require('../modules/VPCBox');
require('../modules/VPCView');
require('../modules/IndexView');
