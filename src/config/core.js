'use strict';


var routes = require('./routes');

angular
    .module('baocloud.core', [
        'ui.router'
    ])
    .config(logConfig)
    .config(coreConfig)
    .config(routes.config)
    .constant('EiPath', './EiService')
    .value('CustomerGroup',{selected:[]})
    .value('Queue',{timer:''})
    .run(appRun);

function coreConfig($provide, $controllerProvider, $compileProvider, $filterProvider, $httpProvider){
    console.info('---->coreConfig');
    //var core = angular.module('app.core');
}

function logConfig ($logProvider) {
    console.info('---->logConfig');
    $logProvider.debugEnabled(true);
}
    
function appRun($log, $rootScope , $state, $stateParams, $templateCache, $location, EiPath, CustomerGroup) {
    $log.debug('------>bgcloud.core.appRun,constant:' + EiPath + ',value:' + CustomerGroup);
    //$locationChangeStart $locationChangeSuccess $stateChangeStart $stateNotFound
    $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams, fromState, fromParams){
        /*console.log('toState-->'+JSON.stringify(toState));
        console.log('toParams-->'+JSON.stringify(toParams));
        console.log('fromState-->'+JSON.stringify(fromState));
        console.log('fromParams-->'+JSON.stringify(fromParams));*/
    });

    $rootScope.$on('$viewContentLoaded', function(event) {});
    //$rootScope.$on("$includeContentLoaded", function(event, templateName){});
}



// function routesConfig($stateProvider, $locationProvider, $urlRouterProvider) {
//     //html5路由特性
//     $locationProvider.html5Mode(false);
//     //默认加载页
//     $urlRouterProvider.otherwise('/view');

//     $stateProvider.state('view',{
//         url: '/view',
//         title: 'view',
//         templateUrl: 'views/vpc/vpc.html'
//     })
// }