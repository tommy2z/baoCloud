/**
 *
 */
angular
    .module('baocloud.VPCView')
    .factory('VPCViewHandle', VPCViewHandle)
    .controller('VPCViewCtlr', VPCViewCtlr);

function VPCViewHandle() {
    return {
        resize: function () {}
    }
}

function VPCViewCtlr($scope, $element, VPCViewHandle) {

    this.model = {
        height: 49,
        vpcs: [{
            name: 'subnet01',
            cname: '子网名称01',
            network: '192.168.161.1/16',
            uhosts: [{
                name: '云主机'
            }, {
                name: '云主机'
            }, {
                name: '云主机'
            }, {
                name: '云主机'
            }, {
                name: '云主机'
            }, {
                name: '云主机'
            }, {
                name: '云主机'
            }, {
                name: '云主机'
            }, {
                name: '云主机'
            }, {
                name: '云主机'
            }, {
                name: '云主机'
            }, {
                name: '云主机'
            }, {
                name: '云主机'
            }, {
                name: '云主机'
            }, {
                name: '云主机'
            }]
        }, {
            name: 'subnet02',
            cname: '子网名称02',
            network: '192.168.162.2/16',
            uhosts: [{
                name: '云主机A'
            }, {
                name: '云主机A'
            }, {
                name: '云主机A'
            }, {
                name: '云主机A'
            }, {
                name: '云主机A'
            }, {
                name: '云主机A'
            }]
        }, {
            name: 'subnet03',
            cname: '子网名称03',
            network: '192.168.163.3/16',
            uhosts: [{
                name: '云主机B'
            }, {
                name: '云主机B'
            }, {
                name: '云主机B'
            }, {
                name: '云主机B'
            }]
        }]
    }



    VPCViewHandle.resize = function resize(e) {
        setTimeout(function () {
            $element.find('.view-snippet').toggleClass('full-view-snippet');
            $element.find('.view-panel').toggleClass('full-view-panel');
        }, 0);
    };
}