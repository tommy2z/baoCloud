/**
 *
 */


var Raphael = require('raphael');

angular
    .module('baocloud.VPCBox')
    .directive('subnetBox', subnetBox)
    .controller('SubnetBoxController', SubnetBoxController)
    .controller('VPCController', VPCController);

function subnetBox() {
    var directive = {
        restrict: 'AE',
        replace: true,
        transclude: true,
        scope: {
            model: '='
        },
        link: link,
        template: "<div class='subnet-box'></div>",
        controller: 'SubnetBoxController',
        controllerAs: 'ctrl'

    };
    return directive;
}

function link(scope, element, attr, ctrl) {
    var subnetCount = 3;  //子网 1个
    var boxCount = 2;     //总挂 2个类型
///////////////////////////////////////////////////
    var startX = 185.5;   //X 起点
    var startY = 170;     //y 起点
    var moveX = 110;      //X 间距
    var moveY = 120;      //y 间距

    var point = 'M' + startX + ',50V' + (moveY * (subnetCount - 1) + startY);   //竖线


    var box = Raphael(element[0], '100%', '100%');

    //线样式定义
    var lineStyle = {
        'stroke': '#ffc633',
        'stroke-width': '2'
    }

    var rectStyle = {
        'fill': '#fafbff',
        'stroke': '#c7cfdc',
        'stroke-width': '2'
    }

    var rects = [];

    box.path(point).attr(lineStyle);   //竖线  height:  v++ 延伸 step 画出子网

    for(var i = 1; i <= subnetCount; i++){
        var pathY = moveY * (i-1) + 73;
        var pathV = moveY * (i-1) + 50;
        for(var j = 1; j <= boxCount; j++) {
            var spacePath = 'M'+ (startX + moveX * j + 40) + ',' + pathY + 'V' + pathV +'H' + startX;  //挂载横线
            box.path(spacePath).attr(lineStyle);
            box.rect(startX + moveX * j, moveY * (i-1) + 80, 80, 30, 5).attr(rectStyle);               //方块
        }
    }
}

function SubnetBoxController() {
    var self = this;
}

function VPCController($scope) {
    console.log('--->VPCController');
}
