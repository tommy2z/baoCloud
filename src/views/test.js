define("modules/vpc/acl/acl", function (e, t, i) {
    var n = e("./detail/detail"),
        a = e("pageManager"),
        s = (e("dialog"), e("appUtil")),
        d = e("models/acl"),
        o = e("constants"),
        c = e("es6-promise"),
        r = e("qccomponent"),
        l = e("../vpc/Selector"),
        p = e("widget/pubsubhub/pubsubhub"),
        u = e("./addAcl"),
        v = e("./bindSubnet"),
        f = e("widget/fieldsManager/gridView"),
        m = e("widget/editInPlace/editInPlace"),
        h = e("reporter"),
        b = e("../cacher"),
        g = e("../createLimitsMixin");
    e("widget/region/regionSelector");
    var I = {
            main: '    <div class="manage-area">        <div class="manage-area-title">            ' +
                    '<h2>网络ACL</h2>            <region-selector b-ref="regionSelector" b-model="regio' +
                    'nId" get-white-list="{{getWhiteList}}"></region-selector>            <vpc-select' +
                    ' b-ref="vpcSelector" region-id="{{regionId}}" b-model="vpcId"></vpc-select>     ' +
                    '       <div class="manage-area-title-right">                <a href="https://www' +
                    '.qcloud.com/doc/product/215/5132" target="_blank">                    <b class="' +
                    'links-icon"></b> 网络ACL帮助文档                </a>            </div>        </div>  ' +
                    '      <div class="tc-15-action-panel">            <button class="tc-15-btn m {{_' +
                    'vpcLength <= 1 || overSize ? \'disabled\': \'\'}}" data-title?="overSize && \'该 ' +
                    'VPC 下的网络 ACL 已达到数量上限\'" b-on-click="add()">+新建</button>            <!--<button c' +
                    'lass="tc-15-btn weak m download"></button>-->            <button b-ref="setting"' +
                    ' class="tc-15-btn weak m setting"></button>            <qc-search b-ref="search"' +
                    ' placeholder="请输入ACL名称"></qc-search>        </div>        <acl-grid b-ref="grid"' +
                    ' vpc-id="{{vpcId}}"></acl-grid>    </div>',
            actionTpl: '<span>    <a href="javascript:;" b-on-click="bindSubnet(item.aclId, item.vpcId)"' +
                    '>关联子网</a>    <span class="{{item.subnetNum ? \'disable-link\' : \'\'}}" data-tit' +
                    'le="{{item.subnetNum ? \'无法删除已关联子网的网络 ACL\' : \'\'}}">        <a b-on="{click: d' +
                    'el(item)}" href="javascript:;">删除</a>    </span></span>',
            idTd: '    <p>        <a data-event="nav"  href="/vpc/acl/detail/{{item.vpcId}}/{{item.' +
                    'aclId}}?rid={{regionId}}" class="text-overflow">{{item.aclId}}</a>    </p>    <s' +
                    'pan class="text-overflow m-width" data-name>        {{item.name}}    </span>    ' +
                    '<i class="pencil-icon hover-icon" b-on="{click: changeName(item)}" data-role="re' +
                    'name"></i>',
            editName: '    <form action="javascript:;" b-on-submit="hide(true)">        <div class="tc-' +
                    '15-input-text-wrap m" data-name>            <div class="clr">                <in' +
                    'put type="text" b-model="name" class="tc-15-input-text" maxlength="25">         ' +
                    '   </div>            <p class="tc-15-input-tips">还能输入{{25 - name.length}}个字符，英文、' +
                    '汉字、数字、连接线"-"或下划线"_"</p>        </div>    </form>'
        },
        w = r.extend({
            $mixins: [g({maxAclSize: o.VPC_LIMIT.ACL_PER_VPC_LIMIT_TYPE})]
        }, {
            defaults: {
                maxAclSize: o.MAX_ACL_SIZE
            }
        });
    w.tag("vpc-select", l);
    var y = r.getComponent("grid-view");
    w.tag("acl-grid", y.extend({
        $mixins: y
            .prototype
            .$mixins
            .concat([f("acl_main_grid_fields")]),
        actionTpl: I.actionTpl,
        showDetail: function (e, t) {
            x.showDetail(e, t)
        },
        del: function (e) {
            return function (t) {
                if (!e.$disable && !e.subnetNum) {
                    var i = new(r.getComponent("popup-confirm"))({
                        $data: {
                            trigger: "",
                            title: "确认要删除该 ACL? ",
                            style: "width: 310px;",
                            hideDestroy: !0
                        },
                        handler: t.target,
                        onConfirm: function () {
                            d.request({
                                name: "delAcl",
                                data: {
                                    vpcId: e.vpcId,
                                    aclId: e.aclId
                                },
                                success: function () {
                                    x.refreshList()
                                }
                            })
                        }
                    });
                    i.show()
                }
            }
        },
        changeName: function (e) {
            var t = this.$root;
            return function (i) {
                var n = $(i.target)
                        .closest("tr")
                        .attr("data-index"),
                    a = new m({
                        $data: {
                            trigger: "",
                            hideDestroy: !0,
                            name: e.name,
                            content: I.editName
                        },
                        handler: i.target,
                        $afterDestroy: function () {
                            t.updateItem(n, {
                                $selected: !1
                            })
                        },
                        onConfirm: function () {
                            var t = $(this.$el).find("[data-name]"),
                                i = $(this.$el).find("[data-confirm]"),
                                n = this;
                            return s.isValidName(this.name)
                                ? n.name === e.name || i.hasClass("disabled")
                                    ? void 0
                                    : (i.addClass("disabled"), new c(function (t, a) {
                                        d.request({
                                            name: "updateAclBaseInfo",
                                            data: {
                                                vpcId: e.vpcId,
                                                aclId: e.aclId,
                                                name: n.name
                                            },
                                            success: function (e) {
                                                p.trigger("baseInfoUpdated", [n.name]),
                                                i.removeClass("disabled"),
                                                t(e)
                                            },
                                            fail: function (e) {
                                                i.removeClass("disabled"),
                                                a(e)
                                            }
                                        })
                                    }))
                                : (t.addClass("error"), t.find("input").focus(), !1)
                        }
                    });
                a.show(),
                $(a.$el)
                    .find("input")
                    .select(),
                t.updateItem(n, {
                    $selected: !0
                })
            }
        },
        bindSubnet: v,
        getData: function (e, t) {
            this.$set("regionId", s.getRegionId()),
            x.getList(e, t)
        }
    }, {
        defaults: {
            hasFirst: !1,
            colums: [
                {
                    name: "ID/名称",
                    key: "aclId",
                    required: !0,
                    tdTpl: I.idTd
                }, {
                    name: "关联子网数",
                    key: "subnetNum",
                    width: 150
                }, {
                    name: "所属网络",
                    key: "vpcName",
                    tdTpl: '<span class="text-overflow"><a data-event="nav" href="/vpc/vpc?vpcId={{item.vpcI' +
                            'd}}">{{item.vpcName}} ({{item.vpcCIDR}} | {{item.uniqVpcId}})</a></span>'
                }, {
                    name: "创建时间",
                    key: "createTime",
                    hide: !0
                }, {
                    name: "操作",
                    required: !0,
                    key: "_action"
                }
            ]
        }
    }));
    var x = {
        render: function () {
            if ("detail" == arguments[0]) 
                arguments[1]
                    ? this.showDetail(arguments[1], arguments[2], arguments[3].rid)
                    : a.render404();
            else {
                var t = this.bee = new w(I.main, {
                    add: function () {
                        this._vpcLength > 1 && !this.overSize && u.render(this.vpcId)
                    },
                    getWhiteList: function () {
                        return b.getRegionIdList()
                    }
                });
                this.grid = t.$refs.grid,
                a
                    .appArea
                    .html(t.$el),
                e("../adTip").init(t.$el),
                this
                    .grid
                    .setMaxHeight(),
                this.bindEvent()
            }
        },
        getList: function (e, t) {
            var i = this;
            e = e || {},
            t = function (t, n) {
                t || (n.detail.forEach(function (e) {
                    e._vpcId = s.convertIdToWithPrefix("vpc", e.vpcId)
                }), i.grid.setData({list: n.detail, totalNum: n.totalNum, searchKey: e.searchKey}))
            },
            d.request({
                name: "getAclList",
                data: {
                    vpcId: this.bee.vpcId || void 0,
                    offset: (e.page - 1) * e.count || 0,
                    limit: e.count || 20,
                    name: e.searchKey
                        ? e.searchKey
                        : void 0
                },
                success: function (e) {
                    t(null, e)
                },
                fail: t
            })
        },
        refreshList: function () {
            this
                .grid
                .refresh()
        },
        showDetail: function (e, t, i) {
            var s = a
                    .appArea
                    .children(),
                d = this;
            i = i || d.bee && d.bee.regionId;
            var o = new n({
                $afterDestroy: function () {
                    s.show(),
                    this
                        .grid
                        .setMaxHeight()
                },
                $data: {
                    vpcId: e,
                    aclId: t,
                    regionId: i
                }
            });
            h.click("vpc.ACL.detail"),
            s.hide(),
            a
                .appArea
                .append(o.$el)
        },
        bindEvent: function () {
            var e = this;
            this
                .bee
                .$refs
                .vpcSelector
                .$on("selected", function (t) {
                    e.getList()
                }),
            this
                .bee
                .$refs
                .vpcSelector
                .$watch("list", function (t) {
                    e
                        .bee
                        .$set("_vpcLength", t.length)
                }, !0);
            var t = e.grid,
                i = e.bee;
            s.beeBind(t, i, "totalNum", "totalNum"),
            s.beeBind(i, i, "keyword || !vpcId ? 0 : totalNum", "knownCount"),
            s.beeBind(i, i, "knownCount>=maxAclSize", "overSize"),
            this
                .bee
                .$refs
                .regionSelector
                .$on("selected", function (t) {
                    e.getList()
                }),
            this
                .bee
                .$refs
                .search
                .$on("search", function (t) {
                    e.getList({searchKey: t})
                }),
            this
                .bee
                .$watch("regionId", function () {
                    e.refreshList()
                }),
            p
                .off(".acl")
                .on("added.acl", function (t, i) {
                    i != e.vpcId && e.vpcId || e.refreshList()
                })
                .on("baseInfoUpdated.acl aclSubnetUpdated.acl", function () {
                    e.refreshList()
                }),
            $(this.bee.$refs.setting).click(function () {
                e
                    .grid
                    .showFieldsManager()
            })
        },
        destroy: function () {
            p.off(".acl")
        }
    };
    i.exports = x
}),
define("modules/vpc/acl/AclSelector", function (e, t, i) {
    var n = e("qccomponent"),
        a = e("models/acl");
    i.exports = n.extend({
        $tpl: '    <span class="tc-15-list-det" data-vpc-selector>        {{_fetchList(regionId' +
                ', vpcId)}}        <select b-model="id" data-id="{{id}}" class="tc-15-select m" s' +
                'tyle="min-width:133px" b-style="style">            <option b-repeat="item in lis' +
                't" value="{{item.aclId}}" selected?="item.aclId == id">                {{item.ac' +
                'lId}} ({{item.name}})            </option>        </select>        <div class="{' +
                '{invalid ? \'is-error\' : \'\'}}" style="font-size: 12px; margin-top:3px;">     ' +
                '       <div b-style="{display: invalid && tips ? undefined : \'none\'}" class="f' +
                'orm-input-help">{{tips}}</div>        </div>    </span>',
        $valuekey: "id",
        $afterInit: function () {
            this
                .$watch("id", function (e) {
                    e && this.list && this.$set("id", e)
                })
        },
        _fetchList: function (e, t) {
            var i = this;
            e && a.request({
                name: "getAclList",
                data: {
                    regionId: e,
                    vpcId: this.vpcId || void 0,
                    offset: 0,
                    limit: 999
                },
                success: function (e) {
                    var t = e.detail;
                    i.$set({
                        list: t,
                        id: i.id || t[0] && t[0].aclId,
                        invalid: !t.length
                    })
                }
            })
        }
    }, {
        defaults: {
            tips: "无可用网络Acl"
        }
    })
}),
define("modules/vpc/acl/addAcl", function (e, t) {
    var i = e("appUtil"),
        n = e("dialog"),
        a = e("models/acl"),
        s = e("widget/pubsubhub/pubsubhub"),
        d = e("qccomponent"),
        o = e("../cacher"),
        c = '    <div class="tc-15-list-wrap form acl_layer">        <form action="javascript' +
                ':;">            <div class="tc-15-list-content">                <ul>            ' +
                '        <li class="even">                        <em aria-required="false" class' +
                '="tc-15-list-tit">名称</em>                        <span class="tc-15-list-det">  ' +
                '                           <div class="tc-15-input-text-wrap" data-name>        ' +
                '                         <input type="text" b-model="name" placeholder="ACL_1" c' +
                'lass="tc-15-input-text" maxlength="25">                                 <div cla' +
                'ss="tc-15-input-tips">还能输入{{25 - name.length}}个字符，英文、汉字、数字、连接线"-"或下划线"_"</div>  ' +
                '                           </div>                        </span>                ' +
                '    </li>                    <li class="even">                        <em aria-r' +
                'equired="false" class="tc-15-list-tit">所属网络</em>                        <span cl' +
                'ass="tc-15-list-det">                            <div class="tc-15-select-wrap m' +
                '">                                <select class="tc-15-select" b-model="vpcId"> ' +
                '                                   <option b-repeat="vpc in vpcList" value="{{vp' +
                'c.vpcId}}">{{vpc._vpcName}}</option>                                </select>   ' +
                '                         </div>                        </span>                  ' +
                '  </li>                </ul>            </div>        </form>    </div>';
    t.render = function (e) {
        var t = new d(c, {name: ""}),
            r = $(t.$el).find("[data-name]");
        o
            .getVpcList(i.getRegionId())
            .done(function (n) {
                n
                    .vpcList
                    .forEach(function (e) {
                        e._vpcName = e.vpcName + " (" + e.vpcCIDR + " | " + i.convertIdToWithPrefix("vpc", e.vpcId) + ")"
                    }),
                t.$set({vpcList: n.vpcList, vpcId: e})
            }),
        n.create(t.$el, 560, "", {
            title: "新建网络 ACL",
            button: {
                "确定": function () {
                    return i.isValidName(t.name)
                        ? void a.request({
                            name: "addAcl",
                            data: {
                                vpcId: t.vpcId || t.vpcList[0].vpcId,
                                name: t.name
                            },
                            success: function () {
                                n.hide(),
                                s.trigger("added", [t.vpcId])
                            }
                        })
                        : (r.addClass("error"), r.find("input").focus(), !1)
                }
            }
        }),
        t.$watch("name", function (e) {
            r.hasClass("error") && i.isValidName(e) && r.removeClass("error")
        }),
        $(t.$el)
            .find("input")
            .focus()
    }
}),
define("modules/vpc/acl/bindSubnet", function (e, t, i) {
    var n = e("qccomponent"),
        a = e("manager"),
        s = e("models/acl"),
        d = e("dialog"),
        o = (e("appUtil"), e("widget/pubsubhub/pubsubhub")),
        c = e("reporter"),
        r = {
            bindSubnet: '    <div class="private-network-layer">        <style>            .private-netwo' +
                    'rk-layer .tc-15-table-panel{                margin: 0;            }        </sty' +
                    'le>        <span class="sub-title">选择需要关联的子网</span>        <qc-search b-ref="sea' +
                    'rch" placeholder="请输入子网名称"></qc-search>    </div>'
        };
    i.exports = function (e, t) {
        var i = new(n.getComponent("grid-view"))({
                $data: {
                    count: 200,
                    showState: !1,
                    showPagination: !1,
                    autoMaxHeight: !1,
                    colums: [
                        {
                            name: "子网ID/名称",
                            key: "subnetName",
                            tdTpl: '<p><span class="text-overflow">{{item.uniqSubnetId}}</span></a></p><span class="' +
                                    'text-overflow m-width">{{item.subnetName}}</span>'
                        }, {
                            name: "已关联 ACL",
                            key: "aclBindTag",
                            tdTpl: '    <p>        <span class="text-overflow">{{item.aclBindTag || \'-\'}}</span></' +
                                    'a>    </p>    <span b-if="item.aclBindTag" class="text-overflow">{{item.aclName}' +
                                    '}</span>'
                        }, {
                            name: "CIDR",
                            key: "subnetCIDR"
                        }
                    ],
                    emptyTips: "没有可用子网",
                    maxHeight: 460
                },
                setTrAttr: function (e, t) {
                    return e = e || {},
                    t.$disable
                        ? (e["data-title"] = "该子网已关联 ACL", e)
                        : void 0
                },
                getData: function (e, n) {
                    e = e || {},
                    n = function (t, n) {
                        i.setData({list: n.list, totalNum: n.totalNum, searchKey: e.searchKey})
                    };
                    var d = $.Deferred();
                    a.getVpcSubnetStatisticList({
                        vpcId: t,
                        keyword: e.searchKey
                            ? e.searchKey
                            : void 0,
                        offset: (e.page - 1) * e.count || 0,
                        limit: e.count || 20,
                        checkAcl: !0
                    }, function (e) {
                        var t = e.detail;
                        d.then(function (e) {
                            var i = {},
                                a = e.detail;
                            a.forEach(function (e) {
                                i[e.aclId] = e
                            }),
                            t.map(function (e) {
                                e.$disable = e.aclBindTag,
                                void 0 != i[e.aclBindTag] && (e.aclName = i[e.aclBindTag].name)
                            }),
                            t.sort(function (e) {
                                return e.aclBindTag
                                    ? 1
                                    : -1
                            }),
                            n(null, {
                                list: t,
                                totalNum: t.length
                            })
                        }, function (e) {
                            n(null, {
                                list: t,
                                totalNum: t.length
                            })
                        })
                    }, n),
                    s.request({
                        name: "getAclList",
                        data: {
                            vpcId: t || void 0,
                            offset: 0,
                            limit: 9999,
                            name: e.searchKey
                                ? e.searchKey
                                : void 0
                        },
                        success: d.resolve,
                        fail: function () {
                            d.resolve({detail: []})
                        }
                    })
                }
            }),
            l = new n(r.bindSubnet);
        l
            .$refs
            .search
            .$on("search", function (e) {
                i.getData({searchKey: e})
            }),
        l
            .$el
            .appendChild(i.$el),
        d.create(l.$el, 680, "", {
            title: "关联子网",
            preventResubmit: !0,
            button: {
                "确定": function () {
                    var n = i.getSelected();
                    if (n.length) {
                        var a;
                        a = Promise.resolve(),
                        a.then(function () {
                            s.request({
                                name: "addSubnetAclRule",
                                data: {
                                    vpcId: t,
                                    aclId: e,
                                    subnetIds: n.map(function (e) {
                                        return e.subnetId
                                    })
                                },
                                success: function () {
                                    o.trigger("aclSubnetUpdated"),
                                    d.hide()
                                }
                            })
                        }),
                        c.click("vpc.ACL.addSubnet")
                    } else 
                        d.hide()
                }
            }
        })
    }
}),
define("modules/vpc/acl/detail/aclRules", function (e, t, i) {
    function n(e) {
        for (var t, i = 0; t = u[i]; i++) 
            if (t.proto === e) 
                return t.name
    }
    var a = e("qccomponent"),
        s = e("models/acl"),
        d = e("appUtil"),
        o = e("widget/validator/validatorV2"),
        c = e("constants"),
        r = e("../../createLimitsMixin"),
        l = {
            main: '<div>    <div class="tc-15-list-wrap acl-inf" b-if="!hideHeader">        <div cl' +
                    'ass="head-top-title">            <h3>规则列表</h3>            <a b-if="!isEdit" b-on' +
                    '-click="edit()" href="javascript:;"><i class="pencil-icon "></i>编辑</a>        </' +
                    'div>    </div>    <div style="width: 90%" b-if="!isEdit">        <acl-rules-grid' +
                    ' b-ref="grid" dir="{{dir}}" get-list="{{getList.bind(this)}}"></acl-rules-grid> ' +
                    '   </div>    <div b-style="{display: isEdit ? \'block\': \'none\'}">        <acl' +
                    '-rules-editor b-ref="editor" dir="{{dir}}" get-list="{{getList.bind(this)}}" reg' +
                    'ion-id="{{regionId}}"></acl-rules-editor>        <div class="tc-15-table-panel" ' +
                    'title="默认规则不可编辑" style="position: static">        <div class="tc-15-table-fixed-' +
                    'body">            <table class="tc-15-table-box tc-15-table-rowhover">          ' +
                    '      <colgroup>                    <col style="width:50px;"/>                  ' +
                    '  <col style="width: auto; ">                    <col style="width: auto; ">    ' +
                    '                <col style="width: auto; ">                    <col style="width' +
                    ': 120px; ">                    <col style="width: auto; ">                    <c' +
                    'ol style="width:120px;"/>                </colgroup>                <tbody>     ' +
                    '               <tr>                        <td></td>                        <td>' +
                    '                            <div>                                <span class="te' +
                    'xt-overflow">All traffic</span>                            </div>               ' +
                    '         </td>                        <td>                            <div>     ' +
                    '                           <span class="text-overflow" title="ALL">ALL</span>   ' +
                    '                         </div>                        </td>                    ' +
                    '    <td>                            <div>                                <span c' +
                    'lass="text-overflow" title="0.0.0.0/0">0.0.0.0/0</span>                         ' +
                    '   </div>                        </td>                        <td>              ' +
                    '              <div>                               <span class="text-overflow">拒绝' +
                    '</span>                            </div>                        </td>          ' +
                    '              <td>                            <div>                             ' +
                    '   <span class="text-overflow"></span>                            </div>        ' +
                    '                </td>                        <td></td>                    </tr> ' +
                    '               </tbody>            </table>        </div>        </div>        <' +
                    'div b-if="!overSize" style="margin: 13px 33px">            <a href="javascript:;' +
                    '" b-on-click="addRule()">                + 新增一行            </a>        </div>   ' +
                    '     <div class=\'tc-15-page tc-15-table-panel-edit\'>            <button class=' +
                    '"tc-15-btn" data-save b-on-click="save()">保存</button>            <button class="' +
                    'tc-15-btn weak" b-on-click="cancel()">取消</button>        </div>    </div></div>',
            protoTd: '    <div class="tc-15-select-wrap m">        <select b-on="{change: preSetRule}"' +
                    ' b-model="item._proto"                class="tc-15-select {{item._remove ? \'dis' +
                    'abled\' : \'\'}}" disabled?="item._remove">            <option b-repeat="p in pr' +
                    'otos" selected?="item._proto === p.name"                    data-proto="{{p.prot' +
                    'o}}" data-port="{{p.port}}" data-port-edit?="p.portEdit"                    valu' +
                    'e="{{p.name}}">{{p.name}}</option>        </select>    </div>',
            portTd: '    <span class="text-overflow tc-15-input-text-wrap">        <input type="text"' +
                    ' disabled?="item._remove || item._disablePortEdit"               data-validate="' +
                    'port"               data-port-input               b-model="item.port" class="tc-' +
                    '15-input-text {{item._remove || item._disablePortEdit ? \'disabled\' : \'\'}}" /' +
                    '>    </span>',
            subnetTh: '    <span class="text-overflow">{{dir ? \'源IP\' : \'目标IP\'}}</span>    <!--<div ' +
                    'class="tc-15-bubble-icon tc-15-triangle-align-center">-->        <!--<i class="t' +
                    'c-icon icon-what"></i>-->        <!--<div class="tc-15-bubble tc-15-bubble-top">' +
                    '-->            <!--<div class="tc-15-bubble-inner">IP 为空表示所有 IP</div>-->        ' +
                    '<!--</div>-->    <!--</div>-->',
            subnetTd: '    <span class="text-overflow tc-15-input-text-wrap">        <input type="text"' +
                    ' disabled?="item._remove" data-validate="subnet"               b-model="item.sub' +
                    'net" class="tc-15-input-text {{item._remove ? \'disabled\' : \'\'}}" />    </spa' +
                    'n>',
            opTd: '    <div>        <select class="tc-15-select m  {{item._remove ? \'disabled\' : ' +
                    '\'\'}}"                style="min-width: 65px;"                b-model="item.op"' +
                    ' disabled?="item._remove">            <option value="0" selected?="item.op == 0"' +
                    '>允许</option>            <option value="1" selected?="item.op == 1">拒绝</option>  ' +
                    '      </select>    </div>',
            nameTd: '    <span class="text-overflow">        <input type="text" disabled?="item._remo' +
                    've"               b-model="item.name" maxlength="100" class="tc-15-input-text l ' +
                    'tc-15-input-text-wrap {{item._remove ? \'disabled\' : \'\'}}" />    </span>'
        },
        p = new o({
            typeAttr: "data-validate",
            rules: {
                port: function (e) {
                    e = $.trim(e);
                    var t,
                        i = e.split("-"),
                        n = "填入端口号或端口范围,例如:10或1-10";
                    switch (i.length) {
                        case 1:
                            (/^\d+$/.test(i[0]) && 1 * i[0] >= 1 && i[0] <= 65535 || "ALL" === i[0]) && (t = !0);
                            break;
                        case 2:
                            /^\d+$/.test(i[0]) && /^\d+$/.test(i[1]) && 1 * i[0] >= 1 && i[0] <= 65535 && 1 * i[1] >= 1 && i[1] <= 65535 && 1 * i[1] > 1 * i[0] && (t = !0)
                    }
                    return t
                        ? !1
                        : n
                },
                subnet: function (e) {
                    var t = d.isValidRouteDest(e) || d.isValidIp(e);
                    return t
                        ? !1
                        : "请输入合法 IP 或 IP 网段"
                }
            },
            showErr: function (e, t, i) {
                i = i || {},
                i.silence || (i.noTips || this.showTips(e, t), $(e).parent().addClass(this.errClass))
            },
            hideErr: function (e) {
                this.hideTips(e),
                $(e)
                    .parent()
                    .removeClass(this.errClass)
            },
            showTips: function (e, t) {
                var i = new(a.getComponent("qc-popover"))({
                    $data: {
                        trigger: "",
                        hideDestroy: !0,
                        content: t
                    },
                    target: e,
                    handler: this.btn
                });
                i.show()
            },
            hideTips: function (e) {
                var t = $(e).data("qc-popover");
                t && t.hide()
            }
        }),
        u = [
            {
                name: "TCP",
                proto: "tcp",
                portEdit: !0
            }, {
                name: "UDP",
                proto: "udp",
                portEdit: !0
            }, {
                name: "All traffic",
                proto: "all",
                port: "ALL"
            }, {
                name: "All TCP",
                proto: "tcp",
                port: "ALL"
            }, {
                name: "All UDP",
                proto: "udp",
                port: "ALL"
            }, {
                name: "ICMP",
                proto: "icmp",
                port: "-"
            }, {
                name: "TCP SSH(22)",
                proto: "tcp",
                port: 22
            }, {
                name: "TCP SMTP(25)",
                proto: "tcp",
                port: 25
            }, {
                name: "DNS",
                proto: "udp",
                port: 53
            }, {
                name: "HTTP",
                proto: "tcp",
                port: 80
            }, {
                name: "POP3",
                proto: "tcp",
                port: 110
            }, {
                name: "IMAP",
                proto: "tcp",
                port: 143
            }, {
                name: "LDAP",
                proto: "tcp",
                port: 389
            }, {
                name: "HTTPS",
                proto: "tcp",
                port: 443
            }, {
                name: "SMTPS",
                proto: "tcp",
                port: 465
            }, {
                name: "IMAPS",
                proto: "tcp",
                port: 993
            }, {
                name: "POP3S",
                proto: "tcp",
                port: 995
            }, {
                name: "MS SQL",
                proto: "tcp",
                port: 1433
            }, {
                name: "MYSQL",
                proto: "tcp",
                port: 3306
            }
        ],
        v = a.extend({
            $tpl: l.main,
            $afterInit: function () {
                this
                    .$refs
                    .editor
                    .$watch("maxSize", function () {
                        this.checkSize()
                    }),
                d.beeBind(this.$refs.editor, this, "overMaxSize", "overSize"),
                this.$watch("[aclId, vpcId]", function () {
                    this.refresh()
                })
            },
            edit: function () {
                this.$set("isEdit", !0),
                p.btn = $(this.$el).find("[data-save]")[0]
            },
            save: function () {
                var e = this,
                    t = this.$refs.editor;
                p.validateAll(this.$el) && (t.removePreRemove(), s.request({
                    name: "addAclAndItem",
                    data: {
                        vpcId: this.vpcId || this.acl.vpcId,
                        aclId: this.aclId || this.acl.aclId,
                        dir: this.dir,
                        acl: this
                            .$refs
                            .editor
                            .list
                            .concat(e.list.slice(-1))
                    },
                    success: function () {
                        e.$set("isEdit", !1),
                        e.refresh()
                    }
                }))
            },
            cancel: function () {
                this.$set("isEdit", !1);
                var e = this
                    .list
                    .slice();
                e.pop(),
                e = e.map(function (e) {
                    return $.extend({
                        _disablePortEdit: "icmp" === e.proto || "ALL" === e.port,
                        _proto: n(e.proto)
                    }, e)
                }),
                this
                    .$refs
                    .editor
                    .setData({list: e, totalNum: e.length})
            },
            addRule: function () {
                this
                    .$refs
                    .editor
                    .insert(this.$refs.editor.list.length)
            },
            refresh: function () {
                this
                    .$refs
                    .grid
                    .refresh(),
                this
                    .$refs
                    .editor
                    .refresh()
            },
            getList: function () {
                var e = this;
                return this._promise = this._promise || new Promise(function (t, i) {
                    s.request({
                        name: "getAclAndItem",
                        data: {
                            vpcId: e.vpcId || e.acl.vpcId,
                            aclId: e.aclId || e.acl.aclId,
                            dir: e.dir
                        },
                        success: function (i) {
                            delete e._promise,
                            e.list = d.clone(i.acl),
                            t(i)
                        },
                        fail: i
                    })
                }),
                this._promise
            }
        });
    v.tag("acl-rules-grid", a.getComponent("grid-view").extend({
        getData: function (e, t) {
            this
                .getList()
                .then(function (e) {
                    t(null, {
                        list: e.acl,
                        totalNum: e.acl.length
                    })
                }, t)
        },
        getCellContent: function (e, t, i) {
            var n,
                a = '<span class="text-overflow">';
            switch (i.key) {
                case "proto":
                    for (var s = 0, d = u.length; d > s; s++) 
                        if (u[s].proto === e) {
                            n = u[s].name;
                            break
                        }
                    a += n;
                    break;
                case "op":
                    a += 0 == e
                        ? "允许"
                        : "拒绝";
                    break;
                default:
                    a = ""
            }
            return a && (a += "</span>"),
            a
        }
    }, {
        defaults: {
            hasFirst: !1,
            showPagination: !1,
            autoMaxHeight: !1,
            showState: !1,
            colums: [
                {
                    name: "协议类型",
                    key: "proto"
                }, {
                    name: "端口",
                    key: "port",
                    width: 120
                }, {
                    name: "IP",
                    key: "subnet",
                    thTpl: "    <span class=\"text-overflow\">        {{dir ? '源IP' : '目标IP'}}    </span>"
                }, {
                    name: "策略",
                    key: "op",
                    width: 120
                }, {
                    name: "备注",
                    key: "name",
                    tdTpl: '<span class="text-overflow" title="{{item.name}}">{{item.name ? item.name : "-"}' +
                            '}</span>'
                }
            ]
        }
    }));
    var f = a.getComponent("grid-editor");
    v.tag("acl-rules-editor", f.extend({
        $mixins: [r({maxInSize: c.VPC_LIMIT.ACL_IN_ITEM_PER_ACL_LIMIT_TYPE, maxOutSize: c.VPC_LIMIT.ACL_OUT_ITEM_PER_ACL_LIMIT_TYPE})].concat(f.prototype.$mixins || []),
        $afterInit: function () {
            d.beeBind(this, this, "dir ? maxInSize : maxOutSize", "maxSize"),
            this.$watch("[maxSize, _length]", function () {
                this.overMaxSize
                    ? (this.overSizeTips || this.showTips("每个 ACL 最多只能创建 " + this.maxSize + " 条" + (this.dir
                        ? "入站"
                        : "出站") + "规则"), this.overSizeTips = !0)
                    : (this.overSizeTips && this.hideTips(), this.overSizeTips = !1)
            }),
            f
                .prototype
                .$afterInit
                .call(this)
        },
        actionTpl: '<span class="text-overflow"><a href="javascript:;" class="links" b-on-click="(it' +
                'em._remove ? restore : preRemove)($parent.$index)">{{item._remove ? "恢复删除" : "删除' +
                '"}}</a></span>',
        preSetRule: function (e) {
            var t = this.$root,
                i = $(e.target),
                n = i.children("option:selected");
            i
                .find("option")
                .removeProp("selected"),
            n.prop("selected", "selected");
            var a = n.data(),
                s = i
                    .closest("tr")
                    .attr("data-index"),
                d = t.list[s],
                o = {
                    _disablePortEdit: !a.portEdit,
                    proto: a.proto
                };
            (a.port || "-" === d.port) && (o.port = a.port),
            t.updateItem(s, o)
        },
        getData: function (e, t) {
            var i = this;
            this
                .getList()
                .then(function (e) {
                    var a = e
                        .acl
                        .map(function (e) {
                            return $.extend({
                                _disablePortEdit: "icmp" === e.proto || "ALL" === e.port,
                                _proto: n(e.proto)
                            }, e)
                        });
                    a.pop(),
                    t(null, {
                        list: a,
                        totalNum: a.length
                    }),
                    i.checkSize()
                }, t)
        },
        events: {
            "keydown [data-validate]": function (e) {
                setTimeout(function () {
                    $(e.target)
                        .parent()
                        .hasClass(p.errClass) && p.validate(e.target, {
                        noTips: !0
                    })
                }, 0)
            }
        }
    }, {
        defaults: {
            protos: u,
            canResizeColum: !1,
            maxSize: c.MAX_ACLRULE_SIZE,
            maxInSize: c.MAX_ACLRULE_SIZE,
            maxOutSize: c.MAX_ACLRULE_SIZE,
            emptyTips: '没有自定义规则 <a b-on-click="insert(0)" href="javascript:;">+新增</a>',
            newData: {
                proto: "tcp",
                _proto: "TCP",
                port: "",
                subnet: "",
                op: 1,
                name: ""
            },
            colums: [
                {
                    name: "协议类型",
                    key: "proto",
                    tdTpl: l.protoTd
                }, {
                    name: "端口",
                    key: "port",
                    tdTpl: l.portTd
                }, {
                    name: "IP",
                    key: "subnet",
                    thTpl: l.subnetTh,
                    tdTpl: l.subnetTd
                }, {
                    name: "策略",
                    key: "op",
                    tdTpl: l.opTd,
                    width: 120
                }, {
                    name: "备注",
                    key: "name",
                    tdTpl: l.nameTd
                }, {
                    name: "操作",
                    key: "_action",
                    width: 120
                }
            ]
        }
    })),
    i.exports = v
}),
define("modules/vpc/acl/detail/baseInfo", function (e, t, i) {
    var n = e("appUtil"),
        a = e("dialog"),
        s = e("qccomponent"),
        d = e("es6-promise"),
        o = (e("manager"), e("widget/pubsubhub/pubsubhub")),
        c = e("models/acl"),
        r = e("../bindSubnet"),
        l = {
            baseInfo: '<div>    <div class="tc-15-list-wrap acl-inf">        <div class="tc-15-list-wra' +
                    'p private-network-inf" style="display: ; ">            <div class="head-top-titl' +
                    'e"><h3>基本信息</h3>                <a href="javascript:;" b-on-click="edit(true)" b' +
                    '-if="!isEdit"><i class="pencil-icon "></i>编辑</a>            </div>            <f' +
                    'orm action="javascript:;" b-on-submit="save()">                <div class="tc-15' +
                    '-list-content">                    <ul>                        <li>             ' +
                    '               <em class="tc-15-list-tit">名称</em>                            <sp' +
                    'an b-if="!isEdit" class="tc-15-list-det">{{acl.name}}</span>                    ' +
                    '        <div class="tc-15-input-text-wrap" b-if="isEdit" data-name-field>       ' +
                    '                         <input type="text" b-ref="input" class="tc-15-input-tex' +
                    't" b-model="name" maxlength="25">                                <div class="tc-' +
                    '15-input-tips">还能输入{{25 - name.length}}个字符，英文、汉字、数字、连接线"-"或下划线"_"</div>         ' +
                    '                   </div>                        </li>                        <l' +
                    'i><em class="tc-15-list-tit">ID</em><span class="tc-15-list-det">{{acl.aclId}}</' +
                    'span>                        </li>                        <li><em class="tc-15-l' +
                    'ist-tit">所属网络</em><span class="tc-15-list-det" ><a style="margin:0" data-event="' +
                    'nav" href="/vpc/vpc?rid={{regionId}}&unVpcId={{acl.uniqVpcId}}">{{acl.uniqVpcId}' +
                    '}</a>({{acl.vpcName}}|{{acl.vpcCIDR}})</span>                        </li>      ' +
                    '                  <li><em class="tc-15-list-tit">创建时间</em><span class="tc-15-lis' +
                    't-det">{{acl.createTime}}</span>                        </li>                   ' +
                    ' </ul>                </div>                <div class="tc-15-page tc-15-table-p' +
                    'anel-edit" style="margin-left: 95px;" b-if="isEdit">                    <button ' +
                    'class="tc-15-btn" b-ref="saveBtn" type="submit">保存</button>                    <' +
                    'button class="tc-15-btn weak" b-on-click="cancel()">取消</button>                <' +
                    '/div>            </form>        </div>    </div>    <div style="width: 90%">    ' +
                    '    <div class="manage-area-title"><h3>关联子网</h3></div>        <div class="tc-15-' +
                    'action-panel">            <button class="tc-15-btn m" b-on-click="bindSubnet(acl' +
                    '.aclId, acl.vpcId)">+新增关联</button>            <button class="tc-15-btn weak m {{' +
                    'disabledUnBindBtn ? \'disabled\' : \'\'}}" b-on-click="unBindSubnet()">批量解绑</but' +
                    'ton>        </div>        <acl-subnet-grid b-ref="grid" vpc-id="{{vpcId}}" acl-i' +
                    'd="{{aclId}}" count="100" get-base-info="{{getBaseInfo}}"></acl-subnet-grid>    ' +
                    '</div></div>'
        },
        p = s.extend({
            $tpl: l.baseInfo,
            $afterInit: function () {
                p
                    .__super__
                    .$afterInit
                    .call(this),
                this.$set("regionId", n.getRegionId()),
                this.refresh(),
                this.bindEvent()
            },
            edit: function (e) {
                this.$set("isEdit", e),
                this.$nameField = $(this.$el).find("[data-name-field]"),
                e && this
                    .$refs
                    .input
                    .select()
            },
            save: function () {
                var e = this,
                    t = $(this.$refs.saveBtn);
                return e.name === e.acl.name
                    ? void e.edit(!1)
                    : t.hasClass("disabled")
                        ? void 0
                        : n.isValidName(e.name)
                            ? (t.addClass("disabled"), new d(function (i, n) {
                                c.request({
                                    name: "updateAclBaseInfo",
                                    data: {
                                        vpcId: e.acl.vpcId,
                                        aclId: e.acl.aclId,
                                        name: e.name
                                    },
                                    success: function (n) {
                                        o.trigger("baseInfoUpdated", [e.name]),
                                        e.edit(!1),
                                        t.removeClass("disabled"),
                                        i(n)
                                    },
                                    fail: function (e) {
                                        t.removeClass("disabled"),
                                        n(e)
                                    }
                                })
                            }))
                            : (this.$nameField.addClass("error"), !1)
            },
            cancel: function () {
                this.edit(!1),
                this.$set("name", this.acl.name)
            },
            refresh: function () {
                var e = this;
                this
                    .getBaseInfo()
                    .then(function (t) {
                        e.$set({name: t.name, acl: t})
                    })
            },
            bindEvent: function () {
                var e = this;
                o
                    .off(".baseInfo")
                    .on("baseInfoUpdated.baseInfo", function (t, i) {
                        e.$set("acl.name", i)
                    })
                    .on("aclSubnetUpdated.baseInfo", function () {
                        e
                            .$refs
                            .grid
                            .refresh(),
                        e.refresh()
                    }),
                this
                    .$refs
                    .grid
                    .$watch("list", function () {
                        var t = this.getSelected();
                        e.$set("disabledUnBindBtn", !t.length)
                    }, !0),
                this.$watch("name", function (e) {
                    this.$nameField && this
                        .$nameField
                        .hasClass("error") && n.isValidName(e) && this
                        .$nameField
                        .removeClass("error")
                })
            },
            $afterDestroy: function () {
                o.off(".baseInfo")
            },
            bindSubnet: r,
            unBindSubnet: function () {
                if (!this.disabledUnBindBtn) {
                    var e = this.$refs.grid;
                    a.create("确认要解绑所选子网?", 480, "", {
                        title: "解绑子网",
                        preventResubmit: !0,
                        button: {
                            "确定": function () {
                                e.unBind(e.getSelected().map(function (e) {
                                    return e.subnetId
                                }))
                            }
                        }
                    })
                }
            }
        }),
        u = s
            .getComponent("grid-view")
            .extend({
                actionTpl: '<a b-on="{click: confirmUnBind(item)}" href="javascript:;">解绑</a>',
                confirmUnBind: function (e) {
                    return function (t) {
                        var i = this.$root;
                        if (!e.$disable) {
                            var n = new(s.getComponent("popup-confirm"))({
                                $data: {
                                    trigger: "",
                                    title: "确认要解绑该子网? ",
                                    style: "width: 310px;",
                                    hideDestroy: !0
                                },
                                attachEl: i.$el,
                                handler: t.target,
                                onConfirm: function () {
                                    i.unBind([e.subnetId])
                                }
                            });
                            n.show()
                        }
                    }
                },
                unBind: function (e) {
                    var t = this;
                    t
                        .list
                        .forEach(function (i, n) {
                            e.indexOf(i.subnetId) > -1 && t.updateItem(n, {
                                $loading: !0,
                                $disable: !0
                            })
                        }),
                    c.request({
                        name: "delSubnetAclRule",
                        data: {
                            vpcId: this.vpcId,
                            aclId: this.aclId,
                            subnetIds: e
                        },
                        success: function () {
                            a.hide(),
                            o.trigger("aclSubnetUpdated")
                        }
                    })
                },
                getData: function (e, t) {
                    this
                        .getBaseInfo()
                        .then(function (e) {
                            t(null, {
                                list: e.subnets,
                                totalNum: e.subnets.length
                            })
                        }, t)
                }
            }, {
                defaults: {
                    showPagination: !1,
                    autoMaxHeight: !1,
                    minHeight: "auto",
                    colums: [
                        {
                            name: "子网名称",
                            key: "name"
                        }, {
                            name: "子网 ID",
                            key: "subnetId"
                        }, {
                            name: "CIDR",
                            key: "CIDR"
                        }, {
                            name: "操作",
                            key: "_action"
                        }
                    ]
                }
            });
    p.tag("acl-subnet-grid", u),
    i.exports = p
}),
define("modules/vpc/acl/detail/detail", function (e, t, i) {
    var n = e("widget/detail/detail"),
        a = e("models/acl"),
        s = e("es6-promise"),
        d = e("appUtil"),
        o = e("widget/pubsubhub/pubsubhub"),
        c = e("./baseInfo"),
        r = e("./aclRules"),
        l = e("tips"),
        p = n.extend({
            $beforeInit: function () {
                p
                    .__super__
                    .$beforeInit
                    .call(this),
                this._tabs = []
            },
            $afterInit: function () {
                var e = this;
                p
                    .__super__
                    .$afterInit
                    .call(this),
                this
                    .getBaseInfo({vpcId: this.vpcId, aclId: this.aclId})
                    .then(function (t) {
                        e.$set({title: t.name, acl: t})
                    }),
                this.bindEvent()
            },
            $beforeDestroy: function () {
                this
                    ._tabs
                    .forEach(function (e) {
                        e.$destroy(!0)
                    }),
                o.off(".detail")
            },
            headerRightTpl: '<a href="https://www.qcloud.com/doc/product/215/5132" target="_blank">        <b' +
                    ' class="links-icon"></b> 网络ACL帮助文档        </a>',
            loadTabs: function (e) {
                var t = this.$refs.tabs,
                    i = this._tabs[e];
                if (!i) {
                    switch (e) {
                        case 0:
                            i = new c({
                                $data: {
                                    vpcId: this.vpcId,
                                    aclId: this.aclId,
                                    acl: $.extend(!0, {}, this.acl)
                                },
                                getBaseInfo: this.getBaseInfo
                            });
                            break;
                        case 1:
                            i = new r({
                                $data: {
                                    regionId: this.regionId,
                                    dir: 1,
                                    acl: $.extend(!0, {}, this.acl)
                                }
                            });
                            break;
                        case 2:
                            i = new r({
                                $data: {
                                    regionId: this.regionId,
                                    dir: 0,
                                    acl: $.extend(!0, {}, this.acl)
                                }
                            })
                    }
                    this._tabs[e] || (this._tabs[e] = i),
                    t
                        .list
                        .$set(e, {content: i.$el})
                }
            },
            back: function () {
                history.back()
            },
            getBaseInfo: function () {
                var e;
                return function (t) {
                    return e || (t = t || {
                        vpcId: this.vpcId,
                        aclId: this.aclId
                    }, e = new s(function (e, i) {
                        a.request({
                            name: "getAclBaseInfo",
                            data: {
                                vpcId: t.vpcId,
                                aclId: t.aclId
                            },
                            success: e,
                            fail: i
                        })
                    }), e = e.then(function (t) {
                        return e = null,
                        t._vpcId = d.convertIdToWithPrefix("vpc", t.vpcId),
                        t
                    }, function (t) {
                        e = null,
                        l.hideFlashNow(),
                        d.renderDetailNoData("ACL", "/vpc/acl")
                    })),
                    e
                }
            }(),
            bindEvent: function () {
                var e = this;
                o
                    .off(".detail")
                    .on("baseInfoUpdated.detail", function () {
                        e
                            .getBaseInfo({vpcId: e.vpcId, aclId: e.aclId})
                            .then(function (t) {
                                e.$set({title: t.name, acl: t})
                            })
                    })
            }
        }, {
            defaults: {
                tabData: {
                    list: [
                        {
                            label: "基本信息"
                        }, {
                            label: "入站规则"
                        }, {
                            label: "出站规则"
                        }
                    ]
                }
            }
        });
    i.exports = p
}),
define("modules/vpc/adTip", function (e, t, i) {
    var n = (e("manager"), e("./conn/cacher"));
    i.exports = {
        init: function (e, t, i) {
            e = $(e),
            t = t || ".manage-area-title:first",
            i = i || "insertAfter",
            n
                .getWhiteListMap()
                .done(function (n) {
                    if (!n.VPC_CONN_XR) {
                        e.find(t);
                        $('    <p class="tc-15-msg" style="margin:0px 20px 20px 20px;font-size:14px;">私有网络跨' +
                                '地域对等连接火爆内测中，提供专线级服务品质的国内、外高可用连接，快来申请吧~ <a target="_blank" href="//www.qcloud.com' +
                                '/event/vpc_cross_region.html">立即查看</a></p>')[i](e.find(t))
                    }
                })
        }
    }
}),
define("modules/vpc/BeeBase", function (e, t, i) {
    var n = e("qccomponent"),
        a = e("widget/popover/HelpTips"),
        s = n.extend({
            destroy: function () {
                this.$destroy(!0)
            }
        });
    s.tag("help-tips", a),
    i.exports = s
}),
define("modules/vpc/Bubble", function (e, t, i) {
    i.exports = e("widget/popover/Editor")
}),
define("modules/vpc/cacher", function (e, t, i) {
    var n = e("widget/cacher/Cacher"),
        a = e("manager"),
        s = e("./cApi"),
        d = e("./util");
    i.exports = new n([
        [
            a, "getVpcRegionWhiteList", "getRegionIdList"
        ],
        [
            a, "queryRegion", "getRegionMap"
        ], {
            host: a,
            fnName: "getVpcView",
            ns: [
                "vpc",
                "subnet.$0.$1.ALL",
                "route.$0.$1.ALL",
                "vpnGw.$0.$1.ALL",
                "userGw.$0.ALL",
                "dcGw.$0.$1.ALL"
            ]
        }, {
            host: a,
            fnName: "getVpcList",
            ns: "vpc.$0.ALL"
        }, {
            host: s,
            fnName: "getVpnGwList",
            lifetime: 3e5,
            ns: "vpnGw.$0.$1.ALL",
            promise: !0
        }, {
            host: s,
            fnName: "getDcGwList",
            lifetime: 3e5,
            ns: "dcGw.$0.ALL",
            promise: !0
        }, {
            host: s,
            fnName: "getConnList",
            lifetime: 3e5,
            ns: "conn.$0.ALL",
            promise: !0
        }, {
            host: s,
            fnName: "getSslVpnList",
            lifetime: 3e5,
            ns: "sslvpn.$0.ALL",
            promise: !0
        },
        [
            a, "getVpnStuff"
        ],
        [
            a, "getVpnGwQuota"
        ],
        [
            a, "getVPCGatewayList", null, 3e5
        ],
        [
            a, "getVPCRouteTable"
        ], {
            host: a,
            fnName: "getVPCRouteTable",
            ns: "route"
        },
        [
            a, "getUserGwVendor"
        ],
        [
            a, "getGwStatusList", null, 3e5
        ], {
            host: a,
            fnName: "getVpnGwStatusList",
            lifetime: 3e5,
            ns: "vpnGw.$0.$1.ALL"
        }, {
            host: a,
            fnName: "getVpnConnStatusList",
            lifetime: 3e5,
            ns: "vpnConn.$0.$1.ALL"
        },
        [
            d, "getZoneList"
        ], {
            host: s,
            fnName: "getVpc",
            ns: "vpc",
            promise: !0
        }, {
            host: s,
            fnName: "getVpcList",
            alias: "getVpcListEx",
            ns: "vpc.$0.ALL",
            promise: !0
        }, {
            host: s,
            fnName: "getVpcSubnetList",
            ns: "subnet.$0.$1.ALL",
            promise: !0
        }, {
            host: s,
            fnName: "getVpnConnList",
            ns: "vpnConn.$0.$1.ALL",
            lifetime: 3e5,
            promise: !0
        }, {
            host: s,
            fnName: "getVpcRouteTable",
            alias: "getVpcRouteTableEx",
            ns: "route",
            promise: !0
        }, {
            host: s,
            fnName: "getVpcRouteTableList",
            alias: "getVpcRouteTableListEx",
            ns: "route.$0.$1.ALL",
            promise: !0
        }, {
            host: s,
            fnName: "getUserGwList",
            promise: !0,
            lifetime: 5e3
        }, {
            host: s,
            fnName: "getUserGwVendor",
            promise: !0
        }, {
            host: s,
            fnName: "getLimits",
            promise: !0
        }, {
            host: s,
            fnName: "getRawLimits",
            promise: !0
        }, {
            host: s,
            fnName: "getDcGwNatData",
            ns: "dcgw-nat",
            promise: !0
        }, {
            host: s,
            fnName: "getIGWList",
            ns: "igw.$0.$1.ALL",
            lifetime: 3e5,
            promise: !0
        }
    ])
}),
define("modules/vpc/cApi", function (e, t, i) {
    function n(e) {
        return parseInt(("" + e).replace(/^\w+_\w+_/, ""), 10)
    }
    function a(e, t) {
        "string" == typeof t && (t = l[t]);
        var i;
        return e
            ? $.isArray(e)
                ? e.map(function (e) {
                    return a(e, t)
                })
                : "object" == typeof e
                    ? (i = {}, Object.keys(e).forEach(function (n) {
                        var s,
                            d,
                            c = e[n],
                            r = t[n],
                            l = n,
                            p = typeof r;
                        "string" === p
                            ? l = r
                            : "object" === p
                                ? (l = r.key || n, "function" == typeof r.translate
                                    ? s = r.translate
                                    : d = r.map)
                                : "function" === p && (s = r),
                        s
                            ? (i["_origin_" + n] = o.clone(c), c = s(c, e))
                            : d && (i["_origin_" + n] = o.clone(c), c = a(c, d)),
                        i[l] = c
                    }), i)
                    : e
            : e
    }
    var s = e("manager"),
        d = e("constants"),
        o = e("appUtil"),
        c = {
            28006: "私有网络CIDR格式错误",
            28009: "创建vpc网络超出数量限制!",
            28010: "子网CIDR不是合法的VPC网段",
            28011: "子网CIDR网段范围不在它所属的VPC网段范围内",
            28012: "子网CIDR网段有重叠",
            28013: "子网数量超出限制",
            28014: '子网名称仅支持中文、英文、数字、下划线、分隔符"-"、小数点',
            28015: "子网CIDR跟已有的子网CIDR有重叠",
            28016: "子网内还有设备，无法删除",
            28018: "路由表目的端必须为网段，如10.10.10.0/24",
            28019: "下一跳类型跟下一跳格式不匹配",
            28020: '路由表名称仅支持中文、英文、数字、下划线、分隔符"-"、小数点',
            28022: "下一跳不存在",
            28023: "有子网关联到该路由表，无法删除",
            28024: "默认路由表不能删除",
            28028: "无法删除具有子网的vpc网络!",
            28029: "vpc名字冲突!",
            28030: "子网名称已存在",
            28031: "路由表名称已存在",
            28032: "私有网络子网名称重复",
            28034: "路由表规则数量超出限制",
            28101: "VPN网关与对端网关的通道已存在",
            28107: "该对端网关已有通道连接!",
            28108: "创建 VPN 通道失败!",
            28109: "对端网关名称已存在!",
            28111: "SLA IP已被使用",
            28112: "SLA IP格式有误",
            28113: "对端SLA IP已被使用",
            28114: "对端SLA IP与本端冲突",
            28115: "VPN 通道名称已存在!",
            29003: "路由表关联失败，子网不可关联含有本子网公网网关路由的路由表"
        },
        r = {
            AddUserGw: {
                28015: "对端网关已存在"
            },
            CreateVpcPeeringConnection: {},
            CreateRouteTable: {
                28017: "所选私有网络的路由表创建数已达上限"
            }
        },
        l = {
            vpc: {
                vpcId: n,
                cidrBlock: "vpcCIDR",
                routeTableNum: "rtbNum",
                vpcDeviceNum: "vpcDevices"
            },
            subnet: {
                vpcId: n,
                vpcCidrBlock: "vpcCIDR",
                subnetId: n,
                routeTableId: {
                    key: "rtbId",
                    translate: n
                },
                routeTableName: "rtbName",
                cidrBlock: "subnetCIDR",
                availableIPNum: "availableIP",
                subnetCreateTime: "createTime"
            },
            routeTable: {
                vpcId: n,
                vpcCidrBlock: "vpcCIDR",
                routeTableName: "rtbName",
                routeTableId: {
                    key: "rtbId",
                    translate: n
                },
                routeTableType: "type",
                routeTableCreateTime: "createTime",
                routeSet: {
                    map: {
                        nextHub: {
                            translate: function (e, t) {
                                return t.unNextHub || e
                            }
                        }
                    }
                }
            },
            routeTableFull: {
                vpcId: n,
                vpcCidrBlock: "vpcCIDR",
                routeTableName: "rtbName",
                routeTableId: {
                    key: "rtbId",
                    translate: n
                },
                routeTableType: "type",
                routeTableCreateTime: "createTime",
                routeSet: {
                    key: "routeList",
                    map: {
                        destinationCidrBlock: "dest",
                        nextHub: {
                            key: "nextHop",
                            translate: function (e, t) {
                                return t.unNextHub || e
                            }
                        }
                    }
                }
            },
            vpnGw: {
                vpcId: n,
                vpcCidrBlock: "vpcCIDR",
                vpnGwId: n,
                vpnGwAddress: "vpnGwWanIp",
                dealId: "orderId"
            },
            vpnConn: {
                vpcId: n,
                vpcCidrBlock: "vpcCIDR",
                vpnGwId: n,
                vpnConnId: n,
                userGwId: "usrGwId",
                userGwName: "usrGwName",
                vpcConnStatus: "state",
                sourceCidr: {
                    key: "snet",
                    translate: function (e) {
                        return e && e.join("\n")
                    }
                },
                destinationCidr: {
                    key: "dnet",
                    translate: function (e) {
                        return e && e.join("\n")
                    }
                },
                IKESet: "IKEArr",
                IPSECSet: "IPSECArr",
                spdAcl: function (e) {
                    return e
                        ? Object
                            .keys(e)
                            .map(function (t, i) {
                                return {
                                    _rule: "规则 " + (i + 1),
                                    vpnGwCidr: t,
                                    userGwCidr: e[t].join("\n"),
                                    _userGwCidr: e[t].join(", ")
                                }
                            })
                        : []
                }
            },
            userGw: {
                userGwId: n,
                userGwAddr: "userGwAddress"
            },
            dcGw: {
                vpcId: n
            },
            conn: {
                vpcId: n,
                peerVpcId: n,
                uniqVpcId: "unVpcId",
                uniqPeerVpcId: "peerUnVpcId"
            }
        },
        p = i.exports = {
            translateMap: l,
            idParser: n,
            translate: a,
            request: function (e, t, i, n) {
                var d = $.Deferred();
                return s.delegateCApi(e, "vpc", t, i, d.resolve, d.reject, function (e, i) {
                    if (n !== !1) {
                        var a,
                            s = r[t];
                        return s && (a = s[e])
                            ? a
                            : (i = i || {}, c[e] || n || i.message || i.msg)
                    }
                }),
                d.translate = function (e) {
                    var t = this,
                        i = $.Deferred();
                    return t.done(function (t) {
                        i.resolve(a(t, e))
                    }).fail(i.reject),
                    i
                },
                d
            },
            getLimits: function (e) {
                var t = d.VPC_LIMIT,
                    i = Object.keys(t);
                return this
                    .getRawLimits(e)
                    .then(function (e) {
                        var n = {};
                        return i.forEach(function (i) {
                            n[i] = e[t[i]]
                        }),
                        n
                    })
            },
            getRawLimits: function (e) {
                var t = d.VPC_LIMIT,
                    i = Object.keys(t),
                    n = $.Deferred();
                return p.request(e, "DescribeVpcLimit", {
                    type: i.map(function (e) {
                        return t[e]
                    })
                    })
                    .then(function (e) {
                        return e.data.limit
                    }, n.reject)
            },
            requestTask: function (e, t, i, n) {
                var a = this,
                    d = a
                        .request
                        .apply(a, arguments),
                    o = $.Deferred();
                return d.then(function (t) {
                    s
                        .checkTask({
                        taskId: t.taskId || t.data && t.data.taskId,
                        regionId: e
                    })
                        .then(o.resolve, o.reject)
                }, o.reject),
                o
            },
            getVpc: function (e, t) {
                var i = $.Deferred();
                return p.request(e, "DescribeVpcEx", {
                    offset: 0,
                    limit: 1,
                    vpcId: t
                }, "查询VPC信息失败！")
                    .done(function (e) {
                        e.data.length
                            ? i.resolve(a(e.data[0], l.vpc))
                            : i.reject(e)
                    })
                    .fail(i.reject),
                i
            },
            getVpcList: function (e, t) {
                var i = $.Deferred();
                return p.request(e, "DescribeVpcEx", {
                    offset: 0,
                    limit: 999,
                    vpcId: t
                }, "加载VPC列表失败！")
                    .done(function (e) {
                        i.resolve({
                            vpcList: a(e.data, l.vpc)
                        })
                    })
                    .fail(i.reject),
                i
            },
            getVpcSubnetList: function (e, t) {
                var i = $.Deferred();
                return p.request(e, "DescribeSubnetEx", {
                    offset: 0,
                    limit: 999,
                    vpcId: t
                }, "加载子网列表失败！")
                    .done(function (e) {
                        i.resolve(a(e.data, l.subnet))
                    })
                    .fail(i.reject),
                i
            },
            getVpcRouteTableList: function (e, t) {
                var i = $.Deferred();
                return p.request(e, "DescribeRouteTable", {
                    offset: 0,
                    limit: 999,
                    vpcId: t
                }, "加载路由表列表失败！")
                    .done(function (e) {
                        i.resolve(a(e.data, "routeTable"))
                    })
                    .fail(i.reject),
                i
            },
            getVpcRouteTable: function (e, t, i) {
                var n = $.Deferred();
                return p.request(e, "DescribeRouteTable", {
                    offset: 0,
                    limit: 1,
                    vpcId: t,
                    routeTableId: i
                }, "读取路由信息失败！")
                    .done(function (e) {
                        e.data.length
                            ? n.resolve(a(e.data[0], "routeTableFull"))
                            : n.reject(e)
                    })
                    .fail(n.reject),
                n
            },
            getVpnGwList: function (e, t) {
                var i = $.Deferred();
                return p.request(e, "DescribeVpnGw", {
                    vpcId: t,
                    offset: 0,
                    limit: 999
                }, "查询VPN网关失败！")
                    .then(function (e) {
                        i.resolve(p.translate(e.data, "vpnGw"))
                    }, i.reject),
                i
            },
            getVpnConnList: function (e, t) {
                var i = $.Deferred();
                return p
                    .request(e, "DescribeVpnConn", {
                        vpcId: t,
                        offset: 0,
                        limit: 999
                    })
                    .then(function (e) {
                        i.resolve(p.translate(e.data, "vpnConn"))
                    }, i.reject),
                i
            },
            getDcGwList: function (e, t) {
                var i = $.Deferred();
                return p.request(e, "DescribeDirectConnectGateway", {
                    vpcId: t,
                    offset: 0,
                    limit: 999
                }, "查询专线网关失败！")
                    .then(function (e) {
                        i.resolve(p.translate(e.data, "dcGw"))
                    }, i.reject),
                i
            },
            getConnList: function (e, t) {
                var i = $.Deferred();
                return p.request(e, "DescribeVpcPeeringConnections", {
                    vpcId: t,
                    offset: 0,
                    limit: 999
                }, "查询对等连接失败！")
                    .then(function (e) {
                        i.resolve(p.translate(e.data, "conn"))
                    }, i.reject),
                i
            },
            getSslVpnList: function (e, t) {
                var i = $.Deferred();
                return p.request(e, "DescribeSSLVpn", {
                    vpcId: t,
                    offset: 0,
                    limit: 999
                }, "查询SSL VPN列表失败！")
                    .done(function (e) {
                        e.sslVpnSet
                            ? i.resolve(e.sslVpnSet)
                            : i.reject(e)
                    })
                    .fail(i.reject),
                i
            },
            getIGWList: function (e, t) {
                var i = $.Deferred();
                return p.request(e, "DescribeNatGateway", {
                    vpcId: t,
                    offset: 0,
                    limit: 999
                }, "查询NAT网关列表失败！")
                    .done(function (e) {
                        e.data
                            ? i.resolve(e.data)
                            : i.reject(e)
                    })
                    .fail(i.reject),
                i
            },
            getUserGwList: function (e) {
                var t = $.Deferred();
                return p
                    .request(e, "DescribeUserGw", {
                        offset: 0,
                        limit: 999
                    })
                    .then(function (e) {
                        t.resolve(p.translate(e.data, "userGw"))
                    }, t.reject),
                t
            },
            addVpnConn: function (e, t) {
                return p.request(e, "AddVpnConn", t, "添加VPN通道失败！")
            },
            modifyVpnConn: function (e, t) {
                return p.request(e, "ModifyVpnConn", t, "修改VPN通道失败！")
            },
            delVpnConn: function (e, t) {
                return p.request(e, "DeleteVpnConn", t, "删除VPN通道失败！")
            },
            getUserGwVendor: function () {
                var e = $.Deferred();
                return p.request(1, "DescribeUserGwVendor", {}, "查询预置对端网关厂商信息失败！")
                    .then(function (t) {
                        e.resolve(t.data)
                    }, e.reject),
                e
            },
            DescribeVpcLimit: function (e, t) {
                return p.request(e, "DescribeVpcLimit", t, "查询 VPC 限制信息失败！")
            },
            getDcGwNatData: function (e, t, i) {
                var n = $.Deferred();
                return p.request(e, "DescribeDirectConnectGatewayNatRule", {
                    vpcId: t,
                    directConnectGatewayId: i
                }, "加载NAT配置失败！")
                    .then(function (e) {
                        return e && e.data
                            ? void n.resolve(e.data)
                            : void n.reject()
                    }, n.reject),
                n
            }
        }
}),
define("modules/vpc/Checkbox", function (e, t, i) {
    var n = e("qccomponent"),
        a = e("widget/form/Checkbox");
    n.tag("cool-checkbox", a),
    i.exports = a
}),
define("modules/vpc/Confirm", function (e, t, i) {
    i.exports = e("widget/popover/BeeConfirm")
}),
define("modules/vpc/conn/Add", function (e, t, i) {
    var n = e("../BeeBase"),
        a = e("appUtil"),
        s = e("manager"),
        d = e("./util"),
        o = e("../cacher"),
        c = e("../cApi"),
        r = e("../DropDown"),
        l = e("./field/Bandwidth"),
        p = e("../vpc/DropDown");
    e("../Checkbox");
    var u = n.extend({
        $tpl: '    <div class="tc-15-rich-dialog">        <div class="tc-15-list-wrap">        ' +
                '    <ul class="form-list">                <li>                    <div class="fo' +
                'rm-label">                        <label for="">名称</label>                    </' +
                'div>                    <div class="form-input">                        <div cla' +
                'ss="form-unit">                            <input type="text" class="tc-15-input' +
                '-text m" data-name="peeringConnectionName" b-model="peeringConnectionName">     ' +
                '                   </div>                    </div>                </li>        ' +
                '        <li>                    <div class="form-label">                        ' +
                '<label for="">本端</label>                    </div>                    <div class' +
                '="form-input">                        <div b-if="VPC_CONN_XR" class="form-unit">' +
                '                            <region-select cls="m" b-model="regionId"></region-s' +
                'elect>                        </div>                        <p class="text" b-if' +
                '="!VPC_CONN_XR">{{regionMap[regionId]}}</p>                    </div>           ' +
                '     </li>                <li>                    <div class="form-label"></div>' +
                '                    <div class="form-input">                        <div class="' +
                'form-unit">                            <vpc-from-select b-ref="fromVpc" cls="m" ' +
                'region-id="{{regionId}}" b-model="fromVpcId" name="fromVpcId"></vpc-from-select>' +
                '                        </div>                    </div>                </li>   ' +
                '             <li>                    <div class="form-label">                   ' +
                '     <label for="">对端</label>                    </div>                    <div ' +
                'class="form-input">                        <div class="form-unit">              ' +
                '              <label class="form-ctrl-label"><input type="radio" name="vpc-conn-' +
                'targetType" value="1" b-model="myAccount" class="tc-15-radio">我的帐户</label>      ' +
                '                      <label class="form-ctrl-label"><input type="radio" name="v' +
                'pc-conn-targetType" value="" b-model="myAccount" class="tc-15-radio">其它帐户</label' +
                '>                        </div>                    </div>                </li>  ' +
                '              <li>                    <div class="form-label"></div>            ' +
                '        <div class="form-input">                        <div b-if="VPC_CONN_XR" ' +
                'class="form-unit">                            <region-select cls="m" b-model="pe' +
                'erRegionId" limit-global="{{!VPC_CONN_XR_GLOBAL}}" from-region-id="{{regionId}}"' +
                '></region-select>                        </div>                        <p class=' +
                '"text" b-if="!VPC_CONN_XR">{{regionMap[regionId]}}</p>                    </div>' +
                '                </li>                <li b-if="myAccount">                    <d' +
                'iv class="form-label"></div>                    <div class="form-input">        ' +
                '                <div class="form-unit">                            <vpc-to-selec' +
                't cls="m" b-ref="toVpc" from-region-id="{{regionId}}" region-id="{{peerRegionId}' +
                '}" b-model="toVpcId" used-conn-map="{{usedConnMap}}"                            ' +
                '               from-vpc-id="{{fromVpcId}}" name="toVpcId"                       ' +
                '                    invalid=""></vpc-to-select>                        </div>   ' +
                '                 </div>                </li>                <li b-if="!myAccount' +
                '">                    <div class="form-label"></div>                    <div cla' +
                'ss="form-input">                        <div class="form-unit">                 ' +
                '           <input type="text" placeholder="对端账号ID" b-model="toCreatorUin" class=' +
                '"tc-15-input-text m" data-name="toCreatorUin">                            <help-' +
                'tips>在用户中心的个人信息中查看</help-tips>                        </div>                    ' +
                '</div>                </li>                <li b-if="!myAccount">               ' +
                '     <div class="form-label"></div>                    <div class="form-input"> ' +
                '                       <div class="form-unit">                            <input' +
                ' type="text" placeholder="对端私有网络ID" b-model="toCreatorVpcId" class="tc-15-input-' +
                'text m" data-name="toCreatorVpcId">                        </div>               ' +
                '     </div>                </li>                <li>                    <div cla' +
                'ss="form-label">                        <label for="">带宽上限</label>              ' +
                '      </div>                    <div class="form-input">                        ' +
                '<div class="form-unit" b-if="!sameRegion">                            <bandwidth' +
                '-field b-model="bandwidth" select-style="width: 280px;" relevance=""></bandwidth' +
                '-field>                        </div>                        <p b-if="sameRegion' +
                '">                            无限制                        </p>                   ' +
                ' </div>                </li>                <li>                    <div class="' +
                'form-label">                        <label for="">计费方式</label>                  ' +
                '  </div>                    <div class="form-input">                        <div' +
                ' b-template b-if="sameRegion">                            免费                    ' +
                '    </div>                        <div b-template b-if="!sameRegion">           ' +
                '                 申请方按日实际使用带宽峰值阶梯计费，按天结算 <a href="//www.qcloud.com/doc/product/21' +
                '5/3079" target="_blank">计费详情</a>                        </div>                  ' +
                '  </div>                </li>            </ul>        </div>        <div style="' +
                'margin-top:20px" b-if="!myAccount">            <i class="plaint-icon"></i> 注：对端帐' +
                '户需要在 {{_formatTime(expireTime, \'yyyy-MM-dd hh:mm\')}} 前接收此请求，对等连接才生效        </d' +
                'iv>    </div>',
        $beforeInit: function () {
            var e = this;
            e
                .__super__
                .$beforeInit
                .apply(e, arguments),
            e._updateExpire(),
            e.$watch("myAccount", e._updateExpire.bind(e)),
            a.beeBind(e, e, "regionIdToStr(regionId)", "region"),
            a.beeBind(e, e, "regionIdToStr(peerRegionId)", "peerRegion"),
            s.queryRegion(function (t) {
                e.$set("regionMap", t)
            }),
            a.beeBind(e, e, "regionId==peerRegionId", "sameRegion"),
            a.beeBind(e, e, "sameRegion ? MAX_BANDWIDTH : 10", "bandwidth"),
            a.beeBind(e, e, "isGlobalLink(regionId, peerRegionId)", "globalLink"),
            a.beeBind(e, e, "count >= LIMIT.VPC_PEER_PER_VPC_LIMIT_TYPE", "reachMax"),
            e.$watch("[regionId, fromVpcId]", function () {
                var t,
                    i,
                    n = e.$refs.fromVpc;
                n && (t = n.getOption(), i = t && t.vpcCIDR),
                e.$set("fromCidr", i)
            }),
            e.$watch("[myAccount, peerRegionId, toVpcId]", function () {
                var t,
                    i,
                    n = e.$refs.toVpc;
                e.myAccount && n && (i = e.$refs.toVpc.getOption(), t = i && i.vpcCIDR),
                e.$set("toCidr", t)
            }),
            a.beeBind(e, e, "_isConflict(fromCidr, toCidr)", "cidrConflict"),
            e.$watch("[regionId, fromVpcId]", a.bufferFn(function () {
                var t = e.regionId;
                e.$set("count", 0),
                $.when(d.getDataProcessorTask(t), c.request(t, "DescribeVpcPeeringConnections", {
                        offset: 0,
                        limit: 100,
                        vpcId: e.fromVpcId
                    }).translate({
                        totalCount: "totalNum",
                        data: {
                            key: "list",
                            map: "conn"
                        }
                    }))
                    .done(function (t, i) {
                        var n = i.list;
                        n.forEach(t),
                        e.$set("usedConnMap", d.getUsedConnMap(n)),
                        e.$set("count", n.length)
                    })
            }, 500))
        },
        _isConflict: function (e, t) {
            return e && t
                ? a.isCIDRIntersect(e, t)
                : !1
        },
        _updateExpire: function () {
            var e = this,
                t = new Date,
                i = new Date(t.getTime() + 6048e5);
            e.$set("expireTime", i)
        },
        _formatTime: function (e, t) {
            return e && a.formatDate(e, t)
        }
    }, {
        defaults: $.extend({}, d)
    });
    u.tag("bandwidth-field", l);
    var v = {};
    d
        .VPC_CONN_XR_REGION
        .forEach(function (e) {
            v[e] = 1
        });
    var f = {};
    d
        .SUPPORT_REGIONAL_VPC_CONN
        .forEach(function (e) {
            f[e] = 1
        }),
    u.tag("region-select", r.extend({
        watchFields: [
            "limitGlobal", "fromRegionId"
        ],
        getData: function (e, t) {
            var i,
                n,
                a = this;
            $.when(o.getRegionMap().done(function (e) {
                i = e
            }), o.getRegionIdList().done(function (e) {
                n = e
            }))
                .done(function () {
                    var t = a.fromRegionId;
                    e(n.map(function (e) {
                        var n = !1;
                        return t && "" + e != "" + t && (t in f
                            ? e in f || (n = !0)
                            : e in v && t in v
                                ? a.limitGlobal && a.isGlobalLink(e, t) && (n = !0)
                                : n = !0), {
                            value: e,
                            text: i[e],
                            disabled: n
                        }
                    }))
                })
                .fail(t)
        }
    }, {
        defaults: $.extend({
            relevance: !1,
            selectStyle: {
                width: "280px"
            }
        }, d)
    })),
    u.tag("vpc-from-select", p.extend({}, {
        defaults: {
            textFields: [
                "unVpcId", "vpcName", "vpcCIDR"
            ],
            useUnId: !0,
            relevance: !1,
            selectStyle: {
                width: "280px"
            }
        }
    })),
    u.tag("vpc-to-select", p.extend({
        watchFields: [
            "fromRegionId", "fromVpcId", "regionId", "usedConnMap"
        ],
        getData: function (e, t) {
            var i = this,
                n = i.usedConnMap || {};
            i
                .__super__
                .getData
                .call(i, function (t) {
                    var a = d.regionIdToStr(i.fromRegionId),
                        s = i.fromVpcId,
                        o = d.regionIdToStr(i.regionId),
                        c = [a, s].join("|"),
                        r = n[c] || {};
                    t = [
                        {
                            text: "请选择...",
                            value: ""
                        }
                    ].concat(t.map(function (e) {
                        var t = [o, e.value].join("|");
                        return c === t
                            ? e.disabled = !0
                            : t in r && (e.disabled = !0, e.text += " （已有对等连接）"),
                        e
                    })),
                    e(t)
                }, t)
        }
    }, {
        defaults: {
            textFields: [
                "unVpcId", "vpcName", "vpcCIDR"
            ],
            useUnId: !0,
            relevance: !1,
            selectStyle: {
                width: "280px"
            }
        }
    })),
    i.exports = u
}),
define("modules/vpc/conn/cacher", function (e, t, i) {
    var n = e("manager"),
        a = e("widget/cacher/Cacher"),
        s = [
            "VPC_CONN_XR", "VPC_CONN_XR_GLOBAL"
        ],
        d = {
            getWhiteListMap: function (e, t) {
                n
                    .queryWhiteList({
                        whiteKey: s
                    }, function (t) {
                        var i = {};
                        s.forEach(function (e) {
                            var n = t[e];
                            i[e] = n && n.length
                        }),
                        e(i)
                    }, t)
            }
        };
    i.exports = new a([
        [d, "getWhiteListMap"]
    ])
}),
define("modules/vpc/conn/Chart", function (e, t, i) {
    var n = e("widget/chart/Recent"),
        a = e("widget/cacher/Cacher"),
        s = e("./util"),
        d = e("manager"),
        o = function (e) {
            if (e) {
                var t;
                for (t = 0; t < e.length; t++) 
                    -1 === e[t] && (e[t] = null)
            }
        },
        c = {
            getChartData: function (e, t, i, n, a, s, c) {
                var r = $.Deferred(),
                    l = ["packets", "packets_loss", "traffic"];
                return d.queryBarad(e, {
                    type: "conn",
                    dimension: [
                        {
                            appid: i,
                            peeringconnectionid: n
                        }
                    ],
                    metric: l,
                    startTime: a,
                    endTime: s,
                    period: c,
                    selfRegionId: t
                }, function (e) {
                    l
                        .forEach(function (t) {
                            e && e[t] && o(e[t])
                        }),
                    r.resolve(e)
                }, r.reject),
                r
            }
        },
        r = new a([
            {
                host: c,
                fnName: "getChartData",
                promise: !0
            }
        ]),
        l = function (e) {
            var t = this,
                i = t.toMb
                    ? 1048576
                    : t.toMbps
                        ? 131072
                        : 1;
            t.div > 1 && (i *= t.div),
            e = e || [];
            for (var n = !1, a = 0, s = e.length; s > a; a++) 
                i > 1 && e[a] && (e[a] = 1 * (e[a] / i).toFixed(3), t.round && (e[a] = Math.round(e[a]))),
                (e[a] || 0 === e[a]) && (n = !0);
            return n || (e = []),
            e
        },
        p = function () {
            var e = $.Deferred(),
                t = this,
                i = t.dayView
                    ? 86400
                    : 300,
                n = t.composedId;
            return $.when(r.getFreshChartData(s.regionStrToId(n.region), s.regionStrToId(n.region), n.appId, n.peeringConnectionId, t.fromTime, t.toTime, i), r.getFreshChartData(s.regionStrToId(n.peerRegion), s.regionStrToId(n.region), n.peerAppId, n.peeringConnectionId, t.fromTime, t.toTime, i))
                .done(function (i, n) {
                    var a;
                    a = t.process
                        ? t.process(i, n)
                        : (t.isIn
                            ? n
                            : i)[t.metric],
                    e.resolve({
                        y: l.call(t, a)
                    })
                }),
            e
        };
    i.exports = n.extend({
        isComposedIdAvailable: function (e) {
            return e && e.peeringConnectionId && e.region && e.appId && e.peerRegion && e.peerAppId
        }
    }, {
        defaults: {
            composedIdExpression: "data",
            charts: [
                {
                    title: "网络出带宽",
                    unit: "Mbps",
                    metric: "traffic",
                    toMbps: !0,
                    div: 300,
                    loadData: p
                }, {
                    title: "网络入带宽",
                    unit: "Mbps",
                    metric: "traffic",
                    isIn: !0,
                    toMbps: !0,
                    div: 300,
                    loadData: p
                }, {
                    title: "网络出包量",
                    unit: "pps",
                    metric: "packets",
                    div: 300,
                    process: function (e, t) {
                        var i,
                            n = e.packets || [],
                            a = e.packets_loss || [],
                            s = [],
                            d = this,
                            o = Math.ceil((d.toTime - d.fromTime) / (d.dayView
                                ? 86400
                                : 300));
                        for (i = 0; o > i; i++) 
                            null !== n[i] && void 0 !== n[i] && null !== a[i] && void 0 !== a[i]
                                ? s[i] = n[i] + a[i]
                                : s[i] = null;
                        return s
                    },
                    loadData: p
                }, {
                    title: "网络入包量",
                    unit: "pps",
                    metric: "packets",
                    div: 300,
                    process: function (e, t) {
                        var i,
                            n = t.packets || [],
                            a = t.packets_loss || [],
                            s = [],
                            d = this,
                            o = Math.ceil((d.toTime - d.fromTime) / (d.dayView
                                ? 86400
                                : 300));
                        for (i = 0; o > i; i++) 
                            null !== n[i] && void 0 !== n[i] && null !== a[i] && void 0 !== a[i]
                                ? s[i] = n[i] + a[i]
                                : s[i] = null;
                        return s
                    },
                    loadData: p
                }, {
                    title: "丢包率",
                    unit: "%",
                    process: function (e, t) {
                        var i,
                            n,
                            a,
                            s = t.packets || [],
                            d = e.packets || [],
                            o = t.packets_loss || [],
                            c = e.packets_loss || [],
                            r = [],
                            l = this,
                            p = Math.ceil((l.toTime - l.fromTime) / (l.dayView
                                ? 86400
                                : 300));
                        for (i = 0; p > i; i++) 
                            null !== d[i] && void 0 !== d[i] && null !== s[i] && void 0 !== s[i] && null !== c[i] && void 0 !== c[i] && null !== o[i] && void 0 !== o[i]
                                ? (n = d[i] + s[i] + c[i] + o[i], a = c[i] + o[i], r[i] = Math.round(1e4 * (n > 0
                                    ? a / n
                                    : a
                                        ? 1
                                        : 0)) / 100)
                                : r[i] = null;
                        return r
                    },
                    loadData: p
                }
            ]
        }
    })
}),
define("modules/vpc/conn/conn", function (e, t, i) {
    e("widget/region/regionSelector");
    var n = e("../BeeBase"),
        a = e("router"),
        s = e("../gridPage"),
        d = e("../cacher"),
        o = e("./util"),
        c = e("../cApi"),
        r = e("appUtil"),
        l = e("dialog"),
        p = e("tips"),
        u = e("../Grid"),
        v = e("../RenameBubble"),
        f = e("./Add"),
        m = e("./Chart"),
        h = e("./Detail"),
        b = e("./validator"),
        g = e("../vpc/Selector"),
        I = e("widget/fieldsManager/gridView"),
        w = n.extend({
            $tpl: '    <div>        <div b-style="{display: detailId ? \'none\' : \'block\'}">     ' +
                    '       <div class="manage-area-title">                <h2>对等连接</h2>             ' +
                    '   <region-selector b-ref="regionSelect" b-model="regionId" b-with="{getWhiteLis' +
                    't:getRegionWhiteList}"></region-selector>                <vpc-select b-ref="vpcS' +
                    'elect" region-id="{{regionId}}" has-all="1" use-un-id="1" b-model="unVpcId"></vp' +
                    'c-select>                <div class="manage-area-title-right">                  ' +
                    '  <a href="https://www.qcloud.com/doc/product/215/5000" target="_blank">        ' +
                    '                <b class="links-icon"></b> 对等连接帮助文档                    </a>     ' +
                    '           </div>            </div>            <div class="tc-15-action-panel"> ' +
                    '               <qc-action-button b-ref="create" name="add" label="+新建" attr="{{ ' +
                    '{\'data-title\':addDisabledText} }}" b-with="{action:actionHandler, disabled:add' +
                    'Disabled}"></qc-action-button>                <qc-action-button float="right" na' +
                    'me="columns" class-name="weak setting" b-with="{action:actionHandler}"></qc-acti' +
                    'on-button>                <qc-search float="right" name="search" placeholder="搜索' +
                    '对等连接的关键词" multiline="false" b-with="{search:actionHandler}" keyword="{{keyword}}' +
                    '"></qc-search>            </div>            <grid b-ref="grid" b-with="gridCfg">' +
                    '</grid>        </div>        <!--图表浮层-->        <div class="sidebar-panel" b-if=' +
                    '"chartId && !detailId">            <a class="btn-close" href="javascript:;" b-on' +
                    '-click="$set(\'chartId\', \'\')">关闭</a>            <!--<div class="pagination">-' +
                    '->                <!--<a href="javascript:;" class="btn-page page-prev {{!hasPre' +
                    'v ? \'\' : \'btn-page-disabled\'}}"-->                   <!--b-on-click="prevCha' +
                    'rt()"-->                   <!--title="上一条"><i class="icon-page-prev"></i></a>-->' +
                    '                <!--<a href="javascript:;" class="btn-page page-next {{!hasNext ' +
                    '? \'\' : \'btn-page-disabled\'}}"-->                   <!--b-on-click="nextChart' +
                    '()"-->                   <!--title="下一条"><i class="icon-page-next"></i></a>-->  ' +
                    '          <!--</div>-->            <div class="sidebar-panel-container nat-sideb' +
                    'ar">                <div class="sidebar-panel-hd">                    <h3>{{char' +
                    'tId}}(<span class="entry-name-txt">{{chartName}}</span>)监控</h3>                <' +
                    '/div>                <div class="sidebar-panel-bd">                    <chart da' +
                    'ta="{{chartData}}"></chart>                </div>            </div>        </div' +
                    '>        <!--详情面板-->        <div b-if="detailId">            <detail region-id="' +
                    '{{regionId}}"                    update-callback="{{detailUpdate}}"             ' +
                    '       tab="{{tab}}" b-model="detailId" data="{{detailData}}"></detail>        <' +
                    '/div>    </div>',
            $beforeInit: function () {
                var e = this;
                w
                    .__super__
                    .$beforeInit
                    .apply(e, arguments),
                e.$watch("regionId", function (t) {
                    t && d
                        .getLimits(t)
                        .done(function (t) {
                            e.$set("LIMIT", t)
                        })
                })
            },
            getRegionWhiteList: function () {
                return $
                    .Deferred()
                    .resolve(o.REGION_WHITELIST)
            }
        }, {
            defaults: {
                tab: "info",
                LIMIT: {
                    VPC_PEER_PER_VPC_LIMIT_TYPE: 20,
                    VPC_PEER_AVA_PER_VPC_LIMIT_TYPE: 10
                }
            }
        });
    w.tag("chart", m),
    w.tag("detail", h),
    w.tag("vpc-select", g),
    w.tag("grid", u.extend({
        $mixins: [I("vpc_conn_columns_v20160422")],
        backList: function () {
            this.$set({searchKey: "", pendingKeyword: ""})
        }
    }, {
        defaults: $.extend({
            colums: [
                {
                    key: "peeringConnectionId",
                    name: "ID/名称",
                    required: !0,
                    tdTpl: '    <p>        <a data-role="detail" href="javascript:;" class="text-overflow"> ' +
                            '           <span class="text-overflow">{{item.peeringConnectionId}}</span>      ' +
                            '  </a>    </p>    <span class="text-overflow m-width"><i b-if="item.renaming" cl' +
                            'ass="n-loading-icon"></i>    {{item.peeringConnectionName || "未命名"}}</span><i b-' +
                            'if="item.can.rename" class="pencil-icon hover-icon" data-role="rename" data-titl' +
                            'e="编辑"></i>'
                }, {
                    key: "_monitor",
                    name: "监控",
                    minWidth: 50,
                    width: 50,
                    tdTpl: '<i b-if="!item.sameRegion" class="dosage-icon" data-role="chart" data-title="查看监' +
                            '控"></i>{{item.sameRegion ? "-":""}}'
                }, {
                    key: "regionName",
                    name: "本端地域"
                }, {
                    key: "unVpcId",
                    name: "本端私有网络",
                    tdTpl: '    <p>        <a data-event="nav" href="/vpc/vpc?rid={{item.regionId}}&unVpcId=' +
                            '{{item.unVpcId}}">            <span class="text-overflow">{{item.unVpcId}}</span' +
                            '>        </a>    </p>    <span class="text-overflow" title="{{item.vpcName}} ({{' +
                            'item.vpcCIDR}})">{{item.vpcName}}</span>'
                }, {
                    key: "peerRegionName",
                    name: "对端地域"
                }, {
                    key: "peerUin",
                    name: "对端账号",
                    tdTpl: '    {{item.uin===item.peerUin ? "我的帐号" : item.peerUin}}'
                }, {
                    key: "uniqPeerVpcId",
                    name: "对端私有网络ID",
                    tdTpl: '    <span b-if="item.uin !== item.peerUin" class="text-overflow" >{{item.peerUnV' +
                            'pcId}}</span>    <div b-template b-if="item.uin === item.peerUin">        <p>   ' +
                            '         <a data-event="nav" href="/vpc/vpc?rid={{item.peerRegionId}}&unVpcId={{' +
                            'item.peerUnVpcId}}">                <span class="text-overflow">{{item.peerUnVpc' +
                            'Id}}</span>            </a>        </p>        <span class="text-overflow" title' +
                            '="{{item.peerVpcName}} ({{item.peerVpcCIDR}})">{{item.peerVpcName}}</span>    </' +
                            'div>'
                }, {
                    key: "bandwidth",
                    name: "带宽上限",
                    tdTpl: "    {{(item.sameRegion || item.bandwidth >= MAX_BANDWIDTH) ? '无上限' : (item.bandw" +
                            "idth >= 1000 ? (item.bandwidth / 1000 + 'Gbps') : (item.bandwidth +'Mbps'))}}"
                }, {
                    key: "state",
                    name: "状态",
                    minWidth: 80,
                    width: 80,
                    tdTpl: '<span class="text-overflow {{getStateCls(item.isOwner, item.state)}}">{{getState' +
                            'Text(item.isOwner, item.state)}}</span>'
                }, {
                    key: "_action",
                    name: "操作",
                    minWidth: 100,
                    required: !0,
                    tdTpl: '    <div b-if="!item.$loading" b-template>{{>getActionsCell(getStateActions(item' +
                            '.isOwner, item.state))}}</div>    <div b-if="item.$loading" b-template>        <' +
                            'i class="n-loading-icon"></i>{{item.$loadingText}}    </div>'
                }
            ],
            regionMap: {},
            outerPaging: !0
        }, o)
    })),
    i.exports = s
        .extend()
        .mixin({
            routeBase: "/vpc/conn",
            watchFields: [
                "regionId", "unVpcId"
            ],
            params: $.extend({}, s.params, {
                keyword: "",
                detailId: {
                    defaults: "",
                    history: !0
                }
            }),
            Bee: w,
            beforeRender: function () {
                var e = this;
                return o
                    .getDynamicConstants()
                    .done(function (t) {
                        e.dynamics = t
                    })
            },
            processBeeConfig: function (e) {
                return e.$data = e.$data || {},
                $.extend(e.$data, this.dynamics),
                e
            },
            onRender: function () {
                var t = this,
                    i = t.$el,
                    n = t.bee,
                    a = n.$refs.vpcSelect,
                    s = function (e) {
                        var t = $(e.target).parents();
                        t.length && !t.is(i.find(".sidebar-panel")) && n.$set("chartId", "")
                    },
                    d = $(document.body),
                    o = r.bufferFn(function () {
                        d.on("click", s)
                    }, 100),
                    c = function () {
                        d.off("click", s)
                    };
                a.$watch("list", function (e) {
                    n.$set("addDisabled", e.length <= 1
                        ? 1
                        : ""),
                    n.$set("addDisabledText", e.length <= 1
                        ? "无可用私有网络"
                        : "")
                }, !0),
                n.$watch("chartId", function (e) {
                    c(),
                    e && (o.destroy(), o())
                }),
                e("../adTip").init(t.$el)
            },
            getData: function () {
                var e = this,
                    t = e.bee,
                    i = t.regionId,
                    n = t.count,
                    a = t.page,
                    s = n * (a - 1),
                    d = n,
                    r = t.keyword,
                    l = $.Deferred();
                return $.when(o.getDataProcessorTask(i), c.request(i, "DescribeVpcPeeringConnections", {
                        offset: s,
                        limit: d,
                        peeringConnectionName: r,
                        vpcId: t.unVpcId || void 0
                    }).translate({
                        totalCount: "totalNum",
                        data: {
                            key: "list",
                            map: "conn"
                        }
                    }))
                    .then(function (e, i) {
                        var n = i.list || [];
                        n.forEach(e),
                        t.$set("usedConnMap", o.getUsedConnMap(n)),
                        l.resolve(i)
                    }, l.reject),
                l
            },
            onGridRename: function (e, t, i) {
                var n = this,
                    a = n.bee,
                    s = a.$refs.grid,
                    c = e.peeringConnectionName;
                new v({
                    value: c,
                    target: $(i.currentTarget).parent(),
                    validate: b.peeringConnectionName,
                    doSave: function (t) {
                        return o
                            .request(e.region !== e.peerRegion, e.regionId, "ModifyVpcPeeringConnection", {
                                peeringConnectionId: e.peeringConnectionId,
                                peeringConnectionName: t
                            })
                            .done(function () {
                                d.resetNamespace("conn", e.regionId, e.peeringConnectionId),
                                d.resetNamespace("conn", e.peerRegionId, e.peeringConnectionId)
                            })
                    },
                    onSave: function (e, i, n) {
                        this.handleSimpleEdit({
                            def: e,
                            host: s.list,
                            index: t,
                            key: "peeringConnectionName",
                            loadingKey: "renaming",
                            newValue: i,
                            oldValue: n
                        })
                    }
                })
            },
            onGridChart: function (e, t, i) {
                var n = this,
                    a = n.bee,
                    s = a.$refs.grid.list;
                t =+ t || 0,
                a.$set({
                    chartId: e.peeringConnectionId,
                    chartName: e.peeringConnectionName,
                    chartData: e,
                    hasPrev: 0 >= t,
                    prevChart: 0 >= t
                        ? $.noop
                        : function () {
                            n.onGridChart(s[t - 1], t - 1)
                        },
                    hasNext: t >= s.length - 1,
                    nextChart: t >= s.length - 1
                        ? $.noop
                        : function () {
                            n.onGridChart(s[t + 1], t + 1)
                        }
                })
            },
            onGridDetail: function (e, t, i) {
                var n = this,
                    a = n.bee,
                    s = a.$refs.grid;
                a.$set({
                    tab: "info",
                    detailData: e,
                    detailUpdate: function (e) {
                        s.updateItem(t, e)
                    },
                    detailId: e.peeringConnectionId
                })
            },
            onActionAdd: function () {
                var e,
                    t = this,
                    i = t.bee,
                    n = i.regionId;
                e = new f({
                    $data: $.extend({
                        regionId: n,
                        peerRegionId: n,
                        myAccount: "1",
                        usedConnMap: i.usedConnMap,
                        LIMIT: i.LIMIT
                    }, o, t.dynamics)
                }),
                l.create(e.$el, "550", "", {
                    title: "新建对等连接",
                    preventResubmit: !0,
                    button: {
                        "创建": function (n) {
                            if (e.cidrConflict) {
                                var s = b._find$el(e.$el, "toVpcId"),
                                    c = "私有网络网段有冲突，无法建立连接";
                                return s && s.length
                                    ? b._toggleError(s, c, {$btn: n})
                                    : p.error(c),
                                void l.toggleBtnDisable(!1)
                            }
                            var c,
                                r = [
                                    "fromVpcId", "peeringConnectionName"
                                ],
                                u = e.regionId,
                                v = e.sameRegion,
                                f = e.myAccount;
                            if (r = f
                                ? r.concat(["toVpcId"])
                                : r.concat(["toCreatorUin", "toCreatorVpcId"]), c = b.multiValidate(e, r, {}, e.$el, {$btn: n})) 
                                return void l.toggleBtnDisable(!1);
                            l
                                .freeze(!0)
                                .toggleBtnLoading(!0);
                            var m = {
                                vpcId: e.fromVpcId,
                                peeringConnectionName: e.peeringConnectionName,
                                peerUin: f
                                    ? i.ownerUin
                                    : e
                                        .toCreatorUin
                                        .trim(),
                                peerVpcId: f
                                    ? e.toVpcId
                                    : e.toCreatorVpcId
                            };
                            v || (m.peerRegion = e.peerRegion, m.bandwidth = e.bandwidth),
                            o
                                .request(!v, u, "CreateVpcPeeringConnection", m, !1)
                                .done(function () {
                                    d.resetNamespace("conn", i.regionId),
                                    i.regionId !== u && i.regionId !== e.peerRegionId && i.$set("regionId", u),
                                    t.bufferReload(),
                                    l.create("要向对等连接发送、接收数据，还需在路由表中添加对端路由", "500", "", {
                                        title: f
                                            ? "对等连接已建立！"
                                            : "对等连接已申请！",
                                        button: {
                                            "修改路由表": function () {
                                                l.hide(),
                                                a.navigate("/vpc/route")
                                            }
                                        }
                                    })
                                })
                                .fail(function (t) {
                                    var i,
                                        n,
                                        a = t && t.data && t.data.cgwerrorCode,
                                        s = "创建对等连接失败",
                                        d = f
                                            ? "toVpcId"
                                            : "toCreatorVpcId";
                                    switch (a) {
                                        case 28002:
                                            s = "未找到此私有网络",
                                            i = d;
                                            break;
                                        case 28112:
                                            s = "私有网络ID有误",
                                            i = d;
                                            break;
                                        case 28146:
                                            s = "对等连接已存在",
                                            i = d;
                                            break;
                                        case 28147:
                                            s = "对等连接创建数已达上限",
                                            i = "fromVpcId";
                                            break;
                                        case 28148:
                                            s = "所选私有网络的对等连接创建数已达上限",
                                            i = "fromVpcId";
                                            break;
                                        case 28149:
                                        case 28156:
                                            s = "私有网络网段有冲突，无法建立连接",
                                            i = d;
                                            break;
                                        case 28150:
                                        case 11031:
                                            s = "对端创建者不存在",
                                            i = "toCreatorUin";
                                            break;
                                        case 28151:
                                            s = "对端对等连接创建数已达上限",
                                            i = d;
                                            break;
                                        case 28152:
                                            s = "对端私有网络的对等连接创建数已达上限",
                                            i = d
                                    }
                                    s && (i && (n = b._find$el(e.$el, i)), n && n.length
                                        ? b._toggleError(n, s)
                                        : p.error(s)),
                                    l
                                        .freeze(!1)
                                        .toggleBtnLoading(!1)
                                        .toggleBtnDisable(!1)
                                })
                        }
                    }
                })
            },
            onGridRemove: function (e, t, i) {
                var n = this,
                    a = n.bee,
                    s = a.$refs.grid;
                n.confirm({
                    title: "确定删除该对等连接？",
                    content: "删除后私有网络路由表中包含该对等连接的路由将一并删除。",
                    confirmKey: "删除"
                }, i)
                    .done(function () {
                        var i = o.request(e.region !== e.peerRegion, a.regionId, "DeleteVpcPeeringConnection", {
                            peeringConnectionId: e.peeringConnectionId
                        }, "删除对等连接失败！")
                            .done(function () {
                                n.bufferReload(),
                                d.resetNamespace("conn", e.regionId, e.peeringConnectionId),
                                d.resetNamespace("conn", e.peerRegionId, e.peeringConnectionId)
                            });
                        v
                            .prototype
                            .handleSimpleEdit({
                                def: i,
                                host: s.list,
                                index: t,
                                key: "$loadingText",
                                loadingKey: "$loading",
                                newValue: "删除中",
                                oldValue: ""
                            })
                    })
            },
            onGridAccept: function (e, t, i) {
                var n = this,
                    a = n.bee,
                    s = a.$refs.grid;
                n.confirm({
                    title: "确定接受该对等连接？",
                    content: "接受后，私有网络之间即可配置路由表相互访问。",
                    confirmKey: "接受"
                }, i)
                    .done(function () {
                        var i = o.request(e.region !== e.peerRegion, a.regionId, "AcceptVpcPeeringConnection", {
                            peeringConnectionId: e.peeringConnectionId
                        }, "接受对等连接失败！")
                            .done(function () {
                                n.bufferReload(),
                                d.resetNamespace("conn", e.regionId, e.peeringConnectionId),
                                d.resetNamespace("conn", e.peerRegionId, e.peeringConnectionId)
                            });
                        v
                            .prototype
                            .handleSimpleEdit({
                                def: i,
                                host: s.list,
                                index: t,
                                key: "$loadingText",
                                loadingKey: "$loading",
                                newValue: "接受中",
                                oldValue: ""
                            })
                    })
            },
            onGridReject: function (e, t, i) {
                var n = this,
                    a = n.bee,
                    s = a.$refs.grid;
                n.confirm({
                    title: "确定拒绝该对等连接？",
                    confirmKey: "拒绝"
                }, i)
                    .done(function () {
                        var i = o.request(e.region !== e.peerRegion, a.regionId, "RejectVpcPeeringConnection", {
                            peeringConnectionId: e.peeringConnectionId
                        }, "拒绝对等连接失败！")
                            .done(function () {
                                n.bufferReload(),
                                d.resetNamespace("conn", e.regionId, e.peeringConnectionId),
                                d.resetNamespace("conn", e.peerRegionId, e.peeringConnectionId)
                            });
                        v
                            .prototype
                            .handleSimpleEdit({
                                def: i,
                                host: s.list,
                                index: t,
                                key: "$loadingText",
                                loadingKey: "$loading",
                                newValue: "拒绝中",
                                oldValue: ""
                            })
                    })
            },
            onGridResubmit: function (e, t, i) {
                var n = this,
                    a = n.bee,
                    s = a.$refs.grid,
                    c = o.request(e.region !== e.peerRegion, a.regionId, "EnableVpcPeeringConnection", {
                        peeringConnectionId: e.peeringConnectionId
                    }, "重新申请对等连接失败！")
                        .done(function () {
                            n.bufferReload(),
                            d.resetNamespace("conn", e.regionId, e.peeringConnectionId),
                            d.resetNamespace("conn", e.peerRegionId, e.peeringConnectionId);
                        });
                v
                    .prototype
                    .handleSimpleEdit({
                        def: c,
                        host: s.list,
                        index: t,
                        key: "$loadingText",
                        loadingKey: "$loading",
                        newValue: "重新申请中",
                        oldValue: ""
                    })
            }
        })
}),
define("modules/vpc/conn/Detail", function (e, t, i) {
    var n = e("../BeeBase"),
        a = e("../cApi"),
        s = e("./Chart"),
        d = e("./Info"),
        o = e("./util"),
        c = (e("../cacher"), e("appUtil")),
        r = "peeringConnectionId",
        l = i.exports = n.extend({
            $tpl: '    <div b-style="{display: hidden ? \'none\' : \'\'}">        <div class="retur' +
                    'n-title-panel bottom-border" b-style="{display:notFound||loading?\'none\':\'\'}"' +
                    '>            <a href="javascript:;" b-on-click="back()" class="btn-return"><i cl' +
                    'ass="btn-back-icon"></i><span>返回</span></a>            <i class="line"></i>     ' +
                    '       <h2>{{data&&data.peeringConnectionName}}详情</h2>            <div class="ma' +
                    'nage-area-title-right">                <a href="https://www.qcloud.com/doc/produ' +
                    'ct/215/5000" target="_blank">                    <b class="links-icon"></b> 对等连接' +
                    '帮助文档                </a>            </div>        </div>        <div b-if="loadi' +
                    'ng" style="text-align:center; padding:100px;">加载中...</div>        <div b-if="not' +
                    'Found" style="text-align:center; padding:100px;">未查询到指定对等连接, <a href="javascript' +
                    ':;" b-on-click="$set(\'id\', \'\')">返回列表</a></div>        <div b-if="!data.sameR' +
                    'egion" class="tab-switch" b-style="{display:notFound||loading?\'none\':\'\'}">  ' +
                    '          <a class="{{tab===\'info\' ? \'cur\' : \'\'}}" b-on-click="$set(\'tab' +
                    '\', \'info\')" href="javascript:;">基本信息</a>            <a class="{{tab===\'chart' +
                    '\' ? \'cur\' : \'\'}} {{data.sameRegion ? \'text-weak\' : \'\'}}" b-on-click="sw' +
                    'itchChart()" href="javascript:;">监控</a>        </div>        <info b-with="{regi' +
                    'onId:regionId, data: data, updateCallback: infoUpdateCallback.bind(this)}" b-if=' +
                    '"!loading && !notFound && tab===\'info\'"></info>        <div class="charts-pane' +
                    'l vpc-line-date-wrap" b-if="!loading && !notFound && tab===\'chart\'">          ' +
                    '  <chart b-with="{data:data}"></chart>        </div>    </div>',
            $valuekey: "id",
            back: function () {
                history.back()
            },
            watchFields: [
                "regionId", "id"
            ],
            $beforeInit: function () {
                var e = this;
                l
                    .__super__
                    .$beforeInit
                    .apply(e, arguments),
                e.$watch("[" + e.watchFields.join(",") + "]", e.bufferReload = c.bufferFn(function () {
                    e.loadData()
                }, 100)),
                e.bindDetail && e.bindDetail(e)
            },
            infoUpdateCallback: function (e) {
                var t = this;
                Object
                    .keys(e)
                    .forEach(function (i) {
                        t.$set("data." + i, e[i])
                    }),
                t.updateCallback && t.updateCallback(e)
            },
            switchChart: function () {
                var e = this;
                e.data && !e.data.sameRegion && e.$set("tab", "chart")
            },
            loadData: function () {
                var e = this,
                    t = e.regionId;
                return !t || !e.id || e.data && e.data[r] === e.id
                    ? void e.$set({
                        loading: !1,
                        notFound: !1
                    })
                    : (e.$set({
                        loading: !0,
                        notFound: !1,
                        data: {}
                    }), void $.when(o.getDataProcessorTask(t), a.request(t, "DescribeVpcPeeringConnections", {
                        offset: 0,
                        limit: 1,
                        peeringConnectionId: e.id
                    }).translate({
                        totalCount: "totalNum",
                        data: {
                            key: "list",
                            map: "conn"
                        }
                    })).done(function (t, i) {
                        var n = i.list;
                        return !n || n.length < 0
                            ? void e.$set({
                                notFound: !0,
                                data: {}
                            })
                            : void e.$set({
                                notFound: !1,
                                data: t(n[0])
                            })
                    }).always(function () {
                        e.$set("loading", !1)
                    }))
            }
        }, {
            defaults: {
                tab: "info",
                loading: !0
            }
        });
    l.tag("info", d),
    l.tag("chart", s)
}),
define("modules/vpc/conn/field/Bandwidth", function (e, t, i) {
    var n = e("../../DropDown"),
        a = e("../util");
    i.exports = n.extend({
        watchFields: [],
        getData: function (e, t) {
            e(a.BANDWIDTH_LIST.map(function (e) {
                return {
                    text: e >= a.MAX_BANDWIDTH
                        ? "无上限"
                        : e >= 1e3
                            ? e / 1e3 + "Gbps"
                            : e + "Mbps",
                    value: e
                }
            }))
        }
    })
}),
define("modules/vpc/conn/Info", function (e, t, i) {
    var n,
        a = e("../cacher"),
        s = (e("../cApi"), e("appUtil"), e("./util")),
        d = e("./validator"),
        o = (e("../Confirm"), e("reporter")),
        c = e("widget/popover/TextEditor"),
        r = c.extend({
            $tpl: '    <div class="in-place-editor" style="position:absolute;">        {{>content}}' +
                    '        <div class="action-line">            <button class="tc-15-btn m" b-on="{' +
                    'click:handleYes}">{{yesText}}</button>            <button class="tc-15-btn m wea' +
                    'k" b-on="{click:handleNo}">{{noText}}</button>        </div>    </div>',
            content: '    <div class="form-unit {{invalid ? \'is-error\' : \'\'}}">        <input type' +
                    '="text" class="tc-15-input-text m" maxlength="{{maxLength}}"               b-sty' +
                    'le="inputStyle"               b-model="value" b-on="{keypress : ifEnter}">      ' +
                    '  <span b-if="postfix" class="text-suffix">{{postfix}}</span>        <p class="f' +
                    'orm-input-help" b-if="dynamicTips && !invalid && !tips">{{>dynamicTips}}</p>    ' +
                    '    <p class="form-input-help" b-if="invalid || tips">{{invalid || tips}}</p>   ' +
                    ' </div>'
        }),
        l = e("../BeeBase");
    n = i.exports = l.extend({
        $tpl: '    <div b-style="{display: hidden ? \'none\' : \'\'}">        <div class="param' +
                '-box nat-form list-ip-form">            <div class="param-hd">                <h' +
                '3>基本信息</h3>                <!--<a class="link-edit" href="javascript:;"><i class' +
                '="pencil-icon "></i>编辑</a>-->            </div>            <div class="param-bd"' +
                '>                <ul class="item-descr-list">                    <li>           ' +
                '             <span class="item-descr-tit">名称</span>                    <span cla' +
                'ss="item-descr-txt">                        {{data.peeringConnectionName}}      ' +
                '                  <a href="javascript:;" b-if="data.can.rename && !renaming" b-o' +
                'n="{click:rename}">更改</a>                        <i b-if="renaming" class="n-loa' +
                'ding-icon"></i>                    </span>                    </li>             ' +
                '       <li>                        <span class="item-descr-tit">ID</span>       ' +
                '                 <span class="item-descr-txt">{{data.peeringConnectionId}}</span' +
                '>                    </li>                    <li>                        <span ' +
                'class="item-descr-tit">状态</span>                        <span class="item-descr-' +
                'txt"><span class="text-overflow {{getStateCls(data.isOwner, data.state)}}">{{get' +
                'StateText(data.isOwner, data.state)}}</span></span>                    </li>    ' +
                '                <li>                        <span class="item-descr-tit">本端地域</s' +
                'pan>                        <span class="item-descr-txt">{{data.regionName}}</sp' +
                'an>                    </li>                    <li>                        <spa' +
                'n class="item-descr-tit">本端私有网络</span>                    <span class="tc-15-lis' +
                't-txt">                        <a data-event="nav" href="/vpc/vpc?rid={{data.reg' +
                'ionId}}&unVpcId={{data.unVpcId}}">{{data.unVpcId}}</a>                          ' +
                '  ({{data.vpcName}} | {{data.vpcCIDR}})                    </span>              ' +
                '      </li>                    <li>                        <span class="item-des' +
                'cr-tit">对端地域</span>                        <span class="item-descr-txt">{{data.p' +
                'eerRegionName}}</span>                    </li>                    <li>         ' +
                '               <span class="item-descr-tit">对端账号</span>                        <' +
                'span class="item-descr-txt">{{data.uin===data.peerUin ? "我的帐号" : data.peerUin}}<' +
                '/span>                    </li>                    <li>                        <' +
                'span class="item-descr-tit">对端私有网络</span>                    <span class="tc-15-' +
                'list-txt">                        <div b-if="data.uin !== data.peerUin" b-templa' +
                'te>{{data.peerUnVpcId}}</div>                        <div b-if="data.uin === dat' +
                'a.peerUin" b-template>                            <a data-event="nav" href="/vpc' +
                '/vpc?rid={{data.peerRegionId}}&unVpcId={{data.peerUnVpcId}}">{{data.peerUnVpcId}' +
                '}</a>                            ({{data.peerVpcName}} | {{data.peerVpcCIDR}})  ' +
                '                      </div>                    </span>                    </li>' +
                '                    <li>                        <span class="item-descr-tit">带宽上' +
                '限</span>                    <span class="item-descr-txt">                       ' +
                ' <div b-if="data.sameRegion" b-template>无上限</div>                        <div b-' +
                'if="!data.sameRegion" b-template>                            {{data.bandwidth >=' +
                ' MAX_BANDWIDTH ? \'无上限\' : (data.bandwidth >= 1000 ? (data.bandwidth / 1000 + \'' +
                'Gbps\') : (data.bandwidth +\'Mbps\')) }}                            <a href="jav' +
                'ascript:;" b-if="data.can.changeBandwidth && !bandwidthChanging" b-on="{click:ch' +
                'angeBandwidth}">更改带宽</a>                            <i b-if="bandwidthChanging" ' +
                'class="n-loading-icon"></i>                        </div>                    </s' +
                'pan>                    </li>                    <li>                        <sp' +
                'an class="item-descr-tit">创建时间</span>                        <span class="item-d' +
                'escr-txt">{{data.createTime}}</span>                    </li>                </u' +
                'l>            </div>        </div>        <div class="param-box nat-form">      ' +
                '      <div class="param-hd">                <h3>相关路由策略<div b-template b-if="data' +
                '.uin===data.peerUin">（本端）</div></h3>            </div>            <route-info re' +
                'gion-id="{{data.regionId}}" un-vpc-id="{{data && data.unVpcId}}" id="{{data && d' +
                'ata.peeringConnectionId}}"></route-info>        </div>        <div class="param-' +
                'box nat-form" b-if="data.uin===data.peerUin">            <div class="param-hd"> ' +
                '               <h3>相关路由策略（对端）</h3>            </div>            <route-info regi' +
                'on-id="{{data.peerRegionId}}" un-vpc-id="{{data && data.peerUnVpcId}}" id="{{dat' +
                'a && data.peeringConnectionId}}"></route-info>        </div>    </div>',
        $beforeInit: function () {
            var e = this;
            n
                .__super__
                .$beforeInit
                .apply(e, arguments),
            s
                .getDynamicConstants()
                .done(function (t) {
                    e.$set(t)
                })
        },
        $afterInit: function () {
            var e = this;
            n
                .__super__
                .$afterInit
                .apply(e, arguments)
        },
        rename: function (e) {
            var t = this,
                i = t.data,
                n = i.peeringConnectionName;
            new r({
                $data: {
                    value: n,
                    maxLength: 25,
                    dynamicTips: '还能输入{{maxLength - value.length}}个字符，英文、汉字、数字、连接线"-"或下划线"_"'
                },
                target: $(e.currentTarget).parent(),
                validate: d.peeringConnectionName,
                doSave: function (e, t) {
                    return s
                        .request(!i.sameRegion, i.regionId, "ModifyVpcPeeringConnection", {
                            peeringConnectionId: i.peeringConnectionId,
                            peeringConnectionName: e
                        })
                        .done(function () {
                            a.resetNamespace("conn", i.regionId, i.peeringConnectionId),
                            a.resetNamespace("conn", i.peerRegionId, i.peeringConnectionId)
                        })
                },
                onSave: function (e, i, n) {
                    this
                        .handleSimpleEdit({
                            def: e,
                            host: t,
                            key: "data.peeringConnectionName",
                            loadingKey: "renaming",
                            newValue: i,
                            oldValue: n
                        })
                        .done(function () {
                            t.updateCallback({peeringConnectionName: i})
                        })
                }
            })
        },
        changeBandwidth: function (e) {
            var t = this,
                i = t.data,
                n = i.bandwidth;
            new r({
                $data: {
                    value: n + "",
                    BANDWIDTH_LIST: s.BANDWIDTH_LIST
                },
                content: '    <div class="form-unit {{invalid ? \'is-error\' : \'\'}}">        <select cla' +
                        'ss="tc-15-select m" b-model="value">            <option b-repeat="bandwidth in B' +
                        'ANDWIDTH_LIST" value="{{bandwidth}}" selected?="bandwidth == value">{{bandwidth ' +
                        '? (bandwidth >= 1000 ? (bandwidth / 1000 + \'Gbps\') : (bandwidth +\'Mbps\')) : ' +
                        '\'不限制\'}}</option>        </select>    </div>',
                target: $(e.currentTarget).parent(),
                doSave: function (e, n) {
                    return s.request(!i.sameRegion, t.regionId, "ModifyVpcPeeringConnection", {
                        peeringConnectionId: i.peeringConnectionId,
                        bandwidth: e
                    })
                },
                onSave: function (e, i, n) {
                    this
                        .handleSimpleEdit({
                            def: e,
                            host: t,
                            key: "data.bandwidth",
                            loadingKey: "bandwidthChanging",
                            newValue: i,
                            oldValue: n
                        })
                        .done(function () {
                            t.updateCallback({bandwidth: i})
                        }),
                    o.click("vpc.peerConn.detail.changeBandwidth")
                }
            })
        }
    }, {
        defaults: $.extend({
            updateCallback: $.noop
        }, s)
    }),
    n.tag("route-info", e("../RouteInfo"))
}),
define("modules/vpc/conn/util", function (e, t, i) {
    var n,
        a = {
            CreateVpcPeeringConnection: "CreateVpcPeeringConnectionEx",
            DeleteVpcPeeringConnection: "DeleteVpcPeeringConnectionEx",
            AcceptVpcPeeringConnection: "AcceptVpcPeeringConnectionEx",
            RejectVpcPeeringConnection: "RejectVpcPeeringConnectionEx",
            EnableVpcPeeringConnection: "EnableVpcPeeringConnectionEx",
            ModifyVpcPeeringConnection: "ModifyVpcPeeringConnectionEx"
        },
        s = e("constants"),
        d = e("appUtil"),
        o = e("../util"),
        c = e("manager"),
        r = e("./cacher"),
        l = e("../cacher"),
        p = e("../cApi"),
        u = [
            [
                1, 4, 7, 8
            ],
            [5],
            [6]
        ],
        v = {};
    u.forEach(function (e, t) {
        e
            .forEach(function (e) {
                v[e] = t
            })
    });
    var f = s.REGIONMAP,
        m = {};
    Object
        .keys(f)
        .forEach(function (e) {
            m[f[e]] = e
        });
    var h = {
            PENDING: 0,
            CONNECTED: 1,
            EXPIRED: 2,
            REJECTED: 3,
            REMOVED: 4,
            OVERDUE: 1e4
        },
        b = {
            remove: "删除",
            accept: "接受",
            reject: "拒绝",
            resubmit: "重新申请",
            rename: "重命名",
            changeBandwidth: "修改带宽"
        };
    Object
        .keys(b)
        .forEach(function (e) {
            b[e] = {
                act: e,
                text: b[e]
            }
        });
    var g = {},
        I = {},
        w = {},
        y = {};
    [
        [
            h.PENDING,
            "申请中...",
            "",
            [b.remove],
            [b.rename, b.changeBandwidth]
        ],
        [
            h.CONNECTED,
            "已连接",
            "text-success",
            [b.remove],
            [b.rename, b.changeBandwidth]
        ],
        [
            h.EXPIRED,
            "已过期",
            "text-weak",
            [b.resubmit, b.remove]
        ],
        [
            h.REJECTED,
            "对端已拒绝",
            "text-weak",
            [b.remove]
        ],
        [
            h.REMOVED,
            "对端已删除",
            "text-weak",
            [b.remove]
        ],
            [
                h.OVERDUE,
                "欠费停服",
                "text-warning",
                [b.remove]
        ]
    ]
        .forEach(function (e) {
            var t = e[0],
                i = e[1],
                n = e[2],
                a = e[3],
                s = e[4] || [],
                d = y[t] = {};
            g[t] = i,
            I[t] = n,
            w[t] = a,
            s
                .concat(a)
                .forEach(function (e) {
                    d[e.act] = !0
                })
        });
    var x = {},
        C = {},
        T = {},
        N = {};
    [
        [
            h.PENDING,
            "待接受",
            "text-warning",
            [b.accept, b.reject]
        ],
        [
            h.CONNECTED,
            "已连接",
            "text-success",
            [b.remove]
        ],
        [h.EXPIRED, "已过期", "text-weak", []
        ],
        [h.REJECTED, "已拒绝", "text-weak", []
        ],
        [
            h.REMOVED,
            "对端已删除",
            "text-weak",
            [b.remove]
        ],
            [
                h.OVERDUE,
                "欠费停服",
                "text-warning",
                [b.remove]
        ]
    ]
        .forEach(function (e) {
            var t = e[0],
                i = e[1],
                n = e[2],
                a = e[3],
                s = e[4] || [],
                d = N[t] = {};
            x[t] = i,
            C[t] = n,
            T[t] = a,
            s
                .concat(a)
                .forEach(function (e) {
                    d[e.act] = !0
                })
        });
    var k = i.exports = {
        REGION_WHITELIST: s.CONN_REGION,
        MAX_BANDWIDTH: 5e3,
        BANDWIDTH_LIST: [
            10,
            20,
            50,
            100,
            200,
            500,
            1e3
        ],
        STATE: h,
        VPC_CONN_XR_REGION: [
            "1", "4", "5", "6", "8"
        ],
        SUPPORT_REGIONAL_VPC_CONN: [
            "7", "11"
        ],
        isGlobalLink: function (e, t) {
            return e === t
                ? !1
                : e in v && t in v
                    ? v[e] !== v[t]
                    : !0
        },
        regionIdToStr: function (e) {
            return f[e]
        },
        regionStrToId: function (e) {
            return m[e]
        },
        getStateText: function (e, t) {
            return (e
                ? g
                : x)[t]
        },
        getStateCls: function (e, t) {
            return (e
                ? I
                : C)[t]
        },
        getStateActions: function (e, t) {
            return (e
                ? w
                : T)[t]
        },
        canDoAction: function (e, t, i) {},
        request: function (e, t, i, s, d) {
            return e && (i = a[i]),
            e
                ? (n || (n = o.makeAsyncTaskToPromise(p, "request", !0)), n.call(this, t, i, s, d))
                : p.request(t, i, s, d)
        },
        getDataProcessorTask: function (e) {
            var t = $.Deferred(),
                i = $.Deferred(),
                n = $.Deferred();
            return c.getComData(i.resolve, i.reject),
            c.queryRegion(n.resolve, n.reject),
            l
                .getRegionIdList()
                .done(function (a) {
                    var s = {},
                        o = a.map(function (e) {
                            var t = $.Deferred();
                            return l
                                .getVpcListEx(e)
                                .done(function (e) {
                                    e
                                        .vpcList
                                        .forEach(function (e) {
                                            s[e.unVpcId] = e
                                        })
                                })
                                .always(t.resolve),
                            t
                        });
                    $
                        .when(i, n, $.when.apply($, o))
                        .then(function (i, n) {
                            var a = "" + i.ownerUin,
                                o = function (e) {
                                    var t = s[e.unVpcId];
                                    t && (e.vpcName = t.vpcName, e.vpcCIDR = t.vpcCIDR);
                                    var i = e.regionId = k.regionStrToId(e.region);
                                    e.regionName = n[i]
                                };
                            t.resolve(function (t) {
                                var i,
                                    n,
                                    s,
                                    c,
                                    r = !1;
                                i = {
                                    appId: t.appId,
                                    uin: t.uin,
                                    vpcId: t.vpcId,
                                    unVpcId: t.unVpcId,
                                    region: t.region
                                },
                                n = {
                                    appId: t.peerAppId,
                                    uin: t.peerUin,
                                    vpcId: t.peerVpcId,
                                    unVpcId: t.peerUnVpcId,
                                    region: t.peerRegion
                                },
                                o(i),
                                o(n),
                                c = t.isOwner = "" + t.uin === a,
                                c || n.uin + "" !== a || (r = !0),
                                r || "" + i.regionId == "" + e || "" + n.regionId != "" + e || (r = !0),
                                r && (s = n, n = i, i = s);
                                var l = {};
                                return Object
                                    .keys(n)
                                    .forEach(function (e) {
                                        l["peer" + d.capitalize(e)] = n[e]
                                    }),
                                $.extend(t, i, l),
                                t.sameRegion = t.regionId === t.peerRegionId,
                                t.global = k.isGlobalLink(t.regionId, t.peerRegionId),
                                t.can = (c
                                    ? y
                                    : N)[t.state] || {},
                                t
                            })
                        }, t.reject)
                }),
            t
        },
        getUsedConnMap: function (e) {
            var t,
                i = {};
            return (e || []).forEach(function (e) {
                var n = [e.region, e.unVpcId].join("|"),
                    a = [e.peerRegion, e.peerUnVpcId].join("|");
                e.uin === e.peerUin && (t = i[n] || (i[n] = {}), t[a] = !0, t = i[a] || (i[a] = {}), t[n] = !0)
            }),
            i
        },
        getDynamicConstants: function () {
            var e = {},
                t = $.Deferred(),
                i = r
                    .getWhiteListMap()
                    .done(function (t) {
                        $.extend(e, t)
                    }),
                n = $.Deferred();
            return c.getComData(t.resolve, t.reject),
            t.done(function (t) {
                e.ownerUin = t.ownerUin
            }),
            $
                .when(t, i)
                .then(function () {
                    n.resolve(e)
                }, n.reject),
            n
        }
    }
}),
define("modules/vpc/conn/validator", function (e, t, i) {
    var n = e("../validator");
    i.exports = n.extend({
        peeringConnectionName: function (e) {
            return n._validName(e, "对等连接名称")
        },
        fromVpcId: n._notEmptyValidate("本端私有网络"),
        toVpcId: n._notEmptyValidate("对端私有网络"),
        toCreatorUin: function (e) {
            return e && e.trim()
                ? void 0
                : "请输入对端账号ID"
        },
        toCreatorVpcId: function (e) {
            return /^vpc-[0-9a-z]{8}$/.test(e)
                ? void 0
                : "请输入“vpc-xxxxxxxx”格式的私有网络ID"
        }
    })
}),
define("modules/vpc/createLimitsMixin", function (e, t, i) {
    var n = e("./cacher");
    i.exports = function (e) {
        return {
            $beforeInit: function () {
                var t = this;
                t.$watch("regionId", function (i) {
                    i && n
                        .getRawLimits(i)
                        .done(function (i) {
                            Object
                                .keys(e)
                                .forEach(function (n) {
                                    var a = e[n],
                                        s = i[a];
                                    s >= 0 && t.$set(n, s)
                                })
                        })
                }, !0)
            }
        }
    }
}),
define("modules/vpc/dc/add", function (e, t, i) {
    var n = e("dialog"),
        a = e("models/directconnect"),
        s = e("router"),
        d = e("qccomponent"),
        o = e("widget/location/location"),
        c = e("widget/baseInfo/validator"),
        r = e("./dcUtil"),
        l = (e("widget/pubsubhub/pubsubhub"), {
            main: '    <div class="tc-15-rich-dialog">        <div class="tc-15-list-wrap form appl' +
                    'y-line">    <form lpformnum="6" >        <div class="tc-15-list-content">       ' +
                    '     <ul>                <li>                  <em aria-required="false" class="' +
                    'tc-15-list-tit">专线名称</em>                  <span class="tc-15-list-det">        ' +
                    '            <div class="tc-15-input-text-wrap">                        <input na' +
                    'me="dcName" type="text" b-model="dc.dcName" data-validate="name" maxlength="25" ' +
                    'class="tc-15-input-text">                    </div>                  </span>    ' +
                    '            </li>                <li><em aria-required="false" class="tc-15-list' +
                    '-tit">接入方式</em>                    <span class="tc-15-list-det">                ' +
                    '        <span b-if="!lineList.length">                            单线接入          ' +
                    '              </span>                        <span b-if="lineList.length">      ' +
                    '                      <label class="tc-15-radio-wrap">                          ' +
                    '      <input type="radio" b-model="dc.model" value="0" name="model" class="tc-15' +
                    '-radio">单线接入</label>                            <label class="tc-15-radio-wrap">' +
                    '                                <input type="radio" b-model="dc.model" value="2"' +
                    ' name="model" class="tc-15-radio">双线热备</label>                        </span>   ' +
                    '                 </span>                </li>                <li b-if="lineList.' +
                    'length && dc.model == 2">                    <em aria-required="false" class="tc' +
                    '-15-list-tit">热备专线</em>                    <span class="tc-15-list-det">        ' +
                    '                <div class="tc-15-select-wrap l">                            <se' +
                    'lect class="tc-15-select" b-model="dc.relationId">                              ' +
                    '  <option value="{{line.dcId1}}" b-repeat="line in lineList">{{line.dcName1}}</o' +
                    'ption>                            </select>                            <div clas' +
                    's="tc-15-input-tips">提交后不能更改</div>                        </div>                ' +
                    '    </span>                </li>                <li><em aria-required="false" cl' +
                    'ass="tc-15-list-tit">您的IDC地址</em><span class="tc-15-list-det">                  ' +
                    '          <div class="tc-15-select-wrap m" data-location>                       ' +
                    '         <!-- 组件插入城市选择器 -->                            </div>                   ' +
                    '         <div class="tc-15-input-text-wrap address">                            ' +
                    '    <input type="text" data-validate="idcRegion" b-model="dc.idcRegion" placehol' +
                    'der="请输入您的机房详细地址" class="tc-15-input-text" maxlength="50">                      ' +
                    '          <div class="tc-15-input-tips">提交后不能更改</div>                           ' +
                    ' </div>                            </span>                </li>                <' +
                    'li><em aria-required="false" class="tc-15-list-tit">专线提供商</em><span class="tc-15' +
                    '-list-det">                            <div class="tc-15-select-wrap l">        ' +
                    '                        <select class="tc-15-select" b-model="dc.isp">          ' +
                    '                          <option value="中国电信">中国电信</option>                    ' +
                    '                <option value="中国联通">中国联通</option>                              ' +
                    '      <option value="中国移动">中国移动</option>                                </select' +
                    '>                                <div class="tc-15-input-tips">提交后不能更改</div>    ' +
                    '                        </div>                            </span>               ' +
                    ' </li>                <li class="adsl"><em aria-required="false" class="tc-15-li' +
                    'st-tit">带宽</em><span class="tc-15-list-det">                            <div cla' +
                    'ss="tc-15-select-wrap m">                                <select name="bandwidth' +
                    '" b-model="dc.bandwidth" class="tc-15-select">                                  ' +
                    '  <option b-repeat="bw in bandwidths" value="{{bw}}">{{bw}}M</option>           ' +
                    '                     </select>                            </div>                ' +
                    '            <span>bps</span>                            <div class="tc-15-input-' +
                    'tips">提交后不能更改</div>                            </span>                </li>     ' +
                    '           <li><em aria-required="false" class="tc-15-list-tit">建设模式</em><span c' +
                    'lass="tc-15-list-det">                                <label class="tc-15-radio-' +
                    'wrap">                                    <input type="radio" name="construct" b' +
                    '-model="dc.construct" value="0" class="tc-15-radio">腾讯云建设</label>               ' +
                    '         <label class="tc-15-radio-wrap">                            <input type' +
                    '="radio" name="construct" b-model="dc.construct" value="1" class="tc-15-radio">用' +
                    '户建设</label>                            </span>                </li>             ' +
                    '   <li style="display:none"><em aria-required="false" class="tc-15-list-tit">价格估' +
                    '算</em><span class="tc-15-list-det">                    {{getPrice(dc.bandwidth, ' +
                    'dc.construct, dc.innerCity)}}                            <div>                  ' +
                    '              <span b-if="dc.price" class="money-tip">{{dc.price}}<em> 元/年</em><' +
                    '/span>                                <span b-if="!dc.price" class="money-tip"> ' +
                    '- </span>                            </div>                            <div clas' +
                    's="tc-15-input-tips">此价格仅用作估算参考，不会进行扣费</div>                            </span> ' +
                    '               </li>                <li><em aria-required="false" class="tc-15-l' +
                    'ist-tit">联系人手机</em><span class="tc-15-list-det">                            <div' +
                    ' class="tc-15-input-text-wrap">                                <input data-valid' +
                    'ate="phone" type="text" b-model="dc.userPhone" class="tc-15-input-text">        ' +
                    '                    </div>                            </span>                </l' +
                    'i>                <li><em aria-required="false" class="tc-15-list-tit">联系人email<' +
                    '/em><span class="tc-15-list-det">                            <div class="tc-15-i' +
                    'nput-text-wrap">                                <input type="text" data-validate' +
                    '="email" b-model="dc.userMail" class="tc-15-input-text">                        ' +
                    '    </div>                            </span>                </li>            </' +
                    'ul>        </div>    </form>    </div>    </div>',
            provTpl: '    <select name="<%=names[0]%>" data-validate="idcCity0" class="tc-15-select" i' +
                    'd="locationProv">        <%for(var key in data){% >        <option value="<%=key' +
                    '%>" data-name="<%=data[key].name%>" <%if(actived[0] == key){% > selected <%}%> >' +
                    '<%=data[key].name%></option>        <%}%>    </select>    <select name="<%=names' +
                    '[1]%>" data-validate="idcCity1" class="tc-15-select" id="locationCity"></select>' +
                    '    <%if(isArea){% >    <select name="<%=names[2]%>" class="tc-15-select" id="lo' +
                    'cationArea" style="display:none"></select>    <%}%>'});i.exports={render:functio' +
                    'n(e){e=e||{};var t=this,i=new d(l.main,{$data:$.extend({dc:{innerCity:0,applyYea' +
                    'rs:1,construct:0,model:0,bandwidth:100},bandwidths:r.bandwidths},e),getPrice:fun' +
                    'ction(e,t,n){a.request({name:"getPrice",data:{goodsCategoryId:49,goodsDetail:{ti' +
                    'meSpan:12,timeUnit:"m",build:1==t?1:2,bandwidth:e,innerCity:n}},success:function' +
                    '(e){i.$set("dc.price",e[0].realTotalCostFull/100)}})}}),p=$(i.$el).find("form")[' +
                    '0];n.create(i.$el,530,"",{title:"申请专线",isMaskClickHide:!1,button:{"确定":function(' +
                    'e){var d=$.extend({},i.$data.dc,{idcCity:$(p.idcCity0).find("option:selected").a' +
                    'ttr("data-name")+$(p.idcCity1).find("option:selected").attr("data-name")}),o=new' +
                    ' c({btn:e[0],rules:{idcRegion:function(e){return e.length>50||!e?"请输入您的机房详细地址":v' +
                    'oid 0},idcCity0:function(e){return"0"==e?"请选择您的 IDC 机房所在省份":void 0},idcCity1:fun' +
                    'ction(e){return"0"==e?"请选择您的 IDC 机房所在城市":void 0}}});o.validateAll(i.$el)&&(e.add' +
                    'Class("disabled"),a.request({name:"applyDC",data:d,success:function(){t.destroy(' +
                    '),e.removeClass("disabled"),setTimeout(function(){n.create("您的专线申请已受理，我们将在 1 个工作' +
                    '日内与您取得联系，请保持手机、邮件畅通！",480,"",{title:"提示",defaultCancelBtn:!1,defaultCancelCb:fun' +
                    'ction(){s.navigate("/vpc/dcRecord")},button:{"确认":function(){s.navigate("/vpc/dc' +
                    'Record")}}})},100)},fail:function(){e.removeClass("disabled")}}))}}}),p.dcName.f' +
                    'ocus(),new o({renderTo:$(i.$el).find("[data-location]"),area:!1,actived:[0,0],na' +
                    'mes:["idcCity0","idcCity1"],provTpl:l.provTpl,change:function(){var e=$(p.idcCit' +
                    'y0).find("option:selected").attr("data-name"),t=$(p.idcCity1).find("option:selec' +
                    'ted").attr("data-name");i.$set("dc.innerCity",1*(r.arList.indexOf(e)>-1||r.arLis' +
                    't.indexOf(t)>-1))}}),a.request({name:"getApplyInfo",success:function(e){var t=e.' +
                    'filter(function(e){return 2!=e.status&&!e.dcId2});i.$set("lineList",t)}})},destr' +
                    'oy:function(){n.destroy()}}}),define("modules/vpc/dc/chart",function(e,t,i){e("H' +
                    'ighcharts");var n,a=e("../BeeBase"),s=e("appUtil"),d="id",o=864e5,c=e("manager")' +
                    ',r={inChart:{metricName:"intraffic",unit:"Mbps",isMbps:!0},outChart:{metricName:' +
                    '"outtraffic",unit:"Mbps",isMbps:!0},delayChart:{unit:"ms",type:"delay",metricNam' +
                    'e:"delay"},dropChart:{unit:"%",type:"lost",metricName:"pkgdrop"}},l={chart:{zoom' +
                    'Type:"x"},title:{text:""},yAxis:{title:{text:""},lineWidth:1,tickPixelInterval:4' +
                    '5,min:0,labels:{style:{fontSize:"12px"}}},plotOptions:{line:{marker:{enabled:!0,' +
                    'lineWidth:2,lineColor:"#ff7a22",fillColor:"#FFFFFF",radius:3,states:{hover:{enab' +
                    'led:!0}}},lineWidth:1,color:"#ff7a22"},spline:{marker:{enabled:!1,radius:3,state' +
                    's:{hover:{enabled:!0}}},lineWidth:1,color:"#ff7a22"}},credits:{enabled:!1},legen' +
                    'd:{enabled:!1},tooltip:{}},p=function(e){var t="0",i=e.getFullYear(),n=e.getMont' +
                    'h()+1,a=e.getDate();return[i,(10>n?t:"")+n,(10>a?t:"")+a].join("-")};n=a.extend(' +
                    '{$valuekey:d,$beforeInit:function(){var e=this;e.actionHandler=function(){e.hand' +
                    'leAction(this.name,this)},this.charts=this.charts||r;var t=new Date,i=new Date(t' +
                    '.getTime()-6*o),n=new Date(t.getTime()-14*o),s=new Date(t.getTime()-29*o);e.date' +
                    'PickerConfig={showCalendar:!0,lang:"zh-cn",tabs:[{label:"实时",from:p(t),to:p(t)},' +
                    '{label:"近7天",from:p(i),to:p(t)},{label:"近15天",from:p(n),to:p(t)},{label:"近30天",f' +
                    'rom:p(s),to:p(t)}],range:{min:p(s),max:p(t)},selected:{from:p(t),to:p(t)}},a.pro' +
                    'totype.$beforeInit.apply(this,arguments),e._promiseCache={}},$afterInit:function' +
                    '(){a.prototype.$afterInit.apply(this,arguments);var e=this,t=function(){e[d]&&!e' +
                    '.hidden&&e.loadData()},i=e.$refs.datePicker;i.setSelectedRange(i.selected.from,i' +
                    '.selected.to),i.$on("datepick",t),e.$watch("hidden",t),e.$watch(d,t,!0)},$tpl:' ' +
                    '   <div class="charts-panel" b-on="events" b-style="{display:hidden?\'none\':\'' +
                    '\'}">        <!--<style>-->            <!--.sidebar-panel .charts-panel .chart-w' +
                    'rap {-->                <!--top: 0;-->            <!--}-->        <!--</style>--' +
                    '>        <!-- 滚动条区域 开始 -->        <div class="physics-cont">            <span b-' +
                    'tag="qc-date-picker" b-ref="datePicker" b-with="datePickerConfig"></span>       ' +
                    ' </div>        <div class="chart-wrap">            <div class="tc-15-data-graph"' +
                    '>                <ul>                    <li class="line-partition">            ' +
                    '            <div class="tc-15-data-graph-title"><strong>进带宽</strong><em>(单位：Mbps' +
                    ')</em></div>                        <div class="tc-15-data-graph-info" b-tag="ch' +
                    'art" time-span="{{timeSpan}}" b-model="inChart"></div>                    </li> ' +
                    '                   <li class="line-partition">                        <div class' +
                    '="tc-15-data-graph-title"><strong>出带宽</strong><em>(单位：Mbps)</em></div>          ' +
                    '              <div class="tc-15-data-graph-info" b-tag="chart" time-span="{{time' +
                    'Span}}" b-model="outChart"></div>                    </li>                    <l' +
                    'i b-if="charts.dropChart" class="line-partition">                        <div cl' +
                    'ass="tc-15-data-graph-title"><strong>丢包率</strong><em>(单位：%)</em></div>          ' +
                    '              <div class="tc-15-data-graph-info" b-tag="chart" time-span="{{time' +
                    'Span}}" b-model="dropChart"></div>                    </li>                    <' +
                    'li b-if="charts.delayChart" class="line-partition">                        <div ' +
                    'class="tc-15-data-graph-title"><strong>延时</strong><em>(单位：ms)</em></div>        ' +
                    '                <div class="tc-15-data-graph-info" b-tag="chart" time-span="{{ti' +
                    'meSpan}}" b-model="delayChart"></div>                    </li>                </' +
                    'ul>            </div>        </div>        <!-- 滚动条区域 结束 -->    </div>',events:{' +
                    '"click [data-action]":function(e){var t=$(e.currentTarget);this.handleAction(t.a' +
                    'ttr("data-action"),null,e),e.preventDefault()}},getCacheKey:function(e){return t' +
                    'his.lineName+"-"+e.apiName},setRange:function(e,t){var i=this,n=s.cgwTimeParse(e' +
                    '+" 00:00:00"),a=s.cgwTimeParse(t+" 23:59:59"),d=Math.round((s.cgwTimeParse(t).ge' +
                    'tTime()-s.cgwTimeParse(e).getTime())/o);i.$set({inChart:"",outChart:"",timeSpan:' +
                    'd});var r="max",l=n.getTime()/1e3,p=a.getTime()/1e3,u=d?86400:300;Object.keys(i.' +
                    'charts).forEach(function(e){("delayChart"==e||"dropChart"==e)&&(r="avg");var t=i' +
                    '.charts[e],n={type:i.type,dimension:i.dimension,metric:t.metricName,period:u,sta' +
                    'rtTime:l,endTime:p,statistics:d?r:"sum"},a=$.param(n);i._promiseCache[a]=i._prom' +
                    'iseCache[a]||new Promise(function(e,t){c.queryBarad(i.regionId,n,function(t){i._' +
                    'promiseCache[a]=null,e(t)},function(e){i._promiseCache[a]=null,t(e)})}),i._promi' +
                    'seCache[a].then(function(n){for(var a=!1,s={y:[],x:[]},d=0,o=n.length;o>d;d++)n[' +
                    'd]&&t.isMbps?s.y[d]=1*(n[d]*(i.traffic?.008/300:.001)).toFixed(3):s.y[d]=n[d],(s' +
                    '.y[d]||0===s.y[d])&&(s.x=[1e3*l,1e3*p,u],a=!0);a||(s={y:[]}),s.unit=t.unit,i.$se' +
                    't(e,s)},function(){i.$set(e,{y:[]})})})},handleAction:function(e,t,i){var n=this' +
                    ';switch(e){case"edit":break;case"return":n.$set(d,"")}},loadData:function(){var ' +
                    'e=this.$refs.datePicker.selected;this.setRange(e.from,e.to)}});var u=a.extend({$' +
                    'tpl:'    <div class="tc-15-data-graph-info" time-span="{{timeSpan}}" style="posi' +
                    'tion:relative;">        <div data-mod="chart" b-if="!noData"></div><span b-if="!' +
                    'data || noData" style="position:absolute;top:50%;left:50%;margin-top:-12px;margi' +
                    'n-left:-24px;">            {{data ? \'没有数据\' : \'加载中...\'}}        </span>    </' +
                    'div>',$valuekey:"data",$afterInit:function(){a.prototype.$afterInit.apply(this,a' +
                    'rguments);var e=this,t=function(){if(!e.data){var t=e.$chart.highcharts();return' +
                    ' void(t&&t.destroy())}e.showChart()};e.$chart=$(e.$el).find('[data-mod="chart"]'' +
                    '),e.$watch("data",t,!0)},showChart:function(){var e,t=this,i=t.data,n=parseInt(t' +
                    '.timeSpan,10)||0,a=!0,d=t.$chart,o={xAxis:{type:"datetime",dateTimeLabelFormats:' +
                    '{hour:"%H:%M"},tickPixelInterval:80,title:{text:""}}};i.y.length&&($.extend(o,l)' +
                    ',e=new Date(i.x[0]),o.series=[{pointStart:e.getTime()-6e4*e.getTimezoneOffset(),' +
                    'data:i.y}],1*n?(o.series[0].pointInterval=864e5,o.xAxis.dateTimeLabelFormats.day' +
                    '="%m-%d",o.xAxis.dateTimeLabelFormats.week="%m-%d",o.tooltip.formatter=function(' +
                    '){var e=new Date(this.x);return e=new Date(e.getTime()+6e4*e.getTimezoneOffset()' +
                    '),[s.formatDate(e,"MM-dd")," ","<b>",this.y,i.unit,"</b>"].join("")}):(o.series[' +
                    '0].pointInterval=3e5,o.series[0].type="spline",o.xAxis.dateTimeLabelFormats.day=' +
                    '"%H:%M",o.xAxis.maxZoom=36e5,o.tooltip.formatter=function(){var e=new Date(this.' +
                    'x);return e=new Date(e.getTime()+6e4*e.getTimezoneOffset()),[s.formatDate(e,"hh:' +
                    'mm")," ","<b>",this.y,i.unit,"</b>"].join("")}),d.highcharts(o),a=!1),this.$set(' +
                    '"noData",a)}});n.tag("chart",u),i.exports=n}),define("modules/vpc/dc/dc",functio' +
                    'n(e,t,i){var n=e("pageManager"),a=e("constants"),s=e("manager"),d=e("dialog"),o=' +
                    'e("panel"),c=e("models/directconnect"),r=e("qccomponent"),l=r.getComponent("grid' +
                    '-view"),p=e("widget/pubsubhub/pubsubhub"),u=e("./detail"),v=e("./add"),f=e("./ch' +
                    'art"),m=e("./dcUtil"),h=e("widget/fieldsManager/gridView"),b=e("./deal/deal.js")' +
                    ',g={main:'    <div class="manage-area">        <div class="manage-area-title">  ' +
                    '          <h2>物理专线</h2>        </div>        <div class="tc-15-action-panel">   ' +
                    '         <button class="tc-15-btn m" data-title?="overSize && \'该 VPC 下的网络 ACL 已' +
                    '达到数量上限\'" b-on-click="add()">+新建</button>            <!--<button class="tc-15-bt' +
                    'n weak m download"></button>-->            <button  b-on-click="fieldsetting()" ' +
                    'class="tc-15-btn weak m setting"></button>            <qc-search  search="{{sear' +
                    'ch.bind(this)}}" placeholder="请输入专线名称或 ID"></qc-search>        </div>        <ph' +
                    'ysical-grid b-ref="grid" vpc-id="{{vpcId}}"></physical-grid>    </div>',idTdTpl:' +
                    ''    <p><a data-event="nav" href="/vpc/dc/detail/{{item.dcId}}">{{item.dcId}}</a' +
                    '></p>    <p><span class="text-overflow m-width">{{item.dcName}}</span></p>',pane' +
                    'l:'    <div>        <div class="sidebar-panel-hd">            <h3 class="run-in"' +
                    '>{{dc.dcName}}监控</h3>        </div>        <div class="sidebar-panel-bd" style="' +
                    'bottom: 40px;">            {{> chart }}        </div>    </div>'
},I=r.extend({a' +
                    'dd:function(){s.isPrepaid().then(function(){v.render()},function(){d.create('   ' +
                    ' <p>        当前为后付费月结的购买方式，需要切换到预付费购买方式才能购买专线，查看        <a target="_blank" href="' +
                    'https://www.qcloud.com/doc/product/213/5041">详细说明</a>    </p>',"500","",{title:"' +
                    '提示",button:{"切换到预付费":function(){location.href="http://manage.qcloud.com/shopping' +
                    'cart/changeguide.php"}}})})},fieldsetting:function(){this.$refs.grid.showFieldsM' +
                    'anager()},search:function(e){this.$refs.grid.getData({searchKey:e})}});I.tag("ph' +
                    'ysical-grid",l.extend({$mixins:[h("dc_phy_grid_fields")],getData:function(e,t){v' +
                    'ar i=this;t=function(e,t){e||i.setData(t)},c.request({name:"getDCInfo",success:f' +
                    'unction(i){var n=i.filter(function(t){if(m.fixParam(t),e.searchKey){var i=new Re' +
                    'gExp(e.searchKey);if(!i.test(t.dcName)&&!i.test(t.dcId))return}return e.modelFil' +
                    'ter?e.modelFilter.indexOf(t.model+"")>-1:!0});t(null,{list:n,totalNum:n.length,s' +
                    'earchKey:e.searchKey})}})},renew:function(e){b.render(e)},showMonitor:function(e' +
                    '){o.show({module:{render:function(t,i){var n=new r(g.panel,{chart:new f({id:e.dc' +
                    'Id,lineName:e.lineName,type:"dc_line",dimension:[{directconnectid:e.dcId,directc' +
                    'onnectname:e.lineName}],charts:{inChart:{metricName:"intraffic",unit:"Mbps",isMb' +
                    'ps:!0},outChart:{metricName:"outtraffic",unit:"Mbps",isMbps:!0}}}).$el,$data:{dc' +
                    ':e}});t.html(n.$el)},destroy:function(){}},data:{}})},filterModel:function(e){th' +
                    'is.getData({modelFilter:e})}},{defaults:{hasFirst:!1,modelFilter:[{label:"单线接入",' +
                    'value:"0"},{label:"双线热备",value:"2"}],colums:[{name:"ID/名称",key:"dcId",tdTpl:g.id' +
                    'TdTpl,required:!0},{name:"监控",key:"_monitor",width:50,minWidth:50,tdTpl:'<i clas' +
                    's="dosage-icon" b-on-click="showMonitor(item)" data-title="查看监控"></i>'},{name:"I' +
                    'DC 所在地",key:"idcRegion"},{name:"专线提供商",key:"isp",width:100},{name:"专线状态",key:"_s' +
                    'tatus",tdTpl:'    <span class="text-overflow {{item._statusClass}}">        {{it' +
                    'em._status}}    </span>'},{name:"建设模式",key:"_construct"},{name:"接入方式",key:"model' +
                    '",tdTpl:'    <span class="text-overflow">        {{item.model == 0 ? \'单线接入\' : ' +
                    '\'双线热备\'}}        <a b-if="item.model != 0" data-event="nav" href="/vpc/dc/detai' +
                    'l/{{item.dcId2}}">{{item.dcId2}}</a>    </span>',thTpl:'    <grid-view-header-fi' +
                    'lter col="{{col}}" filter-options="{{modelFilter}}" change="{{filterModel.bind(t' +
                    'his.$root)}}"></grid-view-header-filter>'},{name:"带宽",key:"_bandwidth"},{name:"类' +
                    '型",key:"dcType",hide:!0},{name:"到期时间",key:"endTime"},{name:"操作",key:"_action",re' +
                    'quired:!0,tdTpl:'    <a href="javascript:;" b-on-click="renew(item)" class="link' +
                    's">续费</a>'}]}})),i.exports={render:function(t,i,s){var d=this;if("detail"===t&&i' +
                    ')d.isDetail=!0,u.render(i,s);else{var o=d.bee=new I(g.main,{$data:{maxDcSize:a.M' +
                    'AX_DC_SIZE}});this.grid=o.$refs.grid,n.appArea.html(o.$el),this.grid.setMaxHeigh' +
                    't(),d.isDetail=!1,e("../adTip").init(o.$el)}},destroy:function(){this.isDetail&&' +
                    'u.destroy(),p.off(m.events.DCCHANGED)}}}),define("modules/vpc/dc/dcUtil",functio' +
                    'n(e,t,i){var n={0:"已完成",1:"申请中",2:"申请驳回",3:"待付款",4:"实施中",5:"实施中",6:"删除作废"},a={0:' +
                    '"运行中",1:"已欠费",2:"删除中"},s={0:"已连接",1:"申请中",2:"配置中",3:"修改中",4:"删除中",5:"作废"},d={0:"' +
                    '关闭",1:"IP映射",2:"IP端口映射"},o=[2,4,6,8,10,20,30,40,50,60,70,80,90,100],c=["上海","北京"' +
                    ',"天津","深圳","广州"],r={DCCHANGED:"dcChanged",DCRECORDCHANGED:"dcRecordChanged",DCGW' +
                    'CHANGED:"dcGwChanged",DCCONNCHANGED:"dcConnChanged"},l={bgpAsn:function(e){var t' +
                    '=1*e;return!e||/\d+/.test(e)&&(t>=1&&64511>=t||t>=65100&&65299>=t)?void 0:"请输入有效' +
                    '的 ASN (公有:1-64511, 私有:65100-65299)"},bgpKey:function(e){var t,i;return i=e.repla' +
                    'ce(/\d/g,"").replace(/[a-z]/gi,"").replace(/[!@#$%^&*()]/g,""),(!e||e.length<=12' +
                    '8&&!i)&&(t=!0),t?void 0:"请输入正确的 BGP 密钥"}},p=function(e){e._construct=1==e.constr' +
                    'uct?"用户建设":"腾讯云建设",e._bandwidth=e.bandwidth+"Mbps",e._operatorPhone=e.operatorPh' +
                    'one?e.operatorPhone:"-";var t="中国移动"===e.isp?"30":"16";if(e.applyId)switch(e._st' +
                    'atus=n[e.status],e._applyTime=e.applyTime.split(/\s+/)[0],1*e.status){case 0:e._' +
                    'completeTime=e.completeTime&&e.completeTime.split(/\s+/)[0],e._renew=!0;break;ca' +
                    'se 1:e._completeTime="预计"+t+"个工作日",e._canDel=!0;break;case 2:e._completeTime="-"' +
                    ',e._canDel=!0;break;case 3:e._completeTime="预计"+t+"个工作日",e._canDel=!0;break;case' +
                    ' 4:e._completeTime="预计"+t+"个工作日";break;case 5:e.completeTime&&(e._completeTime="' +
                    '预计于 "+e.completeTime.split(/\s+/)[0]+" 完成")}else switch(e._status=a[e.status],1*' +
                    'e.status){case 0:e._statusClass="succeed",e._renew=!0;break;case 1:e._renew=!0;c' +
                    'ase 2:e._statusClass="error"}};i.exports={events:r,fixParam:p,dcConnStatus:s,arL' +
                    'ist:c,bandwidths:o,rules:l,dcGwNatTypes:d}}),define("modules/vpc/dc/deal/deal.js' +
                    '",function(e,t,i){var n=e("dialog"),a=e("models/directconnect"),s=e("qccomponent' +
                    '"),d=e("widget/pills/pills"),o=e("../dcUtil"),c=e("widget/pubsubhub/pubsubhub");' +
                    'i.exports={render:function(e){var t=e._renew,i=t?50:49,r=this,l=new s('    <div ' +
                    'class="tc-15-rich-dialog">        <div class="tc-15-list-wrap">        <p class=' +
                    '"tc-15-msg" b-if="!isRenew">注：部分内容和专线经理协商后有修改，请确认</p>        <div class="tc-15-l' +
                    'ist-wrap form apply-line">            <form lpformnum="5">                <div c' +
                    'lass="tc-15-list-content">                    <ul>                        <li>  ' +
                    '                          <em aria-required="false" class="tc-15-list-tit">专线名称<' +
                    '/em>                            <span class="tc-15-list-det">                   ' +
                    '             {{dc.dcName1 || dc.dcName}}                            </span>     ' +
                    '                   </li>                        <li>                            ' +
                    '<em aria-required="false" class="tc-15-list-tit">专线 ID</em>                     ' +
                    '       <span class="tc-15-list-det">                                <a href="/vp' +
                    'c/dcRecord/detail/{{dc.dcId1 || dc.dcId}}" data-event="nav" class="link-name">{{' +
                    'dc.dcId1 || dc.dcId}}</a>                            </span>                    ' +
                    '    </li>                        <div b-template b-if="!isRenew">               ' +
                    '             <li><em aria-required="false" class="tc-15-list-tit">接入方式</em>     ' +
                    '                           <span class="tc-15-list-det">                        ' +
                    '            {{dc.model == 0 ? \'单线接入\' : \'双线热备\'}}                             ' +
                    '       <a b-if="item.model != 0" data-event="nav" href="/vpc/dcRecord/detail/{{d' +
                    'c.dcId2}}">{{dc.dcId2}}</a>                                </span>              ' +
                    '              </li>                            <li><em aria-required="false" cla' +
                    'ss="tc-15-list-tit">您的IDC地址</em>                                <span class="tc-' +
                    '15-list-det">{{dc.idcCity}} {{dc.idcRegion}}</span>                            <' +
                    '/li>                            <li><em aria-required="false" class="tc-15-list-' +
                    'tit">专线提供商</em>                                <span class="tc-15-list-det">{{dc' +
                    '.isp}}</span>                            </li>                            <li cl' +
                    'ass="adsl">                                <em aria-required="false" class="tc-1' +
                    '5-list-tit">带宽</em>                                <span class="tc-15-list-det">' +
                    '{{dc.bandwidth}}Mbps</span>                            </li>                    ' +
                    '        <li><em aria-required="false" class="tc-15-list-tit">类型</em>            ' +
                    '                    <span class="text-style">{{dc.dcType}}</span>               ' +
                    '             </li>                            <li>                              ' +
                    '  <em aria-required="false" class="tc-15-list-tit">建设模式</em>                    ' +
                    '            <span class="tc-15-list-det">{{dc.construct == 1 ? \'用户建设\' : \'腾讯云建' +
                    '设\'}}</span>                            </li>                        </div>     ' +
                    '                   <li>                            <em aria-required="false" cla' +
                    'ss="tc-15-list-tit">单价</em>                            {{getPrice(timeSpan)}}   ' +
                    '                         <span class="tc-15-list-det">{{price}} <span b-if="!pri' +
                    'ce">-</span> 元/年</span>                        </li>                        <li>' +
                    '                            <em aria-required="false" class="tc-15-list-tit">{{d' +
                    'c._renew ? \'续费\' : \'购买\'}}时长</em>                            <span data-timesp' +
                    'an>                        <!--<div role="radiogroup" class="tc-15-rich-radio">-' +
                    '->                            <!--<button aria-checked="true" data-value="1" rol' +
                    'e="radio" class="tc-15-btn first checked m">1年-->                            <!-' +
                    '-</button>-->                            <!--<button aria-checked="true" data-va' +
                    'lue="2" role="radio" class="tc-15-btn m">2年-->                            <!--</' +
                    'button>-->                            <!--<button aria-checked="false" data-valu' +
                    'e="3" role="radio" class="tc-15-btn last  m">3年-->                            <!' +
                    '--</button>-->                        <!--</div>-->                        </spa' +
                    'n>                        </li>                        <li>                     ' +
                    '       <em aria-required="false" class="tc-15-list-tit">总费用</em>                ' +
                    '            <span class="tc-15-list-det">                                <div>  ' +
                    '                                  <span class="money-tip" b-if="!totalCost">-</s' +
                    'pan>                                    <span class="money-tip" b-if="totalCost"' +
                    '>{{totalCost}}<em>元</em></span>                                </div>           ' +
                    '                     <div class="tc-15-input-tips" b-if="!isRenew && dc.construc' +
                    't == 1" style="line-height: 21px;">                                    用户建设专线需一次' +
                    '性缴纳初装费15000元，计入初次购买订单， <br />                                    后面专线续费无需重复缴纳此部分' +
                    '费用。                                </div>                            </span>    ' +
                    '                    </li>                    </ul>                </div>        ' +
                    '    </form>        </div>    </div>    </div>',{$data:{timeSpan:12,isRenew:t,dc:' +
                    'e},getPrice:function(t){a.request({name:"getPrice",data:{goodsCategoryId:i,goods' +
                    'Detail:{timeSpan:t||12,timeUnit:"m",build:1==e.construct?1:2,bandwidth:e.bandwid' +
                    'th,innerCity:e.innerCity}},success:function(e){l.$set({price:(e[0].price/100*12)' +
                    '.toFixed(2),totalCost:(e[0].totalCost/100).toFixed(2)})}})}});new d({renderTo:$(' +
                    'l.$el).find("[data-timespan]"),data:[{name:"1年",value:12,active:!0},{name:"2年",v' +
                    'alue:24},{name:"3年",value:36}],change:function(){l.$set("timeSpan",this.val())}}' +
                    '),n.create(l.$el,580,"",{title:"确认订单",preventResubmit:!0,button:{"确认":function()' +
                    '{var s={goodsCategoryId:i,goodsDetail:{timeSpan:l.timeSpan,timeUnit:"m",dcId:e.d' +
                    'cId||e.dcId1,build:1==e.construct?1:2,bandwidth:e.bandwidth,innerCity:e.innerCit' +
                    'y,idcCity:e.idcCity,isp:e.isp,model:e.model}};e.model>0&&e.dcId2&&(s.goodsDetail' +
                    '.relationId=e.dcId2),t&&(s.goodsDetail.endTime=e.endTime),a.request({name:"payBy' +
                    'Cart",data:s,success:function(i){var s=i.dealIds[0];Promise.all([new Promise(fun' +
                    'ction(i,n){t?i():a.request({name:"updateDCApplyInfo",data:{applyId:e.applyId,dea' +
                    'lId:s},success:i,fail:n})}),new Promise(function(e,t){a.request({name:"getDealsB' +
                    'yCond",data:{dealIds:[s]},success:e,fail:t})})]).then(function(e){var t=e[1].dea' +
                    'ls[0].dealId;c.trigger(o.events.DCRECORDCHANGED),c.trigger(o.events.DCCHANGED),n' +
                    '.create('    <div class="coin-alert">        <table class="ui-popmsg"><tbody><tr' +
                    '>            <td height="72" class="i">                <i class="ico ico-confirm' +
                    ' mr10"></i>            </td>            <td height="72" class="t">              ' +
                    '  <span class="info">                    <span class="tit">下单成功, 是否立即去支付?</span>' +
                    '<br>                </span>            </td>        </tr></tbody></table>    </d' +
                    'iv>',480,"",{title:"下单成功",button:{"去付款":function(){window.open("http://manage.qc' +
                    'loud.com/deal/dealsConfirm.php?payMode=1&dealList[]="+t),r.destroy()}}})})}})}}}' +
                    ')},destroy:function(){n.hide()}}}),define("modules/vpc/dc/detail",function(e,t,i' +
                    '){var n=e("pageManager"),a=(e("constants"),e("widget/detail/detail")),s=(e("appU' +
                    'til"),e("router"),e("./dcUtil")),d=e("models/directconnect"),o=e("widget/pubsubh' +
                    'ub/pubsubhub"),c=e("./chart"),r=e("widget/baseInfo/baseInfo"),l=a.extend({backUr' +
                    'l:"/vpc/dc",$beforeInit:function(){l.__super__.$beforeInit.call(this),this._tabs' +
                    '=[]},$afterInit:function(){var e=this;l.__super__.$afterInit.call(this),e.load()' +
                    ',this.bindEvent()},$beforeDestroy:function(){this._tabs.forEach(function(e){e.$d' +
                    'estroy(!0)}),o.off(".detail")},load:function(){var e=this;this.getDCInfo(this.dc' +
                    'Id).then(function(t){e.$set({dc:t,title:t.dcName})})},loadTabs:function(e){var t' +
                    '=this.$refs.tabs,i=this._tabs[e];if(!i){switch(e){case 0:i=new r({detailTpl:'   ' +
                    ' <div class="tc-15-list-content">        <ul>            <li><em class="tc-15-li' +
                    'st-tit">名称</em>                <span b-if="!isEdit" class="tc-15-list-det">{{inf' +
                    'o.dcName}}</span>                <div class="tc-15-input-text-wrap" b-if="isEdit' +
                    '">                    <input type="text" data-validate="name" class="tc-15-input' +
                    '-text" b-model="info.dcName" maxlength="25">                    <div class="tc-1' +
                    '5-input-tips">还能输入{{25 - info.dcName.length}}个字符，支持中文、英文、数字、下划线、分隔符"-"、小数点</div>' +
                    '                </div>            </li>            <li><em class="tc-15-list-tit' +
                    '">ID</em><span class="tc-15-list-det">{{info.dcId}}</span>            </li>     ' +
                    '       <li><em class="tc-15-list-tit">申请状态</em><span class="tc-15-list-det">{{in' +
                    'fo._status}}</span>            </li>            <li><em class="tc-15-list-tit">接' +
                    '入方式</em>                <span class="tc-15-list-det">                    {{info.' +
                    'model == 0 ? \'单线接入\' : \'双线热备\'}}                    <a b-if="info.model != 0" ' +
                    'data-event="nav" href="/vpc/dcRecord/detail/{{info.dcId2}}">{{info.dcId2}}</a>  ' +
                    '              </span>            </li>            <li><em class="tc-15-list-tit"' +
                    '>专线提供商</em><span class="tc-15-list-det">{{info.isp}}</span>            </li>    ' +
                    '        <li><em class="tc-15-list-tit">建设模式</em><span class="tc-15-list-det">{{i' +
                    'nfo._construct}}</span>            </li>            <li><em class="tc-15-list-ti' +
                    't">带宽</em><span class="tc-15-list-det">{{info._bandwidth}}</span>            </l' +
                    'i>            <li><em class="tc-15-list-tit">类型</em><span class="tc-15-list-det"' +
                    '>{{info.dcType}}</span>            </li>            <li><em class="tc-15-list-ti' +
                    't">IDC所在地</em><span class="tc-15-list-det">{{info.idcCity}} {{info.idcRegion}}</' +
                    'span>            </li>            <li b-if="info._applyTime">                <em' +
                    ' class="tc-15-list-tit">申请时间</em><span class="tc-15-list-det">{{info._applyTime}' +
                    '}</span>            </li>            <li b-if="info._completeTime">             ' +
                    '   <em class="tc-15-list-tit">完成时间</em><span class="tc-15-list-det">{{info._comp' +
                    'leteTime}}</span>            </li>            <li b-if="info.endTime">          ' +
                    '      <em class="tc-15-list-tit">到期时间</em><span class="tc-15-list-det">{{info.en' +
                    'dTime}}</span>            </li>        </ul>    </div>',$data:{id:this.dcId,chan' +
                    'geEvent:s.events.DCCHANGED},getData:this.getDCInfo,updateData:function(e){return' +
                    ' new Promise(function(t,i){d.request({name:"updateDCInfo",data:e,success:functio' +
                    'n(e){o.trigger(s.events.DCCHANGED),t(e)},fail:i})})}});break;case 1:var n=this.d' +
                    'c;i=new c({id:this.dc.dcId,lineName:this.dc.lineName,type:"dc_line",dimension:[{' +
                    'directconnectid:n.dcId,directconnectname:n.lineName}],charts:{inChart:{metricNam' +
                    'e:"intraffic",unit:"Mbps",isMbps:!0},outChart:{metricName:"outtraffic",unit:"Mbp' +
                    's",isMbps:!0}}})}this._tabs[e]||(this._tabs[e]=i),t.list.$set(e,{content:i.$el})' +
                    '}},getDCInfo:function(){var e;return function(t){return e||(e=new Promise(functi' +
                    'on(e,i){d.request({name:"getDCInfo",data:{dcId:t},success:e,fail:i})}).then(func' +
                    'tion(t){var i=t[0];return s.fixParam(i),e=null,i||n.render404(),i},function(t){t' +
                    'hrow e=null,t})),e}}(),bindEvent:function(){}},{defaults:{tabData:{list:[{label:' +
                    '"基本信息"},{label:"监控"}]}}});i.exports={render:function(e,t){var i=new l($.extend({' +
                    'dcId:e},t));n.appArea.html(i.$el),o.on(s.events.DCCHANGED,function(){i.load()})}' +
                    ',destroy:function(){o.off(s.events.DCCHANGED)}}}),define("modules/vpc/dcConn/add' +
                    '",function(e,t,i){var n=e("dialog"),a=e("manager"),s=e("models/directconnect"),d' +
                    '=(e("router"),e("constants")),o=(e("../cApi"),e("qccomponent")),c=e("widget/base' +
                    'Info/validator"),r=e("../dc/dcUtil"),l=(e("appUtil"),e("../util")),p=e("../simpl' +
                    'e-vpc-selector"),u=e("../simple-region-selector"),v=e("widget/pubsubhub/pubsubhu' +
                    'b"),f=e("../cacher"),m=o.extend({$afterInit:function(){this.$watch("vif.vpcId",f' +
                    'unction(e){this.vif.regionId&&this.describeDirectConnectGateway(e)});var e=this,' +
                    't="VPC_DC_NAT",i=function(){e.$set("whiteListLoaded",!0)};a.getAllWhiteList(func' +
                    'tion(n){n&&n[t]&&n[t].length&&e.$set("inNatWhiteList",!0),i()},i),l.beeBind(e,e,' +
                    '"NAT_REGIONS[vif.regionId] && inNatWhiteList","displayNat")},describeDirectConne' +
                    'ctGateway:function(){var e;return function(t){var i=this;return e=e||new Promise' +
                    '(function(n,a){s.request({name:"describeDirectConnectGateway",data:{vpcId:t,regi' +
                    'onId:i.vif.regionId},success:function(t){var a=t&&t.accepterVpcSet&&t.accepterVp' +
                    'cSet[0]&&t.accepterVpcSet[0].directConnectGatewayName;i.$set({loaded:!0,_dcGwNam' +
                    'e:a||"",accepterVpcSet:t.accepterVpcSet||[]}),n(a),e=null},fail:function(t){e=nu' +
                    'll,i.$set("loaded",!0),a(t)}})})}}()},{defaults:{enableNat:"1"}});m.tag("vpc-sel' +
                    'ect",p),m.tag("region-selector",u.extend({getWhiteList:function(){var e=d.DC_REG' +
                    'ION;return f.getRegionIdList().then(function(t){return t.filter(function(t){retu' +
                    'rn e&&e.length?e.indexOf(t)>-1:t})})}})),i.exports={render:function(e){e=e||{};v' +
                    'ar t=this,i={};d.DC_NAT_REGION.forEach(function(e){i[e]=!0});var a=new m('    <d' +
                    'iv class="tc-15-rich-dialog">        <div class="tc-15-list-wrap form apply-line' +
                    '">            <form action="javascript:;">                <span class="tc-15-lis' +
                    't-content">                    <ul>                        <li><em aria-required' +
                    '="false" class="tc-15-list-tit">通道名称</em><span class="tc-15-list-det">          ' +
                    '                  <div class="tc-15-input-text-wrap">                           ' +
                    '     <input name="dcName" type="text" b-model="vif.vifName" maxlength="25" data-' +
                    'validate="name" class="tc-15-input-text">                            </div>     ' +
                    '                       </span>                        </li>                     ' +
                    '   <li>                            <em aria-required="false" class="tc-15-list-t' +
                    'it">物理专线</em>                            <span class="tc-15-list-det">          ' +
                    '                      <div class="tc-15-select-wrap l">                         ' +
                    '           <select class="tc-15-select" b-model="vif.dcId">                     ' +
                    '                   <option value="{{line.dcId}}" b-repeat="line in lineList">{{l' +
                    'ine.dcName}}</option>                                    </select>              ' +
                    '                  </div>                            </span>                     ' +
                    '   </li>                        <li>                            <em aria-require' +
                    'd="false" class="tc-15-list-tit">私有网络</em>                            <span clas' +
                    's="tc-15-list-det">                                <div style="overflow: hidden"' +
                    '>                                    <region-selector b-model="vif.regionId" sty' +
                    'le="max-width: 150px;"></region-selector>                                    <vp' +
                    'c-select region-id="{{vif.regionId}}" b-model="vif.vpcId" style="max-width: 180p' +
                    'x;"></vpc-select>                                </div>                         ' +
                    '       <div class="tc-15-input-tips">                                    物理专线与私有' +
                    '网络间只能创建一个专线通道                                </div>                            <' +
                    '/span>                        </li>                        <li>                 ' +
                    '           <em aria-required="false" class="tc-15-list-tit">专线网关</em>           ' +
                    '                 <span class="tc-15-list-det" b-style="{display: loaded ? \'\': ' +
                    '\'none\'}">                                <span b-if="accepterVpcSet.length">  ' +
                    '                                  <select data-dc class="tc-15-select m" b-model' +
                    '="directConnectGatewayId">                                        <option b-repe' +
                    'at="item in accepterVpcSet" value="{{item.directConnectGatewayId}}">            ' +
                    '                                {{item.directConnectGatewayName}} ({{item.direct' +
                    'ConnectGatewayId}})                                        </option>            ' +
                    '                        </select>                                </span>        ' +
                    '                        <span b-if="!accepterVpcSet.length">                    ' +
                    '                <span>无可用专线网关</span>                                    <a href=' +
                    '"/vpc/dcGw?add=true" data-add target="_blank">立即创建</a>                          ' +
                    '      </span>                            </span>                        </li>   ' +
                    '                     <li><em aria-required="false" class="tc-15-list-tit">路由方式</' +
                    'em><span class="tc-15-list-det">                                <label class="tc' +
                    '-15-radio-wrap">                                    <input type="radio" name="co' +
                    'nstruct" b-model="vif.routeType" value="0" class="tc-15-radio">BGP 路由</label>   ' +
                    '                     <label class="tc-15-radio-wrap">                           ' +
                    ' <input type="radio" name="construct" b-model="vif.routeType" value="1" class="t' +
                    'c-15-radio">静态路由</label>                            </span>                     ' +
                    '   </li>                        <div b-template b-if="vif.routeType == 0">      ' +
                    '                      <li>                                <em aria-required="fal' +
                    'se" class="tc-15-list-tit">BGP ASN</em><span class="tc-15-list-det">            ' +
                    '                <div class="tc-15-input-text-wrap">                             ' +
                    '   <input type="text" data-validate="bgpAsn" b-model="vif.bgpAsn" class="tc-15-i' +
                    'nput-text">                            </div>                            </span>' +
                    '                            </li>                            <li>               ' +
                    '                 <em aria-required="false" class="tc-15-list-tit">BGP 密钥</em><sp' +
                    'an class="tc-15-list-det">                            <div class="tc-15-input-te' +
                    'xt-wrap">                                <input type="text" data-validate="bgpKe' +
                    'y" b-model="vif.bgpKey" class="tc-15-input-text">                            </d' +
                    'iv>                            </span>                            </li>         ' +
                    '               </div>                        <li>                            注: ' +
                    '我们将自动为您配置 Vlan ID 和 Peer Ip, 如有疑问请与专线经理联系                        </li>          ' +
                    '          </ul>                </div>            </form>        </div>    </div>' +
                    '',{$data:$.extend({vif:{routeType:"0"},NAT_REGIONS:i},e)}),o=$(a.$el).find("form' +
                    '")[0];n.create(a.$el,530,"",{title:"创建专线通道",isMaskClickHide:!1,button:{"确定":func' +
                    'tion(e){var i=$.extend({},a.$data.vif,{region:d.REGIONMAP[a.vif.regionId],vpcIns' +
                    'tanceId:$(a.$el).find("[data-vpc-selector]").find("option:selected").attr("data-' +
                    'uvpcid")}),n=new c({btn:e[0],rules:r.rules});if(!e.hasClass("disable")&&n.valida' +
                    'teAll(a.$el)){if(!a.accepterVpcSet.length)return void n.showErr($(a.$el).find("[' +
                    'data-add]"),"无可用专线网关, 请先创建一个");i.directConnectGatewayId=$(a.$el).find("[data-dc]' +
                    '").val(),e.addClass("disable"),new Promise(function(e,t){s.request({name:"create' +
                    'VIF",data:i,success:e,fail:t})}).then(function(){t.destroy(),e.removeClass("disa' +
                    'ble"),v.trigger(r.events.DCCONNCHANGED)},function(){e.removeClass("disable")})}}' +
                    '}}),o.dcName.focus()},destroy:function(){n.hide()}}}),define("modules/vpc/dcConn' +
                    '/dcConn",function(e,t,i){var n=e("pageManager"),a=e("constants"),s=e("panel"),d=' +
                    'e("models/directconnect"),o=e("qccomponent"),c=o.getComponent("grid-view"),r=e("' +
                    'widget/pubsubhub/pubsubhub"),l=e("./detail"),p=e("./add"),u=e("../dc/chart"),v=e' +
                    '("../dc/dcUtil"),f=e("widget/fieldsManager/gridView"),m=o.extend({$afterInit:fun' +
                    'ction(){var e=this;d.request({name:"getDCInfo",success:function(t){var i=t.filte' +
                    'r(function(e){return 0==e.status});e.$set("lineList",i)}})},add:function(){var e' +
                    '=this.$refs.grid;this.lineList&&this.lineList.length&&e&&e.list&&p.render({lineL' +
                    'ist:this.lineList,dcConnList:e.list})},fieldsetting:function(){this.$refs.grid.s' +
                    'howFieldsManager()},search:function(e){var t=this.$refs.grid;t.getData({searchKe' +
                    'y:e})}});m.tag("dcrecord-grid",c.extend({$mixins:[f("dc_conn_grid_fields")],getD' +
                    'ata:function(e,t){var i=this;t=function(e,t){e||i.setData(t)},d.request({name:"g' +
                    'etVIFInfo",success:function(i){var n=i.filter(function(t){if(t._status=v.dcConnS' +
                    'tatus[t.status],t._canDel=t.status<2,e.searchKey){var i=new RegExp(e.searchKey);' +
                    'if(!i.test(t.vifName)&&!i.test(t.vifId))return}return t});t(null,{list:n,totalNu' +
                    'm:n.length,searchKey:e.searchKey})},fail:function(){}})},del:function(e){return ' +
                    'function(t){if(!e.$disable&&e._canDel){var i=new(o.getComponent("popup-confirm")' +
                    ')({$data:{trigger:"",title:"确认要删除该条记录? ",style:"width: 310px;",hideDestroy:!0},h' +
                    'andler:t.target,onConfirm:function(){d.request({name:"deleteVIF",data:{vifList:[' +
                    '{dcId:e.dcId,vifId:e.vifId}]},success:function(){r.trigger(v.events.DCCONNCHANGE' +
                    'D)}})}});i.show()}}},showMonitor:function(e){s.show({module:{render:function(t,i' +
                    '){var n=new o('    <div>        <div class="sidebar-panel-hd">            <h3 cl' +
                    'ass="run-in">{{dc.vifName}}监控</h3>        </div>        <div class="sidebar-pane' +
                    'l-bd" style="bottom: 40px;">            {{> chart }}        </div>    </div>',{c' +
                    'hart:new u({id:e.dcId,lineName:e.lineName,type:"dc_channel",dimension:[{directco' +
                    'nnectconnid:e.vifId,directconnectid:e.dcId,directconnectname:e.lineName}]}).$el,' +
                    '$data:{dc:e}});t.html(n.$el)},destroy:function(){}},data:{}})}},{defaults:{hasFi' +
                    'rst:!1,showPagination:!1,colums:[{name:"ID/名称",key:"vifId",tdTpl:'    <p><a data' +
                    '-event="nav" href="/vpc/dcConn/detail/{{item.vifId}}">{{item.vifId}}</a></p>    ' +
                    '<p><span class="text-overflow m-width">{{item.vifName}}</span></p>',required:!0}' +
                    ',{name:"监控",key:"_monitor",width:100,tdTpl:'<i class="dosage-icon" b-on-click="s' +
                    'howMonitor(item)" data-title="查看监控"></i>'},{name:"连接状态",key:"_status",tdTpl:"   ' +
                    ' <span class=\"text-overflow {{item.status == 0 ? 'succeed' : (item.status > 3 ?' +
                    ' 'error' : 'warning')}}\">        {{item._status}}    </span>",width:100},{name:' +
                    '"物理专线",key:"dcId",tdTpl:'    <span class="text-overflow">        <a href="/vpc/d' +
                    'c/detail/{{item.dcId}}?backUrl=/vpc/dcConn" data-event="nav" class="links">{{ite' +
                    'm.dcId}}</a>    </span>'},{name:"私有网络",key:"vpcId",tdTpl:'    <p>        <a data' +
                    '-event="nav" href="/vpc/vpc?unVpcId={{item.vpcInstanceId}}&backUrl=/vpc/dcConn">' +
                    '            <span class="text-overflow">{{item.vpcInstanceId}}</span>        </a' +
                    '>    </p>    <span class="text-overflow">        {{item.vpcName}}    </span>'},{' +
                    'name:"关联专线网关",key:"directConnectGatewayId ",tdTpl:'    <div b-if="item.directCon' +
                    'nectGatewayId">        <p>            <span class="text-overflow">              ' +
                    '  {{item.directConnectGatewayId}}            </span>        </p>        <span>  ' +
                    '          {{item.directConnectGatewayName}}        </span>    </div>    <span b-' +
                    'if="!item.directConnectGatewayId">        -    </span>'},{name:"路由方式",key:"route' +
                    'Type",tdTpl:'    <span class="text-overflow">        {{item.routeType == 0 ? \'B' +
                    'GP 路由\' : \'静态路由\'}}        <a b-if="item.model != 0" data-event="nav" href="/vp' +
                    'c/dcRecord/detail/{{item.dcId2}}">{{item.dcId2}}</a>    </span>'},{name:"PeerIp"' +
                    ',key:"peerId"},{name:"BGP ASN",key:"bgpAsn",hide:!0},{name:"BGP密钥",key:"bgpKey",' +
                    'hide:!0},{name:"申请时间",key:"applyTime",hide:!0},{name:"操作",key:"_action",tdTpl:' ' +
                    '   <span class="{{item._canDel ? \'\' : \'disable-link\'}}">        <a href="jav' +
                    'ascript:;" data-title?="item._canDel ? \'\' : \'只有已连接或申请中的专线通道可删除\'" b-on="{clic' +
                    'k: del(item)}" class="links">删除</a>    </span>',required:!0}]}})),i.exports={ren' +
                    'der:function(t,i,s){var d=this;if("detail"===t)i?(d.isDetail=!0,l.render(i,s)):n' +
                    '.render404();else{var o=d.bee=new m('    <div class="manage-area">        <div c' +
                    'lass="manage-area-title">            <h2>专线通道</h2>        </div>        <div cla' +
                    'ss="tc-15-action-panel">            <button class="tc-15-btn m {{lineList.length' +
                    ' || \'disabled\'}}" data-title="{{(lineList.length ? \'\' : \'创建专线通道前需要有个可用的专线\'' +
                    ')}}" b-on-click="add()">创建通道</button>            <!--<button class="tc-15-btn we' +
                    'ak m download"></button>-->            <button b-ref="setting" b-on-click="field' +
                    'setting()" class="tc-15-btn weak m setting"></button>            <qc-search b-re' +
                    'f="search" search="{{search.bind(this)}}" placeholder="请输入专线通道名称或 ID"></qc-searc' +
                    'h>        </div>        <dcrecord-grid b-ref="grid" vpc-id="{{vpcId}}"></dcrecor' +
                    'd-grid>    </div>',{$data:{maxDcSize:a.MAX_DC_SIZE}});this.grid=o.$refs.grid,n.a' +
                    'ppArea.html(o.$el),e("../adTip").init(o.$el),this.grid.setMaxHeight(),d.isDetail' +
                    '=!1,this.bindEvents()}},destroy:function(){this.isDetail&&l.destroy(),r.off(v.ev' +
                    'ents.DCCONNCHANGED)},bindEvents:function(){var e=this;r.on(v.events.DCCONNCHANGE' +
                    'D,function(){e.grid.refresh()})}}}),define("modules/vpc/dcConn/detail",function(' +
                    'e,t,i){var n=e("pageManager"),a=(e("constants"),e("widget/detail/detail")),s=(e(' +
                    '"appUtil"),e("router")),d=e("models/directconnect"),o=e("widget/pubsubhub/pubsub' +
                    'hub"),c=e("../dc/dcUtil"),r=e("widget/copy/copy"),l=e("modules/vpc/IPInput"),p=e' +
                    '("../dc/chart"),u=e("widget/baseInfo/baseInfo").extend({});u.tag("ip-input",l);v' +
                    'ar v=a.extend({$beforeInit:function(){v.__super__.$beforeInit.call(this),this._t' +
                    'abs=[]},$afterInit:function(){var e=this;v.__super__.$afterInit.call(this),e.loa' +
                    'd()},$beforeDestroy:function(){this._tabs.forEach(function(e){e.$destroy(!0)}),o' +
                    '.off(".detail")},load:function(){var e=this;this.getInfo(this.id).then(function(' +
                    't){e.$set({dc:t,title:t.vifName})})},loadTabs:function(e){var t=this.$refs.tabs,' +
                    'i=this._tabs[e];if(!i){switch(e){case 0:i=new u({detailTpl:'    <div class="tc-1' +
                    '5-list-content">        <ul>            <li><em class="tc-15-list-tit">专线通道名称</e' +
                    'm>                <span class="tc-15-list-det">                    <span b-if="!' +
                    'isEdit">{{info.vifName}}</span>                    <div class="tc-15-input-text-' +
                    'wrap" b-if="isEdit">                        <input type="text" b-model="info.vif' +
                    'Name" data-validate="name" maxlength="25" class="tc-15-input-text">             ' +
                    '           <div class="tc-15-input-tips">还能输入{{25 - info.vifName.length}}个字符，支持中' +
                    '文、英文、数字、下划线、分隔符"-"、小数点</div>                    </div>                </span>   ' +
                    '         </li>            <li><em class="tc-15-list-tit">专线通道 ID</em><span class' +
                    '="tc-15-list-det">{{id}}</span>            </li>            <li><em class="tc-15' +
                    '-list-tit">连接状态</em><span class="tc-15-list-det">{{info._status}}</span>        ' +
                    '    </li>            <li>                <em class="tc-15-list-tit">物理专线</em>   ' +
                    '             <span class="tc-15-list-det">                    <a href="/vpc/dc/d' +
                    'etail/{{info.dcId}}" data-event="nav">{{info.dcName}}</a></span>            </li' +
                    '>            <li>                <em class="tc-15-list-tit">私有网络</em>           ' +
                    '     <span class="tc-15-list-det">                    <a href="/vpc/vpc?unVpcId=' +
                    '{{info.vpcInstanceId}}" data-event="nav">{{info.vpcInstanceId}}</a>             ' +
                    '       <span>({{info.vpcName}})</span>                </span>            </li>  ' +
                    '          <li>                <em class="tc-15-list-tit">关联专线网关</em>            ' +
                    '    <span class="tc-15-list-det">                    <span>{{info.directConnectG' +
                    'atewayName}} ({{info.directConnectGatewayId}})</span>                </span>    ' +
                    '        </li>            <li><em class="tc-15-list-tit">VLAN ID</em>            ' +
                    '    {{info.vlanId}}                <!--<span b-if="!isEdit">{{info.vlanId}}</spa' +
                    'n>-->                <!--<span b-if="isEdit" class="tc-15-list-det">-->         ' +
                    '           <!--<input b-model="info.vlanId" type="text" class="tc-15-input-text"' +
                    '>-->                <!--</span>-->            </li>            <li>             ' +
                    '   <em class="tc-15-list-tit">Peer IP</em>                <span class="tc-15-lis' +
                    't-det">                    {{info.peerIp || \'-\'}}                    <i class=' +
                    '"copy-icon" style="display: none;" data-clipboard-text="{{info.peerIp}}"></i>   ' +
                    '             </span>            </li>            <li>                <em class="' +
                    'tc-15-list-tit">路由方式</em>                <span class="tc-15-list-det">{{info.rou' +
                    'teType == 0 ? \'BGP 路由\' : \'静态路由\'}}</span>            </li>            <li b-i' +
                    'f="info.routeType == 0">                <em class="tc-15-list-tit">BGP ASN</em> ' +
                    '               <span b-if="!isEdit || info.status != 0">{{info.bgpAsn}}</span>  ' +
                    '              <span b-if="isEdit && info.status == 0" class="tc-15-list-det">   ' +
                    '                 <input data-validate="bgpAsn" b-model="info.bgpAsn" type="text"' +
                    ' class="tc-15-input-text"></span>            </li>            <li b-if="info.rou' +
                    'teType == 0">                <em class="tc-15-list-tit">BGP 密钥</em>             ' +
                    '   <span b-if="!isEdit || info.status != 0" class="tc-15-list-det">             ' +
                    '       {{info.bgpKey}}                    <i style="display: none;" data-clipboa' +
                    'rd-text="{{info.bgpKey}}" class="copy-icon"></i>                </span>         ' +
                    '       <span b-if="isEdit && info.status == 0" class="tc-15-list-det">          ' +
                    '          <input data-validate="bgpKey" b-model="info.bgpKey" type="text" class=' +
                    '"tc-15-input-text">                </span>            </li>        </ul>    </di' +
                    'v>',
$data:{id:this.id,changeEvent:c.events.DCCONNCHANGED},rules:c.rules,getData' +
                    ':this.getInfo,updateData:function(e){return new Promise(function(t,i){d.request(' +
                    '{name:"updateVIFInfo",data:e,success:function(e){o.trigger(c.events.DCCONNCHANGE' +
                    'D),t(e)},fail:i})})}}),i.$on("loaded",function(){new r({handle:$(i.$el).find("[d' +
                    'ata-clipboard-text]"),container:$(i.$el).find("[data-clipboard-text]").closest("' +
                    'span")})});break;case 1:var n=this.dc;i=new p({id:this.dc.dcId,lineName:this.dc.' +
                    'lineName,type:"dc_channel",dimension:[{directconnectconnid:n.vifId,directconnect' +
                    'id:n.dcId,directconnectname:n.lineName}]})}this._tabs[e]||(this._tabs[e]=i),t.li' +
                    'st.$set(e,{content:i.$el})}},getInfo:function(){var e;return function(t){return ' +
                    'e||(e=new Promise(function(e,i){d.request({name:"getVIFInfo",data:{vifId:t},succ' +
                    'ess:e,fail:i})}).then(function(t){var i=t[0];return i._status=c.dcConnStatus[i.s' +
                    'tatus],e=null,i||n.render404(),i},function(t){throw e=null,t})),e}}(),back:funct' +
                    'ion(){s.navigate("/vpc/dcConn")}},{defaults:{tabData:{list:[{label:"基本信息"},{labe' +
                    'l:"监控"}]}}});i.exports={render:function(e,t){var i=new v($.extend({id:e},t));n.a' +
                    'ppArea.html(i.$el),o.on(c.events.DCCONNCHANGED,function(){i.load()})},destroy:fu' +
                    'nction(){o.off(c.events.DCCONNCHANGED)}}}),define("modules/vpc/dcGw/add",functio' +
                    'n(e,t,i){var n=e("dialog"),a=e("appUtil"),s=e("qccomponent"),d=(e("models/direct' +
                    'connect"),e("../cApi")),o=e("../cacher"),c=e("widget/baseInfo/validator"),r=e(".' +
                    './vpc/DropDown"),l=s.extend({}),p=e("widget/pubsubhub/pubsubhub"),u=e("../dc/dcU' +
                    'til");e("router");l.tag("vpc-select",r),i.exports={render:function(e,t){var i=[]' +
                    ';e.forEach(function(t,n){n++;for(var a;a=e[n];n++)t.unVpcId==a.unVpcId&&t.type!=' +
                    'a.type&&i.push(t.unVpcId)});var s=new l('    <div class="tc-15-rich-dialog">    ' +
                    '    <div class="tc-15-list-wrap form nat-form">            <form action="javascr' +
                    'ipt:;" class="vpc-line-layer">                <div class="tc-15-list-content">  ' +
                    '                  <ul>                        <li class="even"><em aria-required' +
                    '="false" class="tc-15-list-tit">所在网络</em>                            <span class' +
                    '="tc-15-list-det">                                <vpc-select b-model="unVpcId" ' +
                    'use-un-id="1" vpc-filter-list="{{vpcFilterList}}" region-id="{{regionId}}"></vpc' +
                    '-select>                            </span>                        </li>        ' +
                    '                <li b-if="displayNat"><em class="tc-15-list-tit">网关类型</em>      ' +
                    '                      <span class="tc-15-list-det">                             ' +
                    '   <label class="tc-15-radio-wrap" data-title?="curVpcNatType==1 && \'该私有网络已创建此类' +
                    '型网关\'">                                    <input name="vpc-dcGw-enableNat" valu' +
                    'e="1" disabled?="curVpcNatType==1"                                           b-m' +
                    'odel="enableNat" type="radio" class="tc-15-radio">支持NAT</label>                 ' +
                    '               <label class="tc-15-radio-wrap" data-title?="curVpcNatType==\'\' ' +
                    '&& \'该私有网络已创建此类型网关\'">                                    <input name="vpc-dcGw-' +
                    'enableNat" value=""  disabled?="curVpcNatType==\'\'"                            ' +
                    '               b-model="enableNat" type="radio" class="tc-15-radio">不支持NAT</labe' +
                    'l>                                <span class="text-weak">（提交后不可更改）</span>      ' +
                    '                      </span>                        </li>                      ' +
                    '  <li class="even">                            <em aria-required="false" class="' +
                    'tc-15-list-tit">名称</em>                            <span class="tc-15-list-det">' +
                    '                                <div class="tc-15-input-text-wrap">             ' +
                    '                       <input type="text" data-validate="name" maxlength="25" b-' +
                    'model="directConnectGatewayName" placeholder="请输入专线网关的名称" class="tc-15-input-tex' +
                    't">                                </div>                            </span>    ' +
                    '                    </li>                    </ul></div>            </form>     ' +
                    '   </div>    </div>',{$afterInit:function(){this.$watch("unVpcId",function(e){e&' +
                    '&(this.$set("curVpcNatType",this.getTypeByVpcId(e)),this.$set("enableNat",this.c' +
                    'urVpcNatType?"":"1"))},!0)},$data:{regionId:a.getRegionId(),vpcFilterList:i,enab' +
                    'leNat:t?"1":"",displayNat:t},getTypeByVpcId:function(t){var i;return e.some(func' +
                    'tion(e){return t==e.unVpcId?(i=e.type,!0):void 0}),i}});n.create(s.$el,"530","",' +
                    '{title:"创建专线网关",button:{"确定":function(e){var i=new c({btn:e[0]});i.validateAll(s' +
                    '.$el)&&d.request(s.regionId,"CreateDirectConnectGateway",{vpcId:s.unVpcId,direct' +
                    'ConnectGatewayName:s.directConnectGatewayName,type:t&&s.enableNat?1:0},"创建失败").d' +
                    'one(function(){p.trigger(u.events.DCGWCHANGED),o.resetNamespace("dcGw",s.regionI' +
                    'd,s.unVpcId),n.hide()})}}})},destroy:function(){n.hide()}}}),define("modules/vpc' +
                    '/dcGw/dcGw",function(e,t,i){var n=(e("pageManager"),e("manager")),a=e("constants' +
                    '"),s=e("panel"),d=e("appUtil"),o=e("models/directconnect"),c=e("../vpc/Selector"' +
                    '),r=e("qccomponent"),l=e("../Grid"),p=e("widget/pubsubhub/pubsubhub"),u=e("../ca' +
                    'cher"),v=e("../cApi"),f=e("./add"),m=e("../dc/chart"),h=e("../dc/dcUtil"),b=e("w' +
                    'idget/fieldsManager/gridView"),g=e("../util"),I=e("./Detail"),w=e("../moduleBase' +
                    '"),y=function(e){return[].concat([{name:"ID/名称",key:"directConnectGatewayId",tdT' +
                    'pl:'    <p>        <a b-if="displayNat" b-on-click="handleAction(\'info\', item)' +
                    '" href="javascript:;">            <span class="text-overflow">{{item.directConne' +
                    'ctGatewayId}}</span>        </a>        <span b-if="!displayNat">            <sp' +
                    'an class="text-overflow">{{item.directConnectGatewayId}}</span>        </span>  ' +
                    '  </p>    <p><span class="text-overflow m-width">{{item.directConnectGatewayName' +
                    '}}</span></p>',required:!0},{name:"监控",key:"_minitor",width:100,tdTpl:'<i class=' +
                    '"dosage-icon" b-on-click="showMonitor(item)" data-title="查看监控"></i>'},{name:"所属网' +
                    '络",key:"vpcId",tdTpl:g.vpcTdTpl},{name:"专线通道数",key:"_vifNum"}],e?[{name:"NAT配置状态' +
                    '",key:"type",tdTpl:'{{item.type ? "支持" : "不支持"}}'}]:[],[{name:"创建时间",key:"create' +
                    'Time",hide:!0},{name:"操作",key:"_action",tdTpl:'    <span class="{{item._canDel ?' +
                    ' \'\' : \'disable-link\'}}">        <a href="javascript:;" b-on="{click: del(ite' +
                    'm)}"           data-title?="item._canDel ? \'\' : (item._vifNumber ? \'请先删除链接该专线' +
                    '网关的专线通道\' : \'请确该专线网关下的专线通道都状态是已连接或申请中\')"           class="links">删除</a>    </s' +
                    'pan>    <!--<a href="javascript:;" b-on-click="_updateDCStatus(item)" class="lin' +
                    'ks">转</a>-->',required:!0}])};e("widget/region/regionSelector");var x=r.extend({' +
                    '$tpl:'    <div>        <div class="manage-area"  b-style="{display: directConnec' +
                    'tGatewayId ? \'none\' : \'block\'}">            <div class="manage-area-title"> ' +
                    '               <h2>专线网关</h2>                <region-selector b-ref="regionSelect' +
                    'or" b-model="regionId" get-white-list="{{getRegionWhiteList}}"></region-selector' +
                    '>                <span b-tag="vpc-select" b-ref="vpcSelect" region-id="{{regionI' +
                    'd}}" vpc-id="{{vpcId}}" use-un-id="1" b-model="unVpcId"></span>                <' +
                    'div class="manage-area-title-right">                    <a href="https://www.qcl' +
                    'oud.com/doc/product/215/4976" target="_blank">                        <b class="' +
                    'links-icon"></b> 专线接入帮助文档                    </a>                </div>         ' +
                    '   </div>            <div class="tc-15-action-panel">                <button cla' +
                    'ss="tc-15-btn m {{canAdd&&whiteListLoaded ? \'\' : \'disabled\'}}" data-title?="' +
                    '(!canAdd) && \'每个私有网络只能创建两个专线网关, 请确保有空闲的私有网络\'" b-on-click="showAdd()">+新建</butt' +
                    'on>                <!--<button class="tc-15-btn weak m download"></button>-->   ' +
                    '             <button b-ref="setting" b-on-click="fieldsetting()" class="tc-15-bt' +
                    'n weak m setting"></button>                <qc-search b-ref="search" search="{{s' +
                    'earch.bind(this)}}" keyword="{{keyword}}" placeholder="请输入专线网关名"></qc-search>   ' +
                    '         </div>            <dcgw-grid b-ref="grid" colums="{{columns}}" display-' +
                    'nat="{{displayNat}}" handle-action="{{handleAction}}" region-id="{{regionId}}" u' +
                    'n-vpc-id="{{unVpcId}}"></dcgw-grid>        </div>        <dcgw-detail b-ref="det' +
                    'ail" hidden="{{!directConnectGatewayId}}" region-id="{{regionId}}" data="{{activ' +
                    'eDcGw}}" b-model="directConnectGatewayId" tab="{{tab}}"></dcgw-detail>    </div>' +
                    '',$afterInit:function(){var e=this,t=e.$refs.grid;["page","count","keyword"].for' +
                    'Each(function(i){g.beeBindBoth(e,t,i,"pending"+g.capitalize(i))})},showAdd:funct' +
                    'ion(){this.canAdd&&f.render(this.$refs.grid.list,this.displayNat)},fieldsetting:' +
                    'function(){this.$refs.grid.showFieldsManager()},search:function(e){this.$set("ke' +
                    'yword",e)},getRegionWhiteList:function(){var e=a.DC_REGION;return u.getRegionIdL' +
                    'ist().then(function(t){return t.filter(function(t){return e&&e.length?e.indexOf(' +
                    't)>-1:t})})}});x.tag("vpc-select",c),x.tag("dcgw-grid",l.extend({$mixins:[b("dc_' +
                    'gw_grid_fields")],backList:function(){this.$set("searchKey",""),this.$set("pendi' +
                    'ngKeyword","")},getData:function(e){var t=this,n=t.pendingPage||e.page||1,a=t.pe' +
                    'ndingCount||e.count||20,s=t.pendingKeyword||e.searchKey,c=(n-1)*a,r=a,l=function' +
                    '(e,i){e||t.setData(i),t.hideLoading()};t.showLoading("加载中..."),v.request(t.regio' +
                    'nId,"DescribeDirectConnectGateway",{offset:c,limit:r,directConnectGatewayName:s,' +
                    'vpcId:t.unVpcId||void 0}).translate({data:{key:"data",map:"dcGw"}}).done(functio' +
                    'n(e){var t=e.data;u.getVpcList(d.getRegionId()).done(function(e){var n=i.exports' +
                    '.bee;n.$set("canAdd",2*e.vpcList.length>t.length),n.add&&!n._showed&&(n.showAdd(' +
                    '),n._showed=!0)}),t.length?o.request({name:"getVIFInfo",success:function(i){t.fo' +
                    'rEach(function(e){e._vif=e._vif||[],i.forEach(function(t){e.directConnectGateway' +
                    'Id==t.directConnectGatewayId&&e._vif.push(t)}),e._vifNumber=e._vif.length,e._vif' +
                    'Num=e._vifNumber+"个",e._canDel=!e._vif.some(function(e){return e.status>=2})&&!e' +
                    '._vifNumber}),l(null,{list:t,totalNum:e.totalCount,searchKey:s})}}):l(null,{list' +
                    ':[],totalNum:0,searchKey:s})})},del:function(e){return function(t){var i=this.$r' +
                    'oot;if(!e.$disable&&e._canDel){var a=new(r.getComponent("popup-confirm"))({$data' +
                    ':{trigger:"",title:"确认要删除该专线网关? ",style:"width: 310px;",hideDestroy:!0},handler:' +
                    't.target,onConfirm:function(){o.request({name:"deleteDirectConnectGateway",data:' +
                    '{directConnectGatewayId:e.directConnectGatewayId,vpcId:e.vpcId},success:function' +
                    '(e){i.showLoading(),n.checkTask({taskId:e.taskId}).then(function(){i.hideLoading' +
                    '(),p.trigger(h.events.DCGWCHANGED)},function(){i.hideLoading(),p.trigger(h.event' +
                    's.DCGWCHANGED)})}})}});a.show()}}},showMonitor:function(e){var t=this;s.show({mo' +
                    'dule:{render:function(i,n){var a=new r('    <div>        <div class="sidebar-pan' +
                    'el-hd">            <h3 class="run-in">{{dc.directConnectGatewayName}}监控</h3>    ' +
                    '    </div>        <div class="sidebar-panel-bd" style="bottom: 40px;">          ' +
                    '  {{> chart }}        </div>    </div>',{chart:new m({traffic:!0,id:e.vpcId,regi' +
                    'onId:t.regionId,type:"vpg",dimension:[{uniqvpcid:e.unVpcId,uniqvpgid:e.directCon' +
                    'nectGatewayId}],charts:{inChart:{metricName:"intraffic",unit:"Mbps",isMbps:!0},o' +
                    'utChart:{metricName:"outtraffic",unit:"Mbps",isMbps:!0}}}).$el,$data:{dc:e}});i.' +
                    'html(a.$el)},destroy:function(){}},data:{}})}},{defaults:{hasFirst:!1,showPagina' +
                    'tion:!1,colums:y(!1)}})),x.tag("dcgw-detail",I),i.exports=w.extend({routeBase:"/' +
                    'vpc/dcGw",params:$.extend({},w.params,{add:"",keyword:"",tab:"",directConnectGat' +
                    'ewayId:{route:"dcGwId",history:!0,defaults:""}}),Bee:x,getBeeConfig:function(){v' +
                    'ar e=this,t=w.getBeeConfig.apply(e,arguments),i={};return a.DC_NAT_REGION.forEac' +
                    'h(function(e){i[e]=!0}),$.extend(t.$data,{canAdd:!1,maxDcSize:a.MAX_DC_SIZE,NAT_' +
                    'REGIONS:i,regionId:d.getRegionId(),columns:y(!1),handleAction:function(t,i){var ' +
                    'n=e.bee;switch(t){case"chart":n.$set({activeDcGw:i,directConnectGatewayId:i.dire' +
                    'ctConnectGatewayId,tab:"chart"});break;case"info":if(!n.displayNat)return;n.$set' +
                    '({activeDcGw:i,directConnectGatewayId:i.directConnectGatewayId,tab:"info"})}}}),' +
                    't},onRender:function(){var t=this,i=t.bee,a=t.grid=i.$refs.grid,s=i.$refs.vpcSel' +
                    'ect,d="VPC_DC_NAT",o=function(){i.$set("whiteListLoaded",!0)};s.$watch("list",fu' +
                    'nction(e){i.$set("canAdd",e.length<=1?!1:""),i.$set("addDisabledText",e.length<=' +
                    '1?"无可用私有网络":"")},!0),i.$watch("displayNat",function(e){i.$set("columns",y(e))}),' +
                    'n.getAllWhiteList(function(e){e&&e[d]&&e[d].length&&i.$set("inNatWhiteList",!0),' +
                    'o()},o),g.beeBind(i,i,"NAT_REGIONS[regionId] && inNatWhiteList","displayNat"),e(' +
                    '"../adTip").init(t.$el),a.setMaxHeight(),t.isDetail=!1,t.bindEvents(),t.refresh(' +
                    ')},refresh:function(){this.grid.bufferReload()},bindEvents:function(){var e=this' +
                    ',t=e.bee;p.on(h.events.DCGWCHANGED,function(){e.refresh()});var i=function(){e.r' +
                    'efresh()};t.$watch("regionId",function(e){t.$set("unVpcId",void 0),t.$set("regio' +
                    'nId",e),i()}),["unVpcId","keyword","page","count"].forEach(function(e){t.$watch(' +
                    'e,i)}),g.beeBind(t.$refs.detail,t,"tab")},onDestroy:function(){}})}),define("mod' +
                    'ules/vpc/dcGw/dcNatModel",function(e,t,i){var n=e("models/api"),a=[],s=function(' +
                    'e){e=e||{};var t=$.extend({},e);return e.write?t.cache=!1:(a.push(t),t.cache="ca' +
                    'che"in e?e.cache:!0),function(i,s){return e.write&&a.forEach(function(e){e.chang' +
                    'ed=!0}),n.request({cmd:e.cmd,serviceType:"vpc",data:i},$.extend({cache:t.cache,c' +
                    'acheTag:"vpc_dc_gw_nat",flush:t.changed},s)).then(function(e){return t.cache&&(t' +
                    '.changed=!1),e}).then(e.successHandler||function(e){return e},e.errorHandler||fu' +
                    'nction(e){throw e})}};t.DescribeLocalIPTranslationNatRule=function(e,t){return n' +
                    '.request({cmd:"DescribeLocalIPTranslationNatRule",serviceType:"vpc",data:e},t).t' +
                    'hen(function(e){return e.data})},t.DescribePeerIPTranslationNatRule=s({cmd:"Desc' +
                    'ribePeerIPTranslationNatRule",successHandler:function(e){return e.data}}),t.Desc' +
                    'ribeLocalSourceIPPortTranslationNatRule=function(e,t){return n.request({cmd:"Des' +
                    'cribeLocalSourceIPPortTranslationNatRule",serviceType:"vpc",data:e},t).then(func' +
                    'tion(e){return e.data})},t.DescribeLocalDestinationIPPortTranslationNatRule=s({c' +
                    'md:"DescribeLocalDestinationIPPortTranslationNatRule",successHandler:function(e)' +
                    '{return e.data}}),t.CreateLocalIPTranslationNatRule=s({cmd:"CreateLocalIPTransla' +
                    'tionNatRule",write:!0}),t.DeleteLocalIPTranslationNatRule=s({cmd:"DeleteLocalIPT' +
                    'ranslationNatRule",write:!0}),t.CreateLocalSourceIPPortTranslationNatRule=s({cmd' +
                    ':"CreateLocalSourceIPPortTranslationNatRule",write:!0}),t.DeleteLocalSourceIPPor' +
                    'tTranslationNatRule=s({cmd:"DeleteLocalSourceIPPortTranslationNatRule",write:!0}' +
                    '),t.CreateLocalDestinationIPPortTranslationNatRule=s({cmd:"CreateLocalDestinatio' +
                    'nIPPortTranslationNatRule",write:!0}),t.DeleteLocalDestinationIPPortTranslationN' +
                    'atRule=s({cmd:"DeleteLocalDestinationIPPortTranslationNatRule",write:!0}),t.Crea' +
                    'tePeerIPTranslationNatRule=s({cmd:"CreatePeerIPTranslationNatRule",write:!0}),t.' +
                    'DeletePeerIPTranslationNatRule=s({cmd:"DeletePeerIPTranslationNatRule",write:!0}' +
                    '),t.ModifyLocalIPTranslationNatRule=s({cmd:"ModifyLocalIPTranslationNatRule",wri' +
                    'te:!0}),t.ModifyPeerIPTranslationNatRule=s({cmd:"ModifyPeerIPTranslationNatRule"' +
                    ',write:!0}),t.DescribeLocalIPTranslationAclRule=s({cmd:"DescribeLocalIPTranslati' +
                    'onAclRule",successHandler:function(e){return e.data.forEach(function(e){0==e.des' +
                    'tinationPort&&(e.destinationPort=""),0==e.sourcePort&&(e.sourcePort="")}),e.data' +
                    '}}),t.ModifyLocalSourceIPPortTranslationNatRule=s({cmd:"ModifyLocalSourceIPPortT' +
                    'ranslationNatRule",write:!0}),t.ModifyLocalDestinationIPPortTranslationNatRule=s' +
                    '({cmd:"ModifyLocalDestinationIPPortTranslationNatRule",write:!0}),t.SetLocalIPTr' +
                    'anslationAclRule=s({write:!0,cmd:"SetLocalIPTranslationAclRule"}),t.DeleteLocalI' +
                    'PTranslationAclRule=s({write:!0,cmd:"DeleteLocalIPTranslationAclRule"}),t.Descri' +
                    'beLocalSourceIPPortTranslationAclRule=s({cmd:"DescribeLocalSourceIPPortTranslati' +
                    'onAclRule",successHandler:function(e){return e.data.forEach(function(e){0==e.des' +
                    'tinationPort&&(e.destinationPort=""),0==e.sourcePort&&(e.sourcePort="")}),e.data' +
                    '}}),t.SetLocalSourceIPPortTranslationAclRule=s({write:!0,cmd:"SetLocalSourceIPPo' +
                    'rtTranslationAclRule"}),t.DeleteLocalSourceIPPortTranslationAclRule=s({write:!0,' +
                    'cmd:"DeleteLocalSourceIPPortTranslationAclRule"})}),define("modules/vpc/dcGw/Det' +
                    'ail",function(e,t,i){var n=e("../BeeBase"),a=(e("../cacher"),e("../cApi")),s=(e(' +
                    '"../dc/dcUtil"),e("../dc/chart")),d=e("./InfoPanel"),o="directConnectGatewayId",' +
                    'c=e("./nat/ipsrc/IPSrc"),r=e("./nat/ipdest/IPDest"),l=e("./nat/srcipport/IPPortC' +
                    'onfig"),p=e("./nat/ipportdest/IPPortDest"),u=i.exports=n.extend({$tpl:'    <div ' +
                    'b-style="{display: hidden ? \'none\' : \'\'}">        <div class="return-title-p' +
                    'anel bottom-border" b-style="{display:notFound||loading?\'none\':\'\'}">        ' +
                    '    <a href="javascript:;" b-on-click="back()" class="btn-return"><i class="btn-' +
                    'back-icon"></i><span>返回</span></a>            <i class="line"></i>            <h' +
                    '2>{{data&&data.directConnectGatewayName}}详情</h2>            <div class="manage-a' +
                    'rea-title-right">                <a href="https://www.qcloud.com/doc/product/215' +
                    '/4976" target="_blank">                    <b class="links-icon"></b> 专线接入帮助文档  ' +
                    '              </a>            </div>        </div>        <div b-if="loading" st' +
                    'yle="text-align:center; padding:100px;">加载中...</div>        <div b-if="notFound"' +
                    ' style="text-align:center; padding:100px;">未查询到指定专线网关, <a href="javascript:;" b-' +
                    'on-click="$set(\'directConnectGatewayId\', \'\')">返回列表</a></div>        <div cla' +
                    'ss="tab-switch" b-style="{display:notFound||loading?\'none\':\'\'}">            ' +
                    '<a class="{{tab===\'info\' ? \'cur\' : \'\'}}" b-on-click="$set(\'tab\', \'info' +
                    '\')" data-report="vpc.directlineGW.detail" href="javascript:;">基本信息</a>         ' +
                    '   <a class="{{tab===\'chart\' ? \'cur\' : \'\'}}" b-on-click="$set(\'tab\', \'c' +
                    'hart\')" data-report="vpc.directlineGW.monitor" href="javascript:;">监控</a>      ' +
                    '      <span b-template  b-if="data && data.type">                <a class="{{tab' +
                    '===\'localiptranslation\' ? \'cur\' : \'\'}}" b-on-click="$set(\'tab\', \'locali' +
                    'ptranslation\')" href="javascript:;">本端IP转换</a>                <a class="{{tab==' +
                    '=\'peeriptranslation\' ? \'cur\' : \'\'}}" b-on-click="$set(\'tab\', \'peeriptra' +
                    'nslation\')" href="javascript:;">对端IP转换</a>                <a class="{{tab===\'l' +
                    'ocalsrcipporttranslation\' ? \'cur\' : \'\'}}" b-on-click="$set(\'tab\', \'local' +
                    'srcipporttranslation\')" href="javascript:;">本端源IP端口转换</a>                <a cla' +
                    'ss="{{tab===\'localdestipporttranslation\' ? \'cur\' : \'\'}}" b-on-click="$set(' +
                    '\'tab\', \'localdestipporttranslation\')" href="javascript:;">本端目的IP端口转换</a>    ' +
                    '        </span>        </div>        <dcgw-info b-with="{regionId:regionId, data' +
                    ': data}" b-if="!loading && !notFound && tab===\'info\'"></dcgw-info>        <dcg' +
                    'w-chart b-with="{regionId:regionId, dimension: [{ uniqvpcid: data.unVpcId, uniqv' +
                    'pgid: data.directConnectGatewayId }], id: data&&data.vpcId}" b-if="!loading && !' +
                    'notFound && tab===\'chart\'"></dcgw-chart>        <dcgw-nat-ip-src b-with="{regi' +
                    'onId:regionId, data: data}"                        b-if="!loading && !notFound &' +
                    '& tab===\'localiptranslation\' && data.type"></dcgw-nat-ip-src>        <dcgw-nat' +
                    '-ip-dest b-with="{regionId:regionId, data: data}"                        b-if="!' +
                    'loading && !notFound && tab===\'peeriptranslation\' && data.type"></dcgw-nat-ip-' +
                    'dest>        <dcgw-nat-ipport-config b-with="{regionId:regionId, data: data}"   ' +
                    '                     b-if="!loading && !notFound && tab===\'localsrcipporttransl' +
                    'ation\' && data.type"></dcgw-nat-ipport-config>        <dcgw-nat-ipport-dest b-w' +
                    'ith="{regionId:regionId, data: data}"                        b-if="!loading && !' +
                    'notFound && tab===\'localdestipporttranslation\' && data.type"></dcgw-nat-ipport' +
                    '-dest>    </div>',$valuekey:o,idParser:a.idParser,back:function(){history.back()' +
                    '},$afterInit:function(){u.__super__.$afterInit.call(this);var e=this,t=function(' +
                    '){if(e.regionId&&e[o]&&!e.hidden){e.$set("notFound",!1);var t=this.data;t&&t[o]=' +
                    '==this[o]?(t&&!this.subCfg.dcGw||this.subCfg.dcGw[o]!=this[o])&&this.$set("data"' +
                    ',t):(e.$set({data:{}}),e.load())}};["regionId",o,"hidden"].forEach(function(i){e' +
                    '.$watch(i,t)}),e.$set("subCfg",{loaded:!1,regionId:e.regionId,dcGw:e.dcGw}),t(),' +
                    '$(e.$el).on("REFRESHDETAIL",function(t,i){e.$set("data.directConnectGatewayName"' +
                    ',i)})},load:function(){var e=this,t=e,i=t.data;!t[o]||i&&t[o]===i[o]||(t.$set({l' +
                    'oading:!0,notFound:!1,data:{}}),a.request(t.regionId,"DescribeDirectConnectGatew' +
                    'ay",{offset:0,limit:1,directConnectGatewayId:t[o]}).done(function(e){var i=e.dat' +
                    'a[0],n=!0;i&&(n=!1,i=a.translate(i,"dcGw")),t.$set({notFound:n,data:i||{}})}).al' +
                    'ways(function(){t.$set("loading",!1)}))}},{defaults:{tab:"info"}});u.tag("dcgw-i' +
                    'nfo",d),u.tag("dcgw-chart",s.extend({type:"vpg",charts:{inChart:{metricName:"int' +
                    'raffic",unit:"Mbps",isMbps:!0},outChart:{metricName:"outtraffic",unit:"Mbps",isM' +
                    'bps:!0}}})),u.tag("dcgw-nat-ip-src",c),u.tag("dcgw-nat-ip-dest",r),u.tag("dcgw-n' +
                    'at-ipport-config",l),u.tag("dcgw-nat-ipport-dest",p)}),define("modules/vpc/dcGw/' +
                    'InfoPanel",function(e,t,i){var n=e("widget/baseInfo/baseInfo"),a=e("../cacher"),' +
                    's=e("../cApi"),d=e("../dc/dcUtil"),o=(e("./util"),e("../BeeBase")),c=e("appUtil"' +
                    '),r=e("widget/pubsubhub/pubsubhub"),l=i.exports=o.extend({$afterInit:function(){' +
                    'var e=this;o.prototype.$afterInit.call(this),this.$watch("data",function(t){t&&e' +
                    '.$refs.infos&&(e.$refs.infos.origin=c.clone(t))},!0)},$tpl:'    <div b-style="{d' +
                    'isplay:hidden?\'none\':\'\'}">        <dcgw-info b-ref="infos" b-with="{regionId' +
                    ':regionId, info: data}"></dcgw-info>    </div>'});l.tag("dcgw-info",n.extend({de' +
                    'tailTpl:'    <div>        <div class="tc-15-list-content">            <ul>      ' +
                    '          <li><em class="tc-15-list-tit">名称</em>                    <span b-if="' +
                    '!isEdit" class="tc-15-list-det">{{info.directConnectGatewayName}}</span>        ' +
                    '            <div class="tc-15-input-text-wrap" b-if="isEdit">                   ' +
                    '     <input type="text" data-validate="name" class="tc-15-input-text" b-model="i' +
                    'nfo.directConnectGatewayName" maxlength="25">                        <div class=' +
                    '"tc-15-input-tips">还能输入{{25 - info.directConnectGatewayName.length}}个字符，支持中文、英文、' +
                    '数字、下划线、分隔符"-"、小数点</div>                    </div>                </li>          ' +
                    '      <li><em class="tc-15-list-tit">ID</em><span class="tc-15-list-det">{{info.' +
                    'directConnectGatewayId}}</span>                </li>                <li><em clas' +
                    's="tc-15-list-tit">所属网络</em>                <span class="tc-15-list-det">       ' +
                    '             {{info.vpcName}}({{info.vpcCidrBlock}} | <a data-event="nav" href="' +
                    '/vpc/vpc?rid={{regionId}}&unVpcId={{info.unVpcId}}">{{info.unVpcId}}</a>)       ' +
                    '         </span>                </li>                <li><em class="tc-15-list-t' +
                    'it">所在地域</em><span class="tc-15-list-det">{{regionMap && regionMap[regionId]}}</' +
                    'span>                </li>                <li>                    <em class="tc-' +
                    '15-list-tit">网关类型</em>                    <span class="tc-15-list-det">{{info.ty' +
                    'pe ? \'支持NAT\' : \'不支持NAT\'}}</span>                </li>            </ul>      ' +
                    '  </div>        <div class="param-box">            <div class="param-hd">       ' +
                    '         <h3>相关路由策略</h3>            </div>            <route-info region-id="{{r' +
                    'egionId}}" un-vpc-id="{{info && info.unVpcId}}" id="{{info && info.directConnect' +
                    'GatewayId}}"></route-info>        </div>    </div>',changeEvent:d.events.DCCHANG' +
                    'ED,netTypeMap:d.dcGwNatTypes,getData:function(){var e=this;return new Promise(fu' +
                    'nction(t,i){a.getRegionMap().then(function(i){e.$set("regionMap",i),t(e.data)},i' +
                    ')})},updateData:function(e){var t=this,i=t.info;return s.request(t.regionId,"Mod' +
                    'ifyDirectConnectGateway",{vpcId:i.unVpcId,directConnectGatewayId:i.directConnect' +
                    'GatewayId,directConnectGatewayName:i.directConnectGatewayName}).done(function(){' +
                    'r.trigger(d.events.DCGWCHANGED),$(t.$el).trigger("REFRESHDETAIL",[i.directConnec' +
                    'tGatewayName]),t.edit(!1)})}})),l.tag("route-info",e("../RouteInfo"))}),define("' +
                    'modules/vpc/dcGw/nat/AclEditor",function(e,t,i){var n=e("appUtil"),a=Bee.getComp' +
                    'onent("grid-editor");i.exports=a.extend({cancel:function(){return this.setData({' +
                    'list:n.clone(this.originalList)}),Promise.resolve()},save:function(){return Prom' +
                    'ise.reject("实现该方法来保存 acl 规则")},protoChange:function(e,t){var i=this;setTimeout(f' +
                    'unction(){"ALL"==e.protocol&&i.updateItem(t,{sourceCidr:"0.0.0.0/0",sourcePort:"' +
                    '",destinationCidr:"0.0.0.0/0",destinationPort:""})},0)},checkItemBeenModified:fu' +
                    'nction(e){return this.originalList.some(function(t){return!e.aclRuleId||e.aclRul' +
                    'eId!=t.aclRuleId||e.protocol==t.protocol&&e.sourcePort==t.sourcePort&&e.destinat' +
                    'ionCidr==t.destinationCidr&&e.destinationPort==t.destinationPort?void 0:!0})},bu' +
                    'ildItem:function(e){var t={protocol:e.protocol,sourcePort:e.sourcePort,destinati' +
                    'onCidr:e.destinationCidr,destinationPort:e.destinationPort};return e.sourceCidr&' +
                    '&(t.sourceCidr=e.sourceCidr),e.aclRuleId&&(t.aclRuleId=e.aclRuleId),t},generateS' +
                    'etData:function(){var e=[],t=[],i=[],n=this;return this.list.forEach(function(a)' +
                    '{a._remove?a.aclRuleId&&t.push(this.buildItem(a)):a.aclRuleId?n.checkItemBeenMod' +
                    'ified(a)&&i.push(this.buildItem(a)):e.push(this.buildItem(a))}.bind(this)),{addD' +
                    'ata:e,deleteData:t.map(function(e){return e.aclRuleId}),modifyData:i}}},{default' +
                    's:{dragable:!1,insertBtn:!1,maxSize:100,canResizeColum:!1,bottomInsertBtn:!0,emp' +
                    'tyTips:"还没有映射规则",colums:[{width:50,minWidth:50},{name:"序号",key:"_index",tdTpl:" ' +
                    '   {{$parent.$index + 1}}",width:50,minWidth:50},{name:"策略",tdTpl:"允许",width:100' +
                    '},{name:"协议",key:"protocol",tdTpl:'    <select class="tc-15-select m" b-model="i' +
                    'tem.protocol" disabled?="item._remove" b-on-change="protoChange(item, $parent.$i' +
                    'ndex)">        <option value="TCP">TCP</option>        <option value="UDP">UDP</' +
                    'option>        <option value="ALL">ALL</option>    </select>'},{name:"源IP",key:"' +
                    'originalIP",tdTpl:'    <input type="text" class="tc-15-input-text m ip-input" da' +
                    'ta-validate="srcCidr"           disabled?="item._remove || item.protocol == \'AL' +
                    'L\'" b-model="item.sourceCidr" placeholder="IP或CIDR">'},{name:"源端口",key:"sourceP' +
                    'ort",tdTpl:'    <input type="text" class="tc-15-input-text m ip-input" data-vali' +
                    'date="srcPort"           disabled?="item._remove || item.protocol == \'ALL\'" b-' +
                    'model="item.sourcePort" placeholder="0-65535">'},{name:"目的IP",key:"destinationCi' +
                    'dr",tdTpl:'    <input type="text" class="tc-15-input-text m ip-input" data-valid' +
                    'ate="destCidr"           disabled?="item._remove || item.protocol == \'ALL\'" b-' +
                    'model="item.destinationCidr" placeholder="IP或CIDR">'},{name:"目的端口",key:"destinat' +
                    'ionPort",tdTpl:'    <input type="text" class="tc-15-input-text m ip-input" data-' +
                    'validate="destPort"           disabled?="item._remove || item.protocol == \'ALL' +
                    '\'" b-model="item.destinationPort" placeholder="0-65535">'},{name:"操作",key:"_act' +
                    'ion",width:100}]}})}),define("modules/vpc/dcGw/nat/AclGrid",function(e,t,i){var ' +
                    'n=(e("appUtil"),Bee.getComponent("grid-view")),a=e("./validator"),s=new a;i.expo' +
                    'rts=n.extend({_edit:function(e,t){var i=this.$root;return function(n){i.$isEdit|' +
                    '|i.loading||(i.$set("$isEdit",!0),i.updateItem(t,{$isEdit:!0,_protocol:e.protoco' +
                    'l,_sourceCidr:e.sourceCidr,_sourcePort:e.sourcePort,_destinationCidr:e.destinati' +
                    'onCidr,_destinationPort:e.destinationPort}))}},delItem:function(){return Promise' +
                    '.reject("实现该方法以删除 acl 规则")},saveItem:function(e){return Promise.reject("实现该方法以保存' +
                    '单挑 acl 规则")},protoChange:function(e,t){var i=this;setTimeout(function(){"ALL"==e' +
                    '._protocol&&i.updateItem(t,{_sourceCidr:"0.0.0.0/0",_sourcePort:"",_destinationC' +
                    'idr:"0.0.0.0/0",_destinationPort:""})},0)},save:function(e,t){var i=this;return ' +
                    'i.saveItem(e,t).then(function(){i.$set("$isEdit",!1),i.updateItem(t,{$pending:!1' +
                    ',$isEdit:!1}),i.onUpdate()},function(){i.$set("$isEdit",!1),i.updateItem(t,{$pen' +
                    'ding:!1})})},_save:function(e,t){var i=this.$root;return function(n){s.btn=n.tar' +
                    'get,!e.$pending&&s.validateAll($(n.target).closest("tr"))&&(i.updateItem(t,{$pen' +
                    'ding:!0}),i.save(e,t))}},_cancel:function(e){var t=this.$root;return function(){' +
                    't.$set("$isEdit",!1),t.updateItem(e,{$isEdit:!1})}},del:function(e){var t=this;t' +
                    '.delItem(e).then(function(){t.onUpdate()})},checkDel:function(e,t){t=t||{};var i' +
                    '=this.$root;return function(n){if(!i.$isEdit){var a=new(Bee.getComponent("popup-' +
                    'confirm"))({$data:$.extend({trigger:"",title:i.delTipsTitle,content:i.delTipsCon' +
                    'tent,style:"width: 310px;",hideDestroy:!0},t),handler:n.target,attachEl:$(n.targ' +
                    'et).closest("[data-nat-wrap]")[0],onConfirm:function(){i.del(e)}});a.show()}}}},' +
                    '{defaults:{minHeight:"auto",emptyTips:"还没有映射规则",hasFirst:!1,showState:!1,canResi' +
                    'zeColum:!1,autoMaxHeight:!1,showPagination:!1,colums:[{width:29,minWidth:29},{na' +
                    'me:"序号",key:"_index",tdTpl:"    {{$parent.$index + 1}}",width:50,minWidth:50},{n' +
                    'ame:"策略",tdTpl:"{{item._act || '允许'}}"},{name:"协议",key:"protocol",tdTpl:'    <se' +
                    'lect class="tc-15-select m" b-model="item._protocol" disabled?="item._remove" b-' +
                    'if="item.$isEdit"        b-on-change="protoChange(item, $parent.$index)">       ' +
                    ' <option value="TCP">TCP</option>        <option value="UDP">UDP</option>       ' +
                    ' <option value="ALL">ALL</option>    </select>    <span b-if="!item.$isEdit">   ' +
                    '     {{item.protocol}}    </span>'},{name:"源IP",key:"sourceCidr",tdTpl:'    <inp' +
                    'ut type="text" class="tc-15-input-text m ip-input" data-validate="srcCidr" b-if=' +
                    '"item.$isEdit"           disabled?="item._remove || item._protocol == \'ALL\'" b' +
                    '-model="item._sourceCidr" placeholder="IP或CIDR">    <span b-if="!item.$isEdit"> ' +
                    '       {{item.sourceCidr}}    </span>'},{name:"源端口",key:"sourcePort",tdTpl:'    ' +
                    '<input type="text" class="tc-15-input-text m ip-input" data-validate="srcPort" b' +
                    '-if="item.$isEdit"           disabled?="item._remove || item._protocol == \'ALL' +
                    '\'" b-model="item._sourcePort" placeholder="0-65535">    <span b-if="!item.$isEd' +
                    'it">        {{item.sourcePort == \'0-0\' || item.sourcePort == \'0\' || item.sou' +
                    'rcePort == \'\' ? \'ALL\' : item.sourcePort}}    </span>'},{name:"目的IP",key:"des' +
                    'tinationCidr",tdTpl:'    <input type="text" class="tc-15-input-text m ip-input" ' +
                    'data-validate="destCidr" b-if="item.$isEdit"           disabled?="item._remove |' +
                    '| item._protocol == \'ALL\'" b-model="item._destinationCidr" placeholder="IP或CID' +
                    'R">    <span b-if="!item.$isEdit">        {{item.destinationCidr}}    </span>'},' +
                    '{name:"目的端口",key:"destinationPort",tdTpl:'    <input type="text" class="tc-15-in' +
                    'put-text m ip-input" data-validate="destPort" b-if="item.$isEdit"           disa' +
                    'bled?="item._remove || item._protocol == \'ALL\'" b-model="item._destinationPort' +
                    '" placeholder="0-65535">    <span b-if="!item.$isEdit">        {{item.destinatio' +
                    'nPort == \'0-0\' || item.destinationPort == \'0\' || item.destinationPort == \'' +
                    '\' ? \'ALL\' : item.destinationPort}}    </span>'},{name:"操作",key:"_action",tdTp' +
                    'l:'    <div b-if="!item._act">        <span b-if="!item.$isEdit" class="{{$isEdi' +
                    't ? \'disable-link\' : \'\'}}">            <a href="javascript:;" b-on="{click: ' +
                    '_edit(item, $parent.$index)}">修改</a>            <a href="javascript:;" b-on="{cl' +
                    'ick: checkDel(item)}">删除</a>        </span>        <div b-if="item.$isEdit">    ' +
                    '        <button type="button" b-on="{click: _save(item, $parent.$index)}" class=' +
                    '"tc-15-btn m {{item.$pending ? \'disabled\': \'\'}}">保存</button>            <but' +
                    'ton type="button" b-on="{click: _cancel($parent.$index)}"  class="tc-15-btn m we' +
                    'ak">取消</button>        </div>    </div>'}]}})}),define("modules/vpc/dcGw/nat/Bee' +
                    'Base",function(e,t,i){var n=e("../../BeeBase").extend({}),a=e("../../IPInput"),s' +
                    '=e("../../Grid");n.tag("dcgw-ip-input",a.extend({},{defaults:{type:"ip safeIp al' +
                    'lClass"}})),n.tag("dcgw-port-input",n.extend({$valuekey:"value",$tpl:'    <span ' +
                    'class="tc-15-input-text-wrap">        <input type="text" data-name="{{name}}"   ' +
                    '            b-model="value" class="tc-15-input-text m"               style="min-' +
                    'width: 36px;width:36px;padding:0;height:22px;line-height:22px;text-align: center' +
                    ';"               data-trigger="focus"               data-style="min-width:100px"' +
                    '               qc-popover="范围：1~65535" />    </span>'})),n.tag("dcgw-rule-list",' +
                    's.extend({$afterInit:function(){s.prototype.$afterInit.apply(this,arguments),thi' +
                    's.afterInit&&this.afterInit()},onRowRemove:function(e,t,i){this.removeHandler(e,' +
                    't,i)},getInvalids:function(){return this.list.filter(function(e){return e.invali' +
                    'd})}},{defaults:{hasFirst:!0,showPagination:!1,initGetData:!1}})),i.exports=n}),' +
                    'define("modules/vpc/dcGw/nat/ipdest/add",function(e,t,i){var n=e("dialog"),a=e("' +
                    'modules/vpc/dcGw/dcNatModel"),s=e("../BeeBase"),d=e("../validator"),o=e("appUtil' +
                    '"),c=e("tips"),r=(e("models/manager"),new d);r.errClass="vpc-input-error is-erro' +
                    'r",i.exports=function(e,t){var i=!!t;if(i){t=o.clone(t);var d=t.originalIP,l=t.t' +
                    'ranslationIP;
}return new Promise(function(p,u){var v=new s('    <div class="tc-' +
                    '15-list-wrap">        <form action="javascript:;">            <div class="form-w' +
                    'rap">                <ul class="form-list">                    <li>             ' +
                    '           <div aria-required="false" class="form-label">                       ' +
                    '     <label for="">原IP</label></div>                        <span class="form-in' +
                    'put">                            <span data-originalIP>                         ' +
                    '       <dcgw-ip-input b-model="item.originalIP" name="originalIP"></dcgw-ip-inpu' +
                    't>                            </span>                        </span>            ' +
                    '        </li>                    <li>                        <div aria-required=' +
                    '"false" class="form-label">                            <label for="">映射IP</label' +
                    '></div>                        <span class="form-input">                        ' +
                    '    <span data-translationIP>                                <dcgw-ip-input b-mo' +
                    'del="item.translationIP" name="translationIP"></dcgw-ip-input>                  ' +
                    '          </span>                        </span>                    </li>       ' +
                    '             <li>                        <div class="form-label">               ' +
                    '             <label for="">备注</label>                        </div>             ' +
                    '           <span class="form-input">                            <input type="tex' +
                    't" data-validate="desc" b-model="item.description" placeholder="25个字符以内，可不填" cla' +
                    'ss="tc-15-input-text m" maxlength="25">                            <div class="t' +
                    'c-15-input-tips">还能输入{{25 - (item.description.length || 0)}}个字符</div>           ' +
                    '             </span></li>                </ul>            </div>        </form> ' +
                    '   </div>',{$data:{item:t||{}},validate:function(){return o.isValidIp(this.item.' +
                    'originalIP)?o.isValidIp(this.item.translationIP)?!0:(r.showErr($(v.$el).find("[d' +
                    'ata-translationIP]"),"映射IP无效"),!1):(r.showErr($(v.$el).find("[data-originalIP]")' +
                    ',"原IP无效"),!1)}});n.create(v.$el,540,"",{title:i?"修改对端IP映射":"新增对端IP映射",preventRes' +
                    'ubmit:!0,defaultCancelCb:function(){u()},button:{"确定":function(t){if(r.btn=t,!r.' +
                    'validateAll(v.$el)||!v.validate())return void n.toggleBtnDisable(!1);var s=v.ite' +
                    'm;(i?a.ModifyPeerIPTranslationNatRule({vpcId:e.unVpcId,directConnectGatewayId:e.' +
                    'directConnectGatewayId,oldTranslationIP:l,oldOriginalIP:d,originalIP:s.originalI' +
                    'P,translationIP:s.translationIP,description:s.description}):a.CreatePeerIPTransl' +
                    'ationNatRule({vpcId:e.unVpcId,directConnectGatewayId:e.directConnectGatewayId,pe' +
                    'erIPTranslation:[{originalIP:s.originalIP,translationIP:s.translationIP,descript' +
                    'ion:s.description}]})).then(function(){p(),n.hide()},function(e){n.toggleBtnDisa' +
                    'ble(!1);var t="";28123==e.code?t="原IP需要在vpc网段内":28131==e.code&&(t="该原IP已存在"),t&&' +
                    '(c.hideFlashNow(),r.showErr($(v.$el).find("[data-originalip]"),t))})}}})})}}),de' +
                    'fine("modules/vpc/dcGw/nat/ipdest/IPDest",function(e,t,i){var n=e("../NatList"),' +
                    'a=e("modules/vpc/dcGw/dcNatModel"),s=e("../BeeBase"),d=(e("appUtil"),e("./add"))' +
                    ',o=(e("models/manager"),s.extend({$tpl:'    <div class="nat-convert-box">       ' +
                    ' <div class="mod-defination show-map">            <div class="dt-text">对端IP转换(Pe' +
                    'er IP Translation)</div>            <div class="dd-text">                <p>专线对端' +
                    '原IP映射为新IP，并以新IP与连接的私有网络进行网络互访。                    <a href="javascript:;"  qc-pop' +
                    'over="<i class=\'pic_2\'></i>">示意图</a></p>            </div>        </div>      ' +
                    '  <nat-ip-dest b-with="data"></nat-ip-dest>    </div>'}));o.tag("nat-ip-dest",n.' +
                    'extend({add:function(){return d(this.$data)},gridOpts:{expandable:!1,canEditAcl:' +
                    '!1,item1Tpl:'    <span>        <label class="text-label">原IP:</label>        <la' +
                    'bel>            {{item.originalIP}}        </label>    </span>',item2Tpl:'    <s' +
                    'pan>        <label class="text-label">映射IP:</label>        <label style="margin-' +
                    'right: 1em;">            {{item.translationIP}}        </label>        <label cl' +
                    'ass="text-label" b-if="item.description">            ({{item.description}})     ' +
                    '   </label>    </span>',emptyTips:"您还未配置IP映射",delTipsTitle:"确认删除此IP映射?",editItem' +
                    ':function(e){return d(this.dcGw,e)},del:function(e){return a.DeletePeerIPTransla' +
                    'tionNatRule({vpcId:this.vpcId,directConnectGatewayId:this.dcgwId,peerIPTranslati' +
                    'on:[{originalIP:e.originalIP,translationIP:e.translationIP}]})},getData:function' +
                    '(e,t){var i=e.count||this.count,n=e.page||this.page,s={limit:i,offset:i*(n-1),vp' +
                    'cId:this.vpcId,directConnectGatewayId:this.dcgwId};e.searchKey&&(s.description=e' +
                    '.searchKey),a.DescribePeerIPTranslationNatRule(s).then(function(i){t(null,{searc' +
                    'hKey:e.searchKey,list:i.data,totalNum:i.totalNum})})}}},{defaults:{title:"IP映射"}' +
                    '})),i.exports=o}),define("modules/vpc/dcGw/nat/ipportdest/add",function(e,t,i){v' +
                    'ar n=e("dialog"),a=e("modules/vpc/dcGw/dcNatModel"),s=e("../BeeBase"),d=e("../va' +
                    'lidator"),o=e("appUtil"),c=e("tips"),r=(e("models/manager"),new d);r.errClass="v' +
                    'pc-input-error is-error";var l=function(e){var t=1*e;return t>0&&65535>=t?!0:!1}' +
                    ';i.exports=function(e,t){var i=!!t;if(i){t=o.clone(t);var d=t.originalIP,p=t.tra' +
                    'nslationIP,u=t.originalPort,v=t.translationPort,f=t.proto}return new Promise(fun' +
                    'ction(m,h){var b=new s('    <div class="tc-15-list-wrap">        <form action="j' +
                    'avascript:;">            <div class="form-wrap">                <ul class="form-' +
                    'list">                    <li>                        <div aria-required="false"' +
                    ' class="form-label">                            <label for="">协议</label></div>  ' +
                    '                      <span class="form-input">                            <div>' +
                    '                                <select class="tc-15-select m" b-model="item.pro' +
                    'to">                                    <option value="tcp">TCP</option>        ' +
                    '                            <option value="udp">UDP</option>                    ' +
                    '            </select>                            </div>                        <' +
                    '/span>                    </li>                    <li>                        <' +
                    'div aria-required="false" class="form-label">                        <label for=' +
                    '"">原IP端口</label></div>                        <span class="form-input">         ' +
                    '                   <span data-originalIP>                                <dcgw-i' +
                    'p-input b-model="item.originalIP" name="originalIP"></dcgw-ip-input>:           ' +
                    '                     <dcgw-port-input b-model="item.originalPort" name="original' +
                    'Port"></dcgw-port-input>                            </span>                     ' +
                    '   </span>                    </li>                    <li>                     ' +
                    '   <div aria-required="false" class="form-label">                            <la' +
                    'bel for="">映射后IP端口</label></div>                        <span class="form-input"' +
                    '>                            <span data-translationIP>                          ' +
                    '      <dcgw-ip-input b-model="item.translationIP" name="translationIP"></dcgw-ip' +
                    '-input>:                                <dcgw-port-input b-model="item.translati' +
                    'onPort" name="translationPort"></dcgw-port-input>                            </s' +
                    'pan>                        </span>                    </li>                    ' +
                    '<li>                        <div class="form-label">                            ' +
                    '<label for="">备注</label>                        </div>                        <s' +
                    'pan class="form-input">                            <input type="text" data-valid' +
                    'ate="desc" b-model="item.description" placeholder="25个字符以内，可不填" class="tc-15-inp' +
                    'ut-text m" maxlength="25">                            <div class="tc-15-input-ti' +
                    'ps">还能输入{{25 - (item.description.length || 0)}}个字符</div>                        ' +
                    '</span></li>                </ul>            </div>        </form>    </div>',{$' +
                    'data:{item:t||{}},validate:function(){return o.isValidIp(this.item.originalIP)?o' +
                    '.isValidIp(this.item.translationIP)?l(this.item.originalPort)?l(this.item.transl' +
                    'ationPort)?!0:(r.showErr($(b.$el).find("[data-translationIP]"),"端口范围 1~65535"),!' +
                    '1):(r.showErr($(b.$el).find("[data-originalIP]"),"端口范围 1~65535"),!1):(r.showErr(' +
                    '$(b.$el).find("[data-translationIP]"),"映射后IP无效"),!1):(r.showErr($(b.$el).find("[' +
                    'data-originalIP]"),"原IP无效"),!1)}});n.create(b.$el,540,"",{title:i?"编辑本端目的IP端口映射"' +
                    ':"新增本端目的IP端口映射",preventResubmit:!0,defaultCancelCb:function(){h()},button:{"确定":' +
                    'function(t){if(r.btn=t,!r.validateAll(b.$el)||!b.validate())return void n.toggle' +
                    'BtnDisable(!1);var s=b.item;(i?a.ModifyLocalDestinationIPPortTranslationNatRule(' +
                    '{vpcId:e.unVpcId,directConnectGatewayId:e.directConnectGatewayId,oldOriginalPort' +
                    ':u,oldTranslationPort:v,oldOriginalIP:d,oldTranslationIP:p,oldProto:f,proto:s.pr' +
                    'oto,originalIP:s.originalIP,translationIP:s.translationIP,originalPort:s.origina' +
                    'lPort,translationPort:s.translationPort,description:s.description}):a.CreateLoca' +
                    'lDestinationIPPortTranslationNatRule({vpcId:e.unVpcId,directConnectGatewayId:e.d' +
                    'irectConnectGatewayId,localDestinationIPPortTranslation:[{proto:s.proto,original' +
                    'IP:s.originalIP,translationIP:s.translationIP,originalPort:s.originalPort,transl' +
                    'ationPort:s.translationPort,description:s.description}]})).then(function(){m(),n' +
                    '.hide()},function(e){n.toggleBtnDisable(!1),28134==e.code&&(r.showErr($(b.$el).f' +
                    'ind("[data-originalip]"),"原ip需要在vpc网段内"),c.hideFlashNow())})}}})})}}),define("mo' +
                    'dules/vpc/dcGw/nat/ipportdest/IPPortDest",function(e,t,i){var n=e("../NatList"),' +
                    'a=e("modules/vpc/dcGw/dcNatModel"),s=e("../BeeBase"),d=(e("appUtil"),e("./add"))' +
                    ',o=(e("models/manager"),s.extend({$tpl:'    <div class="nat-convert-box">       ' +
                    ' <div class="mod-defination show-map">            <div class="dt-text">本端目的IP端口转' +
                    '换(Local Destination IP Port Translation)</div>            <div class="dd-text"> ' +
                    '               <p>专线对端主动访问私有网络，需访问映射后的IP端口与私有网络内原IP端口进行通信，回包不受影响。               ' +
                    '     <a href="javascript:;"  qc-popover="<i class=\'pic_4\'></i>">示意图</a></p>   ' +
                    '         </div>        </div>        <ip-port-dest b-with="data"></ip-port-dest>' +
                    '    </div>'}));o.tag("ip-port-dest",n.extend({$afterInit:function(){n.prototype.' +
                    '$afterInit.call(this);var e=this.$refs.grid,t=e.colums.slice();t.unshift({name:"' +
                    '",tdTpl:'    <span>        <label class="text-label">协议：</label>        <label> ' +
                    '           {{item.proto.toUpperCase()}}        </label>    </span>',width:200}),' +
                    'e.setColums(t)},add:function(){return d(this.$data)},gridOpts:{expandable:!1,can' +
                    'EditAcl:!1,item1Tpl:'    <span>        <label class="text-label">原IP端口:</label> ' +
                    '       <label>            {{item.originalIP}}:{{item.originalPort}}        </lab' +
                    'el>    </span>',item2Tpl:'    <span>        <label class="text-label">映射IP端口:</l' +
                    'abel>        <label style="margin-right: 1em;">            {{item.translationIP}' +
                    '}:{{item.translationPort}}        </label>        <label class="text-label" b-if' +
                    '="item.description">            ({{item.description}})        </label>    </span' +
                    '>',emptyTips:"您还未配置IP池",delTipsTitle:"确认删除此映射?",editWording:"修改IP端口映射",editItem:' +
                    'function(e){return d(this.dcGw,e)},del:function(e){return a.DeleteLocalDestinati' +
                    'onIPPortTranslationNatRule({vpcId:this.vpcId,directConnectGatewayId:this.dcgwId,' +
                    'localDestinationIPPortTranslation:[{proto:e.proto,originalIP:e.originalIP,transl' +
                    'ationIP:e.translationIP,originalPort:e.originalPort,translationPort:e.translatio' +
                    'nPort}]})},getData:function(e,t){var i=e.count||this.count,n=e.page||this.page,s' +
                    '={limit:i,offset:i*(n-1),vpcId:this.vpcId,directConnectGatewayId:this.dcgwId};e.' +
                    'searchKey&&(s.description=e.searchKey),a.DescribeLocalDestinationIPPortTranslati' +
                    'onNatRule(s).then(function(i){t(null,{searchKey:e.searchKey,list:i.data,totalNum' +
                    ':i.totalNum})})}}},{defaults:{title:"IP端口映射"}})),i.exports=o}),define("modules/v' +
                    'pc/dcGw/nat/ipsrc/add",function(e,t,i){var n=e("dialog"),a=e("modules/vpc/dcGw/d' +
                    'cNatModel"),s=e("../BeeBase"),d=e("../validator"),o=e("appUtil"),c=e("tips"),r=e' +
                    '("models/manager"),l=new d;l.errClass="vpc-input-error is-error",i.exports=funct' +
                    'ion(e,t){var i=!!t,d=(e.vpcCidrBlock||"0.0.0.0/0").split("/"),p=d[0],u=d[1];if(i' +
                    '){t=o.clone(t);var v=t.originalIP,f=t.translationIP}return new Promise(function(' +
                    'd,m){var h=new s('    <div class="tc-15-list-wrap">        <form action="javascr' +
                    'ipt:;">            <div class="form-wrap">                <ul class="form-list">' +
                    '                    <li>                        <div aria-required="false" class' +
                    '="form-label">                        <label for="">原IP</label></div>           ' +
                    '             <span class="form-input">                            <span data-ori' +
                    'ginalIP>                                <dcgw-ip-input b-model="item.originalIP"' +
                    ' name="originalIP"></dcgw-ip-input>                            </span>          ' +
                    '              </span>                    </li>                    <li>          ' +
                    '              <div aria-required="false" class="form-label">                    ' +
                    '        <label for="">映射IP</label></div>                        <span class="for' +
                    'm-input">                            <span data-translationIP>                  ' +
                    '              <dcgw-ip-input b-model="item.translationIP" name="translationIP"><' +
                    '/dcgw-ip-input>                            </span>                        </span' +
                    '>                    </li>                    <li>                        <div c' +
                    'lass="form-label">                            <label for="">备注</label>          ' +
                    '              </div>                        <span class="form-input">           ' +
                    '                 <input type="text" data-validate="desc" b-model="item.descripti' +
                    'on" placeholder="25个字符以内，可不填" class="tc-15-input-text m" maxlength="25">        ' +
                    '                    <div class="tc-15-input-tips">还能输入{{25 - (item.description.l' +
                    'ength || 0)}}个字符</div>                        </span></li>                </ul> ' +
                    '           </div>        </form>    </div>',{$afterInit:function(){$(this.$el).f' +
                    'ind("[data-originalIP], [data-translationIP]").on("change, input",function(){l.h' +
                    'ideErr($(this))})},$data:{item:t||{},vpcCidrNet:p,vpcCidrMask:u},validate:functi' +
                    'on(){return o.isValidIp(this.item.originalIP)?o.isValidIp(this.item.translationI' +
                    'P)?!0:(l.showErr($(h.$el).find("[data-translationIP]"),"映射IP无效"),!1):(l.showErr(' +
                    '$(h.$el).find("[data-originalIP]"),"原IP无效"),!1)}});n.create(h.$el,540,"",{title:' +
                    'i?"修改本端IP映射":"新增本端IP映射",preventResubmit:!0,defaultCancelCb:function(){m()},butto' +
                    'n:{"确定":function(t){if(l.btn=t,!l.validateAll(h.$el)||!h.validate())return void ' +
                    'n.toggleBtnDisable(!1);var s=h.item;(i?a.ModifyLocalIPTranslationNatRule({vpcId:' +
                    'e.unVpcId,directConnectGatewayId:e.directConnectGatewayId,oldTranslationIP:f,old' +
                    'OriginalIP:v,originalIP:s.originalIP,translationIP:s.translationIP,description:s' +
                    '.description}):a.CreateLocalIPTranslationNatRule({vpcId:e.unVpcId,directConnectG' +
                    'atewayId:e.directConnectGatewayId,localIPTranslation:[{originalIP:s.originalIP,t' +
                    'ranslationIP:s.translationIP,description:s.description}]}).then(function(e){retu' +
                    'rn e=e.data||{},e.taskId?r.checkTask({taskId:e.taskId}):e})).then(function(){d()' +
                    ',n.hide()},function(e){n.toggleBtnDisable(!1);var t="";28123==e.code?t="原IP需要在vp' +
                    'c网段内":28127==e.code&&(t="该原IP已存在"),t&&(l.showErr($(h.$el).find("[data-originalip' +
                    ']"),t),c.hideFlashNow())})}}})})}}),define("modules/vpc/dcGw/nat/ipsrc/IPSrc",fu' +
                    'nction(e,t,i){var n=e("../NatList"),a=e("appUtil"),s=e("modules/vpc/dcGw/dcNatMo' +
                    'del"),d=e("../BeeBase"),o=e("../AclGrid"),c=e("../AclEditor"),r=e("./add"),l=e("' +
                    'models/manager"),p=(e("tips"),d.extend({$tpl:'    <div class="nat-convert-box"> ' +
                    '       <div class="mod-defination show-map">            <div class="dt-text">本端I' +
                    'P转换(Local IP Translation)</div>            <div class="dd-text">                ' +
                    '<p>私有网络内原IP映射为新IP，并以新IP与专线对端进行网络互访。<a href="javascript:;"  qc-popover="<i class=' +
                    '\'pic_1\'></i>">示意图</a></p>                <p>对端主动访问时，不受NAT规则限制。</p>            ' +
                    '</div>        </div>        <nat-acl-list b-with="data"></nat-acl-list>    </div' +
                    '>'}));p.tag("nat-acl-list",n.extend({add:function(){return r(this.$data)},gridOp' +
                    'ts:{item1Tpl:'    <i class="icon-cdb-table"></i>    <span>        <label class="' +
                    'text-label">原IP:</label>        <label>            {{item.originalIP}}        </' +
                    'label>    </span>',item2Tpl:'    <span>        <label class="text-label">映射IP:</' +
                    'label>        <label style="margin-right: 1em;">            {{item.translationIP' +
                    '}}        </label>        <label class="text-label" b-if="item.description">    ' +
                    '        ({{item.description}})        </label>    </span>',emptyTips:"您还未配置IP映射"' +
                    ',delTipsTitle:"确认删除此IP映射?",delTipsContent:"删除IP映射将一同删除此规则对应的ACL策略",editItem:func' +
                    'tion(e){return r(this.dcGw,e)},del:function(e){var t=this;return s.DeleteLocalIP' +
                    'TranslationNatRule({vpcId:this.vpcId,directConnectGatewayId:this.dcgwId,localIPT' +
                    'ranslation:[{originalIP:e.originalIP,translationIP:e.translationIP}]}).then(func' +
                    'tion(e){return e=e.data||{},t.showLoading(),e.taskId?l.checkTask({taskId:e.taskI' +
                    'd}).then(function(e){return t.hideLoading(),e},function(e){throw t.hideLoading()' +
                    ',e}):e})},getData:function(e,t){var i=e.count||this.count,n=e.page||this.page,a=' +
                    '{limit:i,offset:i*(n-1),vpcId:this.vpcId,directConnectGatewayId:this.dcgwId};e.s' +
                    'earchKey&&(a.description=e.searchKey),s.DescribeLocalIPTranslationNatRule(a).the' +
                    'n(function(i){t(null,{searchKey:e.searchKey,list:i.data,totalNum:i.totalNum})})}' +
                    '}},{defaults:{title:"IP映射"}})),p.tag("acl-grid",o.extend({delTipsTitle:"确认删除此网络A' +
                    'CL规则？",$beforeInit:function(){var e=o.defaults.colums.slice();e.splice(4,1),this' +
                    '.setColums(e)},delItem:function(e){return s.DeleteLocalIPTranslationAclRule({vpc' +
                    'Id:this.vpcId,directConnectGatewayId:this.dcgwId,originalIP:this.natItem.origina' +
                    'lIP,translationIP:this.natItem.translationIP,aclRules:[e.aclRuleId]})},saveItem:' +
                    'function(e){return s.SetLocalIPTranslationAclRule({vpcId:this.vpcId,directConnec' +
                    'tGatewayId:this.dcgwId,originalIP:this.natItem.originalIP,translationIP:this.nat' +
                    'Item.translationIP,setData:JSON.stringify({modifyData:[{aclRuleId:e.aclRuleId,pr' +
                    'otocol:e._protocol,sourcePort:e._sourcePort,destinationCidr:e._destinationCidr,d' +
                    'estinationPort:e._destinationPort}]})})},getData:function(e,t){var i=this,n=i.na' +
                    'tItem;this.showLoading(),t=function(e,t){i.setData(t),i.hideLoading()},s.Describ' +
                    'eLocalIPTranslationAclRule({vpcId:this.vpcId,directConnectGatewayId:this.dcgwId,' +
                    'originalIP:n.originalIP,translationIP:n.translationIP}).then(function(e){e.push(' +
                    '{protocol:"ALL",sourcePort:"ALL",destinationCidr:"ALL",destinationPort:"ALL",_ac' +
                    't:"拒绝"}),t(null,{list:e})},t)}})),p.tag("acl-editor",c.extend({$beforeInit:funct' +
                    'ion(){var e=c.defaults.colums.slice();e.splice(4,1),this.setColums(e)},getData:f' +
                    'unction(e,t){var i=this,n=i.natItem;this.showLoading(),t=function(e,t){i.setData' +
                    '(t),i.hideLoading()},s.DescribeLocalIPTranslationAclRule({vpcId:this.vpcId,direc' +
                    'tConnectGatewayId:this.dcgwId,originalIP:n.originalIP,translationIP:n.translatio' +
                    'nIP}).then(function(e){t(null,{list:e,originalList:a.clone(e)})},t)},save:functi' +
                    'on(){var e=this.generateSetData();return e.addData.length||e.deleteData.length||' +
                    'e.modifyData.length?s.SetLocalIPTranslationAclRule({vpcId:this.vpcId,directConne' +
                    'ctGatewayId:this.dcgwId,originalIP:this.natItem.originalIP,translationIP:this.na' +
                    'tItem.translationIP,setData:JSON.stringify(e)}):Promise.resolve()}})),i.exports=' +
                    'p}),define("modules/vpc/dcGw/nat/NatList",function(e,t,i){var n=e("appUtil"),a=e' +
                    '("qccomponent"),s=e("./validator"),d=new s,o=a.extend({$tpl:'    <div class="nat' +
                    '-wrap" data-nat-wrap style="margin-right: 20px;">        <div class="tc-action-g' +
                    'rid">            <div class="tb-title-wrap"><span class="tb-title">{{title}}</sp' +
                    'an></div>            <div class="justify-grid">                <div class="col">' +
                    '                    <button type="button" class="tc-15-btn m" b-if="!isEdit" b-o' +
                    'n-click="_add()"><b class="icon-add"></b>新增</button>                </div>      ' +
                    '          <div class="col">                    <qc-search b-if="!isEdit" placeho' +
                    'lder="{{placeholder}}" search="{{search.bind(this)}}"></qc-search>              ' +
                    '  </div>            </div>        </div>        <div class="table-without-hd ip-' +
                    'mapping-tb">            <nat-grid b-ref="grid"  b-with="gridOpts" dc-gw="{{$data' +
                    '}}"                      on-edit-state-change="{{onEditStateChange.bind(this)}}"' +
                    '                      vpc-id="{{unVpcId}}" dcgw-id="{{directConnectGatewayId}}">' +
                    '</nat-grid>        </div>    </div>',$afterInit:function(){o.__super__.$afterIni' +
                    't.call(this),this.$watch("[directConnectGatewayId, unVpcId]",function(e){setTime' +
                    'out(function(){this.$refs.grid.listFn({})}.bind(this),0)})},refresh:function(e){' +
                    'this.$refs.grid.refresh(e)},search:function(e){this.$refs.grid.listFn({page:1,se' +
                    'archKey:e.trim()})},_add:function(){var e=this,t=this.$refs.grid;e.isEdit||this.' +
                    'add().then(function(i){e.refresh(function(){var n=e.getNewIndex(i,e.list);(n>=0|' +
                    '|n<t.list.length)&&t.toggleNatGroup(n,!0)})},function(){})},getNewIndex:function' +
                    '(e,t){return 0},add:function(){return Promise.reject("实现该方法已新建 nat 规则")},onEditS' +
                    'tateChange:function(e){this.$set("isEdit",e)}},{defaults:{title:"标题 - title",pla' +
                    'ceholder:"搜索备注内容",MAXNATSIZE:1e3}}),c=a.getComponent("grid-view").extend({$after' +
                    'Init:function(){var e=this;if(c.__super__.$afterInit.call(this),!this.expandable' +
                    '){var t=this.colums.slice();t.splice(0,1),this.setColums(t)}this.$on("gridUpdate' +
                    '",function(){this.list.forEach(function(t,i){e.toggleNatGroup(i,!1),$(e.$el).fin' +
                    'd("tr[data-expand]").remove()})})},editWording:"修改IP映射",delTipsTitle:"确认删除?",del' +
                    'TipsContent:"",expandTpl:'    <div>        <acl-grid b-ref="grid" b-if="!isEdit"' +
                    '                  on-update="{{onUpdate.bind(this)}}"                  vpc-id="{' +
                    '{vpcId}}" dcgw-id="{{dcgwId}}" nat-item="{{item}}"></acl-grid>        <div b-if=' +
                    '"isEdit">            <acl-editor b-ref="editor" do-save="{{doSave.bind(this)}}" ' +
                    '                       vpc-id="{{vpcId}}" dcgw-id="{{dcgwId}}" nat-item="{{item}' +
                    '}"></acl-editor>            <div style="padding: 1.5em">                <button ' +
                    'type="button" class="tc-15-btn m {{$pending ? \'disabled\': \'\'}}" b-on="{click' +
                    ': save}">保存</button>                <button type="button" class="tc-15-btn m wea' +
                    'k" b-on-click="cancel()">取消</button>            </div>        </div>    </div>',' +
                    'getCellContent:function(e,t,i){return"item1"==i.name?this.item1Tpl:"item2"==i.na' +
                    'me?this.item2Tpl:void 0},changeEditState:function(e){this.$set("isEdit",e),this.' +
                    'onEditStateChange&&this.onEditStateChange(e)},action:function(e,t,i){var n=this;' +
                    'if(!this.isEdit){n.changeEditState(!0);var a=this[e];switch(e){case"editItem":th' +
                    'is.foldAll();break;case"editRule":}a&&a.call(this,t,i).then(function(){n.changeE' +
                    'ditState(!1),n.refresh()},function(){n.changeEditState(!1)})}},_checkDel:functio' +
                    'n(e,t){t=t||{};var i=this.$root;return function(n){if(!i.isEdit){var s=new(a.get' +
                    'Component("popup-confirm"))({$data:$.extend({trigger:"",title:i.delTipsTitle,con' +
                    'tent:i.delTipsContent,style:"width: 320px;",hideDestroy:!0},t),handler:n.target,' +
                    'onConfirm:function(){i.del(e).then(function(){i.refresh()},function(){})}});s.sh' +
                    'ow()}}},editItem:function(e){return Promise.reject("实现该方法以编辑 Nat 规则")},editRule:' +
                    'function(e,t){var i=this;return new Promise(function(e,n){i.toggleNatGroup(t,!0)' +
                    ',i.curExpand?i.curExpand.edit(function(t){t?e():n()}):n("编辑的 nat 规则异常")})},del:f' +
                    'unction(e){return Promise.reject("实现该方法以删除 Nat 规则")},getRule:function(e){return ' +
                    'Promise.reject("实现该方法以获取 acl 规则数据")},foldAll:function(e){this.list.forEach(funct' +
                    'ion(t,i){t.$isOpen&&i!==e&&this.toggleNatGroup(i,!1)}.bind(this))},toggleNatGrou' +
                    'p:function(e,t){if(this.expandable){var i=this.list[e],s=this,o=$(s.$refs.body).' +
                    'children().children("tr[data-index="+e+"]"),c=$('<tr data-expand><td class="sub-' +
                    'table-nat-wrap" colspan="'+s.colums.length+'"></td></tr>');if(t="boolean"==typeo' +
                    'f t?t:!i.$isOpen,this.foldAll(e),t!=i.$isOpen&&!i.$isEditing)if(this.updateItem(' +
                    'e,{$isOpen:t}),t){var r=new a(s.expandTpl,{$context:s.$context,$data:{vpcId:s.vp' +
                    'cId,dcgwId:s.dcgwId,item:i,regionId:n.getRegionId(),otherEditing:s.editing},_cb:' +
                    'function(){},onUpdate:function(){this.$refs.grid.refresh(),this.$refs.editor&&th' +
                    'is.$refs.editor.refresh()},doSave:function(){var e=this,t=this.$refs.editor;e.$s' +
                    'et("$pending",!0),t.save().then(function(){e.$set("$pending",!1),r._cb(!0),r.onU' +
                    'pdate()},function(){e.$set("$pending",!1)})},save:function(e){var t=this,i=this.' +
                    '$refs.editor;this.$pending||(i?(d.btn=e.currentTarget,d.validateAll(i.$el)&&t.do' +
                    'Save()):r._cb(!0))},cancel:function(){var e=this.$refs.editor;e?e.cancel().then(' +
                    'function(){r._cb(!1)}):r._cb(!1)},edit:function(e){this.$set("isEdit",!0),i.$isE' +
                    'diting=!0,e=e||function(){},this._cb=function(){this.$set("isEdit",!1),i.$isEdit' +
                    'ing=!1,e()}}});s.curExpand=r,c.find("td").append(r.$el),o.after(c)}else c=o.next' +
                    '("[data-expand]"),c.find("td").slideDown("slow",function(){c.remove()})}},events' +
                    ':{"click tr[data-index]":function(e){!this.isEdit&&$(e.currentTarget).closest('[' +
                    'data-role="grid-view"],[data-role="grid-editor"]').is(this.$el)&&("A"==e.target.' +
                    'tagName||$(e.target).parents("a").length||this.toggleNatGroup($(e.currentTarget)' +
                    '.attr("data-index")))}}},{defaults:{showHeader:!1,hasFirst:!1,autoMaxHeight:!1,e' +
                    'xpandable:!0,canEditAcl:!0,colums:[{name:"",width:30,tdTpl:"    <i b-if=\"!isEdi' +
                    't\" class=\"cdb-monitor-{{item.$isOpen ? 'open' : 'close'}}\"></i>"},{name:"item' +
                    '1",tdTpl:"    <span class=\"text-overflow\">请传入内容模板 'itemTpl' | item: {{item | j' +
                    'son}}</span>"},{name:"item2",tdTpl:""},{name:"操作",width:220,tdTpl:'    <a href="' +
                    'javascript:;" class="{{isEdit ? \'disable\' : \'\'}}" b-on-click="action(\'editI' +
                    'tem\', item, $parent.$index)">{{editWording}}</a>    <a href="javascript:;" clas' +
                    's="{{isEdit ? \'disable\' : \'\'}}" b-if="canEditAcl" b-on-click="action(\'editR' +
                    'ule\', item, $parent.$index)">编辑ACL规则</a>    <a href="javascript:;" class="{{isE' +
                    'dit ? \'disable\' : \'\'}}" b-on="{click: _checkDel(item)}">删除</a>'}]}});o.tag("' +
                    'nat-grid",c),i.exports=o}),define("modules/vpc/dcGw/nat/srcipport/add",function(' +
                    'e,t,i){var n=e("dialog"),a=e("modules/vpc/dcGw/dcNatModel"),s=e("qccomponent"),d' +
                    '=e("../validator"),o=e("appUtil"),c=e("models/manager"),r=new d;i.exports=functi' +
                    'on(e,t){var i=!!t;if(i){t=o.clone(t);var d=t.translationIPPool}return new Promis' +
                    'e(function(o,l){var p=new s('    <div class="tc-15-list-wrap">        <form acti' +
                    'on="javascript:;">            <div class="form-wrap">                <ul class="' +
                    'form-list">                    <li>                        <div aria-required="f' +
                    'alse" class="form-label">                        <label for="">映射IP池</label></di' +
                    'v>                        <span class="form-input">                            <' +
                    'div class="tc-15-input-text-wrap">                                <input type="t' +
                    'ext" autofocus data-validate="ipPool" b-model="item.translationIPPool" placehold' +
                    'er="IP或IP段" class="tc-15-input-text m">                            </div>       ' +
                    '                 </span>                    </li>                    <li>       ' +
                    '                 <div class="form-label">                            <label for=' +
                    '"">备注</label>                        </div>                        <span class="' +
                    'form-input">                            <input type="text" data-validate="desc" ' +
                    'b-model="item.description" placeholder="25个字符以内，可不填" class="tc-15-input-text m" ' +
                    'maxlength="25">                            <div class="tc-15-input-tips">还能输入{{2' +
                    '5 - (item.description.length || 0)}}个字符</div>                        </span></li' +
                    '>                </ul>            </div>        </form>    </div>',{$data:{item:' +
                    't||{}}});n.create(p.$el,540,"",{title:i?"修改映射IP池":"新增映射IP池",preventResubmit:!0,d' +
                    'efaultCancelCb:function(){l()},button:{"确定":function(t){if(r.btn=t,!r.validateAl' +
                    'l(p.$el))return void n.toggleBtnDisable(!1);var s=p.item;(i?a.ModifyLocalSourceI' +
                    'PPortTranslationNatRule({vpcId:e.unVpcId,directConnectGatewayId:e.directConnectG' +
                    'atewayId,oldTranslationIPPool:d,translationIPPool:s.translationIPPool,descriptio' +
                    'n:s.description}):a.CreateLocalSourceIPPortTranslationNatRule({vpcId:e.unVpcId,d' +
                    'irectConnectGatewayId:e.directConnectGatewayId,localSourceIPPortTranslation:[{tr' +
                    'anslationIPPool:s.translationIPPool,description:s.description}]}).then(function(' +
                    'e){return e=e.data||{},e.taskId?c.checkTask({taskId:e.taskId}):e})).then(functio' +
                    'n(){o(),n.hide()},function(){n.toggleBtnDisable(!1)})}}})})}}),define("modules/v' +
                    'pc/dcGw/nat/srcipport/IPPortConfig",function(e,t,i){var n=e("../NatList"),a=e("a' +
                    'ppUtil"),s=e("modules/vpc/dcGw/dcNatModel"),d=e("qccomponent"),o=e("../AclGrid")' +
                    ',c=e("../AclEditor"),r=e("./add"),l=e("./showConflict"),p=e("tips"),u=e("models/' +
                    'manager"),v=d.extend({$tpl:'    <div class="nat-convert-box">        <div class=' +
                    '"mod-defination show-map">            <div class="dt-text">本端源IP端口转换(Local Sourc' +
                    'e IP Port Translation)</div>            <div class="dd-text">                <p>' +
                    '将私有网络内的源IP和端口映射为指定IP池内随机端口主动访问专线对端网络，回包不受影响。                    <a href="javascr' +
                    'ipt:;"  qc-popover="<i class=\'pic_3\'></i>">示意图</a></p>                <p>注：当本端' +
                    'IP转换和本端源IP端口转换冲突时，优先匹配本端IP转换</p>            </div>        </div>        <nat-acl' +
                    '-list b-with="data"></nat-acl-list>    </div>'});v.tag("nat-acl-list",n.extend({' +
                    '$afterInit:function(){n.prototype.$afterInit.call(this);var e=this.$refs.grid,t=' +
                    'e.colums.slice();t.splice(2,1),e.setColums(t)},add:function(){return r(this.$dat' +
                    'a)},gridOpts:{emptyTips:"您还未配置映射IP池",item1Tpl:'    <i class="icon-cdb-table"></i' +
                    '>    <span>        <label class="text-label">            映射IP池：        </label> ' +
                    '       <label style="margin-right: 1em">{{item.translationIPPool}}</label>      ' +
                    '  <label class="text-label" b-if="item.description">            ({{item.descript' +
                    'ion}})        </label>    </span>',delTipsTitle:"确认删除此IP池?",delTipsContent:"删除IP' +
                    '池将一同删除此规则对应的ACL策略",editWording:"修改映射IP池",editItem:function(e){return r(this.dcGw' +
                    ',e)},del:function(e){var t=this;return s.DeleteLocalSourceIPPortTranslationNatRu' +
                    'le({vpcId:this.vpcId,directConnectGatewayId:this.dcgwId,localSourceIPPortTransla' +
                    'tion:[{translationIPPool:e.translationIPPool}]}).then(function(e){return e=e.dat' +
                    'a||{},t.showLoading(),e.taskId?u.checkTask({taskId:e.taskId}).then(function(e){r' +
                    'eturn t.hideLoading(),e},function(){throw t.hideLoading(),err}):e})},getData:fun' +
                    'ction(e,t){var i=e.count||this.count,n=e.page||this.page,a={limit:i,offset:i*(n-' +
                    '1),vpcId:this.vpcId,directConnectGatewayId:this.dcgwId};e.searchKey&&(a.descript' +
                    'ion=e.searchKey),s.DescribeLocalSourceIPPortTranslationNatRule(a).then(function(' +
                    'i){t(null,{searchKey:e.searchKey,list:i.data,totalNum:i.totalNum})})}}},{default' +
                    's:{title:"映射IP池"}})),v.tag("acl-grid",o.extend({delTipsTitle:"确认删除此网络ACL规则？",del' +
                    'TipsContent:"",delItem:function(e){return s.DeleteLocalSourceIPPortTranslationAc' +
                    'lRule({vpcId:this.vpcId,directConnectGatewayId:this.dcgwId,translationIPPool:thi' +
                    's.natItem.translationIPPool,aclRules:[e.aclRuleId]})},_saveItem:function(e){retu' +
                    'rn s.SetLocalSourceIPPortTranslationAclRule({vpcId:this.vpcId,directConnectGatew' +
                    'ayId:this.dcgwId,translationIPPool:this.natItem.translationIPPool,setData:JSON.s' +
                    'tringify(e)},{tipErr:!1})},saveItem:function(e,t){var i=this;return this._saveIt' +
                    'em({modifyData:[{aclRuleId:e.aclRuleId,protocol:e._protocol,sourceCidr:e._source' +
                    'Cidr,sourcePort:e._sourcePort,destinationCidr:e._destinationCidr,destinationPort' +
                    ':e._destinationPort}]})["catch"](function(e){var n=e.data;throw 29110==n.cgwerro' +
                    'rCode?l(JSON.parse(n.message.split("conflict")[1]),i.natItem,{dcgwId:i.dcgwId,vp' +
                    'cId:i.vpcId}).then(function(e){if(e.deleteData.length){var n=e.deleteData[0];ret' +
                    'urn i.$set("$isEdit",!1),i.del(n)}if(e.modifyData.length){var a=e.modifyData[0],' +
                    's={_protocol:a.protocol,_sourceCidr:a.sourceCidr,_sourcePort:a.sourcePort,_desti' +
                    'nationCidr:a.destinationCidr,_destinationPort:a.destinationPort};return i.update' +
                    'Item(t,s),i.$set("$isEdit",!1),i.save(i.list[t],t)}}):p.error(n.message),e})},ge' +
                    'tData:function(e,t){var i=this,n=i.natItem;this.showLoading(),t=function(e,t){i.' +
                    'setData(t),i.hideLoading()},s.DescribeLocalSourceIPPortTranslationAclRule({vpcId' +
                    ':this.vpcId,directConnectGatewayId:this.dcgwId,translationIPPool:n.translationIP' +
                    'Pool}).then(function(e){e.push({protocol:"ALL",sourceCidr:"ALL",sourcePort:"ALL"' +
                    ',destinationCidr:"ALL",destinationPort:"ALL",_act:"拒绝"}),t(null,{list:e})},t)}},' +
                    '{})),v.tag("acl-editor",c.extend({getData:function(e,t){var i=this,n=i.natItem;t' +
                    'his.showLoading(),t=function(e,t){i.setData(t),i.hideLoading()},s.DescribeLocalS' +
                    'ourceIPPortTranslationAclRule({vpcId:this.vpcId,directConnectGatewayId:this.dcgw' +
                    'Id,translationIPPool:n.translationIPPool}).then(function(e){t(null,{list:e,origi' +
                    'nalList:a.clone(e)})},t)},getIndexByItem:function(e){var t;return this.list.some' +
                    '(function(i,n){return e.aclRuleId&&e.aclRuleId==i.aclRuleId||!e.aclRuleId&&e.pro' +
                    'tocol==i.protocol&&e.sourcePort==i.sourcePort&&e.destinationCidr==i.destinationC' +
                    'idr&&e.destinationPort==i.destinationPort?(t=n,
!0):void 0}),t},_save:function(e' +
                    '){var t=this;return s.SetLocalSourceIPPortTranslationAclRule({vpcId:this.vpcId,d' +
                    'irectConnectGatewayId:this.dcgwId,translationIPPool:this.natItem.translationIPPo' +
                    'ol,setData:JSON.stringify(e)},{tipErr:!1})["catch"](function(e){var i=e.data;thr' +
                    'ow 29110==i.cgwerrorCode?l(JSON.parse(i.message.split("conflict")[1]),t.natItem,' +
                    '{dcgwId:t.dcgwId,vpcId:t.vpcId}).then(function(e){if(e.deleteData.length){var i=' +
                    'e.deleteData[0],n=t.getIndexByItem(i);t.preRemove(n)}else if(e.modifyData.length' +
                    '){var a=e.modifyData[0],n=t.getIndexByItem(e.originAcl);t.updateItem(n,a)}t.doSa' +
                    've()}):p.error(i.message),e})},save:function(){var e=this.generateSetData();retu' +
                    'rn e.addData.length||e.deleteData.length||e.modifyData.length?this._save(e):Prom' +
                    'ise.resolve()}})),i.exports=v}),define("modules/vpc/dcGw/nat/srcipport/showConfl' +
                    'ict",function(e,t,i){var n=e("qccomponent"),a=e("dialog"),s=e("../validator"),d=' +
                    'e("modules/vpc/dcGw/dcNatModel"),o=e("appUtil"),c=n.extend({}),r=new s;i.exports' +
                    '=function(e,t,i){var n=e[0],s=o.clone(n.acl);return n.curNat=t,n.duplicateNat=n.' +
                    'duplicateAcl[0],n.duplicateAcls=[],n.duplicateAcl.forEach(function(e){e.translat' +
                    'ionIPPool==n.duplicateNat.translationIPPool&&n.duplicateAcls.push(e)}),new Promi' +
                    'se(function(e,t){var o=new c('    <div class="save-rule">        <strong>含有失效的规则' +
                    '</strong>        <p>您正在编辑的规则有一部分和其它IP池的映射规则发生冲突，请修改后继续保存</p>        <div class="' +
                    'layer-nat-table">            <div class="conflict-item conflicting" style="margi' +
                    'n: 1em 0">                <div class="tc-15-table-panel table-without-hd ip-mapp' +
                    'ing-tb">                    <div class="tc-15-table-fixed-body tc-15-table-rowho' +
                    'ver">                        <table class="tc-15-table-box">                    ' +
                    '        <colgroup>                                <col style="width:30px;">     ' +
                    '                           <col style="width:30px;">                            ' +
                    '    <col>                                <col>                                <c' +
                    'ol>                                <col>                                <col>   ' +
                    '                             <col>                                <col>         ' +
                    '                       <col>                            </colgroup>             ' +
                    '               <tbody>                            <tr class="tr-hover">         ' +
                    '                       <td>                                    <div>            ' +
                    '                            <span>1</span>                                    </' +
                    'div>                                </td>                                <td>   ' +
                    '                                 <div>                                        <i' +
                    ' class="cdb-monitor-open"></i>                                    </div>        ' +
                    '                        </td>                                <td colspan="7">   ' +
                    '                                 <div>                                        <i' +
                    ' class="icon-cdb-table"></i>                                            <span cl' +
                    'ass="text-overflow">映射IP池：{{info.duplicateNat.translationIPPool}}<span class="te' +
                    'xt-label" b-if="info.duplicateNat.description">({{info.duplicateNat.description}' +
                    '})</span>                                        </span>                        ' +
                    '            </div>                                </td>                         ' +
                    '       <td class="text-right">                                    <div>         ' +
                    '                           </div>                                </td>          ' +
                    '                  </tr>                            <tr>                         ' +
                    '       <td colspan="10" class="sub-table-nat-wrap">                             ' +
                    '       <div class="tc-15-table-panel">                                        <d' +
                    'iv class="tc-15-table-fixed-head">                                            <t' +
                    'able class="tc-15-table-box">                                                <co' +
                    'lgroup>                                                    <col style="width:113' +
                    'px">                                                    <col style="width:80px">' +
                    '                                                    <col>                       ' +
                    '                             <col>                                              ' +
                    '      <col>                                                    <col>            ' +
                    '                                        <col>                                   ' +
                    '                 <col>                                                </colgroup' +
                    '>                                                <thead>                        ' +
                    '                        <tr>                                                    ' +
                    '<th class="text-right">                                                        <' +
                    'div>序号</div>                                                    </th>           ' +
                    '                                         <th>                                   ' +
                    '                     <div>策略</div>                                              ' +
                    '      </th>                                                    <th>             ' +
                    '                                           <div>协议</div>                        ' +
                    '                            </th>                                               ' +
                    '     <th>                                                        <div>源IP</div> ' +
                    '                                                   </th>                        ' +
                    '                            <th>                                                ' +
                    '        <div>源端口</div>                                                    </th> ' +
                    '                                                   <th>                         ' +
                    '                               <div>目的IP</div>                                  ' +
                    '                  </th>                                                    <th> ' +
                    '                                                       <div>目的端口</div>          ' +
                    '                                          </th>                                 ' +
                    '                   <th>                                                        <' +
                    'div></div>                                                    </th>             ' +
                    '                                   </tr>                                        ' +
                    '        </thead>                                            </table>            ' +
                    '                            </div>                                        <div c' +
                    'lass="tc-15-table-fixed-body tc-15-table-rowhover">                             ' +
                    '               <table class="tc-15-table-box">                                  ' +
                    '              <colgroup>                                                    <col' +
                    ' style="width:113px">                                                    <col st' +
                    'yle="width:80px">                                                    <col>      ' +
                    '                                              <col>                             ' +
                    '                       <col>                                                    ' +
                    '<col>                                                    <col>                  ' +
                    '                                  <col>                                         ' +
                    '       </colgroup>                                                <tbody>       ' +
                    '                                         <tr b-repeat="acl in info.duplicateAcls' +
                    '">                                                    <td class="ordinal">      ' +
                    '                                                  <div>{{$index + 1}}</div>     ' +
                    '                                               </td>                            ' +
                    '                        <td>                                                    ' +
                    '    <div>允许</div>                                                    </td>      ' +
                    '                                              <td>                              ' +
                    '                          <div>{{acl.protocol}}</div>                           ' +
                    '                         </td>                                                  ' +
                    '  <td>                                                        <div>{{acl.sourceC' +
                    'idr}}</div>                                                    </td>            ' +
                    '                                        <td>                                    ' +
                    '                    <div>{{acl.sourcePort}}</div>                               ' +
                    '                     </td>                                                    <t' +
                    'd>                                                        <div>{{acl.destination' +
                    'Cidr}}</div>                                                    </td>           ' +
                    '                                         <td>                                   ' +
                    '                     <div>{{acl.destinationPort}}</div>                         ' +
                    '                           </td>                                                ' +
                    '    <td> </td>                                                </tr>             ' +
                    '                                   </tbody>                                     ' +
                    '       </table>                                        </div>                   ' +
                    '                 </div>                                </td>                    ' +
                    '        </tr>                            <tr class="tr-hover">                  ' +
                    '              <td>                                    <div>                     ' +
                    '                   <span>2</span>                                    </div>     ' +
                    '                           </td>                                <td>            ' +
                    '                        <div>                                        <i class="c' +
                    'db-monitor-open"></i>                                    </div>                 ' +
                    '               </td>                                <td colspan="7">            ' +
                    '                        <div>                                        <i class="i' +
                    'con-cdb-table"></i>                                        <span class="text-ove' +
                    'rflow">映射IP池：{{info.curNat.translationIPPool}}<span class="text-label" b-if="inf' +
                    'o.curNat.description">({{info.curNat.description}})</span></span>               ' +
                    '                     </div>                                </td>                ' +
                    '                <td class="text-right">                                    <div>' +
                    '                                    </div>                                </td> ' +
                    '                           </tr>                            <tr>                ' +
                    '                <td colspan="10" class="sub-table-nat-wrap">                    ' +
                    '                <div class="tc-15-table-panel">                                 ' +
                    '       <div class="tc-15-table-fixed-head">                                     ' +
                    '       <table class="tc-15-table-box">                                          ' +
                    '      <colgroup>                                                    <col style="' +
                    'width:113px">                                                    <col style="wid' +
                    'th:80px">                                                    <col>              ' +
                    '                                      <col>                                     ' +
                    '               <col>                                                    <col>   ' +
                    '                                                 <col>                          ' +
                    '                          <col>                                                <' +
                    '/colgroup>                                                <thead>               ' +
                    '                                 <tr>                                           ' +
                    '         <th class="text-right">                                                ' +
                    '        <div>序号</div>                                                    </th>  ' +
                    '                                                  <th>                          ' +
                    '                              <div>策略</div>                                     ' +
                    '               </th>                                                    <th>    ' +
                    '                                                    <div>协议</div>               ' +
                    '                                     </th>                                      ' +
                    '              <th>                                                        <div>源' +
                    'IP</div>                                                    </th>               ' +
                    '                                     <th>                                       ' +
                    '                 <div>源端口</div>                                                 ' +
                    '   </th>                                                    <th>                ' +
                    '                                        <div>目的IP</div>                         ' +
                    '                           </th>                                                ' +
                    '    <th>                                                        <div>目的端口</div> ' +
                    '                                                   </th>                        ' +
                    '                            <th>                                                ' +
                    '        <div>操作</div>                                                    </th>  ' +
                    '                                              </tr>                             ' +
                    '                   </thead>                                            </table> ' +
                    '                                       </div>                                   ' +
                    '     <div class="tc-15-table-fixed-body tc-15-table-rowhover">                  ' +
                    '                          <table class="tc-15-table-box conflict-b">            ' +
                    '                                    <colgroup>                                  ' +
                    '                  <col style="width:113px">                                     ' +
                    '               <col style="width:80px">                                         ' +
                    '           <col>                                                    <col>       ' +
                    '                                             <col>                              ' +
                    '                      <col>                                                    <' +
                    'col>                                                    <col>                   ' +
                    '                             </colgroup>                                        ' +
                    '        <tbody>                                                <tr>             ' +
                    '                                       <td class="ordinal">                     ' +
                    '                                   <div>1</div>                                 ' +
                    '                   </td>                                                    <td>' +
                    '                                                        <div>允许</div>           ' +
                    '                                         </td>                                  ' +
                    '                  <td>                                                        <d' +
                    'iv class="form-unit">                                                           ' +
                    ' <select class="tc-15-select m" b-model="acl.protocol" value="{{info.acl.protoco' +
                    'l}}" disabled?="_remove">                                                       ' +
                    '         <option value="TCP">TCP</option>                                       ' +
                    '                         <option value="UDP">UDP</option>                       ' +
                    '                                         <option value="ALL">ALL</option>       ' +
                    '                                                     </select>                  ' +
                    '                                      </div>                                    ' +
                    '                </td>                                                    <td>   ' +
                    '                                                     <div class="form-unit">    ' +
                    '                                                        <input type="text" class' +
                    '="tc-15-input-text m ip-input" data-validate="srcCidr"                          ' +
                    '                                         disabled?="_remove" b-model="acl.source' +
                    'Cidr"                                                                   value="{' +
                    '{info.acl.sourceCidr}}" placeholder="IP或CIDR">                                  ' +
                    '                      </div>                                                    ' +
                    '</td>                                                    <td>                   ' +
                    '                                     <div class="form-unit">                    ' +
                    '                                        <input type="text" class="tc-15-input-te' +
                    'xt m ip-input" data-validate="srcPort"                                          ' +
                    '                         disabled?="_remove"                                    ' +
                    '                               value="{{info.acl.sourcePort}}" b-model="acl.sour' +
                    'cePort" placeholder="0-65535">                                                  ' +
                    '      </div>                                                    </td>           ' +
                    '                                         <td>                                   ' +
                    '                     <div class="form-unit">                                    ' +
                    '                        <input type="text" class="tc-15-input-text m ip-input" d' +
                    'ata-validate="destCidr"                                                         ' +
                    '          disabled?="_remove" value="{{info.acl.destinationCidr}}"              ' +
                    '                                                     b-model="acl.destinationCid' +
                    'r" placeholder="IP或CIDR">                                                       ' +
                    ' </div>                                                    </td>                ' +
                    '                                    <td>                                        ' +
                    '                <div class="form-unit">                                         ' +
                    '                   <input type="text" class="tc-15-input-text m ip-input" data-v' +
                    'alidate="destPort"                                                              ' +
                    '     disabled?="_remove" b-model="acl.destinationPort"                          ' +
                    '                                         value="{{info.acl.destinationPort}}" pl' +
                    'aceholder="0-65535">                                                        </di' +
                    'v>                                                    </td>                     ' +
                    '                               <td>                                             ' +
                    '           <div>                                                            <a h' +
                    'ref="javascript:;" b-on-click="toggleRemove()">{{_remove ? \'恢复删除\' : \'删除\'}}</' +
                    'a>                                                        </div>                ' +
                    '                                    </td>                                       ' +
                    '         </tr>                                                </tbody>          ' +
                    '                                  </table>                                      ' +
                    '  </div>                                    </div>                              ' +
                    '  </td>                            </tr>                            </tbody>    ' +
                    '                    </table>                    </div>                </div>    ' +
                    '            <!--<div class="conflict-line">-->                    <!--<div class' +
                    '="conflict-line-text">冲突</div>-->                    <!--<div class="conflict-li' +
                    'ne-triangle"></div>-->                <!--</div>-->            </div>        </d' +
                    'iv>    </div>',{$afterInit:function(){var e,t=this,a={vpcId:i.vpcId,directConnec' +
                    'tGatewayId:i.dcgwId,translationIPPool:n.duplicateNat.translationIPPool,alcRuleId' +
                    's:n.duplicateAcls.map(function(e){return e.aclRuleId})};e=d.DescribeLocalSourceI' +
                    'PPortTranslationAclRule(a),e.then(function(e){t.$set("info.duplicateAcls",e)})},' +
                    '$data:{info:n},toggleRemove:function(){this.$set("_remove",!this._remove)}});a.c' +
                    'reate(o.$el,950,"",{title:"保存映射规则",preventResubmit:!0,defaultCancelCb:function()' +
                    '{t()},button:{"确定":function(t){r.btn=t;var i=o.acl;if(i.aclRuleId=o.info.acl.acl' +
                    'RuleId,!r.validateAll(o.$el))return void a.toggleBtnDisable();var n={originAcl:s' +
                    ',deleteData:[],modifyData:[]};o._remove?n.deleteData.push(i):n.modifyData.push(i' +
                    '),a.hide(),e(n)}}})})}}),define("modules/vpc/dcGw/nat/validator",function(e,t,i)' +
                    '{var n=e("widget/baseInfo/validator"),a=e("appUtil"),s=n.extend({errClass:"is-er' +
                    'ror"}),d=function(e){var t=e.split("/"),i=1*t[1];if(!e)return!0;if(a.isValidIp(t' +
                    '[0])){if(1==t.length)return!0;if(2==t.length&&i>=0&&32>=i)return!0}return!1},o=f' +
                    'unction(e){var t=e.split("-");if(!e)return!0;var i=1*t[0],n=1*t[1];if(i>=0&&6553' +
                    '5>=i){if(1==t.length)return!0;if(2==t.length&&n>=0&&65535>=n&&n>=i)return!0}retu' +
                    'rn!1};s.rules=$.extend({},n.rules,{ipPool:function(e){var t=e.split("-").map(fun' +
                    'ction(e){return e.trim()}),i="映射IP池支持IP、IP段，如：<br>IP：10.1.0.1<br>IP段：10.1.1.1-10' +
                    '.1.1.255，IP段前3位需要相同";if(a.isValidIp(e))i=!1;else if(2==t.length&&a.isValidIp(t[0' +
                    '])&&a.isValidIp(t[1])){var n=t[0].split("."),s=t[1].split("."),d=1*n.pop(),o=1*s' +
                    '.pop();n.join()==s.join()&&o>=d&&(i="")}return i},desc:function(e){return e.leng' +
                    'th>25?"请输入25个字符以内, 可不填":void 0},srcCidr:function(e){return d(e)?void 0:"请输入IP或CI' +
                    'DR, 为空表示所有IP"},srcPort:function(e){return o(e)?void 0:"请输入端口或端口范围, 为空表示所有端口。如22-' +
                    '45或32"},destCidr:function(e){return d(e)?void 0:"请输入IP或CIDR, 为空表示所有IP"},destPort' +
                    ':function(e){return o(e)?void 0:"请输入端口或端口范围, 为空表示所有端口。如22-45或32"}}),i.exports=s}' +
                    '),define("modules/vpc/dcGw/util",function(e,t,i){var n={IP_SRC:"localIPTranslati' +
                    'on",IP_DEST:"peerIPTranslation",IPPORT_SRC:"localSourceIPPortTranslation",IPPORT' +
                    '_DEST:"localDestinationIPPortTranslation"},a=[[n.IP_SRC,n.IP_DEST,n.IPPORT_SRC,n' +
                    '.IPPORT_DEST]];i.exports={types:n,typeList:Object.keys(n).map(function(e){return' +
                    ' n[e]}),typeGroups:a,getLockedStatus:function(e){var t,i={},n={},s={};return a.s' +
                    'ome(function(t,a){var s=!1;t.forEach(function(t){var n=e[t];n&&n.length&&(i[t]=!' +
                    '0,s=!0)}),s&&(n[a]=!0)}),t=Object.keys(n),t.length?(a.forEach(function(e,t){n[t]' +
                    '||e.forEach(function(e){s[e]=!0})}),s):s}}}),define("modules/vpc/dcRecord/dcReco' +
                    'rd",function(e,t,i){var n=e("pageManager"),a=e("manager"),s=e("dialog"),d=e("con' +
                    'stants"),o=e("models/directconnect"),c=e("qccomponent"),r=c.getComponent("grid-v' +
                    'iew"),l=e("widget/pubsubhub/pubsubhub"),p=e("./detail"),u=e("../dc/add"),v=e("..' +
                    '/dc/dcUtil"),f=e("widget/fieldsManager/gridView"),m=e("../dc/deal/deal"),h=c.ext' +
                    'end({add:function(){a.isPrepaid().then(function(){u.render()},function(){s.creat' +
                    'e('    <p>        当前为后付费月结的购买方式，需要切换到预付费购买方式才能购买专线，查看        <a target="_blank" ' +
                    'href="https://www.qcloud.com/doc/product/213/5041">详细说明</a>    </p>',"500","",{t' +
                    'itle:"提示",button:{"切换到预付费":function(){location.href="http://manage.qcloud.com/sh' +
                    'oppingcart/changeguide.php"}}})})},fieldsetting:function(){this.$refs.grid.showF' +
                    'ieldsManager()},search:function(e){var t=this.$refs.grid;t.getData({searchKey:e}' +
                    ')}});h.tag("dcrecord-grid",r.extend({$mixins:[f("dc_record_grid_fields")],getDat' +
                    'a:function(e,t){var i=this;t=function(e,t){e||i.setData(t)},o.request({name:"get' +
                    'ApplyInfo",success:function(i){var n=i.filter(function(t){if(v.fixParam(t),e.sea' +
                    'rchKey){var i=new RegExp(e.searchKey);if(!i.test(t.dcName1)&&!i.test(t.dcId1))re' +
                    'turn}return e.modelFilter?e.modelFilter.indexOf(t.model+"")>-1:!0}),a=n.filter(f' +
                    'unction(e){return e.dealId}),s=n.map(function(e){return e.dealId}).filter(functi' +
                    'on(e){return e});s.length?o.request({name:"getDealsByCond",data:{dealIds:s},succ' +
                    'ess:function(i){i=i.deals,a.forEach(function(e){var t=i.filter(function(t){retur' +
                    'n e.dealId==t.dealId})[0];t&&(e._shouldPay=1==t.status,e._shouldPay&&(e._bigDeal' +
                    'Id=t.bigDealId))}),t(null,{list:n,totalNum:n.length,searchKey:e.searchKey})}}):t' +
                    '(null,{list:n,totalNum:n.length,searchKey:e.searchKey})},fail:function(){}})},de' +
                    'l:function(e){return function(t){if(!e.$disable&&0!=e.status&&4!=e.status&&5!=e.' +
                    'status){var i=new(c.getComponent("popup-confirm"))({$data:{trigger:"",title:"确认要' +
                    '删除该条记录? ",style:"width: 310px;",hideDestroy:!0},handler:t.target,onConfirm:funct' +
                    'ion(){o.request({name:"deleteDCApply",data:{applyId:e.applyId},success:function(' +
                    '){l.trigger(v.events.DCRECORDCHANGED)}})}});i.show()}}},deal:function(e){m.rende' +
                    'r(e)},pay:function(e){window.open("http://manage.qcloud.com/deal/dealsConfirm.ph' +
                    'p?payMode=1&dealList[]="+e.dealId)},renew:function(e){m.render(e)},filterModel:f' +
                    'unction(e){this.getData({modelFilter:e})},_updateDCStatus:function(e){o.request(' +
                    '{name:"updateApplyStatus",data:{applyId:e.applyId,status:3}})}},{defaults:{hasFi' +
                    'rst:!1,showPagination:!1,modelFilter:[{label:"单线接入",value:"0"},{label:"双线热备",val' +
                    'ue:"2"}],colums:[{name:"ID/名称",key:"dcId",tdTpl:'    <p class="text-overflow"><a' +
                    ' data-event="nav" href="/vpc/dcRecord/detail/{{item.dcId1}}">{{item.dcId1}}</a><' +
                    '/p>    <p><span class="text-overflow m-width">{{item.dcName1}}</span></p>',requi' +
                    'red:!0},{name:"IDC 所在地",key:"idcRegion"},{name:"专线提供商",key:"isp"},{name:"申请状态",k' +
                    'ey:"_status"},{name:"建设模式",key:"_construct"},{name:"接入方式",key:"model",tdTpl:'   ' +
                    ' <span class="text-overflow">        {{item.model == 0 ? \'单线接入\' : \'双线热备\'}}  ' +
                    '      <a b-if="item.model != 0" data-event="nav" href="/vpc/dcRecord/detail/{{it' +
                    'em.dcId2}}">{{item.dcId2}}</a>    </span>',thTpl:'    <grid-view-header-filter c' +
                    'ol="{{col}}" filter-options="{{modelFilter}}" change="{{filterModel.bind(this.$r' +
                    'oot)}}"></grid-view-header-filter>'},{name:"带宽",key:"_bandwidth"},{name:"类型",key' +
                    ':"dcType",hide:!0},{name:"联系人手机",key:"userPhone"},{name:"联系人Email",key:"userMail' +
                    '"},{name:"申请时间",key:"_applyTime",hide:!0},{name:"完成时间",key:"_completeTime",hide:' +
                    '!0},{name:"操作",key:"_action",minWidth:120,tdTpl:'    <a href="javascript:;" b-on' +
                    '-click="deal(item)" b-if="item.status==3 && !item._bigDealId">确认订单</a>    <a hre' +
                    'f="javascript:;" b-on-click="pay(item)" b-if="item.status==3 && item._bigDealId"' +
                    '>去支付</a>    <!--<a href="javascript:;" b-on-click="renew(item)" b-if="item._rene' +
                    'w">续费</a>-->    <a href="javascript:;" b-if="item._canDel" b-on="{click: del(ite' +
                    'm)}" class="links">删除</a>    <span b-if="item.status == 4 || item.status == 5 ||' +
                    ' item.status == 0">-</span>',required:!0}]}})),i.exports={render:function(t,i){v' +
                    'ar a=this;if("detail"===t)i?(a.isDetail=!0,p.render(i)):n.render404();else{var s' +
                    '=a.bee=new h('    <div class="manage-area">        <div class="manage-area-title' +
                    '">            <h2>申请记录</h2>        </div>        <div class="tc-15-action-panel"' +
                    '>            <button class="tc-15-btn m" b-on-click="add()">+申请专线</button>      ' +
                    '      <!--<button class="tc-15-btn weak m download"></button>-->            <but' +
                    'ton b-ref="setting" b-on-click="fieldsetting()" class="tc-15-btn weak m setting"' +
                    '></button>            <qc-search b-ref="search" search="{{search.bind(this)}}" p' +
                    'laceholder="请输入申请记录名或 ID"></qc-search>        </div>        <dcrecord-grid b-ref' +
                    '="grid" vpc-id="{{vpcId}}"></dcrecord-grid>    </div>',{$data:{maxDcSize:d.MAX_D' +
                    'C_SIZE}});this.grid=s.$refs.grid,n.appArea.html(s.$el),e("../adTip").init(s.$el)' +
                    ',this.grid.setMaxHeight(),a.isDetail=!1,this.bindEvents()}},destroy:function(){t' +
                    'his.isDetail&&p.destroy(),l.off(v.events.DCRECORDCHANGED)},bindEvents:function()' +
                    '{var e=this;l.on(v.events.DCRECORDCHANGED,function(){e.grid.refresh()})}}}),defi' +
                    'ne("modules/vpc/dcRecord/detail",function(e,t,i){var n=e("pageManager"),a=(e("co' +
                    'nstants"),e("widget/detail/detail")),s=(e("appUtil"),e("router")),d=e("models/di' +
                    'rectconnect"),o=e("widget/pubsubhub/pubsubhub"),c=e("../dc/dcUtil"),r=e("widget/' +
                    'baseInfo/baseInfo"),l=a.extend({$beforeInit:function(){l.__super__.$beforeInit.c' +
                    'all(this),this._tabs=[]},$afterInit:function(){var e=this;l.__super__.$afterInit' +
                    '.call(this);var t=new r({$data:{id:this.dcId,changeEvent:c.events.DCRECORDCHANGE' +
                    'D},showEdit:!1,detailTpl:'    <div class="tc-15-list-content">        <ul>      ' +
                    '      <li><em class="tc-15-list-tit">名称</em>                <span b-if="!isEdit"' +
                    ' class="tc-15-list-det">{{info.dcName1}}</span>                <div class="tc-15' +
                    '-input-text-wrap" b-if="isEdit">                    <input type="text" data-vali' +
                    'date="name"  class="tc-15-input-text" b-model="info.dcName1" maxlength="25">    ' +
                    '                <div class="tc-15-input-tips">还能输入{{25 - info.dcName1.length}}个字' +
                    '符，支持中文、英文、数字、下划线、分隔符"-"、小数点</div>                </div>            </li>        ' +
                    '    <li><em class="tc-15-list-tit">ID</em><span class="tc-15-list-det">{{info.dc' +
                    'Id1}}</span>            </li>            <li><em class="tc-15-list-tit">申请状态</em' +
                    '><span class="tc-15-list-det">{{info._status}}</span>            </li>          ' +
                    '  <li><em class="tc-15-list-tit">接入方式</em>                <span class="tc-15-lis' +
                    't-det">                    {{info.model == 0 ? \'单线接入\' : \'双线热备\'}}            ' +
                    '        <a b-if="info.model != 0" data-event="nav" href="/vpc/dcRecord/detail/{{' +
                    'info.dcId2}}">{{info.dcId2}}</a>                </span>            </li>        ' +
                    '    <li><em class="tc-15-list-tit">专线提供商</em><span class="tc-15-list-det">{{info' +
                    '.isp}}</span>            </li>            <li><em class="tc-15-list-tit">建设模式</e' +
                    'm><span class="tc-15-list-det">{{info._construct}}</span>            </li>      ' +
                    '      <li><em class="tc-15-list-tit">带宽</em><span class="tc-15-list-det">{{info.' +
                    '_bandwidth}}</span>            </li>            <li><em class="tc-15-list-tit">类' +
                    '型</em><span class="tc-15-list-det">{{info.dcType}}</span>            </li>      ' +
                    '      <li><em class="tc-15-list-tit">IDC所在地</em><span class="tc-15-list-det">{{i' +
                    'nfo.idcCity}} {{info.idcRegion}}</span>            </li>            <li b-if="in' +
                    'fo._applyTime">                <em class="tc-15-list-tit">申请时间</em><span class="' +
                    'tc-15-list-det">{{info._applyTime}}</span>            </li>            <li b-if=' +
                    '"info._completeTime">                <em class="tc-15-list-tit">完成时间</em><span c' +
                    'lass="tc-15-list-det">{{info._completeTime}}</span>            </li>            ' +
                    '<li b-if="info.endTime">                <em class="tc-15-list-tit">到期时间</em><spa' +
                    'n class="tc-15-list-det">{{info.endTime}}</span>            </li>            <li' +
                    '><em class="tc-15-list-tit">联系人手机</em>                <span b-if="!isEdit" class' +
                    '="tc-15-list-det">{{info.userPhone}}</span>                <div class="tc-15-inp' +
                    'ut-text-wrap" b-if="isEdit">                    <input type="text" data-validate' +
                    '="phone" class="tc-15-input-text" b-model="info.userPhone">                    <' +
                    'div class="tc-15-input-tips" data-tip></div>                </div>            </' +
                    'li>            <li><em class="tc-15-list-tit">联系人email</em>                <span' +
                    ' b-if="!isEdit" class="tc-15-list-det">{{info.userMail}}</span>                <' +
                    'div class="tc-15-input-text-wrap" b-if="isEdit">                    <input type=' +
                    '"text" data-validate="email" class="tc-15-input-text" b-model="info.userMail">  ' +
                    '                  <div class="tc-15-input-tips" data-tip></div>                <' +
                    '/div>            </li>            <li><em class="tc-15-list-tit">专线经理电话</em><spa' +
                    'n class="tc-15-list-det">{{info._operatorPhone}}</span>            </li>        ' +
                    '</ul>    </div>',getData:function(t){var i=this;return e.getDCInfo(t).then(funct' +
                    'ion(e){return 0!=e.status&&2!=e.status&&i.$set("showEdit",!0),e})},updateData:fu' +
                    'nction(e){return new Promise(function(t,i){d.request({name:"updateDCApplyInfo",d' +
                    'ata:e,success:function(e){o.trigger(c.events.DCRECORDCHANGED),t(e)},fail:i})})}}' +
                    ');e.$set("content",t.$el),e.load()},$beforeDestroy:function(){this._tabs.forEach' +
                    '(function(e){e.$destroy(!0)}),o.off(".detail")},load:function(){var e=this;this.' +
                    'getDCInfo(this.dcId).then(function(t){e.$set({dc:t,title:t.dcName1})})},getDCInf' +
                    'o:function(){var e;return function(t){return e||(e=new Promise(function(e,i){d.r' +
                    'equest({name:"getApplyInfo",data:{dcId:t},success:e,fail:i})}).then(function(t){' +
                    'var i=t[0];return c.fixParam(i),e=null,i?i:void n.render404()},function(t){throw' +
                    ' e=null,t})),e}}(),back:function(){s.navigate("/vpc/dcRecord")}});i.exports={ren' +
                    'der:function(e){var t=new l({dcId:e});n.appArea.html(t.$el),o.on(c.events.DCRECO' +
                    'RDCHANGED,function(){t.load()})},destroy:function(){o.off(c.events.DCRECORDCHANG' +
                    'ED)}}}),define("modules/vpc/DropDown",function(e,t,i){var n=e("./BeeBase"),a=e("' +
                    'appUtil"),s=n.extend({$tpl:'<div class="tc-15-select-wrap {{cls}} {{invalid ? \'' +
                    'error\' : \'\'}}"><select class="tc-15-select {{relevance ? \'relevance-select\'' +
                    ' : \'\'}}" b-style="selectStyle" b-model="value" b-attr="selectAttr" data-name="' +
                    '{{name}}" disabled?="disabled"><option b-repeat="option in options" value="{{opt' +
                    'ion.value}}" disabled?="option.disabled" selected?="option.value === value">{{op' +
                    'tion.text}}</option></select>        <span style="font-size: 12px;vertical-align' +
                    ': middle;margin-left:10px;" b-if="afterText">{{afterText}}</span>        <div b-' +
                    'style="{display: invalid || tip ? undefined : \'none\'}" class="tc-15-select-tip' +
                    's">{{invalid || tip}}</div></div>',$valuekey:"value",$afterInit:function(){n.pro' +
                    'totype.$afterInit.apply(this,arguments);var e=this,t=e.loadTask=$.Deferred(),i=a' +
                    '.bufferFn(function(){e.$set("loading",!0),"pending"!==t.state()&&(t=e.loadTask=$' +
                    '.Deferred()),e.getData(function(i){e.$set("options",i);var n=e.getOption();i=i.f' +
                    'ilter(function(e){return!e.disabled}),!n&&i.length?e.$set("value",""+i[0].value)' +
                    ':"string"!=typeof e.value&&e.$set("value",""+e.value),t.resolve(i)},t.reject)}),' +
                    's=this.watchFields||[];s.forEach(function(t){e.$watch(t,i)}),this.$watch("value"' +
                    ',function(){e.onChange(e.value,e)}),i()},getOption:function(e){
var t,i=this,n=i' +
                    '.options||[];return e=arguments.length?e:i.value,n.forEach(function(i){i.disable' +
                    'd||i.value+""!=e+""||(t=i)}),t},getData:$.noop,onChange:$.noop,getValue:function' +
                    '(){return this.value},getText:function(){return $(this.$el).find("select option:' +
                    'selected").text()}},{defaults:{relevance:!0}});i.exports=s}),define("modules/vpc' +
                    '/eni/detail/changeSubnet",function(e,t,i){var n=e("widget/selectors/SubnetSelect' +
                    'orAlt"),a=e("qccomponent"),s=(e("../model"),e("models/api")),d=e("dialog"),o=e("' +
                    '../Validator"),c=e("manager"),r=new o,l=a.extend({});l.tag("subnet-selector",n.e' +
                    'xtend({parseData:function(e){var t=this;return e.list.forEach(function(e){e.unSu' +
                    'bnetId==t.curSubnetId&&(e.isCur="当前子网")}),e}},{defaults:$.extend({},n.defaults,{' +
                    'colums:[{name:"子网ID/名称",key:"unSubnetId",tdTpl:'<p>{{item.unSubnetId}}</p><p cla' +
                    'ss="text-overflow">{{item.subnetName}}</p>'},{name:"CIDR",key:"cidrBlock"},{name' +
                    ':" ",key:"isCur"}]})})),i.exports={render:function(e,t){var i=new l('    <div cl' +
                    'ass="binding-ip">        <div class="tc-15-option-hd mb10">            <h4>请选择您要' +
                    '更换的子网：</h4>        </div>        <subnet-selector b-ref="selector" check-key="su' +
                    'bnetId" zone-ids="{{[eni.zoneId]}}" vpc-id="{{eni.vpcId}}" cur-subnet-id="{{eni.' +
                    'subnetId}}"></subnet-selector>        <div class="ip-switchover">            <p ' +
                    'class="txt">更换子网后将同时更换主IP</p>            <!--<select class="tc-15-select m" b-mo' +
                    'del="_type">-->                <!--<option value="0">自动分配IP</option>-->         ' +
                    '       <!--<option value="1">手动填写IP</option>-->            <!--</select>-->     ' +
                    '       新IP:            <span>                <input type="text" disabled class="' +
                    'tc-15-input-text m" b-if="_type==0"                       placeholder="系统将自动分配IP' +
                    '地址">                <input type="text" b-model="privateIpAddress" b-if="_type==1' +
                    '"                       data-validate="ip" class="tc-15-input-text m"           ' +
                    '            placeholder="请输入IP地址">            </span>        </div>    </div>',{' +
                    '$data:{regionId:t,eni:e,_type:1}});return new Promise(function(t,n){d.create(i.$' +
                    'el,750,"",{title:"更换子网",preventResubmit:!0,defaultCancelCb:function(){i.$destroy' +
                    '(),n()},button:{"确定":function(a){r.btn=a;var o,l=e,p=i.$refs.selector;if(e.priva' +
                    'teIpAddressesSet.some(function(e){e.primary&&(o=e.privateIpAddress)}),!r.validat' +
                    'eAll(i.$el))return void d.toggleBtnDisable(!1);if(!p.$checked)return r.showTips(' +
                    '$(i.$el).find(".tc-15-table-panel")[0],"请选择一个资源"),void d.toggleBtnDisable(!1);va' +
                    'r u=p.$checked.replace(/^.+_(\d+)$/,"$1");d.toggleBtnLoading(!0),s.request({cmd:' +
                    '"ModifyInstanceVpcConfig",serviceType:"cvm",data:{instanceId:l.instanceSet.insta' +
                    'nceId,srcSubnetId:l.intSubnetId,dstSubnetId:u,srcVpcIp:o,dstVpcIp:i.privateIpAdd' +
                    'ress,vpcId:l.intVpcId}}).then(function(e){return e.requestId?c.checkTask({taskId' +
                    ':e.requestId}):void 0}).then(function(e){return d.hide(),e},function(e){d.toggle' +
                    'BtnDisable(!1),d.toggleBtnLoading(!1)}).then(t,n)}}})})},destroy:function(){}}})' +
                    ',define("modules/vpc/eni/detail/Detail",function(e,t,i){var n=e("../model"),a=e(' +
                    '"appUtil"),s=e("widget/page/DetailPage"),d=e("./Param"),o=e("./IPManager"),c=e("' +
                    './Sg"),r=e("widget/page/BeeRoute");i.exports=(new r).mixin({routeBase:"/vpc/eni/' +
                    'detail",params:{regionId:{route:"rid",fixed:!0,initial:function(){return a.getRe' +
                    'gionId()},defaults:"",formula:function(e){return""+e||a.getRegionId()}},id:"",ac' +
                    'tiveIndex:{route:"tab"}},Bee:s.extend({backUrl:"/vpc/eni",notFoundTips:'<div sty' +
                    'le="padding: 100px; text-align: center" b-if="notFound">        <div class="tc-1' +
                    '5-list-content">未找到该弹性网卡</div></div>',loadData:function(e){return n.DescribeNetw' +
                    'orkInterfaces({networkInterfaceId:e,showInt:1}).then(function(e){var t=e.data?e.' +
                    'data[0]:null;return{title:t&&t.eniName||"",item:t}})}},{defaults:{tabData:{list:' +
                    '[{label:"基本信息",Component:d,refresh:!0},{label:"IP 管理",Component:o,refresh:!0},{l' +
                    'abel:"关联安全组",Component:c}]}}})})}),define("modules/vpc/eni/detail/IPManager/assi' +
                    'gnIp",function(e,t,i){var n=e("qccomponent"),a=e("dialog"),s=(e("models/api"),e(' +
                    '"manager")),d=e("modules/vpc/eni/model"),o=e("tips"),c=e("../../Validator"),r=ne' +
                    'w c;t.render=function(e,t){var i=new n('    <div class="tc-15-list-wrap form app' +
                    'ly-line ">        <form action="javascript:;" lpformnum="2">            <div cla' +
                    'ss="tc-15-list-content">                <ul>                    <li>            ' +
                    '            <em class="tc-15-list-tit" aria-requireda="false">所属子网</em>         ' +
                    '               <span class="tc-15-list-det">                            {{subnet' +
                    '.subnetName}} ({{subnet.unSubnetId}})                        </span>            ' +
                    '        </li>                    <li>                        <em class="tc-15-li' +
                    'st-tit" aria-requireda="false">子网CIDR</em>                        <span class="t' +
                    'c-15-list-det">                            {{subnet.cidrBlock}}                 ' +
                    '           </span>                    </li>                    <li>             ' +
                    '           <em class="tc-15-list-tit" aria-requireda="false">子网可用IP</em>        ' +
                    '                <span class="tc-15-list-det">                            {{subne' +
                    't.availableIPNum}}                        </span>                    </li>      ' +
                    '              <li>                        <em class="tc-15-list-tit" aria-requir' +
                    'eda="false">IP 配额</em>                        <span class="tc-15-list-det">     ' +
                    '                       {{limit}}                        </span>                 ' +
                    '   </li>                    <li>                        <em class="tc-15-list-ti' +
                    't" aria-requireda="false">可用配额</em>                        <span class="tc-15-li' +
                    'st-det">                            {{availableIPNum}}个                        <' +
                    '/span>                    </li>                    <li class="ip-allot">        ' +
                    '                <em class="tc-15-list-tit" aria-requireda="false">分配IP</em>     ' +
                    '                       <span class="tc-15-list-det">                            ' +
                    '    <div b-repeat="item in list" style="margin-bottom: 0.5em;">                 ' +
                    '                   <select class="tc-15-select m" b-model="item._type">         ' +
                    '                               <option value="0">自动分配</option>                  ' +
                    '                      <option value="1">手动填写</option>                           ' +
                    '         </select>                                    <span>                    ' +
                    '                    <input type="text" disabled class="tc-15-input-text m" b-if=' +
                    '"item._type==0"                                               placeholder="系统将自动' +
                    '分配IP地址">                                        <input data-ip="{{item.privateIp' +
                    'Address}}" type="text" b-model="item.privateIpAddress" b-if="item._type==1"     ' +
                    '                                          data-validate="ip" class="tc-15-input-' +
                    'text m"                                               placeholder="请输入IP地址">    ' +
                    '                                </span>                                    <a hr' +
                    'ef="javascript:;" b-on-click="del($index)">删除</a>                               ' +
                    ' </div>                                <div>                                    ' +
                    '<a class="{{availableIPNum - list.length > 0 ? \'\' : \'text-weak\'}}"          ' +
                    '                             data-title?="availableIPNum - list.length <= 0 && ' +
                    '\'无可用配额\'" href="javascript:;" b-on-click="add()">新增</a>                        ' +
                    '        </div>                            </span>                    </li>      ' +
                    '          </ul>            </div>        </form>    </div>',{$afterInit:function' +
                    '(){var i=this;d.DescribeNetworkInterfaceAttribute({vpcId:e.vpcId,networkInterfac' +
                    'eId:e.networkInterfaceId}).then(function(e){i.$set({limit:e.limit,availableIPNum' +
                    ':Math.min(e.availableIPNum,t.availableIPNum)})})},add:function(){this.availableI' +
                    'PNum-this.list.length>0&&this.list.push({})},del:function(e){this.list.splice(e,' +
                    '1)},$data:{subnet:t,list:[{}]}});return new Promise(function(t,n){a.create(i.$el' +
                    ',550,"",{preventResubmit:!0,title:"分配内网IP",button:{"确定":function(c){if(r.btn=c,!' +
                    'r.validateAll(i.$el))return void a.toggleBtnDisable(!1);var l=[],p=[];i.list.for' +
                    'Each(function(e){0==e._type?p.push(e):l.push(e)}),l=l.map(function(e){return{pri' +
                    'vateIpAddress:e.privateIpAddress,primary:!1}}),l.length||p.length?(a.toggleBtnLo' +
                    'ading(!0),d.AssignPrivateIpAddresses({vpcId:e.vpcId,networkInterfaceId:e.network' +
                    'InterfaceId,privateIpAddressSet:l,secondaryPrivateIpAddressCount:p.length},{tipE' +
                    'rr:!1}).then(function(e){return e=e.data,s.checkTask({taskId:e.taskId}).then(fun' +
                    'ction(){a.hide()})})["catch"](function(e){var t=e.msg;a.toggleBtnDisable(!1),a.t' +
                    'oggleBtnLoading(!1);var n,s,d=t.match(/privateIpAddress\(([\d\.]+)\)/);d=d&&d[1]' +
                    ',d&&(n=$(i.$el).find('[data-ip="'+d+'"]'),29215==e.code?(s="该内网IP不在弹性网卡子网内",o.hi' +
                    'deFlash(1)):29216==e.code?(s="该内网IP已经被使用",o.hideFlash(1)):29236==e.code&&(s="该IP' +
                    '为子网保留IP, 不能使用",o.hideFlash(1)),n[0]&&s?r.showErr(n[0],s):o.error(e.uiMsg))}).the' +
                    'n(t,n)):(a.hide(),Promise.reject())}}})})}}),define("modules/vpc/eni/detail/IPMa' +
                    'nager/bindEip",function(e,t,i){var n=e("qccomponent"),a=e("dialog"),s=e("models/' +
                    'api"),d=e("manager"),o=(e("modules/vpc/eni/model"),e("constants")),c=e("widget/s' +
                    'electors/EipSelector"),r=e("appUtil"),l=e("tips");t.show=function(e,t,i){i=i||r.' +
                    'getRegionId();var p=n.extend({});p.tag("eip-selector",c);var u=new p('    <div c' +
                    'lass="binding-ip">        <div class="tc-15-option-hd mb10">            <h4>请选择"' +
                    '内网IP {{item.privateIpAddress}}"要绑定的弹性公网IP</h4>            <div class="tc-15-list' +
                    '-det-wrap" style="overflow: hidden;">                <span class="tc-15-list-det' +
                    '">                    <label class="tc-15-radio-wrap">                        <i' +
                    'nput type="radio" value="0" b-model="_type" class="tc-15-radio">使用现有EIP绑定</label' +
                    '>                    <label class="tc-15-radio-wrap">                        <in' +
                    'put type="radio" value="1" b-model="_type" class="tc-15-radio">新建EIP绑定</label>  ' +
                    '              </span>            </div>        </div>        <div b-if="_type ==' +
                    ' 1">            <p>弹性公网IP申请后将立即绑定，空置时收取<span class="text-warning">{{eipPrice}}元/' +
                    '小时</span>的费用。</p>        </div>        <div b-if="_type == 0">            <!--拉取' +
                    '未绑定的 eip-->            <eip-selector b-ref="eipSelector" region-id="{{regionId}}' +
                    '" extra-param="{{ {status: [4]} }}">            </eip-selector>        </div>   ' +
                    ' </div>',{$afterInit:function(){this.$set("_type",0)},applyEip:function(){var e=' +
                    'this;return l.showLoading(),s.request({regionId:i,cmd:"CreateEip",data:{goodsNum' +
                    ':1},serviceType:"eip"}).then(function(t){return t=t.data||{},e.eipId=t.eipIds[0]' +
                    ',t.requestId?d.checkTask({taskId:t.requestId})["catch"](function(e){throw l.erro' +
                    'r("EIP 创建失败"),e}):void 0})["catch"](function(e){throw l.stopLoading(),e}).then(f' +
                    'unction(e){return l.stopLoading(),e})},$data:{regionId:i,item:e,eipPrice:o.EIP_R' +
                    'EGION_PRICE[i].price}});return new Promise(function(i,n){a.create(u.$el,720,"",{' +
                    'preventResubmit:!0,title:"绑定弹性公网IP",defaultCancelCb:function(){u.$destroy(),n()}' +
                    ',button:{"确定":function(){var o,c;1==u._type?o=u.applyEip().then(function(){c=u.e' +
                    'ipId}):(o=Promise.resolve(),c=u.$refs.eipSelector.$checked),o.then(function(){s.' +
                    'request({serviceType:"eip",cmd:"EipBindInstance",data:{networkInterfaceId:t.netw' +
                    'orkInterfaceId,privateIpAddress:e.privateIpAddress,eipId:c}}).then(function(e){r' +
                    'eturn e=e.data||{},e.requestId?(a.hide(),d.checkTask({taskId:e.requestId})):void' +
                    ' 0}).then(i,n)})}}})})},t.destroy=function(){a.hide()},t.unBind=function(e,t){re' +
                    'turn s.request({cmd:"EipUnBindInstance",serviceType:"eip",data:{eipId:e.eipId,ne' +
                    'tworkInterfaceId:t.networkInterfaceId,privateIpAddress:e.privateIpAddress}}).the' +
                    'n(function(e){return e=e.data||{},e.requestId?d.checkTask({taskId:e.requestId}):' +
                    'void 0})}}),define("modules/vpc/eni/detail/IPManager",function(e,t,i){var n=e("q' +
                    'ccomponent"),a=(e("appUtil"),e("constants")),s=e("modules/vpc/eni/model"),d=e("w' +
                    'idget/grid/qcGrid"),o=e("manager"),c=e("./IPManager/bindEip"),r=e("./IPManager/a' +
                    'ssignIp"),l=e("dialog"),a=e("constants"),p=e("models/api"),u=e("../Validator"),v' +
                    '=new u,f=n.extend({$tpl:'    <div class="tc-15-list-wrap">        <div class="tc' +
                    '-15-action-panel">            <div class="pull-left elastic-nw-title">          ' +
                    '      <button type="button" class="tc-15-btn m {{canAdd ? \'\': \'disabled\'}}" ' +
                    '                       b-on-click="assignIp(eni)" data-title?="canAdd ? \'\' : a' +
                    'ddTips">                    分配内网IP                </button></div>            <!-' +
                    '-<qc-search search="{{search.bind(this)}}" placeholder="请输入告警策略名称"></qc-search>-' +
                    '->        </div>        <ip-grid b-if="eni && eni.networkInterfaceId" b-ref="gri' +
                    'd" eni="{{eni}}" subnet="{{subnet}}" region-id="{{regionId}}" fetch="{{ fetch.bi' +
                    'nd(this) }}"></ip-grid>    </div>',$afterInit:function(){var e=this,t={};this.id' +
                    '&&(t.networkInterfaceId=this.id),this.instanceId&&(t.instanceId=this.instanceId)' +
                    ',t.showInt=1,this.fetch(t).then(function(t){e.$set("eni",t),t.vpcId&&p.request({' +
                    'serviceType:"vpc",cmd:"DescribeSubnetEx",data:{vpcId:t.vpcId,subnetId:t.subnetId' +
                    '}}).then(function(t){t=t.data[0]||{},e.$set("subnet",t)}).then(function(){e.chec' +
                    'kCanAdd()}),e.$refs.grid.$on("gridUpdate",function(){e.checkCanAdd()})})},fetch:' +
                    'function(e,t){return s.DescribeNetworkInterfaces(e,t).then(function(e){var t=e.d' +
                    'ata&&e.data[0]||{};return t})},checkCanAdd:function(){var e=this,t=this.eni;e.su' +
                    'bnet&&s.DescribeNetworkInterfaceAttribute({vpcId:t.vpcId,networkInterfaceId:t.ne' +
                    'tworkInterfaceId}).then(function(t){var i=Math.min(t.availableIPNum,e.subnet.ava' +
                    'ilableIPNum);e.$set({limit:t.limit,availableIPNum:i,canAdd:i,addTips:i?"":"无可用IP' +
                    '配额"})})},assignIp:function(){var e=this,t=e.eni,i=e.subnet;this.canAdd&&t&&i&&r.' +
                    'render(t,i).then(function(){e.$refs.grid.refresh()},function(){})}},{defaults:{}' +
                    '});f.tag("ip-grid",d.extend({$afterInit:function(){d.prototype.$afterInit.call(t' +
                    'his),this.$watch("eni",function(e){e&&e.networkInterfaceId&&this.refresh()}),thi' +
                    's.getData()},checkAuth:function(e,t){return!e.$disable},getData:function(e,t,i){' +
                    'var n=this;t=function(e,t){n.setData(t)},this.fetch({networkInterfaceId:n.eni.ne' +
                    'tworkInterfaceId,showInt:1},i).then(function(e){t(null,{list:e.privateIpAddresse' +
                    'sSet||[]})},t)},refresh:function(){this.getData({},"",{flush:!0})},bindEip:funct' +
                    'ion(e){var t=this;this.checkAuth(e)&&c.show(e,this.eni).then(function(){t.refres' +
                    'h()})},unBindEip:function(e,t){var i=this.$root;return this.checkDel(e,{title:"确' +
                    '认解绑此弹性公网IP",content:'弹性公网IP解绑后，将收取<span class="text-warning">'+a.EIP_REGION_PRIC' +
                    'E[i.regionId].price+"元/小时</span>的费用， 请到弹性公网IP控制台进行管理。",onConfirm:function(){i.up' +
                    'dateItem(t,{$disable:!0}),c.unBind(e,i.eni).then(function(){i.refresh()})}})},ch' +
                    'angePrimaryIp:function(e){var t=this;if(this.subnet&&this.checkAuth(e)){var i=ne' +
                    'w n('    <div class="tc-15-list-wrap form apply-line peer-layer">        <form a' +
                    'ction="javascript:;" lpformnum="1">            <div class="tc-15-list-content"> ' +
                    '               <ul>                    <li><em class="tc-15-list-tit" aria-requi' +
                    'reda="false">所属子网</em><span class="tc-15-list-det">                            {' +
                    '{subnet.subnetName}} ( {{subnet.unSubnetId}} )                            </span' +
                    '>                    </li>                    <li><em class="tc-15-list-tit" ari' +
                    'a-requireda="false">子网CIDR</em><span class="tc-15-list-det">                    ' +
                    '         {{subnet.cidrBlock}}                            </span>                ' +
                    '    </li>                    <li><em class="tc-15-list-tit" aria-requireda="fals' +
                    'e">当前主IP</em><span class="tc-15-list-det">                            {{pip.priv' +
                    'ateIpAddress}}                            </span>                    </li>      ' +
                    '              <li>                        <em class="tc-15-list-tit" aria-requir' +
                    'ed="false">新IP</em>                        <span class="tc-15-list-det">        ' +
                    '                    <div class="tc-15-input-text-wrap">                         ' +
                    '       <input type="text" data-validate="ip" autofocus                          ' +
                    '             b-model="newIp" class="tc-15-input-text" placeholder="请输入IP地址">    ' +
                    '                        </div>                        </span>                   ' +
                    ' </li>                </ul>            </div>        </form>    </div>',{$data:{' +
                    'subnet:this.subnet,pip:e}});l.create(i.$el,550,"",{title:"修改主IP",preventResubmit' +
                    ':!0,button:{"确定":function(n){if(v.btn=n,!v.validateAll(i.$el))return void l.togg' +
                    'leBtnDisable(!1);if(i.newIp==e.privateIpAddress)return void l.hide();var a=t.eni' +
                    ';l.toggleBtnLoading(!0),p.request({cmd:"ModifyInstanceVpcConfig",serviceType:"cv' +
                    'm",data:{instanceId:a.instanceSet.instanceId,srcSubnetId:a.intSubnetId,dstSubnet' +
                    'Id:a.intSubnetId,srcVpcIp:e.privateIpAddress,dstVpcIp:i.newIp,vpcId:a.intVpcId}}' +
                    ').then(function(e){return e.requestId?o.checkTask({taskId:e.requestId}).then(fun' +
                    'ction(e){return l.hide(),e}):void 0}).then(function(){t.refresh()},function(e){l' +
                    '.toggleBtnDisable(!1),l.toggleBtnLoading(!1)})}}})}},releaseIp:function(e,t){var' +
                    ' i=this.$root;return this.checkDel(e,{title:"确定释放此内网IP？",content:"内网IP释放后将自动解关联弹' +
                    '性公网IP和负载均衡"+(e.eipId?'，弹性IP未关联云主机时将收取<span class="text-warning">'+a.EIP_REGION_P' +
                    'RICE[i.regionId].price+"元/小时</span>的费用。":"。"),onConfirm:function(){var n=i.eni;i' +
                    '.updateItem(t,{$disable:!0}),s.UnassignPrivateIpAddresses({vpcId:n.vpcId,network' +
                    'InterfaceId:n.networkInterfaceId,privateIpAddress:[e.privateIpAddress]}).then(fu' +
                    'nction(e){e=e.data,o.checkTask({taskId:e.taskId}).then(function(){i.refresh()})}' +
                    ')}})},update:function(e,t,i){var n=this;return s.ModifyPrivateIpAddress({vpcId:t' +
                    'his.eni.vpcId,networkInterfaceId:this.eni.networkInterfaceId,privateIpAddress:e.' +
                    'privateIpAddress,description:i}).then(function(e){return n.refresh(),e})}},{defa' +
                    'ults:{hasFirst:!1,showPagination:!1,showState:!1,initGetData:!1,autoMaxHeight:!1' +
                    ',colums:[{name:"内网IP",key:"privateIpAddress"},{name:"类型",key:"_type"},{name:"已绑定' +
                    '弹性公网IP",key:"eipId",tdTpl:'    <p>        <span style="vertical-align: middle">{' +
                    '{item.wanIp ? item.wanIp : \'无\'}}</span>        <a b-if="item.eipId" href="java' +
                    'script:;" b-on="{click: unBindEip(item, $parent.$index)}">解绑</a>        <a b-if=' +
                    '"!item.eipId" href="javascript:;" b-on-click="bindEip(item)">绑定</a>    </p>    <' +
                    'p>        {{item.eipId}}    </p>'},{name:"备注",key:"description",tdTpl:'    <span' +
                    ' class="text-overflow m-width">{{item.description||\'-\'}}</span>    <i class="p' +
                    'encil-icon hover-icon" b-on="{click: change(item, \'changeDesc\', item.descripti' +
                    'on)}"></i>'},{name:"操作",key:"_action",tdTpl:'    <span>    <p class="{{item.inst' +
                    'anceNum ? \'disable-link\' : \'\'}}">        <a href="javascript:;" b-if="item.p' +
                    'rimary" b-on-click="changePrimaryIp(item)">修改主IP</a>        <a href="javascript:' +
                    ';" b-if="!item.primary" b-on="{click: releaseIp(item, $parent.$index)}">释放</a>  ' +
                    '  </p></span>'}]}})),i.exports=f}),define("modules/vpc/eni/detail/Param",functio' +
                    'n(e,t,i){var n=e("qccomponent"),a=e("appUtil"),s=e("constants"),d=e("modules/vpc' +
                    '/eni/model"),o=e("models/api"),c=e("manager"),r=e("widget/editInPlace/editInPlac' +
                    'e"),l=e("../Validator"),p=e("./changeSubnet"),u=n.extend({$tpl:'    <div>       ' +
                    ' <div class="param-box">            <div class="param-hd">                <h3>基本' +
                    '信息</h3>            </div>            <div class="param-bd" b-if="info">         ' +
                    '       <ul class="item-descr-list">                    <li>                     ' +
                    '   <em class="item-descr-tit">名称</em>                        <span class="item-d' +
                    'escr-txt">{{info.eniName}}                            <i data-rename class="penc' +
                    'il-icon hover-icon" b-on-click="rename(info, info.eniName)" title="编辑"></i>     ' +
                    '                   </span>                    </li>                    <li>     ' +
                    '                   <em class="item-descr-tit">ID</em>                        <sp' +
                    'an class="item-descr-txt">{{info.networkInterfaceId}}</span>                    ' +
                    '</li>                    <li>                        <em class="item-descr-tit">' +
                    'MAC地址</em>                        <span class="item-descr-txt">{{info.macAddress' +
                    '}}</span>                    </li>                    <li>                      ' +
                    '  <em class="item-descr-tit">地域</em>                        <span class="item-de' +
                    'scr-txt">                            {{info.regionName}}                        ' +
                    '</span>                    </li>                    <li>                        ' +
                    '<em class="item-descr-tit">可用区</em>                        <span class="item-des' +
                    'cr-txt">                            {{info.zoneName}}                        </s' +
                    'pan>                    </li>                    <li>                        <em' +
                    ' class="item-descr-tit">所属网络</em>                        <span class="item-descr' +
                    '-txt">                            <a data-event="nav" href="/vpc/vpc?rid={{info.' +
                    'regionId}}">{{info.vpcId}}</a> ({{info.vpcName}})                        </span>' +
                    '                    </li>                    <li>                        <em cla' +
                    'ss="item-descr-tit">所属子网</em>                        <span class="item-descr-txt' +
                    '">                            <a data-event="nav" href="/vpc/subnet?rid={{info.r' +
                    'egionId}}&subnetId={{info.subnetId}}">                                {{info.sub' +
                    'netId}}                            </a> ({{subnet.subnetName}})                 ' +
                    '           <span class="{{info.instanceSet.instanceId ? \'\': \'disable-link\'}}' +
                    '" data-title?="info.instanceSet.instanceId ? false : \'无法更换子网\'">               ' +
                    '                 <a style="margin-left: 0.5em"  b-on-click="changeSubnet(info)" ' +
                    'href="javascript:;">更换子网</a>                            </span>                 ' +
                    '       </span>                    </li>                    <li>                 ' +
                    '       <em class="item-descr-tit">子网CIDR</em>                        <span class' +
                    '="item-descr-txt">                            {{subnet.cidrBlock}}              ' +
                    '          </span>                    </li>                    <li>              ' +
                    '          <em class="item-descr-tit">绑定云主机</em>                        <span cla' +
                    'ss="item-descr-txt">                            <span>                          ' +
                    '      <a data-event="nav" href="/cvm/detail/{{info.regionId}}/{{info.instanceSet' +
                    '.instanceId}}">{{info.instanceSet.instanceId}}</a>                            </' +
                    'span>                        </span>                    </li>                   ' +
                    ' <li>                        <em class="item-descr-tit">创建时间</em>               ' +
                    '         <span class="item-descr-txt">{{info.createTime}}</span>                ' +
                    '    </li>                </ul>            </div>        </div>',$afterInit:funct' +
                    'ion(){this.getData()},getData:function(e){var t=this;return this.fetch({networkI' +
                    'nterfaceId:this.id,showInt:1},e).then(function(e){var i=a.getRegionId();return e' +
                    '.regionName=s.REGIONNAMES[i],e.regionId=i,t.$set("info",e),e.vpcId&&o.request({s' +
                    'erviceType:"vpc",cmd:"DescribeSubnetEx",data:{vpcId:e.vpcId,subnetId:e.subnetId}' +
                    '}).then(function(e){e=e.data[0]||{},t.$set("subnet",e)}),e.zoneId&&c.queryZoneNa' +
                    'me({regionId:i,zoneId:e.zoneId}).then(function(e){t.$set("info.zoneName",e)}),e}' +
                    ')},fetch:function(e,t){return d.DescribeNetworkInterfaces(e,t).then(function(e){' +
                    'var t=e.data&&e.data[0]||{};return t})},rename:function(e,t){var i=this;if(e.eni' +
                    'Name){t=e.eniName;var n=new r({$data:{trigger:"",hideDestroy:!0,name:t,content:'' +
                    '    <form action="javascript:;" b-on-submit="hide(true)" style="width: 250px;"> ' +
                    '       <div class="form-unit">            <input type="text" b-model="name" data' +
                    '-validate="name" maxlength="25"                   class="tc-15-input-text m" sty' +
                    'le="float: none; width: 180px;">            <p class="form-input-help">还能输入{{25 ' +
                    '- name.length}}个字符，英文、汉字、数字、连接线"-"或下划线"_"</p>        </div>    </form>'},handler' +
                    ':$(i.$el).find("[data-rename]")[0],onConfirm:function(){var n=new l({errClass:"i' +
                    's-error"}),a=$(this.$el).find("[data-confirm]");if(n.validate($(this.$el).find("' +
                    'input")[0],{noTips:!0})){if(this.name===t||a.hasClass("disabled"))return;return ' +
                    'a.addClass("disabled"),i.updateName(e,this.name).then(function(e){return a.remov' +
                    'eClass("disabled"),e},function(e){throw a.removeClass("disabled"),e})}return!1}}' +
                    ');n.show()}},updateName:function(e,t){var i=this;return d.ModifyNetworkInterface' +
                    '({vpcId:e.vpcId,networkInterfaceId:e.networkInterfaceId,eniName:t}).then(functio' +
                    'n(e){return i.getData(),$(i.$el).trigger("REFRESHDETAIL"),e})},changeSubnet:func' +
                    'tion(e){var t=this;e.instanceSet.instanceId&&p.render(e,this.info.regionId).then' +
                    '(function(){t.getData({flush:!0})},function(){})}});i.exports=u}),define("module' +
                    's/vpc/eni/detail/Sg",function(e,t,i){var n=e("qccomponent"),a=(e("appUtil"),e("c' +
                    'onstants"),e("modules/vpc/eni/model"));i.exports=n.extend({$tpl:'<div id="dfwDet' +
                    'ail"></div>',$afterInit:function(){var e=this;Promise.all([e.fetch({networkInter' +
                    'faceId:this.id}),new Promise(function(e){nmcLoad("app.js",function(){seajs.use("' +
                    'modules/cvm/view/dfwdetail",e)})})]).then(function(t){var i=t[0],n=t[1];n.render' +
                    '($(e.$el),{firewalls:i.groupSet,canEdit:!1,showMsg:!1})})},fetch:function(e,t){r' +
                    'eturn a.DescribeNetworkInterfaces(e,t).then(function(e){var t=e.data&&e.data[0]|' +
                    '|{};return t})}})}),define("modules/vpc/eni/eni",function(e,t,i){var n=(e("pageM' +
                    'anager"),e("appUtil")),a=e("widget/page/ListPage"),s=e("./model"),d=e("models/ap' +
                    'i"),o=e("modules/vpc/eni/detail/Detail"),c=a.extend({panelTpl:'    <div class="t' +
                    'c-15-action-panel">        <button class="tc-15-btn m disabled" data-title="暂时不支' +
                    '持新建弹性网卡">+新建</button>        <button b-on-click="fieldSetting()" class="tc-15-bt' +
                    'n weak m setting"></button>        <button b-on-click="refresh()" class="tc-15-b' +
                    'tn weak m refresh right-item"></button>        <qc-search b-ref="search" search=' +
                    '"{{search.bind(this)}}"                   placeholder="{{searchPlaceHolder}}"></' +
                    'qc-search>    </div>',gridOpts:{hasFirst:!1,emptyTips:"您还没有弹性网卡",colums:[{name:"' +
                    'ID/名称",key:"networkInterfaceId",required:!0,tdTpl:'    <div>        <p class="te' +
                    'xt-overflow">            <a href="/{{prefix}}/eni/detail?rid={{regionId}}&id={{i' +
                    'tem.networkInterfaceId}}" data-event="nav">                {{item.networkInterfa' +
                    'ceId}}            </a></p>        <div>            <span class="text-overflow m-' +
                    'width">{{item.eniName}}</span>            <i class="pencil-icon hover-icon" b-on' +
                    '="{click: change(item, \'changeName\', item.eniName)}"></i>        </div>    </d' +
                    'iv>'},{name:"网卡属性",key:"_type"},{name:"所属网络",key:"vpcId",tdTpl:'    <p class="te' +
                    'xt-overflow">        <a href="/vpc/vpc?rid={{regionId}}&unVpcId={{item.vpcId}}" ' +
                    'data-event="nav">            {{item.vpcId}}        </a>    </p>    <span class="' +
                    'text-overflow m-width">{{item.vpcName}}</span>'},{name:"所属子网",key:"subnetId",tdT' +
                    'pl:'    <p class="text-overflow">        <a href="/vpc/subnet?rid={{regionId}}&s' +
                    'ubnetId={{item.subnetId}}" data-event="nav">            {{item.subnetId}}       ' +
                    ' </a>    </p>'},{name:"绑定云主机",key:"instanceSet",tdTpl:'        <span b-if="item.' +
                    'instanceSet.instanceId">            <p class="text-overflow">                <a ' +
                    'href="/cvm/detail/{{regionId}}/{{item.instanceSet.instanceId}}" data-event="nav"' +
                    '>                    {{item.instanceSet.instanceId}}                </a>        ' +
                    '    </p>            <p class="text-overflow">                <span>             ' +
                    '       {{item.instanceSet.alias}}                </span>            </p>        ' +
                    '</span>        <span b-if="!item.instanceSet.instanceId">            -        </' +
                    'span>    </p>'},{name:"内网IP",key:"privateIpAddressesSet",tdTpl:'    <p class="te' +
                    'xt-overflow">        <a href="/{{prefix}}/eni/detail?rid={{regionId}}&id={{item.' +
                    'networkInterfaceId}}&tab=1" data-event="nav">            {{item.privateIpAddress' +
                    'esSet.length}}个        </a>    </p>',width:100},{name:"创建时间",key:"createTime",hi' +
                    'de:!0}],getData:function(e,t){var i=this;t=function(e,t){i.setData(t)};var a=e.c' +
                    'ount||this.count,o=e.page||this.page,c={regionId:n.getRegionId(),limit:a,offset:' +
                    'a*(o-1)};e.searchKey&&(/^eni-\w{8}$/.test(e.searchKey)?c.networkInterfaceId=e.se' +
                    'archKey:/^ins-\w{8}$/.test(e.searchKey)?c.instanceId=e.searchKey:c.eniName=e.sea' +
                    'rchKey),s.DescribeNetworkInterfaces(c,{flush:this.$refreshing}).then(function(e)' +
                    '{var n=e.data||[],a=[];n.forEach(function(e){e.instanceSet.instanceId&&a.push(e.' +
                    'instanceSet.instanceId)}),a.length&&d.request({cmd:"DescribeInstanceMainInfo",se' +
                    'rviceType:"cvm",data:{uInstanceIds:a,offset:0,limit:a.length}}).then(function(e)' +
                    '{e=e.data.deviceList,n.forEach(function(t,n){t.instanceSet.instanceId&&e.some(fu' +
                    'nction(e){return e.uInstanceId==t.instanceSet.instanceId?(t.instanceSet.alias=e.' +
                    'alias,i.updateItem(n,{instanceSet:t.instanceSet}),!0):void 0})})}),t(null,{list:' +
                    'e.data||[],prefix:"vpc",totalNum:e.totalNum||0})},t)},update:function(e,t,i){var' +
                    ' n=this;return s.ModifyNetworkInterface({vpcId:e.vpcId,networkInterfaceId:e.netw' +
                    'orkInterfaceId,eniName:i}).then(function(e){return n.refresh(),e})}}},{detail:o,' +
                    'defaults:{title:"弹性网卡",hasProjectSelector:!1}});i.exports=c}),define("modules/vp' +
                    'c/eni/model",function(e,t,i){var n=e("models/api"),a=e("appUtil"),s=[],d=functio' +
                    'n(e){e=e||{};var t=$.extend({},e);return e.write?t.cache=!1:(s.push(t),t.cache="' +
                    'cache"in e?e.cache:!0),function(i,a){return e.write&&s.forEach(function(e){e.cha' +
                    'nged=!0}),n.request({cmd:e.cmd,serviceType:"vpc",data:i},$.extend({cache:t.cache' +
                    ',cacheTag:"vpc_networkinterface",flush:t.changed},a)).then(function(e){return t.' +
                    'cache&&(t.changed=!1),e}).then(e.successHandler||function(e){return e},e.errorHa' +
                    'ndler||function(e){throw e})}};t.DescribeNetworkInterfaces=a.beforeFn(d({cmd:"De' +
                    'scribeNetworkInterfaces",successHandler:function(e){return e=e.data,e.data&&e.da' +
                    'ta.forEach&&e.data.forEach(function(e){e._type=e.primary?"主网卡":"辅助网卡",e.privateI' +
                    'pAddressesSet&&e.privateIpAddressesSet.forEach(function(e){e._type=e.primary?"主I' +
                    'P":"辅助IP"})}),e}}),function(e){e.hideSecondary=1}),t.ModifyNetworkInterface=d({c' +
                    'md:"ModifyNetworkInterface",write:!0}),t.AssignPrivateIpAddresses=d({cmd:"Assign' +
                    'PrivateIpAddresses",write:!0}),t.UnassignPrivateIpAddresses=d({cmd:"UnassignPriv' +
                    'ateIpAddresses",write:!0}),t.DescribeNetworkInterfaceAttribute=d({cmd:"DescribeN' +
                    'etworkInterfaceAttribute",successHandler:function(e){return e.data}}),t.ModifyPr' +
                    'ivateIpAddress=d({cmd:"ModifyPrivateIpAddress",write:!0})}),define("modules/vpc/' +
                    'eni/Validator",function(e,t,i){var n=e("widget/baseInfo/validator"),a=e("appUtil' +
                    '");i.exports=n.extend({errClass:"is-error"}),i.exports.rules=$.extend({},n.rules' +
                    ',{ip:function(e){return a.isValidIp(e)?void 0:"请输入合法的IP"}})}),define("modules/vp' +
                    'c/Grid",function(e,t,i){var n=e("widget/grid/LocalGrid"),a=e("./BeeBase"),s=e(".' +
                    '/gridColumnSelector"),d=e("util");i.exports=n.extend({$beforeInit:function(){var' +
                    ' e=this.$get("colums"),t=this.$get("cacheKey");t&&(e=s.getColumns(e,t),this.$rep' +
                    'lace("colums",e)),n.prototype.$beforeInit.apply(this,arguments)},showColumnsWind' +
                    'ow:function(){s.show(this)},getActionsCell:function(e){return d.tmpl('    <% for' +
                    ' (var i=0, item; item = actions[i]; i++ ) { %>        <% if(!item.hide){ %>     ' +
                    '       <div <% if(item.disabled){ %> class="disable-link" <% } %> style="display' +
                    ':inline-block">                <a <% if(item.report){ %> data-report="<%= item.r' +
                    'eport %>" <% } %> data-role="<%= item.disabled ? "none" : item.act %>" href="#" ' +
                    'data-title="<%= item.title %>"><%= item.text %></a>            </div>        <% ' +
                    '} %>    <% } %>',{actions:e})},destroy:a.prototype.destroy},{defaults:{outerPagi' +
                    'ng:!1}})}),define("modules/vpc/gridColumnSelector",function(e,t,i){var n=e("widg' +
                    'et/fieldsManager/fieldsManager"),a="colums",s=function(e,t){var i={};return t?t.' +
                    'forEach(function(e){i[e]=!0}):i=null,e.forEach(function(e){e.hide=i?!i.hasOwnPro' +
                    'perty(e.key):e.hide,e.defaults=!e.hide}),e};t.show=function(e){var t=e.$get(a),i' +
                    '={};t.forEach(function(e){i[e.key]=e});var d=new n({data:i,cacheKey:e.cacheKey,c' +
                    'allback:function(i){var n={};n[a]=s(t,i.fields),e.$set(n)}});d.showDialog()},t.g' +
                    'etColumns=function(e,t){var i=localStorage[t]&&localStorage[t].split(",");return' +
                    ' s(e,i)}}),define("modules/vpc/gridPage",function(e,t,i){var n=e("./moduleBase")' +
                    ',a=e("widget/page/mixins/grid");i.exports=n.extend().mixin(a)}),define("modules/' +
                    'vpc/index/index",function(e,t,i){var n=e("router"),a=e("widget/betaTip/betaTip")' +
                    ',s=e("pageManager"),d=e("../util");i.exports={render:function(){var e=d.hasInWhi' +
                    'teList();e.done(function(){setTimeout(function(){n.navigate("/vpc/vpc",void 0,!0' +
                    ')},10)}),e.fail(function(){s.displaySidebar(!1),a.show("vpc")})},destroy:functio' +
                    'n(){}}}),["acl","conn","dc","dcConn","dcGw","dcRecord","index","route","subnet",' +
                    '"topo","userGw","vpc","vpcConn","vpnGw"].forEach(function(e){define("modules/vpc' +
                    '2/"+e+"/"+e,function(e,t,i){var n=e("router");t.render=function(){var e=n.getFra' +
                    'gment();n.navigate(e.replace(/^(\/debug)?\/vpc2\b/i,"/vpc"),void 0,!0)}})}),defi' +
                    'ne("modules/vpc/IPInput",function(e,t,i){var n=e("./BeeBase"),a=e("qccomponent")' +
                    ',s=e("widget/alg/Locks"),d=e("util");
e("./jCaret"),d.insertStyle(".vpc-ip-input' +
                    '.vpc-input-error input, .vpc-input-error .vpc-ip-input input, .vpc-ip-input.vpc-' +
                    'input-error select,.vpc-input-error .vpc-common-field{ border-color: #e1504a!imp' +
                    'ortant;color: #e1504a!important;outline: 0 none!important}.vpc-ip-input{line-hei' +
                    'ght:22px;display: inline-block;vertical-align: middle;}.vpc-ip-input input, .vpc' +
                    '-ip-input select{vertical-align: top;}");var o=function(){var e=function(e){retu' +
                    'rn e=parseInt(e,10)||0,Math.max(0,Math.min(255,e))};return function(t){var i=t.s' +
                    'plit(".");return 256*(256*(256*e(i[0])+e(i[1]))+e(i[2]))+e(i[3])}}(),c=function(' +
                    'e,t,i,n){var a,s,d="";if(1>=t)d+=n>i?i+"-"+n:i;else{for(s=[],a=e;n>=a;a+=t)a>=i&' +
                    '&s.push(a);s.length>4&&(s=s.slice(0,2).concat("...").concat(s[s.length-1])),d+=s' +
                    '.join(",")}return d},r="_batchUpdatePending",l=n.extend({$valuekey:"value",$tpl:' +
                    ''    <em><em b-style="{display: !dropdown && !editable ? undefined : \'none\'}">' +
                    '{{value}}</em><input            type="text"            class="tc-input-text" b-s' +
                    'tyle="{marginLeft:part===\'part1\'?undefined:\'3px\',marginRight:part===\'last\'' +
                    '?undefined:\'3px\', display:(editable && !dropdown)?undefined:\'none\'}"        ' +
                    '    style="width: 36px;height: 22px;line-height: 22px;padding: 0;text-align: cen' +
                    'ter;"            b-model="value" b-on="{blur:onBlur,focus:onFocus,keypress:onlyN' +
                    'um,keydown:ifGoSwitch,click:focus}"            qc-popover="范围：{{ranges}}"       ' +
                    '     data-trigger="focus"            data-style="min-width:100px"            dat' +
                    'a-hide-destroy="1"            disabled?="dataDisabled" ><select            class' +
                    '="tc-15-select"            style="line-height:22px;height: 24px; padding:0;width' +
                    ': 50px;min-width: inherit;font-size:12px;"            b-style="{margin:part===\'' +
                    'part1\' ? \'0 3px 0 0\' : \'0 3px\'}"            b-if="dropdown"            b-mo' +
                    'del="value"><option b-repeat="option in dropdown" value="{{option}}">{{option}}<' +
                    '/option></select></em>',minMask:0,maxMask:32,net:0,$beforeInit:function(){var e,' +
                    't=this;t.locks=new s;var i=t.updateLimit=function(){if(t.locks.isLocked())return' +
                    ' void(t[r]=!0);var i,n=1,a=Math.min(8,Math.max(0,t.minMask)),s=Math.min(8,Math.m' +
                    'ax(0,t.maxMask,a)),d=t.outerMin||0,o=t.outerMax||255,l=parseInt(t.net,10)||0,p=l' +
                    ',u=0;for(u=a;s>u;u++)p&=~(1<<7-u);n=1<<8-s,d=Math.max(d,p),"part1"===t.part&&(/' +
                    '\ballClass\b/.test(t.type)||(d=Math.max(1,d),o=Math.min(223,o))),"part4"===t.par' +
                    't&&(i=/\bsafeIp\b/.test(t.type),(i||/\bsafeIpNoNet\b/.test(t.type))&&(d=Math.max' +
                    '(1,d)),(i||/\bsafeIpNoBroadcast\b/.test(t.type))&&(o=Math.min(254,o))),o=Math.mi' +
                    'n(o,p+(Math.pow(2,s-a)-1)*n),t.$set({step:n,base:p,min:d,max:o,ranges:c(p,n,d,o)' +
                    ',editable:8>a&&s>0&&a!==s}),e=d>=o,t.doFormula()};t.$watch("net",i),t.$watch("mi' +
                    'nMask",i),t.$watch("maxMask",i),t.$watch("outerMin",i),t.$watch("outerMax",i),t.' +
                    '$watch("value",function(i){e&&t.doFormula()}),n.prototype.$beforeInit.apply(t,ar' +
                    'guments),i()},$afterInit:function(){n.prototype.$afterInit.apply(this,arguments)' +
                    ',this.$input=$(this.$el).find("input")},beginUpdating:function(e){e=e||"default"' +
                    ',this.locks.lock(e)},endUpdating:function(e){var t=this;t.locks.unlock(e),!t.loc' +
                    'ks.isLocked()&&t[r]&&(t[r]=!1,t.updateLimit())},isValid:function(){var e=this.fo' +
                    'rmulaValue();return""+e==""+this.value},doFormula:function(){if(!(this.dropdown|' +
                    '|this.editable&&!this.value||this.focused)){var e=this.formulaValue();""+e!=""+t' +
                    'his.value&&this.$set("value",e),this.$set("goFormula",Math.random())}},formulaVa' +
                    'lue:function(){var e=parseInt(this.value,10)||0;return e=Math.max(this.min,e),e=' +
                    'Math.min(this.max,e),e=Math.round((e-this.base)/this.step)*this.step+this.base,t' +
                    'his.formuler&&(e=this.formuler(e)),e},ifGoSwitch:function(e){var t=this.$input.v' +
                    'al(),i=e.which,n=this.$input.range(),a=n.start===n.end,s=a&&0===n.start,d=a&&n.s' +
                    'tart===t.length;d&&39===i?(e.preventDefault(),this.$set("goNext",[!1,!1])):!s||3' +
                    '7!==i&&8!==i||(e.preventDefault(),this.$set("goPrev",[!0,!1]))},onlyNum:function' +
                    '(e){var t={8:"backspace",9:"tab",37:"left",38:"up",39:"right",40:"down"};if(!(e.' +
                    'ctrlKey||e.keyCode in t||!e.which&&46===e.keyCode)){var i=this,n=String.fromChar' +
                    'Code(e.which),a=i.$input,s=a.range(),d=/^[0-9]$/.test(n),o="."===n,c=s.start===s' +
                    '.end&&s.start===a.val().length;d||e.preventDefault(),c&&(o?i.$set("goNext",[!1,!' +
                    '0]):d&&setTimeout(function(){+a.val()>100&&i.$set("goNext",[!1,!0])},0))}},focus' +
                    ':function(e,t){var i=this.$input;i.is(":visible")&&setTimeout(function(){i.focus' +
                    '(),e===!0&&i.caret(i.val().length),t!==!1&&i.select()},0)},onFocus:function(){th' +
                    'is.focused=!0},onBlur:function(){this.focused=!1,this.doFormula()},getValue:func' +
                    'tion(){return this.doFormula(),this.value}}),p=["part1","part2","part3","part4"]' +
                    ',u="_syncTimer",v=n.extend({$valuekey:"value",$tpl:'<em data-name="{{name}}" b-a' +
                    'ttr="attr" class="vpc-ip-input"><div b-tag="part" b-ref="part1" b-with="part1Cfg' +
                    '" data-disabled="{{disabled}}" type="{{type}}" min-mask="{{minMask-0}}" max-mask' +
                    '="{{maxMask-0}}" net="{{netParts[0]}}" b-model="part1"></div>.<div b-tag="part" ' +
                    'b-ref="part2" b-with="part2Cfg" data-disabled="{{disabled}}" type="{{type}}" min' +
                    '-mask="{{minMask-8}}" max-mask="{{maxMask-8}}" net="{{netParts[1]}}" b-model="pa' +
                    'rt2"></div>.<div b-tag="part" b-ref="part3" b-with="part3Cfg" data-disabled="{{d' +
                    'isabled}}" type="{{type}}" min-mask="{{minMask-16}}" max-mask="{{maxMask-16}}" n' +
                    'et="{{netParts[2]}}" b-model="part3"></div>.<div b-tag="part" b-ref="part4" b-wi' +
                    'th="part4Cfg" data-disabled="{{disabled}}" type="{{type}}" min-mask="{{minMask-2' +
                    '4}}" max-mask="{{maxMask-24}}" net="{{netParts[3]}}" b-model="part4"></div></em>' +
                    '',$beforeInit:function(){var e=this;n.prototype.$beforeInit.apply(this,arguments' +
                    '),e.locks=new s,this.$watch("value",function(){var t=(this.value||"...").split("' +
                    '.");p.forEach(function(i,n){e.$set(i,t[n])})},!0),this.$watch("net",function(){t' +
                    'his.$set("netParts",(this.net||"0.0.0.0").split("."))},!0),p.forEach(function(t)' +
                    '{var i=t+"Cfg";e.$set(i,$.extend({part:t},e[i]))})},$afterInit:function(){n.prot' +
                    'otype.$afterInit.apply(this,arguments);var e=this,t=e.$refs,i=function(){e[u]&&c' +
                    'learTimeout(e[u]),e.beginUpdating("watchUpdate"),e[u]=setTimeout(function(){e[u]' +
                    '&&(e[u]=null,e._syncValue(),e.updatePartsLimit()),e.endUpdating("watchUpdate")},' +
                    '0)};p.forEach(function(n,a){var s=t[n];a<p.length-1&&s.$watch("goNext",function(' +
                    'e){t[p[a+1]].focus(e[0],e[1])}),a>0&&s.$watch("goPrev",function(e){t[p[a-1]].foc' +
                    'us(e[0],e[1])}),e.$watch(n,i),s.$watch("goFormula",i)}),e.updatePartsLimit()},ea' +
                    'chPart:function(e){var t=this,i=t.$refs;p.forEach(function(n,a){var s=i[n];s&&e.' +
                    'call(t,s,a)})},beginUpdating:function(e){e=e||"default";var t=this,i=t.locks.isL' +
                    'ocked();t.locks.lock(e),i||t.eachPart(function(e){e.beginUpdating("parent")})},e' +
                    'ndUpdating:function(e){e=e||"default";var t=this;t.locks.unlock(e),t.locks.isLoc' +
                    'ked()||t.eachPart(function(e){e.endUpdating("parent")})},$beforeDestroy:function' +
                    '(){this[u]=null},getValue:function(){return this.value},_syncValue:function(){va' +
                    'r e=this,t=e.$refs,i=p.map(function(e){return t[e].value}),n=i.join(".");n!==thi' +
                    's.value&&this.$set("value",n)},updatePartsLimit:function(){var e=this,t=e.$refs,' +
                    'i=!0,n=o(e.net),a=e.minMask,s=function(e,t){var i=32-t,n=e>>>i<<i>>>0,a=n+(1<<i>' +
                    '>>0)-1;return{min:n,max:a}},d=function(e,t){return e>>>8*(3-t)&255},c=s(n,a),r=/' +
                    '\bip\b/.test(this.type),l=/\bnoGateway\b/.test(this.type),u=r,v=r,f=r&&l;f?c.min' +
                    '+=2:u&&c.min++,v&&c.max--,p.forEach(function(e,o){var r,l,p,u=t[e];i?(a=Math.max' +
                    '(a,8*o),r=s(n,a),l=Math.max(c.min,r.min),p=Math.min(c.max,r.max),u.$set({outerMi' +
                    'n:d(l,o),outerMax:d(p,o)})):u.$set({outerMin:0,outerMax:255}),i=i&&u.isValid(),i' +
                    '&&(n|=parseInt(u.value,10)<<8*(3-o)>>>0)})}},{defaults:$.extend({},n.prototype.d' +
                    'efaults,{type:"ip noGateway",value:"...",net:"0.0.0.0",minMask:0,maxMask:32})});' +
                    'v.tag("part",l),a.tag("vpc-ip-input",v),i.exports=v}),define("modules/vpc/jCaret' +
                    '",function(e,t,i){!function(e,t){var i=document.createElement("input"),n={setSel' +
                    'ectionRange:"setSelectionRange"in i||"selectionStart"in i,createTextRange:"creat' +
                    'eTextRange"in i||"selection"in document},a=/\r\n/g,s=/\r/g,d=function(t){return"' +
                    'undefined"!=typeof t.value?t.value:e(t).text()},o=function(t,i){"undefined"!=typ' +
                    'eof t.value?t.value=i:e(t).text(i)},c=function(e,t){var i=d(e).replace(s,""),n=i' +
                    '.length;return"undefined"==typeof t&&(t=n),t=Math.floor(t),0>t&&(t=n+t),0>t&&(t=' +
                    '0),t>n&&(t=n),t},r=function(e,t){return e.hasAttribute?e.hasAttribute(t):"undefi' +
                    'ned"!=typeof e[t]},l=function(e,t,i,n){this.start=e||0,this.end=t||0,this.length' +
                    '=i||0,this.text=n||""};l.prototype.toString=function(){return JSON.stringify(thi' +
                    's,null,"    ")};var p=function(e){return e.selectionStart},u=function(e){var t,i' +
                    ',n,s,o,c;return e.focus(),e.focus(),i=document.selection.createRange(),i&&i.pare' +
                    'ntElement()===e?(s=d(e),o=s.length,n=e.createTextRange(),n.moveToBookmark(i.getB' +
                    'ookmark()),c=e.createTextRange(),c.collapse(!1),t=n.compareEndPoints("StartToEnd' +
                    '",c)>-1?s.replace(a,"\n").length:-n.moveStart("character",-o)):0},v=function(e){' +
                    'return e?n.setSelectionRange?p(e):n.createTextRange?u(e):t:t},f=function(e,t){e.' +
                    'setSelectionRange(t,t)},m=function(e,t){var i=e.createTextRange();i.move("charac' +
                    'ter",t),i.select()},h=function(e,t){e.focus(),t=c(e,t),n.setSelectionRange?f(e,t' +
                    '):n.createTextRange&&m(e,t)},b=function(e,t){var i=v(e),n=d(e).replace(s,""),a=+' +
                    '(i+t.length+(n.length-i)),c=+e.getAttribute("maxlength");if(r(e,"maxlength")&&a>' +
                    'c){var l=t.length-(a-c);t=t.substr(0,l)}o(e,n.substr(0,i)+t+n.substr(i)),h(e,i+t' +
                    '.length)},g=function(e){var t=new l;t.start=e.selectionStart,t.end=e.selectionEn' +
                    'd;var i=Math.min(t.start,t.end),n=Math.max(t.start,t.end);return t.length=n-i,t.' +
                    'text=d(e).substring(i,n),t},I=function(e){var t=new l;e.focus();var i=document.s' +
                    'election.createRange();if(i&&i.parentElement()===e){var n,a,s,o,c=0,r=0,p=d(e);n' +
                    '=p.length,a=p.replace(/\r\n/g,"\n"),s=e.createTextRange(),s.moveToBookmark(i.get' +
                    'Bookmark()),o=e.createTextRange(),o.collapse(!1),s.compareEndPoints("StartToEnd"' +
                    ',o)>-1?c=r=n:(c=-s.moveStart("character",-n),c+=a.slice(0,c).split("\n").length-' +
                    '1,s.compareEndPoints("EndToEnd",o)>-1?r=n:(r=-s.moveEnd("character",-n),r+=a.sli' +
                    'ce(0,r).split("\n").length-1)),c-=p.substring(0,c).split("\r\n").length-1,r-=p.s' +
                    'ubstring(0,r).split("\r\n").length-1,t.start=c,t.end=r,t.length=t.end-t.start,t.' +
                    'text=a.substr(t.start,t.length)}return t},w=function(e){return e?n.setSelectionR' +
                    'ange?g(e):n.createTextRange?I(e):t:t},y=function(e,t,i){e.setSelectionRange(t,i)' +
                    '},x=function(e,t,i){var n,s=e.createTextRange(),o=d(e),c=t;for(n=0;c>n;n++)-1!==' +
                    'o.substr(n,1).search(a)&&(t-=1);for(c=i,n=0;c>n;n++)-1!==o.substr(n,1).search(a)' +
                    '&&(i-=1);s.moveEnd("textedit",-1),s.moveStart("character",t),s.moveEnd("characte' +
                    'r",i-t),s.select()},$=function(e,t,i){t=c(e,t),i=c(e,i),n.setSelectionRange?y(e,' +
                    't,i):n.createTextRange&&x(e,t,i)},C=function(t,i){var n=e(t),a=n.val(),s=w(t),d=' +
                    '+(s.start+i.length+(a.length-s.end)),o=+n.attr("maxlength");if(n.is("[maxlength]' +
                    '")&&d>o){var c=i.length-(d-o);i=i.substr(0,c)}var r=a.substr(0,s.start),l=a.subs' +
                    'tr(s.end);n.val(r+i+l);var p=s.start,u=p+i.length;$(t,s.length?p:u,u)},T=functio' +
                    'n(e){var t=window.getSelection(),i=document.createRange();i.selectNodeContents(e' +
                    '),t.removeAllRanges(),t.addRange(i)},N=function(e){var t=document.body.createTex' +
                    'tRange();t.moveToElementText(e),t.select()},k=function(t){var i=e(t);return i.is' +
                    '("input, textarea")||t.select?void i.select():void(n.setSelectionRange?T(t):n.cr' +
                    'eateTextRange&&N(t))},P=function(){document.selection?document.selection.empty()' +
                    ':window.getSelection&&window.getSelection().removeAllRanges()};e.fn.extend({care' +
                    't:function(){var e=this.filter("input, textarea");if(0===arguments.length){var t' +
                    '=e.get(0);return v(t)}if("number"==typeof arguments[0]){var i=arguments[0];e.eac' +
                    'h(function(e,t){h(t,i)})}else{var n=arguments[0];e.each(function(e,t){b(t,n)})}r' +
                    'eturn this},range:function(){var e=this.filter("input, textarea");if(0===argumen' +
                    'ts.length){var t=e.get(0);return w(t)}if("number"==typeof arguments[0]){var i=ar' +
                    'guments[0],n=arguments[1];e.each(function(e,t){$(t,i,n)})}else{var a=arguments[0' +
                    '];e.each(function(e,t){C(t,a)})}return this},selectAll:function(){return this.ea' +
                    'ch(function(e,t){k(t)})}}),e.extend({deselectAll:function(){return P(),this}})}(' +
                    'jQuery)}),define("modules/vpc/moduleBase",function(e,t,i){var n,a=e("router"),s=' +
                    'e("./cacher"),d=e("widget/page/BeeRoute"),o=d.prototype,c=e("./util");n=d.extend' +
                    '({Bee:e("./BeeBase"),pageClass:"vpc-wrap",routeBase:"",params:$.extend({regionId' +
                    ':{route:"rid",fixed:!0,initial:function(){return c.getRegionId()},defaults:"",fo' +
                    'rmula:function(e){return""+e||c.getRegionId()}},vpcId:{defaults:"",formula:funct' +
                    'ion(e){return+e||""}},unVpcId:{initial:function(){return c.getUnVpcId(c.getRegio' +
                    'nId())},defaults:"",formula:function(e){return e||""}},page:{defaults:1,formula:' +
                    'function(e){return+e||1}},count:{defaults:20,formula:function(e){return+e||20}}}' +
                    ',o.params),render:function(e){var t=!0;c.hasInWhiteList().fail(function(){t=!1,s' +
                    'etTimeout(function(){a.navigate("/vpc",void 0,!0)},10)}),t&&o.render.apply(this,' +
                    'arguments)},getBeeConfig:function(){var e=this,t=o.getBeeConfig.apply(e,argument' +
                    's);return $.extend(t,{getWhiteList:function(){return s.getRegionIdList()}})},vpc' +
                    'Id2UnId:function(e){var t,i=$.Deferred(),n=this.bee,a=n.vpcId;return n.regionId&' +
                    '&a&&!n.unVpcId?s.getVpc(n.regionId,a).done(function(e){t=!0,n.$set({vpcId:null,o' +
                    'ldVpcId:e.vpcId,unVpcId:e.unVpcId}),t=!1}).fail(function(){n.$set({vpcId:null})}' +
                    ').always(i.resolve):i.resolve(),e&&["regionId","unVpcId"].forEach(function(e){n.' +
                    '$watch(e,function(){t||s.getVpcListEx(n.regionId).done(function(e){var t,i,a=e.v' +
                    'pcList;for(t=0;t<a.length;t++)if(i=a[t],i.unVpcId===n.unVpcId)return void n.$set' +
                    '("oldVpcId",i.vpcId);n.$set("oldVpcId",null)})})}),i},getModDom:function(e,t){re' +
                    'turn e.find('[data-mod="'+t+'"]')[0]}}),i.exports=new n}),define("modules/vpc/na' +
                    't/Add",function(e,t,i){var n=e("../BeeBase"),a=e("appUtil"),s=e("./util"),d=e(".' +
                    './cacher"),o=e("./cacher"),c=e("../vpc/DropDown");e("../Checkbox");var r=n.exten' +
                    'd({$tpl:'    <div class="tc-15-rich-dialog">        <div class="tc-15-list-wrap"' +
                    '>            <ul class="form-list">                <li>                    <div ' +
                    'class="form-label">                        <label for="">网关名称</label>           ' +
                    '         </div>                    <div class="form-input">                     ' +
                    '   <div class="form-unit">                            <input type="text" class="' +
                    'tc-15-input-text m" data-name="natName" b-model="natName">                      ' +
                    '  </div>                    </div>                </li>                <li>     ' +
                    '               <div class="form-label">                        <label for="">网关类' +
                    '型</label>                    </div>                    <div class="form-input"> ' +
                    '                       <div class="form-unit">                            <type-' +
                    'field select-style="width: 280px;" relevance="" b-model="maxConcurrent"></type-f' +
                    'ield>                        </div>                    </div>                </l' +
                    'i>                <li>                    <div class="form-label">              ' +
                    '          <label for="">所在地域</label>                    </div>                  ' +
                    '  <div class="form-input">                        <p>{{REGION_MAP[regionId]}}</p' +
                    '>                    </div>                </li>                <li>            ' +
                    '        <div class="form-label">                        <label for="">所属网络</labe' +
                    'l>                    </div>                    <div class="form-input">        ' +
                    '                <div class="form-unit">                            <vpc-select c' +
                    'ls="m" region-id="{{regionId}}" b-model="vpcId" name="vpcId"></vpc-select>      ' +
                    '                  </div>                    </div>                </li>         ' +
                    '       <!--   <li>                       <div class="form-label">               ' +
                    '            <label for="">带宽上限</label>                       </div>             ' +
                    '          <div class="form-input">                           <div class="form-un' +
                    'it">                               <bandwidth-field select-style="width: 280px;"' +
                    ' relevance="" b-model="bandwidth"></bandwidth-field>                           <' +
                    '/div>                       </div>                   </li>-->                <li' +
                    '>                    <div class="form-label">                        <label for=' +
                    '"">弹性IP</label>                    </div>                    <div class="form-in' +
                    'put">                        <p b-repeat="item in eipSet">                      ' +
                    '      <eip-field b-model="item.ip" region-id="{{regionId}}" used="{{eipSelected}' +
                    '}" uid="{{$index}}"></eip-field>                            {{>markUsed(item.ip,' +
                    ' $index)}}                            <a href="javascript:;" b-if="!reachMin" b-' +
                    'on-click="removeEip(item.ip, $index)">删除</a>                        </p>        ' +
                    '                <div class="form-input-help add-ip-box">                        ' +
                    '    <a b-if="!reachMax" href="javascript:;" b-on-click="addEip()">+ 添加绑定IP</a> <' +
                    'span>最多可绑定{{MAX_EIP_NUM}}个IP</span>                            <div class="tc-15' +
                    '-bubble-icon"><i class="tc-icon icon-what"></i>                                <' +
                    'div class="tc-15-bubble tc-15-bubble-top">                                    <d' +
                    'iv class="tc-15-bubble-inner">NAT网关配置IP数限制了NAT网关访问某IP端<br>口的最大并发数，单IP最大连接数为<br>6' +
                    '5000，多IP时最大并发连接数为65000xN.<br>如需绑定更多IP请电话联系客户经理。</div>                           ' +
                    '     </div>                            </div>                        </div>     ' +
                    '               </div>                </li>                <li>                  ' +
                    '  <div class="form-label">                        <label for="">网关费用</label>    ' +
                    '                </div>                    <div class="form-input">              ' +
                    '          <div class="form-unit">                            <span class="money-' +
                    'tip" b-if="!igwPriceLoading">{{(igwPrice/100).toFixed(2)}}元<em>/小时</em></span>  ' +
                    '                          <i class="n-loading-icon" b-if="igwPriceLoading"></i> ' +
                    '                       </div>                    </div>                </li>    ' +
                    '            <li>                    <div class="form-label">                    ' +
                    '    <label for="">网络费用</label>                    </div>                    <div' +
                    ' class="form-input">                        <div class="form-unit">             ' +
                    '               <span class="money-tip" b-if="!networkPriceLoading">{{(networkPri' +
                    'ce/100).toFixed(2)}}元<em>/GB</em></span>                            <i class="n-' +
                    'loading-icon" b-if="networkPriceLoading"></i>                        </div>     ' +
                    '               </div>                </li>            </ul>        </div>    </d' +
                    'iv>',$beforeInit:function(){var e=this;e.__super__.$beforeInit.apply(e,arguments' +
                    '),e.$watch("[maxConcurrent, regionId]",function(){e.$set("igwPriceLoading",!0);v' +
                    'ar t=e.maxConcurrent,i=e.regionId;t>0&&i&&o.getIgwPrice(i,t).done(function(t){e.' +
                    '$set("igwPrice",t)}).always(function(){e.$set("igwPriceLoading",!1)})},!0),e.$wa' +
                    'tch("regionId",function(){e.$set("networkPriceLoading",!0);var t=e.regionId;t&&(' +
                    'o.getNetworkPrice(t).done(function(t){e.$set("networkPrice",t)}).always(function' +
                    '(){e.$set("networkPriceLoading",!1)}),o.getLimits(t).done(function(t){e.$set(t)}' +
                    '))},!0),d.getRegionMap().done(function(t){e.$set("REGION_MAP",t)}),a.beeBind(e,e' +
                    ',"eipSet.length<=1","reachMin"),a.beeBind(e,e,"eipSet.length>=MAX_EIP_NUM","reac' +
                    'hMax")},markUsed:function(e,t){var i=this,n=i.eipSelected,a={},s={};Object.keys(' +
                    'n).forEach(function(e){var t=n[e];a[t]=e}),a[t]=e,Object.keys(a).forEach(functio' +
                    'n(e){var t=a[e];t&&(s[t]=e)}),i.$set("eipSelected",s)},removeEip:function(e,t){v' +
                    'ar i=this;i.eipSet.splice(t,1),i.markUsed("",i.eipSet.length)},addEip:function()' +
                    '{var e=this;e.eipSet.push({ip:""})},getLastMatchEipInput:function(e){var t=this;' +
                    'return $(t.$el).find('select[data-name="eip"]').filter(function(){return null===' +
                    'e||$(this).val()===e}).last()}},{defaults:$.extend({eipSelected:{},eipSet:[{}],b' +
                    'andwidth:0},s)});r.tag("vpc-select",c.extend({},{defaults:{textFields:["unVpcId"' +
                    ',"vpcName","vpcCIDR"],useUnId:!0,relevance:!1,selectStyle:{width:"280px"}}})),r.' +
                    'tag("eip-field",e("./field/Eip")),r.tag("bandwidth-field",e("./field/Bandwidth")' +
                    '),r.tag("type-field",e("./field/Type")),i.exports=r}),define("modules/vpc/nat/ca' +
                    'cher",function(e,t,i){var n=e("widget/cacher/Cacher"),a=e("../cApi"),s=e("manage' +
                    'r"),d=e("models/trade"),o=e("constants"),c=o.VPC_LIMIT,r={MAX_IGW_NUM:c.NAT_PER_' +
                    'VPC_LIMIT_TYPE,MAX_EIP_NUM:c.IP_PER_NAT_LIMIT_TYPE},l={getLimits:function(e,t,i)' +
                    '{a.request(e,"DescribeVpcLimit",{type:Object.keys(r).map(function(e){return r[e]' +
                    '})}).then(function(e){var i=e.data.limit,n={};Object.keys(r).forEach(function(e)' +
                    '{n[e]=i[r[e]]}),t(n)},i)},getUsableEipList:function(e,t,i){s.delegateCApi(e,"eip' +
                    '","DescribeEip",{status:[4],offset:0,limit:100},function(e){t(e.data.eipSet.map(' +
                    'function(e){return e.eip}))},i)},getIgwPrice:function(e,t,i,n){d.request({name:"' +
                    'getPrice",data:{regionId:e,payMode:0,goodsCategoryId:"100030",goodsNum:1,goodsDe' +
                    'tail:{timeUnit:"h",size:t/1e4,pid:"10530",timeSpan:1}},success:function(e){i(e[0' +
                    '].realTotalCost)},fail:n})},getNetworkPrice:function(e,t,i){d.request({name:"get' +
                    'Price",data:{regionId:e,payMode:0,goodsCategoryId:"100033",goodsNum:1,goodsDetai' +
                    'l:{traffic:"1",timeUnit:"h",pid:"10531",timeSpan:1}},success:function(e){t(e[0].' +
                    'realTotalCost)},fail:i})}};i.exports=new n([[l,"getLimits"],{host:l,fnName:"getU' +
                    'sableEipList",ns:"eip.$0.ALL",lifetime:6e4},[l,"getIgwPrice"],[l,"getNetworkPric' +
                    'e"]])}),define("modules/vpc/nat/Chart",function(e,t,i){var n=e("widget/chart/Rec' +
                    'ent"),a=e("widget/cacher/Cacher"),s=e("manager"),d={getChartData:function(e,t,i,' +
                    'n,a){var d=$.Deferred();return s.queryBarad(e,{type:"igw",dimension:{uniq_nat_id' +
                    ':t},metric:["conns","wan_in_byte","wan_out_byte","wan_in_pkg","wan_out_pkg"],sta' +
                    'rtTime:i,endTime:n,period:a},d.resolve,d.reject),d}},o=new a([{host:d,fnName:"ge' +
                    'tChartData",promise:!0}]),c=function(){var e=$.Deferred(),t=this,i=t.composedId;' +
                    'return o.getFreshChartData(i.regionId,i.natId,t.fromTime,t.toTime,t.dayView?8640' +
                    '0:300).done(function(i){var n=i[t.metric]||[],a=t.toMb?1048576:t.toMbps?131072:1' +
                    ';t.div>1&&(a*=t.div);for(var s=!1,d=0,o=n.length;o>d;d++)a>1&&n[d]&&(n[d]=1*(n[d' +
                    ']/a).toFixed(3),t.round&&(n[d]=Math.round(n[d]))),(n[d]||0===n[d])&&(s=!0);s||(n' +
                    '=[]),e.resolve({y:n})}).fail(function(){e.resolve({y:[]})}),e};i.exports=n.exten' +
                    'd({isComposedIdAvailable:function(e){return e&&e.regionId&&e.natId}},{defaults:{' +
                    'composedIdExpression:"{regionId:regionId, natId:natId}",charts:[{title:"网络出带宽",u' +
                    'nit:"Mbps",metric:"wan_out_byte",toMbps:!0,div:60,loadData:c},{title:"网络入带宽",uni' +
                    't:"Mbps",metric:"wan_in_byte",toMbps:!0,div:60,loadData:c},{title:"网络出包量",unit:"' +
                    'pps",div:60,round:!0,metric:"wan_out_pkg",loadData:c},{title:"网络入包量",unit:"pps",' +
                    'div:60,round:!0,metric:"wan_in_pkg",loadData:c},{title:"连接数",unit:"个",metric:"co' +
                    'nns",loadData:c}]}})}),define("modules/vpc/nat/Detail",function(e,t,i){var n=e("' +
                    '../BeeBase"),a=e("../cApi"),s=e("./Chart"),d=e("./InfoPanel"),o=e("./util"),c=(e' +
                    '("../cacher"),e("appUtil")),r="natId",l=i.exports=n.extend({$tpl:'    <div b-sty' +
                    'le="{display: hidden ? \'none\' : \'\'}">        <div class="return-title-panel ' +
                    'bottom-border" b-style="{display:notFound||loading?\'none\':\'\'}">            <' +
                    'a href="javascript:;" b-on-click="back()" class="btn-return"><i class="btn-back-' +
                    'icon"></i><span>返回</span></a>            <i class="line"></i>            <h2>{{d' +
                    'ata&&data.natName}}详情</h2>            <div class="manage-area-title-right">     ' +
                    '           <a href="https://www.qcloud.com/doc/product/215/4975" target="_blank"' +
                    '>                    <b class="links-icon"></b> NAT网关帮助文档                </a>   ' +
                    '         </div>        </div>        <div b-if="loading" style="text-align:cente' +
                    'r; padding:100px;">加载中...</div>        <div b-if="notFound" style="text-align:ce' +
                    'nter; padding:100px;">未查询到指定NAT网关, <a href="javascript:;" b-on-click="$set(\'id' +
                    '\', \'\')">返回列表</a></div>        <div class="tab-switch" b-style="{display:notFo' +
                    'und||loading?\'none\':\'\'}">            <a class="{{tab===\'info\' ? \'cur\' : ' +
                    '\'\'}}" b-on-click="$set(\'tab\', \'info\')" data-report="vpc.NATGateway.detail"' +
                    ' href="javascript:;">基本信息</a>            <a class="{{tab===\'chart\' ? \'cur\' :' +
                    ' \'\'}}"  b-on-click="$set(\'tab\', \'chart\')" href="javascript:;">监控</a>      ' +
                    '      <a class="{{tab===\'ip\' ? \'cur\' : \'\'}}" data-report="vpc.NATGateway.d' +
                    'etail.EIP" b-on-click="$set(\'tab\', \'ip\')" href="javascript:;">关联弹性IP</a>    ' +
                    '    </div>        <info b-with="{regionId:regionId, data: data, updateCallback: ' +
                    'infoUpdateCallback.bind(this)}" b-if="!loading && !notFound && tab===\'info\'"><' +
                    '/info>        <div class="charts-panel vpc-line-date-wrap" b-if="!loading && !no' +
                    'tFound && tab===\'chart\'">            <chart b-with="{regionId:regionId, natId:' +
                    ' data&&data.natId}"></chart>        </div>        <div class="charts-panel" b-if' +
                    '="!loading && !notFound && tab===\'ip\'">            <ip-panel b-with="{regionId' +
                    ':regionId, data: data, updateCallback: infoUpdateCallback.bind(this)}"></ip-pane' +
                    'l>        </div>    </div>',$valuekey:"id",idParser:a.idParser,back:function(){h' +
                    'istory.back()},watchFields:["regionId","id"],$beforeInit:function(){var e=this;l' +
                    '.__super__.$beforeInit.apply(e,arguments),e.$watch("["+e.watchFields.join(",")+"' +
                    ']",e.bufferReload=c.bufferFn(function(){e.loadData()},100)),e.bindDetail&&e.bind' +
                    'Detail(e)},infoUpdateCallback:function(e){var t=this;Object.keys(e).forEach(func' +
                    'tion(i){t.$set("data."+i,e[i])}),t.updateCallback&&t.updateCallback(e)},loadData' +
                    ':function(){var e=this,t=e.id,i=e.regionId;return!i||!t||e.data&&e.data[r]===t?v' +
                    'oid e.$set({loading:!1,notFound:!1}):(e.$set({loading:!0,notFound:!1,data:{}}),v' +
                    'oid $.when(a.request(i,"DescribeNatGateway",{offset:0,limit:1,natId:t}),o.getDat' +
                    'aProcessorTask(i)).then(function(t,i){var n=t.data,a=!1;(!n||n.length<0)&&(a=!0)' +
                    ';var s=n&&n[0]||{};i(s),e.$set({notFound:a,data:s})}).always(function(){e.$set("' +
                    'loading",!1)}))}},{defaults:{tab:"info",loading:!0}});l.tag("info",d),l.tag("cha' +
                    'rt",s),l.tag("ip-panel",e("./IPPanel"))}),define("modules/vpc/nat/field/Bandwidt' +
                    'h",function(e,t,i){var n=e("../../DropDown"),a=e("../util");i.exports=n.extend({' +
                    'watchFields:[],getData:function(e,t){e(a.BANDWIDTH_LIST.map(function(e){return{t' +
                    'ext:e?e+"Mbps":"不限制",value:e}}))}},{defaults:{creatable:!0}})}),define("modules/' +
                    'vpc/nat/field/Eip",function(e,t,i){var n=e("../../DropDown"),a=e("../cacher"),s=' +
                    '(e("manager"),i.exports=n.extend({name:"eip",watchFields:["regionId","used"],$be' +
                    'foreInit:function(){var e=this;s.__super__.$beforeInit.apply(e,arguments),e.$wat' +
                    'ch("value",function(t){e.sync&&e.sync(t,e.uid)})},getData:function(e,t){var i=th' +
                    'is,n=i.used||{};a.getUsableEipList(i.regionId).then(function(t){var a=t.map(func' +
                    'tion(e){var t=n[e]&&n[e]!==""+i.uid;return{text:e,value:e,disabled:t}});i.creata' +
                    'ble&&(a=a.concat({text:"新建弹性IP",value:""})),e(a)},t)}},{defaults:{creatable:!0}}' +
                    '))}),define("modules/vpc/nat/field/Type",function(e,t,i){var n=e("../../DropDown' +
                    '"),a=e("../util");i.exports=n.extend({watchFields:[],getData:function(e,t){e(a.T' +
                    'YPE_LIST.map(function(e){return{text:e.text+"（最大并发连接数"+e.value/1e4+"W）",value:e.' +
                    'value}}))}},{defaults:{}})}),define("modules/vpc/nat/InfoPanel",function(e,t,i){' +
                    'var n,a=e("../cacher"),s=e("../cApi"),d=(e("appUtil"),e("./util")),o=(e("./valid' +
                    'ator"),e("../Confirm"),e("reporter")),c=e("widget/popover/TextEditor"),r=c.exten' +
                    'd({$tpl:'    <div class="in-place-editor" style="position:absolute;">        {{>' +
                    'content}}        <div class="action-line">            <button class="tc-15-btn m' +
                    '" b-on="{click:handleYes}">{{yesText}}</button>            <button class="tc-15-' +
                    'btn m weak" b-on="{click:handleNo}">{{noText}}</button>        </div>    </div>'' +
                    ',content:'    <div class="form-unit {{invalid ? \'is-error\' : \'\'}}">        <' +
                    'input type="text" class="tc-15-input-text m" maxlength="{{maxLength}}"          ' +
                    '     b-style="inputStyle"               b-model="value" b-on="{keypress : ifEnte' +
                    'r}">        <span b-if="postfix" class="text-suffix">{{postfix}}</span>        <' +
                    'p class="form-input-help" b-if="dynamicTips && !invalid && !tips">{{>dynamicTips' +
                    '}}</p>        <p class="form-input-help" b-if="invalid || tips">{{invalid || tip' +
                    's}}</p>    </div>'}),l=e("./RenameBubble"),p=e("./cacher"),u=e("../BeeBase");e("' +
                    'widget/grid/LocalGrid");r.tag("bandwidth-field",e("./field/Bandwidth")),r.tag("t' +
                    'ype-field",e("./field/Type")),n=i.exports=u.extend({$tpl:'    <div class="charts' +
                    '-panel">        <div class="param-box nat-form">            <div class="param-hd' +
                    '">                <h3>基本信息</h3>            </div>            <div class="param-b' +
                    'd">                <ul class="item-descr-list">                    <li>         ' +
                    '               <span class="item-descr-tit">网关名称</span>                        <' +
                    'span class="item-descr-txt">{{data.natName}}                            <a href=' +
                    '"javascript:;" b-if="!renaming && !data.$loading" b-on="{click:rename}">更改</a>  ' +
                    '                          <i b-if="renaming" class="n-loading-icon"></i>        ' +
                    '                </span>                    </li>                    <li>        ' +
                    '                <span class="item-descr-tit">网关ID</span>                        ' +
                    '<span class="item-descr-txt">{{data.natId}}</span>                    </li>     ' +
                    '               <li>                        <span class="item-descr-tit">网关类型</sp' +
                    'an>                        <span class="tc-15-list-det">{{TYPE_NAMES[data.maxCon' +
                    'current]}}(最大并发连接数{{data.maxConcurrent/10000}}W)                            <a h' +
                    'ref="javascript:;" b-if="!maxConcurrentChanging && !data.$loading" b-on="{click:' +
                    'changeType}">更改</a>                            <i b-if="maxConcurrentChanging" c' +
                    'lass="n-loading-icon"></i>                        </span>                    </l' +
                    'i>                    <li>                        <span class="item-descr-tit">所' +
                    '属网络</span>                        <span class="item-descr-txt">                 ' +
                    '           <a data-event="nav" href="/vpc/vpc?rid={{regionId}}&unVpcId={{data.un' +
                    'VpcId}}">{{data.unVpcId}}</a>                            ({{data.vpcName}} | {{d' +
                    'ata.vpcCidrBlock}})</span>                    </li>                    <li>     ' +
                    '                   <span class="item-descr-tit">所在地域</span>                     ' +
                    '   <span class="tc-15-list-det">{{REGION_MAP && REGION_MAP[regionId]}}</span>   ' +
                    '                 </li>                    <li>                        <span clas' +
                    's="item-descr-tit">创建时间</span>                        <span class="item-descr-tx' +
                    't">{{data.createTime}}</span>                    </li>                </ul>     ' +
                    '       </div>        </div><!--        <div class="param-box nat-form">         ' +
                    '   <div class="param-hd">                <h3>QOS设置</h3>            </div>       ' +
                    '     <div class="param-bd">                <ul class="item-descr-list">         ' +
                    '           <li>                        <span class="item-descr-tit">带宽上限</span> ' +
                    '                       <span class="item-descr-txt">{{data.bandwidth!=0 ? data.b' +
                    'andwidth+\'Mbps\' : \'不限制\'}}                            <a href="javascript:;" ' +
                    'b-if="!bandwidthChanging" b-on="{click:changeBandwidth}">修改带宽</a>               ' +
                    '             <i b-if="bandwidthChanging" class="n-loading-icon"></i>            ' +
                    '            </span>                    </li>                </ul>            </d' +
                    'iv>        </div>-->        <div class="param-box nat-form">            <div cla' +
                    'ss="param-hd">                <h3>相关路由策略</h3>            </div>            <rout' +
                    'e-info region-id="{{regionId}}" un-vpc-id="{{data && data.unVpcId}}" id="{{data ' +
                    '&& data.natId}}"></route-info>        </div>    </div>',$beforeInit:function(){v' +
                    'ar e=this;n.__super__.$beforeInit.apply(e,arguments),a.getRegionMap().done(funct' +
                    'ion(t){e.$set("REGION_MAP",t)})},rename:function(e){var t=this,i=t.data,n=i.natN' +
                    'ame;new l({$data:{value:n,composedId:{regionId:t.regionId,vpcId:i.vpcId,natId:i.' +
                    'natId}},target:$(e.currentTarget).parent(),onSave:function(e,i,n){this.handleSim' +
                    'pleEdit({def:e,host:t,key:"data.natName",loadingKey:"renaming",newValue:i,oldVal' +
                    'ue:n}).done(function(){t.updateCallback({natName:i})})}})},changeType:function(e' +
                    '){var t=this,i=t.data,n=i.maxConcurrent,a=new r({$data:{value:n+"",oldValue:n,TY' +
                    'PE_LIST:d.TYPE_LIST},content:'    <style>        .clue-customize-type-editor .mo' +
                    'ney-tip{            font-size: 18px;        }        .clue-customize-type-editor' +
                    ' .tc-15-msg{            margin: 10px 0 0 0;        }    </style>    <div class="' +
                    'clue-customize-type-editor form-unit {{invalid ? \'is-error\' : \'\'}}">        ' +
                    '<select class="tc-15-select m" b-model="value">            <option b-repeat="opt' +
                    'ion in TYPE_LIST" value="{{option.value}}" disabled?="option.disabled" selected?' +
                    '="option.value == value">{{option.text}}（最大并发连接数{{option.value/10000}}W）</option' +
                    '>        </select>        <p class="form-input-help text-strong">当前价格：<span clas' +
                    's="money-tip" b-if="oldPrice">{{oldPrice}}元/小时</span><i class="n-loading-icon" b' +
                    '-if="!oldPrice"></i></p>        <p class="form-input-help text-strong">修改后价格：<sp' +
                    'an class="money-tip" b-if="newPrice">{{newPrice}}元/小时</span><i class="n-loading-' +
                    'icon" b-if="!newPrice"></i></p>        <p class="tc-15-msg">按每小时内最高配置计费</p>    <' +
                    '/div>',
target:$(e.currentTarget).parent(),doSave:function(e,n){var a=$.Deferred' +
                    '(),o=t.regionId;return s.request(o,"UpgradeNatGateway",{vpcId:i.vpcId,natId:i.na' +
                    'tId,maxConcurrent:e}).then(function(e){var t=e.billId||e.data.billId;d.checkBill' +
                    'Task(o,t).then(a.resolve,a.reject)},a.reject),a},onSave:function(e,i,n){this.han' +
                    'dleSimpleEdit({def:e,host:t,key:"data.maxConcurrent",loadingKey:"maxConcurrentCh' +
                    'anging",newValue:i,oldValue:n}).done(function(){t.updateCallback({maxConcurrent:' +
                    'i})})}});a.$watch("oldValue",function(e){a.$set("oldPrice",""),p.getIgwPrice(t.r' +
                    'egionId,e).done(function(e){a.$set("oldPrice",(e/100).toFixed(2))})},!0),a.$watc' +
                    'h("value",function(e){a.$set("newPrice",""),p.getIgwPrice(t.regionId,e).done(fun' +
                    'ction(e){a.$set("newPrice",(e/100).toFixed(2))})},!0)},changeBandwidth:function(' +
                    'e){var t=this,i=t.data,n=i.bandwidth;new r({$data:{value:n+"",BANDWIDTH_LIST:d.B' +
                    'ANDWIDTH_LIST},content:'    <div class="form-unit {{invalid ? \'is-error\' : \'' +
                    '\'}}">        <select class="tc-15-select m" b-model="value">            <option' +
                    ' b-repeat="bandwidth in BANDWIDTH_LIST" value="{{bandwidth}}" selected?="bandwid' +
                    'th == value">{{bandwidth ? bandwidth+\'Mbps\' : \'不限制\'}}</option>        </sele' +
                    'ct>    </div>',target:$(e.currentTarget).parent(),doSave:function(e,n){return s.' +
                    'request(t.regionId,"ModifyNatGateway",{vpcId:i.vpcId,natId:i.natId,bandwidth:e})' +
                    '},onSave:function(e,i,n){this.handleSimpleEdit({def:e,host:t,key:"data.bandwidth' +
                    '",loadingKey:"bandwidthChanging",newValue:i,oldValue:n}).done(function(){t.updat' +
                    'eCallback({bandwidth:i})}),o.click("vpc.NATGateway.detail.changeBandwidth")}})}}' +
                    ',{defaults:$.extend({updateCallback:$.noop},d)}),n.tag("route-info",e("../RouteI' +
                    'nfo"))}),define("modules/vpc/nat/IPPanel",function(e,t,i){var n=e("qccomponent")' +
                    ',a=e("../cacher"),s=e("../cApi"),d=e("appUtil"),o=e("widget/grid/LocalGrid"),c=e' +
                    '("../Confirm"),r=e("./util"),l=e("reporter"),p=e("./cacher"),u=i.exports=n.exten' +
                    'd({$tpl:'    <div class="param-box nat-form">        <div class="param-hd">     ' +
                    '       <h3>关联弹性IP</h3>        </div>        <div class="network-ip">            ' +
                    '<p style="font-size:14px;">NAT网关最多可绑定{{MAX_EIP_NUM}}个同地域弹性IP。</p>            <!-' +
                    '-请使用css设置列宽，使用js计算表格整体高度-->            <eip-grid b-ref="grid"                   ' +
                    '   region-id="{{regionId}}"                      handle-action="{{handleAction.b' +
                    'ind(this)}}"                      reach-min="{{reachMin}}" reach-max="{{reachMax' +
                    '}}"                      callback="{{eipGridCallback.bind(this)}}"              ' +
                    '        list="{{eipList}}"></eip-grid>            <div style="margin: 20px 0 50p' +
                    'x 20px;">                <a href="javascript:;"                   data-z-index="' +
                    '9999"                   style="font-size:14px;"                   b-style="{colo' +
                    'r: (reachMax || eipEditing) ? \'#a2a2a2\' : undefined, cursor:reachMax ? \'defau' +
                    'lt\' : undefined}"                   b-on-click="onBind()"                   dat' +
                    'a-title="{{reachMax ? \'已达绑定IP数上限\' : (eipEditing ? \'正在编辑中\' : \'\')}}">+ 绑定弹性I' +
                    'P</a>            </div>        </div>    </div>',$beforeInit:function(){var e=th' +
                    'is;u.__super__.$beforeInit.apply(e,arguments),e.$watch("regionId",function(t){t&' +
                    '&p.getLimits(t).done(function(t){e.$set(t)})},!0),d.beeBind(e,e,"transformEipLis' +
                    't(data.eipSet)","eipList"),d.beeBind(e,e,"eipGridSize <= 1 || data.eipSet.length' +
                    ' <= 1","reachMin"),d.beeBind(e,e,"eipGridSize >= MAX_EIP_NUM","reachMax")},$afte' +
                    'rInit:function(){var e=this;u.__super__.$afterInit.apply(e,arguments),d.beeBind(' +
                    'e.$refs.grid,e,"editing","eipEditing")},eipGridCallback:function(e){d.beeBind(e,' +
                    'this,"list.length","eipGridSize")},transformEipList:function(e){return e.map(fun' +
                    'ction(e){return{ip:e}})},handleAction:function(e,t,i,n){var a="onEip"+d.capitali' +
                    'ze(e),s=[].slice.call(arguments,1);this[a]&&this[a].apply(this,s)},onBind:functi' +
                    'on(){var e=this,t=e.$refs.grid;e.reachMax||e.eipEditing||t.list.push({editing:!0' +
                    '})},onEipCommit:function(e,t,i){var n=this,d=n.data,o=n.$refs.grid,c=e.ip;o.list' +
                    '.$set(t,{checking:!0}),s.requestTask(n.regionId,"EipBindNatGateway",{natId:d.nat' +
                    'Id,vpcId:d.vpcId,assignedEipSet:c?[c]:[],autoAllocEipNum:c?0:1}).done(function()' +
                    '{s.request(n.regionId,"DescribeNatGateway",{natId:d.natId,offset:0,limit:1}).don' +
                    'e(function(e){n.updateCallback(e.data[0]),a.resetNamespace("igw",n.regionId,e.vp' +
                    'cId),p.resetNamespace("eip",n.regionId)})}).fail(function(){o.list.$set(t,{check' +
                    'ing:!1})}),l.click("vpc.NATGateway.detail.EIP.addEIP")},onEipCancel:function(e,t' +
                    ',i){var n=this,a=n.$refs.grid;a.list.splice(t,1)},onEipUnbind:function(e,t,i){va' +
                    'r n=this,d=n.data,o=n.$refs.grid;new c({target:i.currentTarget,box:n.$el,$data:{' +
                    'title:"确定解绑该公网IP？",content:'<p class="text-warning">解绑后，每个EIP将收取'+r.EIP_PRICES[n' +
                    '.regionId]+"元/小时的费用，请到EIP控制台进行管理。</p>",confirmKey:"确定",style:"width:400px;",atta' +
                    'chEl:n.$el},onConfirm:function(){o.list.$set(t,{unbinding:!0}),s.requestTask(n.r' +
                    'egionId,"EipUnBindNatGateway",{natId:d.natId,vpcId:d.vpcId,assignedEipSet:[e.ip]' +
                    '}).done(function(){o.list.splice(t,1),s.request(n.regionId,"DescribeNatGateway",' +
                    '{natId:d.natId,offset:0,limit:1}).done(function(e){n.updateCallback(e.data[0]),a' +
                    '.resetNamespace("igw",n.regionId,e.vpcId),p.resetNamespace("eip",n.regionId)})})' +
                    '.fail(function(){o.list.$set(t,{unbinding:!1})}),l.click("vpc.NATGateway.detail.' +
                    'EIP.deleteEIP")}})}}),v=o.extend({$beforeInit:function(){var e=this;v.__super__.' +
                    '$beforeInit.apply(e,arguments),e.callback&&e.callback(e),d.beeBind(e,e,"isEditin' +
                    'g(list)","editing")},isEditing:function(e){return(e||[]).some(function(e){return' +
                    ' e.editing||e.unbinding})}},{defaults:{showState:!1,showPagination:!1,minHeight:' +
                    '40,colums:[{key:"ip",name:"IP",tdTpl:'    <div b-template b-if="item.editing">  ' +
                    '      <eip-field b-model="item.ip" cls="m" relevance="" region-id="{{regionId}}"' +
                    ' disabled="{{item.checking}}"></eip-field>    </div>    <div b-template b-if="!i' +
                    'tem.editing">        <span class="text-overflow">{{item.ip}}</span>    </div>'},' +
                    '{key:"_action",name:"操作",minWidth:100,tdTpl:'    <div b-template b-if="!item.edi' +
                    'ting && !item.unbinding">        <cell-action act="unbind" disabled="{{reachMin ' +
                    '|| editing}}" title="{{reachMin ? \'至少需要保留一个弹性IP\' : \'\'}}" text="解绑"></cell-ac' +
                    'tion>    </div>    <div b-template b-if="item.unbinding">        <i class="n-loa' +
                    'ding-icon"></i>解绑中...    </div>    <div b-template b-if="item.editing">        <' +
                    'div class="table-btn-set-wrap">            <button class="tc-15-btn m {{item.che' +
                    'cking ? \'disabled\' : \'\'}}" data-role="commit" disabled?="item.checking">保存</' +
                    'button>            <button class="tc-15-btn m weak {{item.checking ? \'disabled' +
                    '\' : \'\'}}" data-role="cancel" disabled?="item.checking">取消</button>           ' +
                    ' <i class="n-loading-icon" b-if="item.checking"></i>            <span class="tex' +
                    't-error" b-if="item.failMsg">{{item.failMsg}}</span>        </div>    </div>'}]}' +
                    '});v.tag("eip-field",e("./field/Eip")),u.tag("eip-grid",v)}),define("modules/vpc' +
                    '/nat/nat",function(e,t,i){e("widget/region/regionSelector"),e("widget/grid/Actio' +
                    'n");var n=e("../BeeBase"),a=e("../vpc/Selector"),s=e("../gridPage"),d=e("../cach' +
                    'er"),o=e("./util"),c=e("../cApi"),r=e("appUtil"),l=e("dialog"),p=e("tips"),u=e("' +
                    '../Grid"),v=e("./RenameBubble"),f=e("./Add"),m=e("./Chart"),h=e("./Detail"),b=e(' +
                    '"./validator"),g=e("widget/fieldsManager/gridView"),I=e("./cacher"),w=(e("consta' +
                    'nts"),e("modules/cvm/eip/common")),y="_operatingMark",x=n.extend({$tpl:'    <div' +
                    '>        <!--页面主体-->        <div b-style="{display: detailId ? \'none\' : \'bloc' +
                    'k\'}">            <div class="manage-area-title">                <h2>NAT网关</h2> ' +
                    '               <region-selector b-ref="regionSelect" b-model="regionId" b-with="' +
                    '{getWhiteList:getRegionWhiteList}"></region-selector>                <vpc-select' +
                    ' has-all="1" use-un-id="1" region-id="{{regionId}}" b-model="unVpcId"></vpc-sele' +
                    'ct>                <div class="manage-area-title-right">                    <a h' +
                    'ref="https://www.qcloud.com/doc/product/215/4975" target="_blank">              ' +
                    '          <b class="links-icon"></b> NAT网关帮助文档                    </a>          ' +
                    '      </div>            </div>            <div class="tc-15-action-panel">      ' +
                    '          <qc-action-button b-ref="create" name="add" label="+新建" attr="{{ {\'da' +
                    'ta-title\':addDisabledText} }}" b-with="{action:actionHandler, disabled:addDisab' +
                    'led}"></qc-action-button>                <qc-action-button float="right" name="c' +
                    'olumns" class-name="weak setting" b-with="{action:actionHandler}"></qc-action-bu' +
                    'tton>                <qc-search float="right" name="search" placeholder="搜索NAT网关' +
                    '的关键词" multiline="false" b-with="{search:actionHandler}" keyword="{{keyword}}"></' +
                    'qc-search>            </div>            <grid b-ref="grid" b-with="gridCfg"></gr' +
                    'id>        </div>        <!--图表浮层-->        <div class="sidebar-panel" b-if="cha' +
                    'rtId && !detailId">            <a class="btn-close" href="javascript:;" b-on-cli' +
                    'ck="$set(\'chartId\', \'\')">关闭</a>            <div class="pagination">         ' +
                    '       <a href="javascript:;" class="btn-page page-prev {{!hasPrev ? \'\' : \'bt' +
                    'n-page-disabled\'}}"                   b-on-click="prevChart()"                 ' +
                    '  title="上一条"><i class="icon-page-prev"></i></a>                <a href="javascr' +
                    'ipt:;" class="btn-page page-next {{!hasNext ? \'\' : \'btn-page-disabled\'}}"   ' +
                    '                b-on-click="nextChart()"                   title="下一条"><i class=' +
                    '"icon-page-next"></i></a>            </div>            <div class="sidebar-panel' +
                    '-container nat-sidebar">                <div class="sidebar-panel-hd">          ' +
                    '          <h3>{{chartId}}(<span class="entry-name-txt">{{chartName}}</span>)监控</' +
                    'h3>                </div>                <div class="sidebar-panel-bd">         ' +
                    '           <chart region-id="{{regionId}}" nat-id="{{chartId}}"></chart>        ' +
                    '        </div>            </div>        </div>        <!--详情面板-->        <div b-' +
                    'if="detailId">            <detail region-id="{{regionId}}"                    up' +
                    'date-callback="{{detailUpdate}}"                    bind-detail="{{bindDetail.bi' +
                    'nd(this)}}"                    tab="{{tab}}" b-model="detailId" data="{{detailDa' +
                    'ta}}"></detail>        </div>    </div>',bindDetail:function(e){r.beeBindBoth(e,' +
                    'this,"tab")},getRegionWhiteList:function(){return $.Deferred().resolve(o.REGION_' +
                    'WHITELIST)}},{defaults:$.extend({tab:"info",vpcCount:0},o)});x.tag("vpc-select",' +
                    'a),x.tag("chart",m),x.tag("detail",h),x.tag("grid",u.extend({$mixins:[g("vpc_igw' +
                    '_columns_v20160331")],getStateCls:function(e){return e=parseInt(e,10),e===o.STAT' +
                    'US.RUNNING?"succeed":e===o.STATUS.OVERDUE_BUFF?"warning":"error"}},{defaults:$.e' +
                    'xtend({colums:[{key:"natId",name:"ID/名称",required:!0,tdTpl:'    <p>        <a da' +
                    'ta-role="{{item.$loading ? \'\' : \'detail\'}}" href="javascript:;" class="text-' +
                    'overflow">            <span class="text-overflow">{{item.natId}}</span>        <' +
                    '/a>    </p>    <span class="text-overflow m-width"><i b-if="item.renaming" class' +
                    '="n-loading-icon"></i>    {{item.natName}}</span>{{{renameIconHtml}}}'},{name:"监' +
                    '控",key:"_monitor",width:50,tdTpl:'<i b-if="!item.$disabled" class="dosage-icon" ' +
                    'data-role="chart" data-title="查看监控"></i>{{item.$disabled ? "-":""}}'},{key:"stat' +
                    'e",name:"状态",tdTpl:'    <div b-if="item.$loadingText || item.$statusText" b-temp' +
                    'late>        <i b-if="item.$loading" class="n-loading-icon"></i><span class="tex' +
                    't-overflow {{item.$statusText ? \'text-danger\' : \'text-weak\'}}">{{item.$loadi' +
                    'ngText || item.$statusText}}</span>    </div>    <span b-if="!item.$loadingText ' +
                    '&& !item.$statusText" class="text-overflow {{getStateCls(item.state)}}">{{STATUS' +
                    '_NAMES[item.state] || "未知"}}</span>'},{key:"maxConcurrent",name:"类型",tdTpl:'{{TY' +
                    'PE_NAMES[item.maxConcurrent] || "未知"}}<p class="text-weak text-overflow">最大并发连接数' +
                    '{{item.maxConcurrent/10000}}万</p>'},{key:"eipCount",name:"绑定弹性IP数"},{key:"vpcId"' +
                    ',name:"所属网络",tdTpl:'    <p>        <a data-event="nav" href="/vpc/vpc?rid={{regi' +
                    'onId}}&unVpcId={{item.unVpcId}}">            <span class="text-overflow">{{item.' +
                    'unVpcId}}</span>        </a>    </p>    <span class="text-overflow" title="{{ite' +
                    'm.vpcName}}({{item.vpcCidrBlock}})">{{item.vpcName}}</span>'},{key:"_action",nam' +
                    'e:"操作",minWidth:100,required:!0,tdTpl:'<cell-action act="remove" text="删除" disab' +
                    'led="{{item.$loading}}"></cell-action>'}],outerPaging:!0},o)})),i.exports=s.exte' +
                    'nd().mixin(e("widget/page/mixins/grid/chart")).mixin({routeBase:"/vpc/nat",param' +
                    's:$.extend({},s.params,{keyword:"",order:{defaults:0,formula:function(e){return"' +
                    'desc"===e?1:0}},orderField:"",detailId:{defaults:"",history:!0}}),Bee:x,watchFie' +
                    'lds:["regionId","unVpcId"],idProperty:"natId",nameProperty:"natName",onRender:fu' +
                    'nction(){var t=this,i=t.bee;i.$watch("regionId",function(e){e&&(I.getLimits(e).d' +
                    'one(function(e){i.$set(e)}),d.getVpcListEx(i.regionId).done(function(e){i.$set("' +
                    'vpcCount",e.vpcList.length)}))},!0),r.beeBind(i,i,"knownCount >= MAX_IGW_NUM","r' +
                    'eachMax"),r.beeBind(i,i,"vpcCount===0","noVpc"),r.beeBind(i,i,"(reachMax || noVp' +
                    'c)","addDisabled"),r.beeBind(i,i,'reachMax ? "此VPC下NAT网关数已达上限" : (noVpc ? "没有可用的' +
                    'VPC" : "")',"addDisabledText"),e("../adTip").init(t.$el)},getData:function(){var' +
                    ' e=this,t=e.bee,i=t.regionId,n=t.unVpcId,a=t.count,s=t.page,d=t.order,r=t.orderF' +
                    'ield,l=a*(s-1),p=a,u=t.keyword,v={offset:l,limit:p};n&&(v.vpcId=n),u&&(v.natName' +
                    '=u),r&&(v.orderField=r,v.order=d>0?"desc":"asc");var f=$.Deferred();return $.whe' +
                    'n(c.request(i,"DescribeNatGateway",v),o.getDataProcessorTask(i)).then(function(t' +
                    ',i){var n=t.data||[];n.forEach(i);var a=t.totalCount||0;e.setRefreshing(n.some(f' +
                    'unction(e){return e.$loading})),f.resolve({totalNum:a,list:n})},f.reject),f},onG' +
                    'ridRename:function(e,t,i){var n=this,a=n.bee,s=a.$refs.grid,d=n.getRefreshingLoc' +
                    'ks(),o=e.natName;d.lock(y),new v({value:o,composedId:{regionId:a.regionId,natId:' +
                    'e.natId,vpcId:e.vpcId},target:$(i.currentTarget).parent(),onSave:function(e,i,n)' +
                    '{this.handleSimpleEdit({def:e,host:s.list,index:t,key:"natName",loadingKey:"rena' +
                    'ming",newValue:i,oldValue:n}),d.unlock(y)},onNo:function(){d.unlock(y)}})},onAct' +
                    'ionAdd:function(){var e,t=this,i=t.bee,n=i.regionId;e=new f({$data:{regionId:n,v' +
                    'pcId:i.unVpcId}}),l.create(e.$el,"550","",{title:"新建NAT网关",preventResubmit:!0,bu' +
                    'tton:{"创建":function(a){var s;if(s=b.multiValidate(e,["natName"],{},e.$el,{$btn:a' +
                    '}))return void l.toggleBtnDisable(!1);l.freeze(!0).toggleBtnLoading(!0);var r=e.' +
                    'eipSet,u=[],v=0;r.forEach(function(e){var t=e.ip;t?u.push(t):v++});var f,m=$.Def' +
                    'erred();f=v>0?w.getEipQuota({regionId:n}):$.Deferred().resolve(),f.then(function' +
                    '(i){return i&&v+i.currentEipNum>i.eipNumQuota?(b._toggleError(e.getLastMatchEipI' +
                    'nput(""),"该地域弹性IP开通数量已达上限"),void m.reject()):void c.request(n,"CreateNatGateway"' +
                    ',{vpcId:e.vpcId,natName:e.natName,maxConcurrent:e.maxConcurrent,bandwidth:e.band' +
                    'width,assignedEipSet:u,autoAllocEipNum:v}).then(function(e){t.bufferReload(),l.h' +
                    'ide();var i=e.billId||e.data.billId;o.checkBillTask(n,i).progress(function(){}).' +
                    'then(m.resolve,function(e){var t;switch(e){case 29104:t="发货失败[#29104]";break;cas' +
                    'e 29105:t="创建超时[#29105]";break;default:t="创建失败"}m.reject(t)})},function(t){var i' +
                    ',n=t&&t.data&&t.data.cgwerrorCode,a=t&&t.data&&t.data.message||"";switch(n){case' +
                    ' 29201:i=a.match(/\[(.*?)\]/),i=i&&i[1]||null,b._toggleError(e.getLastMatchEipIn' +
                    'put(i),i?"该IP已被使用":a);break;case 29202:b._toggleError(e.getLastMatchEipInput("")' +
                    ',"该地域弹性IP开通数量已达上限")}m.reject(a)})},m.reject),m.then(function(){d.resetNamespace(' +
                    '"igw",i.regionId,e.vpcId),I.resetNamespace("eip",i.regionId),t.bufferReload()},f' +
                    'unction(e){"string"==typeof e&&p.error(e),l.freeze(!1).toggleBtnLoading(!1).togg' +
                    'leBtnDisable(!1)})}}})},onGridRemove:function(e,t,i){var n=this,a=n.bee,s=a.$ref' +
                    's.grid.list;n.confirm({title:"确定删除该NAT网关？",content:'NAT网关删除后，将解绑关联EIP。<p class="' +
                    'text-warning">空置后，每个EIP将收取'+(o.EIP_PRICES[a.regionId]||"0.20")+"元/小时的费用，请到EIP控制台' +
                    '进行管理。</p>",confirmKey:"删除",style:"width:400px;",attachEl:a.$el},i).done(function' +
                    '(){s.$set(t,{$loading:!0,$loadingText:"删除中"});c.requestTask(a.regionId,"DeleteNa' +
                    'tGateway",{vpcId:e.vpcId,natId:e.natId},"删除NAT网关失败！").done(function(){n.bufferRe' +
                    'load(!0),d.resetNamespace("igw",a.regionId,e.vpcId,e.natId),I.resetNamespace("ei' +
                    'p",a.regionId)}).fail(function(){s.$set(t,{$loading:!1,$loadingText:""})})})},on' +
                    'GridDetail:function(e,t,i){var n=this,a=n.bee,s=a.$refs.grid;a.$set({tab:"info",' +
                    'detailData:e,detailUpdate:function(e){s.updateItem(t,e)},detailId:e.natId})}})})' +
                    ',define("modules/vpc/nat/RenameBubble",function(e,t,i){var n=e("./validator"),a=' +
                    'e("../cacher"),s=e("../cApi"),d=e("../RenameBubble");i.exports=d.extend({validat' +
                    'e:n.natName,doSave:function(e){var t=this,i=t.composedId;return s.request(i.regi' +
                    'onId,"ModifyNatGateway",{vpcId:i.vpcId,natId:i.natId,natName:e},"修改名称失败！").done(' +
                    'function(){a.resetNamespace("igw",i.regionId,i.vpcId,i.natId)})}})}),define("mod' +
                    'ules/vpc/nat/util",function(e,t,i){var n=e("../cApi"),a=e("constants"),s=e("../c' +
                    'acher"),d={};for(var o in a.EIP_REGION_PRICE)d[o]=a.EIP_REGION_PRICE[o].price;va' +
                    'r c={RUNNING:0,UNAVAILABLE:1,OVERDUE:2},r={};[[c.UNAVAILABLE,"不可用"],[c.RUNNING,"' +
                    '运行中"],[c.OVERDUE,"欠费停服"]].forEach(function(e){var t=e[0],i=e[1];r[t]=i});var l,p' +
                    '={CREATING:0,CREATE_SUCCESS:1,CREATE_FAIL:2,CHANGING:3,CHANGE_FAIL:4,DELETING:5,' +
                    'DELETE_FAIL:6},u={},v={};u[p.CREATING]="创建中",u[p.CREATE_SUCCESS]="创建成功",u[p.CREA' +
                    'TE_FAIL]="创建失败",u[p.CHANGING]="变更中",u[p.CHANGE_FAIL]="变更失败",u[p.DELETING]="删除中",' +
                    'u[p.DELETE_FAIL]="删除失败",v[p.CREATING]=1,v[p.CHANGING]=1,v[p.DELETING]=1,l=$.exte' +
                    'nd({},v),l[p.CREATE_FAIL]=1;var f,m,h={};f={SMALL:1,MEDIUM:2,LARGE:3},m=[{value:' +
                    '1e6,type:f.SMALL,text:"小型"},{value:3e6,type:f.MEDIUM,text:"中型"},{value:1e7,type:' +
                    'f.LARGE,text:"大型"}],m.forEach(function(e){h[e.value]=e.text}),i.exports={REGION_' +
                    'WHITELIST:a.NAT_GW_REGION,STATUS:c,STATUS_NAMES:r,PRODUCTION_STATUS:p,PRODUCTION' +
                    '_STATUS_NAMES:u,TEMP_PRODUCTION_STATUS:v,TYPE:f,TYPE_NAMES:h,TYPE_LIST:m,BANDWID' +
                    'TH_LIST:[10,20,50,100,200,500,1e3,2e3,5e3,0],EIP_PRICES:d,MAX_EIP_NUM:3,MAX_IGW_' +
                    'NUM:10,checkBillTask:function(e,t){var i=$.Deferred(),a=function(){i.notify(),n.' +
                    'request(e,"QueryNatGatewayProductionStatus",{billId:t}).then(function(e){switch(' +
                    'e=e.data||e,e.status){case 0:i.resolve();break;case 1:i.reject(e.errorCode);brea' +
                    'k;case 2:setTimeout(a,3e3)}})};return a(),i},getDataProcessorTask:function(e){va' +
                    'r t=$.Deferred(),i=s.getVpcListEx(e);return i.then(function(e){var i=e.vpcList,n' +
                    '={};i.forEach(function(e){n[e.vpcId]=e}),t.resolve(function(e){var t=e.vpcId,i=n' +
                    '[t];i&&(e.unVpcId=i.unVpcId,e.vpcCidrBlock=i.vpcCIDR);var a=e.productionStatus;a' +
                    ' in v?(e.$loading=!0,e.$loadingText=u[a]):a in l&&(e.$statusText=u[a])})},t.reje' +
                    'ct),t}}}),define("modules/vpc/nat/validator",function(e,t,i){var n=e("../validat' +
                    'or");i.exports=n.extend({natName:function(e){return n._validName(e,"NAT网关名称")}})' +
                    '}),define("modules/vpc/RenameBubble",function(e,t,i){i.exports=e("widget/popover' +
                    '/TextEditor")}),define("modules/vpc/ResourceGrid",function(e,t,i){var n=e("qccom' +
                    'ponent"),a=(e("router"),e("tips")),s=[],d=n.extend({$tpl:'    <div class="tc-15-' +
                    'table-panel resource-include" style="width: 940px;" b-style="style">        <div' +
                    ' class="tc-15-table-fixed-head">            <table class="tc-15-table-box">     ' +
                    '           <colgroup>                    <col class="tc-15-table-col1">         ' +
                    '           <col class="tc-15-table-col2">                    <col class="tc-15-t' +
                    'able-col3">                    <col class="tc-15-table-col4">                </c' +
                    'olgroup>                <thead>                <tr tabindex="0">                ' +
                    '    <th>                        <div>资源类型</div>                    </th>        ' +
                    '            <th>                        <div>包含资源</div>                    </th>' +
                    '                    <th>                        <div>数量</div>                   ' +
                    ' </th>                    <th>                        <div>操作</div>             ' +
                    '       </th>                </tr>                </thead>            </table>   ' +
                    '     </div>        <div class="tc-15-table-fixed-body">            <table class=' +
                    '"tc-15-table-box tc-15-table-rowhover">                <colgroup>               ' +
                    '     <col class="tc-15-table-col1">                    <col class="tc-15-table-c' +
                    'ol2">                    <col class="tc-15-table-col3">                    <col ' +
                    'class="tc-15-table-col4">                </colgroup>                <tbody b-rep' +
                    'eat="res in RESOURCE">                <tr tabindex="0">                    <td r' +
                    'owspan="{{res.list.length}}">                        <div>                      ' +
                    '      <span class="text-overflow">{{ res.label }}</span>                        ' +
                    '</div>                    </td>                    <td>                        <' +
                    'div>                            <span class="text-overflow">{{ res.list[0].label' +
                    ' }}</span>                        </div>                    </td>               ' +
                    '     <td>                        <div>                            <span class="t' +
                    'ext-overflow">{{> fetchResourceInfo(res.list[0], item) }}</span>                ' +
                    '        </div>                    </td>                    <td>                 ' +
                    '       {{> genAdd(res.list[0], item) }}                    </td>                ' +
                    '</tr>                <tr tabindex="0" b-repeat="r in res.list.slice(1)">        ' +
                    '            <td>                        <div>{{r.label}}</div>                  ' +
                    '  </td>                    <td>                        <div>                    ' +
                    '        <span class="text-overflow">{{> fetchResourceInfo(r, item) }}</span>    ' +
                    '                    </div>                    </td>                    <td>     ' +
                    '                   {{> genAdd(r, item) }}                    </td>              ' +
                    '  </tr>                </tbody>            </table>        </div>    </div>',fet' +
                    'chResourceInfo:function(e,t){var i=this.$root;return new Promise(function(n,s){v' +
                    'ar d=e.fetch;"object"==typeof t&&t&&"function"==typeof d?d.apply(e,[i.item,e]).t' +
                    'hen(function(t){var a;a=0==t?"0 个":t?isNaN(t)?t:e.link?"function"==typeof e.link' +
                    '?e.link(i.item):'<a href="'+e.link+'" data-report="vpc.vpc_subnet.detail.cloudRe' +
                    'source"" data-event="nav">'+t+" 个</a>":t+" 个":"-",n(a)},function(){a.hideFlashNo' +
                    'w(),n("-")}):n("-")})},checkAuth:function(e,t,i){return new Promise(function(n,a' +
                    '){var s=!0;switch(e){case"add":s=t.checkAdd?t.checkAdd(i,t):t.add}s.then?s.then(' +
                    'n,a):s?n():a()})},genAdd:function(e,t){var i=this.$root;return new Promise(funct' +
                    'ion(n){i.checkAuth("add",e,t).then(function(){var t=function(e){n('<div><a href=' +
                    '"'+e+'" data-event="nav" target="_blank" data-report="vpc.vpc_subnet.detail.addC' +
                    'loudResource">添加</a></div>')};return e.add&&e.add.then?void e.add.then(t):void t' +
                    '(e.add)},function(){n('<div class="disable-link"><a href="javascript:;">添加</a></' +
                    'div>')})})}},{defaults:{RESOURCE:s}});i.exports=d}),define("modules/vpc/route/Ad' +
                    'd",function(e,t,i){var n=e("../BeeBase"),a=e("appUtil"),s=e("constants"),d=e("ti' +
                    'ps"),o=e("../util"),c=e("./util"),r=e("../createLimitsMixin"),l=e("./validator")' +
                    ';e("../vpc/DropDown");var p=i.exports=n.extend({$mixins:[r({VPC_ROUTE_TABLE_MAX_' +
                    'NUM:s.VPC_LIMIT.RTB_PER_VPC_LIMIT_TYPE,ROUTE_TABLE_RULES_MAX_NUM:s.VPC_LIMIT.ROU' +
                    'TE_PER_RTB_LIMIT_TYPE})],$tpl:'    <div class="create-route" id="addRouteDialogW' +
                    'arp">        <div class="tc-15-list-wrap" style="padding:0 0 20px;">            ' +
                    '<div class="tc-15-list-content">                <ul>                    <li>    ' +
                    '                    <em class="tc-15-list-tit">名称</em>                        <s' +
                    'pan class="tc-15-list-det">                            <div class="tc-15-input-t' +
                    'ext-wrap m">                                <input type="text" data-name="routeT' +
                    'ableName" b-model="routeTableName" class="tc-15-input-text">                    ' +
                    '        </div>                        </span>                    </li>          ' +
                    '          <li>                        <em class="tc-15-list-tit">所属网络</em>      ' +
                    '                  <span class="tc-15-list-det">                            <vpc-' +
                    'vpc-select name="unVpcId"                                  region-id="{{regionId' +
                    '}}" show-cidr="1" cls="m" use-un-id="1" b-model="unVpcId"                       ' +
                    '           invalid="{{vpcRouteFull ? \'此私有网络下路由表创建数已达到上限\' : \'\'}}"            ' +
                    '                ></vpc-vpc-select>                        </span>               ' +
                    '     </li>                </ul>            </div>        </div>        <h2>路由策略<' +
                    '/h2>        <div class="table-panel host-table-panel mirror-table top-layout">  ' +
                    '          <div class="tc-table-fixed">                <div class="tc-table-fixed' +
                    '-head">                    <table class="tc-table tc-table-headcolor">          ' +
                    '              <colgroup>                            <col style="width:28%">     ' +
                    '                       <col style="width:28%">                            <col s' +
                    'tyle="width:28%">                            <col style="width:8%">             ' +
                    '               <col style="width:8%;"/>                        </colgroup>      ' +
                    '                  <thead>                        <tr tabindex="0">              ' +
                    '              <th>                                <span>目的端</span>              ' +
                    '              </th>                            <th>                             ' +
                    '   <span>下一跳类型</span>                            </th>                          ' +
                    '  <th>                                <span>下一跳</span>                          ' +
                    '  </th>                            <th><span>操作</span></th>                     ' +
                    '       <th></th>                        </tr>                        </thead>   ' +
                    '                 </table>                </div>                <div class="tc-ta' +
                    'ble-fixed-body def-scroll" style="overflow: auto; max-height: 221px;">          ' +
                    '          <table class="tc-table">                        <colgroup>            ' +
                    '                <col style="width:28%">                            <col style="w' +
                    'idth:28%">                            <col style="width:28%">                   ' +
                    '         <col style="width:8%">                            <col style="width:8%"' +
                    '>                        </colgroup>                        <tbody b-if="!loadin' +
                    'g && typesData">                            <tr b-repeat="item in routeSet" data' +
                    '-index="{{$index}}" dummy="{{processItem(item)}}">                              ' +
                    '  <td>                                    <span b-if="!item.$editable">{{item.de' +
                    'stinationCidrBlock}}</span>                                    <span b-if="item.' +
                    '$editable">                                        <input type="text" class="inp' +
                    'ut-txt"                                               data-name="destinationCidr' +
                    'Block"                                               b-model="item.destinationCi' +
                    'drBlock" data-auto="" data-position="right" style="width:160px;">               ' +
                    '                     </span>                                </td>               ' +
                    '                 <td>                                    <span b-if="!item.$edit' +
                    'able">Local</span>                                    <div b-if="item.$editable"' +
                    ' class="tc-15-select-wrap m">                                        <select cla' +
                    'ss="tc-15-select nextHopType" data-name=\'nextType\' b-model="item.nextType">   ' +
                    '                                         <option b-repeat="type in editableTypeL' +
                    'ist" value="{{type.type}}">{{type.name}}</option>                               ' +
                    '         </select>                                    </div>                    ' +
                    '            </td>                                <td>                           ' +
                    '         <span b-if="!item.$editable">Local</span>                              ' +
                    '      <span b-if="item.$editable">                                        <vpc-r' +
                    'oute-rule-value types-data="{{typesData}}"                                      ' +
                    '                        next-type="{{item.nextType}}" b-model="item.nextHub"    ' +
                    '                                                          options="{{{ regionId ' +
                    ': regionId, unVpcId : unVpcId }}}"                                              ' +
                    '                 editing="1">                                        </vpc-route' +
                    '-rule-value>                                    </span>                         ' +
                    '       </td>                                <td>                                ' +
                    '    <a b-if="!item.$editable" href="javascript:;" title="-">-</a>               ' +
                    '                     <a b-if="item.$editable" class="ico-remove" href="javascrip' +
                    't:;" data-title="删除" b-on-click="del(item)"></a>                                ' +
                    '</td>                                <td>                                    <di' +
                    'v b-if="!item.$editable" class="tc-15-bubble-icon tc-15-triangle-align-center"  ' +
                    '                                       qc-popover-position="left"               ' +
                    '                          qc-popover-style="width:350px;"                       ' +
                    '                  qc-popover="Local 路由策略由系统默认下发，表示 VPC 内云主机网络互通">               ' +
                    '                         <i class="tc-icon icon-what"></i>                      ' +
                    '              </div>                                </td>                       ' +
                    '     </tr>                        </tbody>                    </table>          ' +
                    '      </div>            </div>        </div>        <div class="btn-add-cont"  s' +
                    'tyle="cursor:default;">            <a href="javascript:;" class="btn_add_route_t' +
                    'able"               data-z-index="9999"               b-style="{color: reachMax ' +
                    '? \'#a2a2a2\' : undefined, cursor:reachMax ? \'default\' : undefined}"          ' +
                    '     b-on-click="add()"               data-title="{{reachMax ? \'路由表策略条数达到上限\' :' +
                    ' \'\'}}">+ 新增一行</a>        </div>    </div>',editableTypeList:c.editableTypeList' +
                    '.slice(0),isTypeEditable:c.isTypeEditable,processItem:function(e){e.$editable=c.' +
                    'isTypeEditable(e)},$beforeInit:function(){var e=this;p.__super__.$beforeInit.app' +
                    'ly(e,arguments),e.bufferReload=a.bufferFn(function(){!e.regionId||!e.unVpcId||e.' +
                    'vpc&&e.vpc.unVpcId===e.unVpcId||(e.$set("loading",!0),c.getExtraData(e.regionId,' +
                    'e.unVpcId).done(function(t){e.$set(t),e.$set("routeSet",[{destinationCidrBlock:"' +
                    'Local",nextType:2,nextHub:"Local"},{}])}).always(function(){e.$set("loading",!1)' +
                    '}))},100),o.beeBind(e,e,"vpc.rtbNum>=VPC_ROUTE_TABLE_MAX_NUM","vpcRouteFull"),o.' +
                    'beeBind(e,e,"routeSet.length>=ROUTE_TABLE_RULES_MAX_NUM","reachMax"),o.beeBind(e' +
                    ',e,"routeSet.length>ROUTE_TABLE_RULES_MAX_NUM","exceedLimit"),["regionId","unVpc' +
                    'Id"].forEach(function(t){e.$watch(t,e.bufferReload)}),e.bufferReload()},validate' +
                    ':function(){var e,t=this;return t.vpcRouteFull?{field:"vpcId",msg:"此私有网络下路由表创建数已' +
                    '达到上限"}:t.exceedLimit?{field:"routeSet",msg:"路由策略超过上限"}:(e=l.routeTableName(t.rou' +
                    'teTableName))?{field:"routeTableName",msg:e}:(e=l.routeSet(t.routeSet,t.$data))?' +
                    '{field:"routeSet",msg:e}:void 0},markError:function(e,t){if(e)if("string"==typeo' +
                    'f e)d.error(e);else switch(e.field){case"routeTableName":l._toggleError($(this.$' +
                    'el).find('[data-name="routeTableName"]'),e.msg,t);break;case"routeSet":this.mark' +
                    'RuleError(e.msg,t)}},markRuleError:function(e,t){if(e){var i=$(this.$el).find('[' +
                    'data-index="'+e.index+'"]'),n=i.find('[data-name="'+e.field+'"]');l._toggleError' +
                    '(n.length?n:i,e.msg,t)}},del:function(e){var t=this.routeSet.indexOf(e);t>=0&&th' +
                    'is.routeSet.splice(t,1)},add:function(){!this.reachMax&&this.routeSet&&this.rout' +
                    'eSet.push({})}},{defaults:{VPC_ROUTE_TABLE_MAX_NUM:s.VPC_ROUTE_TABLE_MAX_NUM,ROU' +
                    'TE_TABLE_RULES_MAX_NUM:s.ROUTE_TABLE_RULES_MAX_NUM}})}),define("modules/vpc/rout' +
                    'e/bindSubnet",function(e,t,i){var n=e("qccomponent"),a=e("manager"),s=e("widget/' +
                    'pubsubhub/pubsubhub"),d=e("dialog"),o=e("../cApi"),c=e("tips"),r=e("reporter"),l' +
                    '={bindSubnet:'    <div class="private-network-layer">        <style>            ' +
                    '.private-network-layer .tc-15-table-panel{                margin: 0;            ' +
                    '}        </style>        <span class="sub-title">选择需要关联的子网</span>        <div da' +
                    'ta-grid>            <grid b-ref="bindGrid"></grid>        </div>    </div>'};i.e' +
                    'xports=function(e,t,i){var p=n.extend({});p.tag("grid",n.getComponent("grid-view' +
                    '").extend({setTrAttr:function(e,t){return e=e||{},t.$disable?(e["data-title"]="已' +
                    '关联该路由表",e):void 0},getData:function(n,a){var s=this;n=n||{},a=a||function(e,t){s' +
                    '.setData({list:t.list,totalNum:t.totalNum,searchKey:n.searchKey})};var d=o.reque' +
                    'st(i,"DescribeSubnetEx",{offset:0,limit:9999,vpcId:e||void 0,subnetName:"",zoneI' +
                    'ds:void 0},"加载子网列表失败").translate({totalCount:"totalNum",data:{key:"detail",map:"' +
                    'subnet"}});d.then(function(e){var i=e.detail;i.map(function(e){e.unRouteTableId=' +
                    '=t&&(e.$disable=!0)}),i.sort(function(e){return e.$disable?1:-1}),a(null,{list:i' +
                    ',totalNum:i.length})},function(e){throw e})}},{defaults:{count:99999,showState:!' +
                    '1,showPagination:!1,autoMaxHeight:!1,colums:[{name:"子网ID/名称",key:"subnetName",td' +
                    'Tpl:'<p><span class="text-overflow">{{item.unSubnetId}}</span></p><span class="t' +
                    'ext-overflow m-width">{{item.subnetName}}</span>'
},{name:"子网CIRD",key:"subnetCI' +
                    'DR"},{name:"已关联路由表",key:"routeBinding",tdTpl:'<p><span class="text-overflow">{{i' +
                    'tem.unRouteTableId}}</span></p><span class="text-overflow">{{item.rtbName}}</spa' +
                    'n>'}],maxHeight:460,emptyTips:"没有可用子网"}}));var u=new p(l.bindSubnet);d.create(u.' +
                    '$el,680,"",{title:"关联子网",preventResubmit:!0,button:{"确定":function(n){var l=u.$re' +
                    'fs.bindGrid,p=l.getSelected();if(p.length){var v=[];p.forEach(function(e){v.push' +
                    '(e.unSubnetId)}),o.request(i,"AssociateSubnets",{vpcId:e,routeTableId:t,subnetId' +
                    's:v},"关联子网失败").then(function(e){e=e.data,a.checkTask({taskId:e.taskId}).then(fun' +
                    'ction(){s.trigger("rtbBindUpdate.rtb"),d.hide()},function(){c.error("关联子网失败"),n.' +
                    'removeClass("btn_unclick"),d.freeze(!1).toggleBtnLoading(!1)})}),r.click("vpc.ro' +
                    'ute.detail.subnet.addSubnet")}else d.hide()}}})}}),define("modules/vpc/route/del' +
                    'Subnet",function(e,t,i){var n=e("qccomponent"),a=(e("manager"),e("dialog")),s=(e' +
                    '("appUtil"),e("../cApi")),d=e("../cacher"),o=e("reporter"),c=e("tips"),r=e("widg' +
                    'et/pubsubhub/pubsubhub");i.exports={render:function(e,t,i,l){var p,u,v=this,f=ne' +
                    'w n('    <div class="select-routing">        <div class="tc-15-option-hd"><h4>解关' +
                    '联子网后，该子网需要重新关联一个路由表，请选择路由表</h4></div>        <div  class="tc-15-table-panel">   ' +
                    '          <div class="tc-15-table-fixed-body">                <table class="tc-1' +
                    '5-table-box tc-15-table-rowhover">                    <colgroup>                ' +
                    '        <col style="width:50px">                        <col style="width:auto">' +
                    '                        <col style="width:auto">                    </colgroup> ' +
                    '                   <tdbody>                        <tr b-repeat="item in list" c' +
                    'lass="{{item.$disabled?\'disable\':\'\'}}">                            <td>     ' +
                    '                           <div class="tc-15-first-radio">                      ' +
                    '              <input name="bindRtb" type="radio" class="tc-15-radio" value="{{it' +
                    'em.unRouteTableId}}" b-model="unRouteTableId" disabled?=item.$disabled>         ' +
                    '                       </div>                             </td>                 ' +
                    '            <td>                                <div>                           ' +
                    '         <p class="text-left">{{item.unRouteTableId}}</p>                       ' +
                    '             <p class="text-left">{{item.rtbName}}</p>                          ' +
                    '      </div>                             </td>                             <td> ' +
                    '                               <div>                                    <p class' +
                    '="text-center">{{item.$disabled?\'现关联\':\'\'}}</p>                              ' +
                    '  </div>                            </td>                        </tr>          ' +
                    '          </tdbody>                </table>            </div>        </div>    <' +
                    '/div>');u=v.getList(e,l),u.then(function(e){p=e.detail,p.map(function(e){e.unRou' +
                    'teTableId==i&&(e.$disabled=!0)}),f.$set("list",p)}),a.create(f.$el,680,"",{title' +
                    ':"解关联子网",preventResubmit:!0,button:{"确定":function(i){var n=f,p=n.unRouteTableId;' +
                    'p?(s.request(l,"AssociateRouteTable",{vpcId:e,subnetId:t,routeTableId:p},"修改子网关联' +
                    '路由表失败！").then(function(i){d.resetNamespace("subnet",l,e,t),r.trigger("rtbBindUpd' +
                    'ate.rtb"),a.hide()},function(e){c.error("修改子网关联路由表失败"),i.removeClass("btn_unclic' +
                    'k"),a.freeze(!1).toggleBtnLoading(!1)}),o.click("vpc.route.detail.subnet.deleteS' +
                    'ubnet")):a.hide()}}})},getList:function(e,t){var i;return i=s.request(t,"Describ' +
                    'eRouteTable",{offset:0,limit:9999,vpcId:e||void 0,routeTableName:""},"加载路由表列表失败"' +
                    ').translate({totalCount:"totalNum",data:{key:"detail",map:"routeTable"}})}}}),de' +
                    'fine("modules/vpc/route/Detail",function(e,t,i){var n,a=e("../BeeBase"),s=e("./R' +
                    'enameBubble"),d=e("manager"),o=e("constants"),c=(e("tips"),e("../cacher"),e("../' +
                    'cApi")),r=e("../util"),l=e("./util"),p=e("appUtil"),u=(e("./validator"),"routeLi' +
                    'st"),v="regionMapTask",f=e("./detailBind"),m=e("./routeInfo"),h=e("./bindSubnet"' +
                    '),b=e("../createLimitsMixin"),g=e("widget/pubsubhub/pubsubhub");e("../subnet/Dro' +
                    'pDown"),n=a.extend({$mixins:[b({maxCount:o.VPC_LIMIT.ROUTE_PER_RTB_LIMIT_TYPE,VP' +
                    'C_ROUTE_TABLE_MAX_NUM:o.VPC_LIMIT.RTB_PER_VPC_LIMIT_TYPE,ROUTE_TABLE_RULES_MAX_N' +
                    'UM:o.VPC_LIMIT.ROUTE_PER_RTB_LIMIT_TYPE})],$valuekey:"unRouteTableId",getTypeMet' +
                    'a:l.getTypeMeta,tab:0,$beforeInit:function(){var e=this;e.actionHandler=function' +
                    '(){e.handleAction(this.name,this)};var t=e[v]=$.Deferred();d.queryRegion(t.resol' +
                    've,t.reject),t.done(function(t){e.$set("regionMap",t)}),a.prototype.$beforeInit.' +
                    'apply(this,arguments),r.beeBind(e,e,"data && data.routeSet && (data.routeSet.len' +
                    'gth > maxCount)","exceedLimit"),e.bufferReload=p.bufferFn(function(t){if(t||e.re' +
                    'gionId&&e.unVpcId&&(!e.vpc||e.vpc.unVpcId!==e.unVpcId)){e.$set("loading",!0);var' +
                    ' i=e.unVpcId;l.getExtraData(e.regionId,e.unVpcId).done(function(t){i==e.unVpcId&' +
                    '&e.$set(t)}).always(function(){e.$set("loading",!1)})}},100),["regionId","unVpcI' +
                    'd"].forEach(function(t){e.$watch(t,e.bufferReload)}),e.bufferReload(),g.on("rtbB' +
                    'indUpdate.rtb",function(){e.bufferReload(!0)})},$afterInit:function(){a.prototyp' +
                    'e.$afterInit.apply(this,arguments);var e=this,t=function(){this.regionId&&this.u' +
                    'nRouteTableId&&(e.$set("editing",!1),e.$set("notFound",!1),this.data&&this.data.' +
                    'unRouteTableId===this.unRouteTableId||e.loadData())};this.$watch("unRouteTableId' +
                    '",t,!0)},getRtbShowId:$.proxy(r.getRtbShowId,r),getRtbShowType:$.proxy(r.getRtbS' +
                    'howType,r),$tpl:'<div b-on="events" style="position:absolute;left:0;top:0;height' +
                    ':100%;width:100%;" b-style="{display: !unRouteTableId ? \'none\' : \'block\'}" >' +
                    '<div b-if="notFound" style="text-align:center; padding:100px;">未查询到指定路由表, <a hre' +
                    'f="javascript:;" b-on-click="$set(\'unRouteTableId\', \'\')">返回列表</a></div><div ' +
                    'class="return-title-panel bottom-border" b-style="{display:notFound?\'none\':\'' +
                    '\'}"><a class="btn-return" b-on-click="back()" href="javascript:;"><i class="btn' +
                    '-back-icon"></i><span>返回</span></a><i class="line"></i><h2>{{ifLoading(data && (' +
                    'data.rtbName + " 详情"))}}</h2><div class="manage-area-title-right"><a href="https' +
                    '://www.qcloud.com/doc/product/215/4954" target="_blank"><b class="links-icon"></' +
                    'b> 路由表帮助文档</a></div></div><div class="tab-switch"  b-if="data && !notFound"><a h' +
                    'ref="javascript:;" b-on-click="$set(\'tab\', 0)" title="基本信息" data-report="vpc.r' +
                    'oute.detail" class="{{tab==0 ? \'cur\': \'\'}}">基本信息</a>        <a href="javascr' +
                    'ipt:;" b-on-click="$set(\'tab\', 1)" title="关联子网" data-report="vpc.route.detail.' +
                    'subnet" class="{{tab==1 ? \'cur\': \'\'}}">关联子网</a>        </div>        <div b-' +
                    'style="{display : !data || notFound || tab != 0 ? \'none\' : \'\'}"><div class="' +
                    'tc-15-list-wrap private-network-inf" b-style="{display:notFound ||tab != 0 ?\'no' +
                    'ne\':\'\'}"><div class="tc-15-list-legend">基本信息</div><div class="tc-15-list-cont' +
                    'ent"><ul><li><em class="tc-15-list-tit">路由表名称</em><span class="tc-15-list-det"><' +
                    'i b-if="data.renaming" class="n-loading-icon"></i>{{ifLoading(data && data.rtbNa' +
                    'me)}}<a b-if="data && !data.renaming" b-on="{click:rename}" href="javascript:;">' +
                    '修改</a></span></li><li><em class="tc-15-list-tit">路由表ID</em><span class="tc-15-li' +
                    'st-det">{{unRouteTableId}}</span></li><li><em class="tc-15-list-tit">地域</em><spa' +
                    'n class="tc-15-list-det">{{regionMap && regionMap[regionId]}}</span></li><li><em' +
                    ' class="tc-15-list-tit">路由表类型</em><span class="tc-15-list-det">{{ifLoading(data ' +
                    '&& getRtbShowType(data.rtbType))}}</span></li><li><em class="tc-15-list-tit">所属网' +
                    '络</em><span class="tc-15-list-det"><a style="margin-left:0px" data-report="vpc.r' +
                    'oute.detail.vpcID" data-event="nav" href="/vpc/vpc?rid={{data.regionId}}&unVpcId' +
                    '={{data.unVpcId}}">{{unVpcId}}</a>({{ifLoading(data && data.vpcName)}}|{{ifLoadi' +
                    'ng(data && data.vpcCIDR)}})</span></li><li><em class="tc-15-list-tit">创建时间</em><' +
                    'span class="tc-15-list-det">{{ifLoading(data && data.createTime)}}</span></li></' +
                    'ul></div></div><div class="tc-15-action-panel" b-style="{display:notFound || tab' +
                    ' != 0 ?\'none\':\'\'}"><h3 class="m-title">路由策略</h3><a href="javascript:;"  data' +
                    '-report="vpc.route.detail.editRouteTable"   b-if="data && typesData && !editing ' +
                    '&& !loading" b-on-click="edit()"><i class="pencil-icon "></i>编辑</a></div><div cl' +
                    'ass="tc-15-table-panel table-routing-policy" data-mod="routeList" b-style="{disp' +
                    'lay:notFound || tab != 0 ?\'none\':\'\'}">            <route-table-info region-i' +
                    'd="{{regionId}}"  un-vpc-id="{{unVpcId}}" data="{{data}}" un-route-table-id="{{u' +
                    'nRouteTableId}}" b-model="editing"></route-table-info>        </div></div><div b' +
                    '-style="{display: !data || notFound || tab != 1 ?\'none\':\'\'}">        <div cl' +
                    'ass="tc-15-list-wrap" >        <div class="tc-15-action-panel">        <button c' +
                    'lass="tc-15-btn m" b-on="{click:bindSubnet}">+新增关联子网</button>        </div>     ' +
                    '   <bind-subnet-grid region-id="{{regionId}}" un-vpc-id="{{unVpcId}}" b-ref="bin' +
                    'dSubnetGrid" b-model="unRouteTableId"></bind-subnet-grid>                </div> ' +
                    '   </div></div>',events:{"click [data-action]":function(e){var t=$(e.currentTarg' +
                    'et);this.handleAction(t.attr("data-action"),null,e),e.preventDefault()}},ifLoadi' +
                    'ng:function(e){return this.data?e:"加载中..."},back:function(){history.back()},rena' +
                    'me:function(e){var t=this;new s({regionId:this.regionId,vpcId:this.vpcId,unVpcId' +
                    ':this.unVpcId,unRouteTableId:this.unRouteTableId,value:this.data.rtbName,target:' +
                    '$(e.currentTarget).closest("span"),onSave:function(e,i,n){t.$set("data.rtbName",' +
                    'i),t.$set("data.renaming",!0),e.always(function(){t.$set("data.renaming",!1)}),e' +
                    '.fail(function(){t.$set("data.rtbName",n)}),e.done(function(){t.$set("event_upda' +
                    'te",{rtbName:i})})}})},edit:function(){var e=this;e.$set("editing",!0)},loadData' +
                    ':function(){var e,t=this;t.$set("data",null),t.loadTask=$.when(t[v],c.request(t.' +
                    'regionId,"DescribeRouteTable",{offset:0,limit:1,vpcId:t.unVpcId,routeTableId:t.u' +
                    'nRouteTableId},"读取路由信息失败！").done(function(t){e=c.translate(t.data[0],"routeTable' +
                    '")})).done(function(){t.$replace("data",e)}).fail(function(){t.$set("notFound",!' +
                    '0)})},bindSubnet:function(){var e=this;h(e.unVpcId,e.unRouteTableId,e.regionId)}' +
                    ',$beforeDestroy:function(){a.prototype.$beforeDestroy.apply(this,arguments),this' +
                    '[u]&&(this[u].destroy(),this[u]=null)}},{defaults:{maxCount:o.ROUTE_TABLE_RULES_' +
                    'MAX_NUM}}),n.tag("route-table-info",m),n.tag("bind-subnet-grid",f),i.exports=n})' +
                    ',define("modules/vpc/route/detailBind",function(e,t,i){var n=(e("qccomponent"),e' +
                    '("../Grid")),a=e("$"),s=(e("appUtil"),e("../cApi")),d=e("./delSubnet"),o=e("widg' +
                    'et/pubsubhub/pubsubhub"),c=e("../cacher");i.exports=n.extend({$valuekey:"unRoute' +
                    'TableId",$beforeInit:function(){var e=this;n.prototype.$beforeInit.apply(this,ar' +
                    'guments),e.$watch("unRouteTableId",function(t){t&&e.bufferReload()},!0)},$afterI' +
                    'nit:function(){var e=this;n.prototype.$afterInit.apply(this,arguments),o.on("rtb' +
                    'BindUpdate.rtb",function(){e.bufferReload()})},getData:function(e,t){var i=this;' +
                    'e=e||{},t=t||function(t,n){i.setData({list:n.list,totalNum:n.totalNum,searchKey:' +
                    'e.searchKey})};var n,d,o=function(e){n={},e.forEach(function(e){n[e.zoneId]=e.zo' +
                    'neName});e.map(function(e){return{label:e.zoneName,value:e.zoneId}})},r=c.getZon' +
                    'eList(i.regionId).done(o),l=i.unRouteTableId,p=i.regionId;d=s.request(p,"Describ' +
                    'eSubnetEx",{offset:0,limit:9999,vpcId:i.unVpcId,subnetName:"",zoneIds:void 0},"加' +
                    '载子网列表失败").translate({totalCount:"totalNum",data:{key:"detail",map:"subnet"}}),a.' +
                    'when(d,r).then(function(e){var i=e.detail,a=[];i.map(function(e){e.unRouteTableI' +
                    'd==l&&a.push(e),e.zonemap=n[e.zoneId]||e.zoneId}),t(null,{list:a,totalNum:a.leng' +
                    'th})},function(e){throw e})},del:function(e,t,i){var n=this;d.render(e,t,i,n.reg' +
                    'ionId),o.on("rtbBindUpdate.rtb",function(){n.bufferReload()})},destroy:function(' +
                    '){this.$destroy(!0)}},{defaults:{hasFirst:!1,colums:[{name:"子网ID/名称",key:"subnet' +
                    'Name",tdTpl:'<p><span class="text-overflow"><a data-event="nav" href="/vpc/subne' +
                    't?rid={{me.regionId}}&subnetId={{item.subnetId}}">{{item.unSubnetId}}</a></span>' +
                    '</p><span class="text-overflow m-width">{{item.subnetName}}</span>'},{name:"可用区"' +
                    ',key:"zonemap"},{name:"CIDR",key:"subnetCIDR"},{name:"操作",key:"_action",tdTpl:'<' +
                    'a href="javascript:;" b-on-click="del(item.unVpcId,item.unSubnetId,unRouteTableI' +
                    'd)">解关联</a>'}]}})}),define("modules/vpc/route/DropDown",function(e,t,i){var n=e(' +
                    '"qccomponent"),a=e("../DropDown"),s=e("../cacher"),d=a.extend({watchFields:["reg' +
                    'ionId","vpcId","unVpcId"],getData:function(e){var t=this;s.getVpcRouteTableListE' +
                    'x(t.regionId,t.unVpcId||t.vpcId).done(function(i){e(i.map(function(e){return{tex' +
                    't:e.rtbName,value:t.useUnId?e.unRouteTableId:e.rtbId}}))})}});n.tag("vpc-rtb-sel' +
                    'ect",d),i.exports=d}),define("modules/vpc/route/RenameBubble",function(e,t,i){va' +
                    'r n=e("../RenameBubble"),a=e("../cacher"),s=e("../cApi"),d=(e("constants"),e("./' +
                    'validator"));e("appUtil");i.exports=n.extend({validate:function(e){return d.rout' +
                    'eTableName(e)},doSave:function(e){var t=this,i=$.Deferred();return s.request(t.r' +
                    'egionId,"DescribeRouteTable",{offset:0,limit:1,vpcId:t.unVpcId,routeTableId:t.un' +
                    'RouteTableId}).done(function(n){var d=n.data[0]||{};s.request(t.regionId,"Modify' +
                    'RouteTableAttribute",{vpcId:t.unVpcId,routeTableId:t.unRouteTableId,routeTableNa' +
                    'me:e,routeSet:d.routeSet},"修改路由表名称失败！").done(function(){a.resetNamespace("route"' +
                    ',t.regionId,t.unVpcId,t.unRouteTableId),a.resetNamespace("route",t.regionId,t.vp' +
                    'cId,t.rtbId)}).then(i.resolve,i.reject)}).fail(i.reject),i}})}),define("modules/' +
                    'vpc/route/route",function(e,t,i){e("widget/region/regionSelector"),e("widget/gri' +
                    'd/Action");var n=e("../BeeBase"),a=e("../vpc/Selector"),s=e("../moduleBase"),d=e' +
                    '("../cacher"),o=e("../util"),c=e("../cApi"),r=e("../Grid"),l=e("./RenameBubble")' +
                    ',p=e("./Detail"),u=e("./Add"),v=e("dialog"),f=e("tips"),m=e("./bindSubnet"),h=e(' +
                    '"widget/pubsubhub/pubsubhub"),b=e("../Confirm"),g="grid",I="regionSelector",w="v' +
                    'pcSelector",y="addDialog",x=n.extend({$tpl:'<div><div b-style="{display: unRoute' +
                    'TableId ? \'none\' : \'block\'}" ><div class="manage-area-title"><h2>路由表</h2><sp' +
                    'an b-tag="region-selector" b-ref="regionSelect" b-model="regionId" b-with="{getW' +
                    'hiteList:getWhiteList}"></span><span b-tag="vpc-select" b-ref="vpcSelect" region' +
                    '-id="{{regionId}}" vpc-id="{{vpcId}}" use-un-id="1" b-model="unVpcId"></span><di' +
                    'v class="manage-area-title-right"><a href="https://www.qcloud.com/doc/product/21' +
                    '5/4954" target="_blank"><b class="links-icon"></b> 路由表帮助文档</a></div></div><div c' +
                    'lass="tc-15-action-panel"><div b-tag="qc-action-button" name="add" label="+新建" a' +
                    'ttr="" b-with="{action:actionHandler, disabled:addDisabled, attr:{\'data-title\'' +
                    ':addDisabledText}}"></div><div b-tag="qc-action-button" float="right" name="colu' +
                    'mns" class-name="weak setting" b-with="{action:actionHandler}"></div><div b-tag=' +
                    '"qc-search" b-ref="search" float="right" name="search" placeholder="搜索路由表关键词" mu' +
                    'ltiline="false" b-with="{search:actionHandler}" keyword="{{keyword}}"></div></di' +
                    'v><div data-mod="grid"></div></div><div b-tag="vpc-route-detail" b-ref="detail" ' +
                    'region-id="{{regionId}}" data="{{rtbData}}" routeTable="{{rtb}}" tab="{{tab}}" u' +
                    'n-vpc-id="{{rtbUnVpcId}}" vpc-name="{{rtbVpcName}}" vpc-cidr="{{rtbVpcCidr}}" ro' +
                    'uter-id="{{routerId}}" b-model="unRouteTableId"></div></div>'});x.tag("vpc-selec' +
                    't",a),x.tag("vpc-route-detail",p),i.exports=s.extend({routeBase:"/vpc/route",par' +
                    'ams:$.extend({},s.params,{keyword:"",rtbUnVpcId:"",unRouteTableId:{route:"unRtbI' +
                    'd",history:!0,defaults:""},tab:""}),Bee:x,getBeeConfig:function(){var e=this,t=s' +
                    '.getBeeConfig.apply(this,arguments),i=function(){e.handleAction(this.name,this)}' +
                    ';return $.extend(t.$data,{actionHandler:i}),$.extend(t,{addDisabled:!0,getWhiteL' +
                    'ist:function(){return d.getRegionIdList()}})},onRender:function(){var t=this,i=t' +
                    '.$el,n=t.bee,a=t.bee.$refs,s=a.search,p=a.vpcSelect,u=!0,v=function(){t[g]&&t[g]' +
                    '.bufferReload()};t.vpcId2UnId(!0).done(function(){u=!1,v()}),n.$watch("regionId"' +
                    ',function(){n.$set("unVpcId",void 0),v()}),["unVpcId","keyword","page","count"].' +
                    'forEach(function(e){n.$watch(e,v)}),p.$watch("list",function(e){n.$set("addDisab' +
                    'led",e.length<=1?1:""),n.$set("addDisabledText",e.length<=1?"无可用私有网络":"")},!0),o' +
                    '.beeBind(n,s,"keyword"),n.$refs.detail.$watch("event_update",function(e){var i=t' +
                    '[g];n.unRouteTableId&&i&&i.updateRowData({unVpcId:n.rtbUnVpcId,unRouteTableId:n.' +
                    'unRouteTableId},e)}),h.on("rtbBindUpdate.rtb",function(){v()}),e("../adTip").ini' +
                    't(t.$el),t[g]=new r({$el:t.getModDom(i,g),$data:{count:n.count,page:n.page,cache' +
                    'Key:"vpc_rtb_columns_v20151124",colums:[{key:"rtbName",name:"ID/名称",order:!0,req' +
                    'uired:!0,tdTpl:'<p><a data-role="detail" href="javascript:;"><span class="text-o' +
                    'verflow">{{item.unRouteTableId}}</span></a></p><span class="text-overflow m-widt' +
                    'h"><i b-if="item.renaming" class="n-loading-icon"></i>{{item.rtbName}}</span>{{{' +
                    'renameIconHtml}}}'},{key:"unVpcId",name:"所属网络",tdTpl:o.vpcTdTpl},{key:"type",nam' +
                    'e:"类型",width:120},{key:"subnetNum",name:"关联子网数",width:120,tdTpl:'<p><a  data-rep' +
                    'ort="vpc.route.subnetNum" data-event="nav" href="/vpc/route?rid={{regionId}}&rtb' +
                    'UnVpcId={{item.unVpcId}}&unRtbId={{item.unRouteTableId}}&tab=1"><span class="tex' +
                    't-overflow">{{item.subnetNum}}</span></a></p>'},{key:"createTime",name:"创建时间",hi' +
                    'de:!0},{key:"_action",name:"操作",minWidth:100,required:!0,tdTpl:'<span>    <cell-' +
                    'action act="remove" text="删除"   disabled="{{item.type === 1 || item.subnetNum > ' +
                    '0}}" title="{{item.type === 1 ? \'无法删除默认路由表\' : item.subnetNum > 0 ? \'无法删除有子网关联' +
                    '的路由表\' : \'\'}}"></cell-action>    <a href="javascript:;" b-on-click="bindSubnet' +
                    '(item.unVpcId,item.unRouteTableId,regionId)">关联子网</a></span>'}]},getCellContent:' +
                    'function(e,t,i){var n;switch(i.key){case"type":n='<span class="text-overflow">'+' +
                    'o.getRtbShowType(e)+"</span>";break;case"createTime":break;case"_action":}return' +
                    ' n},getData:function(e){if(!u){var t,i=this,a=e.count,s=e.page,d=a*(s-1),o=a,r=n' +
                    '.keyword;i.showLoading("加载中..."),t=c.request(n.regionId,"DescribeRouteTable",{of' +
                    'fset:d,limit:o,vpcId:n.unVpcId||void 0,routeTableName:r},"加载路由表列表失败").translate(' +
                    '{totalCount:"totalNum",data:{key:"detail",map:"routeTable"}}),t.done(function(t)' +
                    '{var n=t.detail;n=i.localSort(n,e.orderField,e.order),i.setData({totalNum:t.tota' +
                    'lNum,searchKey:r,list:n})}),t.always(function(){i.hideLoading()})}},onRowRename:' +
                    'function(e,t,i){var a=this;new l({regionId:n.regionId,vpcId:e.vpcId,unVpcId:e.un' +
                    'VpcId,routerId:e.routerId,rtbId:e.rtbId,unRouteTableId:e.unRouteTableId,value:e.' +
                    'rtbName,target:$(i.currentTarget).parent(),onSave:function(e,i,n){a.list.$set(t,' +
                    '{rtbName:i,renaming:!0}),e.done(function(){a.list.$set(t,{renaming:!1})}),e.fail' +
                    '(function(){a.list.$set(t,{rtbName:n,renaming:!1})})}})},onRowDetail:function(e)' +
                    '{n.$set({rtbUnVpcId:e.unVpcId,rtbData:e,unRouteTableId:e.unRouteTableId})},onRow' +
                    'Remove:function(e,t,i){var a=this;new b({target:$(i.currentTarget),box:this.$gri' +
                    'dBody,$data:{title:"确定删除该路由表？"},onConfirm:function(){c.request(n.regionId,"Delet' +
                    'eRouteTable",{vpcId:e.unVpcId,routeTableId:e.unRouteTableId},"删除路由表失败！").done(fu' +
                    'nction(){d.resetNamespace("vpc",n.regionId,e.unVpcId),d.resetNamespace("route",n' +
                    '.regionId,e.unVpcId,e.unRouteTableId),d.resetNamespace("route",n.regionId,e.vpcI' +
                    'd,e.rtbId),a.bufferReload()})}})},bindSubnet:function(e,t,i){m(e,t,i)},backList:' +
                    'function(){this.$set("searchKey",""),n.$set("keyword",void 0)}}),o.beeBind(n,t[g' +
                    '],"regionId"),v()},getOneUnVpcId:function(){var e=this,t=e.bee,i=t.$refs.vpcSele' +
                    'ct;return t.unVpcId||i.list&&i.list.length&&i.list[1].unVpcId},onActionAdd:funct' +
                    'ion(){var e=this,t=e.bee,i=new u({regionId:t.regionId,unVpcId:t.unVpcId});v.crea' +
                    'te(i.$el,"700","",{title:"新建路由表",preventResubmit:!0,button:{"创建":function(t){var' +
                    ' a=i.validate();return a?(v.toggleBtnDisable(!1),void i.markError(a,{$btn:t})):v' +
                    'oid c.request(i.regionId,"CreateRouteTable",{vpcId:i.unVpcId,routeTableName:i.ro' +
                    'uteTableName,routeSet:i.routeSet.map(function(e){return{destinationCidrBlock:e.d' +
                    'estinationCidrBlock,nextType:e.nextType,nextHub:e.nextHub}})},"创建路由表失败！").then(f' +
                    'unction(t){f.success("添加路由表成功"),v.hide(),d.resetNamespace("vpc",i.regionId,i.unV' +
                    'pcId),d.resetNamespace("route",i.regionId,i.unVpcId),d.resetNamespace("route",i.' +
                    'regionId,i.vpc.vpcId);var a=new n('    <div class="dialog-text-cnt">        您的路由' +
                    '表需要关联子网才能生效    </div>');v.create(a.$el,540,"",{title:"路由表创建成功",preventResubmit:!' +
                    '0,button:{"关联子网":function(n){m(i.unVpcId,t.unRouteTableId,e.bee.regionId),n.remo' +
                    'veClass("btn_unclick"),v.freeze(!1).toggleBtnLoading(!1)}}}),e[g].bufferReload()' +
                    '},function(){v.toggleBtnDisable(!1)})}}})},onActionColumns:function(){this[g].sh' +
                    'owColumnsWindow()},onActionSearch:function(e){this.bee.$set("keyword",e.keyword)' +
                    '},onDestroy:function(){this._destroy(g,I,w,y)}})}),define("modules/vpc/route/rou' +
                    'teInfo",function(e,t,i){var n=e("../BeeBase"),a=(e("manager"),e("constants")),s=' +
                    'e("tips"),d=e("../cacher"),o=e("../cApi"),c=e("../util"),r=e("./util"),l=e("appU' +
                    'til"),p=e("./validator"),u=e("../createLimitsMixin"),v="routeList";e("../subnet/' +
                    'DropDown");var f=e("widget/pubsubhub/pubsubhub"),m=n.extend({$mixins:[u({maxCoun' +
                    't:a.VPC_LIMIT.ROUTE_PER_RTB_LIMIT_TYPE,VPC_ROUTE_TABLE_MAX_NUM:a.VPC_LIMIT.RTB_P' +
                    'ER_VPC_LIMIT_TYPE,ROUTE_TABLE_RULES_MAX_NUM:a.VPC_LIMIT.ROUTE_PER_RTB_LIMIT_TYPE' +
                    '})],$valuekey:"editing",getTypeMeta:r.getTypeMeta,$beforeInit:function(){var e=t' +
                    'his;e.actionHandler=function(){e.handleAction(this.name,this)},n.prototype.$befo' +
                    'reInit.apply(this,arguments),c.beeBind(e,e,"data && data.routeSet && (data.route' +
                    'Set.length > maxCount)","exceedLimit"),e.bufferReload=l.bufferFn(function(t){if(' +
                    't||e.regionId&&e.unVpcId&&(!e.vpc||e.vpc.unVpcId!==e.unVpcId)){e.$set("loading",' +
                    '!0);var i=e.unVpcId;r.getExtraData(e.regionId,e.unVpcId).done(function(t){i==e.u' +
                    'nVpcId&&e.$set(t)}).always(function(){e.$set("loading",!1)})}},100),["regionId",' +
                    '"unVpcId"].forEach(function(t){e.$watch(t,e.bufferReload)}),e.bufferReload(),f.o' +
                    'n("rtbBindUpdate.rtb",function(){e.bufferReload(!0)})},$afterInit:function(){n.p' +
                    'rototype.$afterInit.apply(this,arguments);var e=this,t=function(){this.regionId&' +
                    '&this.unRouteTableId&&(e.$set("editing",!1),e.$set("notFound",!1),this.data&&thi' +
                    's.data.unRouteTableId===this.unRouteTableId||e.loadData())};this.$watch("unRoute' +
                    'TableId",t,!0),this.$watch("editing",function(t){e.data&&e.data.routeSet&&t&&(e.' +
                    'backupRules=l.clone(e.data.routeSet.slice(0)))})},$tpl:'        <div class="rout' +
                    'ing-table-mod">            <div class="table-panel host-table-panel mirror-table' +
                    ' top-layout" style="top:0">                <div class="tc-table-fixed">         ' +
                    '           <div class="tc-15-table-fixed-head">                        <table cl' +
                    'ass="tc-table tc-table-headcolor">                            <colgroup>        ' +
                    '                        <col style="width:30%">                                <' +
                    'col style="width:25%">                                <col style="width:25%">   ' +
                    '                             <col style="width:20%">                            ' +
                    '</colgroup>                            <thead>                            <tr ta' +
                    'bindex="0">                                <th><span>目的端</span></th>            ' +
                    '                    <th><span>下一跳类型</span></th>                                <' +
                    'th><span>下一跳</span></th>                                <th><span></span></th>  ' +
                    '                          </tr>                            </thead>             ' +
                    '           </table>                    </div>                    <div class="tc-' +
                    'table-fixed-body def-scroll" style="overflow: visible;">                        ' +
                    '<table class="tc-table">                            <colgroup>                  ' +
                    '              <col style="width:30%">                                <col style=' +
                    '"width:25%">                                <col style="width:25%">             ' +
                    '                   <col style="width:20%">                            </colgroup' +
                    '>                            <tbody b-if="!loading && typesData">               ' +
                    '                 <tr b-repeat="item in data.routeSet" data-index="{{$index}}" du' +
                    'mmy="{{processItem(item)}}">                                    <td>            ' +
                    '                            <span b-if="!item.$editable || !editing">{{item.dest' +
                    'inationCidrBlock}}</span>                                        <span b-if="edi' +
                    'ting && item.$editable">                                            <input type=' +
                    '"text" class="input-txt tc-15-input-text"                                       ' +
                    '            data-name="destinationCidrBlock"                                    ' +
                    '               b-model="item.destinationCidrBlock" data-auto="" data-position="r' +
                    'ight" style="width:160px;">                                        </span>      ' +
                    '                              </td>                                    <td>     ' +
                    '                                   <span b-if="!item.$editable">Local</span>    ' +
                    '                                    <span b-if="!editing && item.$editable">{{ge' +
                    'tTypeMeta(item.nextType).name}}</span>                                        <d' +
                    'iv b-if="editing && item.$editable" class="tc-15-select-wrap m">                ' +
                    '                            <select class="tc-15-select nextHopType" data-name=' +
                    '\'nextType\' b-model="item.nextType">                                           ' +
                    '     <option b-repeat="type in editableTypeList" value="{{type.type}}">{{type.na' +
                    'me}}</option>                                            </select>              ' +
                    '                          </div>                                    </td>       ' +
                    '                             <td>                                        <span b' +
                    '-if="!item.$editable" style="display: inline;">                                 ' +
                    '           Local                                        </span>                 ' +
                    '                       <help-tips position="right" b-if="!item.$editable">      ' +
                    '                                      Local 路由策略由系统默认下发，表示 VPC 内云主机网络互通，<a href=' +
                    '"https://www.qcloud.com/doc/product/215/4954#.E8.B7.AF.E7.94.B1.E8.A7.84.E5.88.9' +
                    '9" target="_blank">点击查看路由策略详情</a>。                                        </help' +
                    '-tips>                                        <span b-if="item.$editable" style=' +
                    '"padding-left:0">                                            <vpc-route-rule-val' +
                    'ue b-model="item.nextHub"                                                       ' +
                    '           types-data="{{typesData}}"                                           ' +
                    '                       next-type="{{item.nextType}}"                            ' +
                    '                                      options="{{{ regionId : regionId, unVpcId ' +
                    ': unVpcId, unRouteTableId : unRouteTableId }}}"                                 ' +
                    '                                 editing="{{editing}}">                         ' +
                    '                   </vpc-route-rule-value>                                      ' +
                    '  </span>                                    </td>                              ' +
                    '      <td>                                        <a b-if="!item.$editable || !e' +
                    'diting" href="javascript:;" title="-">-</a>                                     ' +
                    '   <a b-if="item.$editable && editing" class="ico-remove" href="javascript:;" da' +
                    'ta-title="删除" b-on-click="del(item)"></a>                                    </t' +
                    'd>                                </tr>                            </tbody>     ' +
                    '                   </table>                        <div b-if="editing" class=" a' +
                    'dd-txt-wrap" style="padding:15px 0;">                            <a href="javasc' +
                    'ript:;" b-on-click="add()" class="add_route_table_row" style="text-decoration: n' +
                    'one;">+ 新增一行</a>                        </div>                    </div>        ' +
                    '            <div b-if="editing" class="table-edit">                        <butt' +
                    'on class="tc-15-btn btn_submit {{submitting ? \'disabled\' : \'\'}}" b-on="{clic' +
                    'k:save}"><i b-if="submitting" class="n-loading-icon"></i>确定</button>&nbsp;      ' +
                    '                  <button class="tc-15-btn weak btn_cancel {{submitting ? \'disa' +
                    'bled\' : \'\'}}" b-on-click="cancel()">取消</button>                    </div>    ' +
                    '            </div>            </div>        </div>',events:{"click [data-action]' +
                    '":function(e){var t=$(e.currentTarget);this.handleAction(t.attr("data-action"),n' +
                    'ull,e),e.preventDefault()}},editableTypeList:r.editableTypeList.slice(0),process' +
                    'Item:function(e){e.$editable=r.isTypeEditable(e)},save:function(e){var t,i,n,a=t' +
                    'his,c=a.data;return a.exceedLimit?void s.error("路由策略超过上限"):(t=p.routeSet(c.route' +
                    'Set,a.$data))?"string"==typeof t?s.error(t):(i=$(a.$el).find('[data-index="'+t.i' +
                    'ndex+'"]'),n=i.find('[data-name="'+t.field+'"]'),void p._toggleError(n.length?n:' +
                    'i,t.msg,{$btn:e.currentTarget})):(a.$set("submitting",!0),void o.request(a.regio' +
                    'nId,"ModifyRouteTableAttribute",{vpcId:a.unVpcId,routeTableId:c.unRouteTableId,r' +
                    'outeTableName:c.rtbName,routeSet:c.routeSet.map(function(e){return{destinationCi' +
                    'drBlock:e.destinationCidrBlock,nextType:e.nextType,nextHub:e.nextHub}})},"修改路由表失' +
                    '败！").done(function(){s.success("编辑成功"),a.$set("editing",!1),d.resetNamespace("ro' +
                    'ute",a.regionId,c.unVpcId,c.unRouteTableId),d.resetNamespace("route",a.regionId,' +
                    'c.vpcId,c.rtbId)}).always(function(){a.$set({submitting:!1})}))},cancel:function' +
                    '(){var e=this;e.$set("editing",!1),e.$set("data.routeSet",e.backupRules)},add:fu' +
                    'nction(){this.data.routeSet.push({})},del:function(e){var t=this,i=t.data.routeS' +
                    'et,n=i.indexOf(e);n>=0&&i.splice(n,1)},loadData:function(){var e,t=this;t.$set("' +
                    'data",null),o.request(t.regionId,"DescribeRouteTable",{offset:0,limit:1,vpcId:t.' +
                    'unVpcId,routeTableId:t.unRouteTableId},"读取路由信息失败！").done(function(t){e=o.transla' +
                    'te(t.data[0],"routeTable")}).done(function(){t.$replace("data",e)}).fail(functio' +
                    'n(){t.$set("notFound",!0)})},$beforeDestroy:function(){n.prototype.$beforeDestro' +
                    'y.apply(this,arguments),this[v]&&(this[v].destroy(),this[v]=null)}},{defaults:{m' +
                    'axCount:a.ROUTE_TABLE_RULES_MAX_NUM}});i.exports=m}),define("modules/vpc/route/t' +
                    'ypes",function(e,t,i){var n,a=e("../BeeBase"),s=(e("../cApi"),e("appUtil")),d=e(' +
                    '"manager"),o=e("../cacher"),c=e("../conn/util"),r={},l={rank:0,editable:!0,loadD' +
                    'ata:function(e){return $.Deferred().resolve([])},tpl:"{{value}}",editorTpl:'<inp' +
                    'ut type="text" data-name="nextHub" b-model="value" >',aggregatable:!1,validate:f' +
                    'unction(e,t,i){return e?void 0:"不能为空"}},p=function(){},u=function(e){return p.pr' +
                    'ototype=l,$.extend(new p,e)},v=a.extend({$valuekey:"value",$beforeInit:function(' +
                    '){var e=this;v.__super__.$beforeInit.apply(e,arguments),e.$watch("[nextType, edi' +
                    'ting, typesData]",function(){var t=e.nextType,i=r[t];i&&(e.$set("data",(e.typesD' +
                    'ata||{})[t]),e.$set("tpl",e.editing?i.editorTpl:i.tpl))},!0),e.$watch("unNextHub' +
                    '",function(t){t&&e.nextHub!==t&&e.$set("nextHub",t)})},find:function(e,t,i){var ' +
                    'n;return(e||[]).some(function(e){return e[t]+""==i+""?(n=e,!0):void 0}),n||{}},$' +
                    'tpl:"<div>{{>tpl}}</div>"});a.tag("vpc-route-rule-value",v),n=i.exports={CVM:u({' +
                    'type:0,name:"云主机（公网网关）",loadData:function(e){var t,i,n=$.Deferred(),a=$.Deferred' +
                    '();return o.getVpc(e.regionId,e.unVpcId).then(function(t){var i=t.vpcId;o.getVPC' +
                    'GatewayList(e.regionId,i,0).then(function(e){n.resolve(e.vmGwList)},n.reject)},n' +
                    '.reject),$.when(o.getVpcSubnetList(e.regionId,e.unVpcId).done(function(e){i=e}),' +
                    'n.done(function(e){t=e})).then(function(){a.resolve({list:t,subnetList:i||[]})},' +
                    'a.reject),a},validate:function(e,t,i){if(!e)return"不能为空";var n,a=t.subnetList||[' +
                    '];return a.some(function(t){return t.unRouteTableId===i.unRouteTableId&&s.isCIDR' +
                    'Intersect(t.subnetCIDR,e)?(n="请选择位于非关联子网的公网网关",!0):void 0}),n},aggregatable:!0,t' +
                    'pl:'    <a data-event="nav" href="/cvm/index?rid={{options.regionId}}&vagueIp={{' +
                    'value}}">        <span class="text-overflow">{{value}}</span>    </a>',editorTpl' +
                    ':'    <div class="tc-15-select-wrap m">        <select class="tc-15-select" data' +
                    '-forcesync data-name=\'nextHub\' b-model="value" style="width: 120px;">         ' +
                    '   <option b-if="!data || !data.list || !data.list.length" value="" selected dis' +
                    'abled>无可用公网网关</option>            <option b-repeat="item in data.list" value="{{' +
                    'item.lanIp}}">{{item.lanIp}}</option>        </select>    </div>'}),VPN:u({type:' +
                    '1,name:"VPN网关",loadData:function(e){return o.getVpnGwList(e.regionId,e.unVpcId)}' +
                    ',tpl:'    <p>        <a data-event="nav" href="/vpc/vpnGw?rid={{options.regionId' +
                    '}}&unVpcId={{options.unVpcId}}&detailId={{value}}">            <span class="text' +
                    '-overflow">{{value}}</span>        </a>    </p>    <span class="text-overflow">{' +
                    '{find(data, \'unVpnGwId\', value).vpnGwName}}</span>',editorTpl:'    <div class=' +
                    '"tc-15-select-wrap m">        <select class="tc-15-select" data-forcesync data-n' +
                    'ame=\'nextHub\' b-model="value" style="width: 120px;">            <option b-if="' +
                    '!data || !data.length" value="" selected disabled>无可用VPN网关</option>            <' +
                    'option b-repeat="item in data" value="{{item.unVpnGwId}}">{{item.unVpnGwId}}（{{i' +
                    'tem.vpnGwName}}）</option>        </select>    </div>'
}),LOCAL:u({editable:!1,ty' +
                    'pe:2,name:"Local"}),DC_GW:u({type:3,name:"专线网关",loadData:function(e){return o.ge' +
                    'tDcGwList(e.regionId,e.unVpcId)},tpl:'    <p>        <a data-event="nav" href="/' +
                    'vpc/dcGw?rid={{options.regionId}}&tab=info&dcGwId={{value}}">            <span c' +
                    'lass="text-overflow">{{value}}</span>        </a>    </p>    <span class="text-o' +
                    'verflow">{{find(data, \'directConnectGatewayId\', value).directConnectGatewayNam' +
                    'e}}</span>',editorTpl:'    <div class="tc-15-select-wrap m">        <select clas' +
                    's="tc-15-select" data-forcesync data-name=\'nextHub\' b-model="value" style="wid' +
                    'th: 120px;">            <option b-if="!data || !data.length" value="" selected d' +
                    'isabled>无可用专线网关</option>            <option b-repeat="item in data" value="{{ite' +
                    'm.directConnectGatewayId}}">{{item.directConnectGatewayId}}（{{item.directConnect' +
                    'GatewayName}}）</option>        </select>    </div>'}),CONN:u({type:4,name:"对等连接"' +
                    ',loadData:function(e){var t=$.Deferred();return d.getComData(function(i){var n=i' +
                    '.ownerUin,a=c.STATE;o.getConnList(e.regionId,e.unVpcId).then(function(e){t.resol' +
                    've(e.filter(function(e){return e.uin===n&&e.state===a.PENDING||e.state===a.CONNE' +
                    'CTED}))},t.reject)}),t},tpl:'    <p>        <a data-event="nav" href="/vpc/conn?' +
                    'rid={{options.regionId}}&unVpcId={{options.unVpcId}}&detailId={{value}}">       ' +
                    '     <span class="text-overflow">{{value}}</span>        </a>    </p>    <span c' +
                    'lass="text-overflow">{{find(data, \'peeringConnectionId\', value).peeringConnect' +
                    'ionName}}</span>',editorTpl:'    <div class="tc-15-select-wrap m">        <selec' +
                    't class="tc-15-select" data-forcesync data-name=\'nextHub\' b-model="value" styl' +
                    'e="width: 120px;">            <option b-if="!data || !data.length" value="" sele' +
                    'cted disabled>无可用对等连接</option>            <option b-repeat="item in data" value=' +
                    '"{{item.peeringConnectionId}}">{{item.peeringConnectionId}}（{{item.peeringConnec' +
                    'tionName}}）</option>        </select>    </div>'}),SSLVPN:u({type:7,name:"运维管理VP' +
                    'N",loadData:function(e){return o.getSslVpnList(e.regionId,e.unVpcId)},tpl:'    <' +
                    'p>        <a data-event="nav" href="/sslvpn/vpn">            <span class="text-o' +
                    'verflow">{{value}}</span>        </a>    </p>    <span class="text-overflow">{{f' +
                    'ind(data, \'sslVpnId\', value).sslVpnName}}</span>',editorTpl:'    <div class="t' +
                    'c-15-select-wrap m">        <select class="tc-15-select" data-forcesync data-nam' +
                    'e=\'nextHub\' b-model="value" style="width: 120px;">            <option b-if="!d' +
                    'ata || !data.length" value="" selected disabled>无可用运维管理VPN</option>            <' +
                    'option b-repeat="item in data" value="{{item.sslVpnId}}">{{item.sslVpnId}}（{{ite' +
                    'm.sslVpnName}}）</option>        </select>    </div>'}),IGW:u({type:8,name:"NAT网关' +
                    '",loadData:function(e){return o.getIGWList(e.regionId,e.unVpcId)},tpl:'    <p>  ' +
                    '      <a data-event="nav" href="/vpc/nat?rid={{options.regionId}}&detailId={{val' +
                    'ue}}">            <span class="text-overflow">{{value}}</span>        </a>    </' +
                    'p>    <span class="text-overflow">{{find(data, \'natId\', value).natName}}</span' +
                    '>',editorTpl:'    <div class="tc-15-select-wrap m">        <select class="tc-15-' +
                    'select" data-forcesync data-name=\'nextHub\' b-model="value" style="width: 120px' +
                    ';">            <option b-if="!data || !data.length" value="" selected disabled>无' +
                    '可用NAT网关</option>            <option b-repeat="item in data" value="{{item.natId}' +
                    '}">{{item.natId}}（{{item.natName}}）</option>        </select>    </div>'})},Obje' +
                    'ct.keys(n).forEach(function(e){var t=n[e];r[t.type]=t})}),define("modules/vpc/ro' +
                    'ute/util",function(e,t,i){var n,a,s=e("./types"),d=e("../cacher"),o={};a=Object.' +
                    'keys(s).map(function(e){var t=s[e];return o[t.type]=t,t}).sort(function(e,t){ret' +
                    'urn(t.rank||0)-(e.rank||0)}),n=i.exports={ruleValueTpl:'    <vpc-route-rule-valu' +
                    'e            b-model="item.nextHub"            data="{{dataMap[item.nextType]}}"' +
                    '            options="{{options}}"            item="{{item}}"            editing=' +
                    '"{{editing}}">    </vpc-route-rule-value>',typeMap:o,typeList:a,editableTypeList' +
                    ':a.filter(function(e){return e.editable}),getTypeMeta:function(e){return o[e.nex' +
                    'tType||e]},isTypeEditable:function(e){var t=n.getTypeMeta(e);return!t||t.editabl' +
                    'e},getTypesData:function(e){var t,i={},n=$.Deferred();return t=Object.keys(s).ma' +
                    'p(function(t){var n=s[t],a=$.Deferred();return n.loadData(e).done(function(e){i[' +
                    'n.type]=e}).always(a.resolve),a}),$.when.apply($,t).then(function(){n.resolve(i)' +
                    '},n.reject),n},getExtraData:function(e,t,i){var a,s,o,c,r=$.Deferred();return d.' +
                    'getVpc(e,t).done(function(l){c=l.vpcId,$.when(d.getVpc(e,t).done(function(e){a=e' +
                    '}),i?d.getVpcRouteTableEx(e,t,i).done(function(e){s=e}):$.Deferred().resolve(),n' +
                    '.getTypesData({regionId:e,unVpcId:t,unRouteTableId:i}).done(function(e){o=e})).d' +
                    'one(function(){r.resolve({vpc:a,rtb:s,typesData:o})}).fail(r.reject)}),r}}}),def' +
                    'ine("modules/vpc/route/validator",function(e,t,i){var n,a=e("../validator"),s=e(' +
                    '"./util"),d=e("appUtil");e("constants");n=i.exports=a.extend({routeSet:function(' +
                    'e,t){if(e=e||[],!e.length)return"不能为空";if(e.length>t.ROUTE_TABLE_RULES_MAX_NUM)r' +
                    'eturn"超过"+t.ROUTE_TABLE_RULES_MAX_NUM+"条策略上限";var i,a,o,c={},r={};return e.some(' +
                    'function(e,l){o=l;var p,u,v=e.destinationCidrBlock,f=e.nextType,m=e.nextHub;retu' +
                    'rn d.isValidIp(v)&&(e.destinationCidrBlock=v+="/32"),(p=s.getTypeMeta(f))?p.edit' +
                    'able&&(a=n.destinationCidrBlock(v,t))?(i="destinationCidrBlock",!0):(v=d.formula' +
                    'CIDR(v),!r.hasOwnProperty(v)||p.aggregatable&&r[v]===p.type?(r[v]=p.type,p.edita' +
                    'ble&&(a=p.validate(m,t&&t.typesData&&t.typesData[p.type],t))?(i="nextHub",!0):(u' +
                    '=[v,f,m].join("-"),u in c?(a="重复条目",!0):void(c[u]=1))):(i="destinationCidrBlock"' +
                    ',a="目的端重复",!0)):(i="nextType",a="下一跳类型不正确",!0)}),a&&{index:o,msg:a,field:i}},des' +
                    'tinationCidrBlock:function(e,t){if(!e)return"目的端不能为空";if(!d.isValidRouteDest(e)&' +
                    '&!d.isValidIp(e))return"目的端格式必须为网段，如10.10.10.0/24";var i=t.vpc||{},n=i.vpcCIDR;r' +
                    'eturn n&&d.isCIDRSubset(n,e)?"目的端不能为所属私有网络的子集":void 0},routeTableName:function(e' +
                    ',t,i){return this._validName(e,"路由表名称")}})}),define("modules/vpc/RouteInfo",func' +
                    'tion(e,t,i){var n=e("qccomponent"),a=e("appUtil"),s=e("./cApi"),d="_loadToken",o' +
                    '=i.exports=n.extend({$tpl:'    <div class="tc-15-table-panel">        <div class' +
                    '="tc-15-table-fixed-head">            <table class="tc-15-table-box">           ' +
                    '     <colgroup>                    <col class="tc-15-table-col1" width="250">   ' +
                    '                 <col class="tc-15-table-col2" width="250">                    <' +
                    'col class="tc-15-table-col3">                </colgroup>                <thead> ' +
                    '               <tr tabindex="0">                    <th>                        ' +
                    '<div>                            <span>                                <span>路由表' +
                    'ID/名称</span>                            </span>                        </div>   ' +
                    '                     <i class="resize-line-icon" style="cursor:default;"></i>   ' +
                    '                 </th>                    <th>                        <div>     ' +
                    '                       <span>                                <span>目的端</span>   ' +
                    '                         </span>                        </div>                  ' +
                    '      <i class="resize-line-icon" style="cursor:default;"></i>                  ' +
                    '  </th>                    <th>                        <div>                    ' +
                    '        <span>                                <span>路由表关联子网</span>              ' +
                    '              </span>                        </div>                    </th>    ' +
                    '            </tr>                </thead>            </table>        </div>     ' +
                    '   <div class="tc-15-table-fixed-body">            <table class="tc-15-table-box' +
                    ' tc-15-table-rowhover">                <colgroup>                    <col class=' +
                    '"tc-15-table-col1" width="250">                    <col class="tc-15-table-col2"' +
                    ' width="250">                    <col class="tc-15-table-col3">                <' +
                    '/colgroup>                <tbody>                <tr b-if="loading || invalid ||' +
                    ' !list || !list.length">                    <td colspan="3" class="text-center">' +
                    '<div>                        <span class="text" b-if="loading">                 ' +
                    '           <i class="n-loading-icon"></i>正在加载...                        </span> ' +
                    '                       <span class="text" b-if="invalid">                       ' +
                    '     加载失败                        </span>                        <span class="tex' +
                    't" b-if="!loading && !invalid && (!list || !list.length)">                      ' +
                    '      没有可以显示的数据                        </span>                    </div></td>   ' +
                    '             </tr>                <tr tabindex="0" b-repeat="item in list">     ' +
                    '               <td>                        <div>                            <p> ' +
                    '                               <span class="text-overflow">                     ' +
                    '               {{item.rtbName}}                                    (<a data-even' +
                    't="nav" data-report="vpc.NATGateway_peerConn.detail.routeID" href="/vpc/route?ri' +
                    'd={{regionId}}&rtbUnVpcId={{unVpcId}}&unRtbId={{item.rtbId}}">{{item.rtbId}}</a>' +
                    ')                                </span>                            </p>        ' +
                    '                </div>                    </td>                    <td>         ' +
                    '               <div>                            <p b-repeat="dest in item.destin' +
                    'ationCidrBlockSet"><span class="text-overflow">{{dest}}</span></p>              ' +
                    '          </div>                    </td>                    <td>               ' +
                    '         <div b-repeat="subnet in item.subnetSet">                            <p' +
                    '><span class="text-overflow">                                {{subnet.name}}    ' +
                    '                            (<a data-event="nav" data-report="vpc.NATGateway_pee' +
                    'rConn.detail.SubnetID" href="/vpc/subnet?rid={{regionId}}&subnetId={{subnet.uniq' +
                    'SubnetId}}">{{subnet.uniqSubnetId}}</a>)                            </span></p> ' +
                    '                       </div>                        <div b-if="!item.subnetSet ' +
                    '|| !item.subnetSet.length">                            <p><span class="text-over' +
                    'flow">-</span></p>                        </div>                    </td>       ' +
                    '         </tr>                 </tbody>            </table>        </div>    </d' +
                    'iv>',$beforeInit:function(){var e=this;o.__super__.$beforeInit.apply(e,arguments' +
                    '),e.$watch("[regionId, unVpcId, id]",a.bufferFn(function(){if(e.regionId&&e.unVp' +
                    'cId&&e.id){e.$set({loading:!0,invalid:!1,list:[]});var t=e[d]={};s.request(e.reg' +
                    'ionId,"GetRouteByGatewayId",{gatewayId:e.id,vpcId:e.unVpcId}).done(function(i){i' +
                    'f(t===e[d]){var n;n=i.data||[],e.$set("list",n)}}).fail(function(){e.$set("inval' +
                    'id",!0)}).always(function(){e.$set("loading",!1)})}},100),!0)}})}),define("modul' +
                    'es/vpc/simple-region-selector",function(e,t,i){var n=e("qccomponent"),a=e("manag' +
                    'er"),s=e("appUtil");i.exports=n.extend({$tpl:'    <span class="tc-15-list-det"> ' +
                    '       <select b-model="regionId" class="tc-15-select m" style="min-width:133px"' +
                    ' b-style="style">            <option b-repeat="item in list" value="{{item.regio' +
                    'nId}}">{{item.regionName}}</option>        </select>    </span>',$valuekey:"regi' +
                    'onId",$afterInit:function(){this.getRegionList()},getRegionList:function(){var e' +
                    ',t,i,n=this,d=this.whiteList||[],o=this.regionId||s.getRegionId();i="function"==' +
                    'typeof this.getWhiteList?this.getWhiteList():$.Deferred().resolve(d),t=$.Deferre' +
                    'd(),a.queryRegion(t.resolve,t.reject),$.when(i.done(function(e){d=e}),t.done(fun' +
                    'ction(t){e=s.clone(t)})).done(function(){var t,i=[];for(var a in e)d.length&&d.i' +
                    'ndexOf(a)<0?delete e[a]:i.push({regionName:e[a],regionId:a});i.forEach(function(' +
                    'e){e.regionId===o&&(t=e)}),setTimeout(function(){n.$set({list:i,regionId:(t||i[0' +
                    ']).regionId})},0)})}})}),define("modules/vpc/simple-vpc-selector",function(e,t,i' +
                    '){var n=e("qccomponent"),a=e("./cacher");i.exports=n.extend({$tpl:'    <span cla' +
                    'ss="tc-15-list-det" data-vpc-selector>        {{_getVpcList(regionId)}}        <' +
                    'select b-model="vpcId" class="tc-15-select m" style="min-width:133px" b-style="s' +
                    'tyle">            <option data-uvpcid="{{item.uniqVpcId}}" b-repeat="item in lis' +
                    't" value="{{item.vpcId}}">                {{item.uniqVpcId}} ({{item.vpcName}}) ' +
                    '           </option>        </select>        <div class="{{invalid ? \'is-error' +
                    '\' : \'\'}}">          <div b-style="{display: invalid && tips ? undefined : \'n' +
                    'one\'}" class="form-input-help">{{tips}}</div>        </div>    </span>',$valuek' +
                    'ey:"vpcId",$afterInit:function(){this.$watch("vpcId",function(e){e&&this.list&&t' +
                    'his.$set("unVpcId",this._exchangeVpcId(e,!0))}),this.$watch("unVpcId",function(e' +
                    '){if(e&&this.list){var t=this._exchangeVpcId(e);this.$set("vpcId",t)}})},_exchan' +
                    'geVpcId:function(e,t,i){i=i||this.list;var n="vpcId",a="uniqVpcId";return t&&(a=' +
                    '"vpcId",n="uniqVpcId"),(i.filter(function(t){return t[a]==e})[0]||{})[n]},_getVp' +
                    'cList:function(e){var t=this;e&&a.getVpcList(e).done(function(e){setTimeout(func' +
                    'tion(){var i,n,a=t.vpcFilterList?e.vpcList.filter(function(e){return-1==t.vpcFil' +
                    'terList.indexOf(e.vpcId)}):e.vpcList;t.unVpcId&&(n=t.unVpcId,i=t._exchangeVpcId(' +
                    'n,!1,a)),t.vpcId&&(i=t.vpcId,n=t._exchangeVpcId(i,!0,a)),t.$set({list:a,vpcId:i|' +
                    '|a[0]&&a[0].vpcId,unVpcId:n||a[0]&&a[0].uniqVpcId,invalid:!a.length})},0)})}},{d' +
                    'efaults:{tips:"无可用私有网络"}})}),define("modules/vpc/subnet/Add",function(e,t,i){var' +
                    ' n=e("../BeeBase"),a=e("../cacher"),s=e("constants"),d=e("./validator"),o=e("./R' +
                    'anges"),c=e("util"),r=e("appUtil"),l=e("../cApi"),p=e("../createLimitsMixin"),u=' +
                    'e("dialog"),v=e("tips");e("../vpc/DropDown"),e("../IPInput");var f=function(e,t)' +
                    '{var i,n=[];for(i=e;t>=i;i++)n.push(""+i);return n},m=function(e){if(e){var t=e.' +
                    'vpcCIDR,i=t.split("/");return e.net=i[0],e.mask=i[1],e.subnetMasks=f(e.mask,28),' +
                    'e}};c.insertStyle(".btn-add-cont .disabled{color:#a2a2a2;cursor:default;}.tc-tab' +
                    'le td .vpc-ip-input{ display:inline-block; padding:0; }.tc-table td .vpc-ip-inpu' +
                    't span{ display:inline; padding:0; }");var h=n.extend({$tpl:'    <div class="cre' +
                    'ate-route">        <div class="tc-15-list-wrap" style="padding:0 0 10px;">      ' +
                    '      <ul>                <li>                    <em class="tc-15-list-tit">所属网' +
                    '络</em>                    <span class="tc-15-list-det">                        <' +
                    'span b-tag="vpc-vpc-select" cls="m" region-id="{{regionId}}" use-un-id="1" b-mod' +
                    'el="unVpcId"                              invalid="{{vpcData && vpcData.subnetLi' +
                    'st.length>=maxSubnet ? \'私有网络子网创建数达到上限\' : \'\'}}"                              ' +
                    'after-text="{{vpcData ? \'已有\'+vpcData.subnetList.length+\'个子网\' : \'\'}}"></spa' +
                    'n>                    </span>                </li>            </ul>        </div' +
                    '>        <div class="table-panel host-table-panel mirror-table top-layout">     ' +
                    '       <div class="tc-table-fixed">                <div class="tc-table-fixed-he' +
                    'ad">                    <table class="tc-table tc-table-headcolor">             ' +
                    '           <colgroup>                            <col style="width:25%">        ' +
                    '                    <col style="width:25%">                            <col styl' +
                    'e="width:20%">                            <col style="width:20%">               ' +
                    '             <col style="width:10%">                        </colgroup>         ' +
                    '               <thead>                        <tr tabindex="0">                 ' +
                    '           <th><span>子网名称</span></th>                            <th>           ' +
                    '                     <span>CIDR</span>                            </th>         ' +
                    '                   <th>                                <span>可用区</span>         ' +
                    '                       <help-tips>                                    同一地域下电力和网络' +
                    '互相独立的物理机房，目的是隔离故障，                                    <a href="https://www.qclou' +
                    'd.com/doc/product/215/4927#.E5.8F.AF.E7.94.A8.E5.8C.BA.EF.BC.88zone.EF.BC.89" ta' +
                    'rget="_blank">点击查看可用区容灾实践指南</a>。                                </help-tips>    ' +
                    '                        </th>                            <th>                   ' +
                    '             <span>关联路由表</span>                                <help-tips positi' +
                    'on="left" content="子网须关联一个路由表控制出流量走向。"></help-tips>                            <' +
                    '/th>                            <th><span>操作</span></th>                        ' +
                    '</tr>                        </thead>                    </table>               ' +
                    ' </div>                <div class="tc-table-fixed-body def-scroll" style="overfl' +
                    'ow: auto; max-height: 221px;">                    <table class="tc-table">      ' +
                    '                  <colgroup>                            <col style="width:25%"> ' +
                    '                           <col style="width:25%">                            <c' +
                    'ol style="width:20%">                            <col style="width:20%">        ' +
                    '                    <col style="width:10%">                        </colgroup>  ' +
                    '                      <tbody>                            <tr tabindex="0" b-repe' +
                    'at="subnet in subnetList">                                <td><span><input type=' +
                    '"text" data-name="subnetName" class="input-txt" name="subnetName" b-model="subne' +
                    't.subnetName" data-position="right" data-auto=""></span></td>                   ' +
                    '             <td>                                    <span data-name="cidr" data' +
                    '-position="bottom" data-auto="">                                        <span b-' +
                    'tag="vpc-ip-input" name="subnetNet" type="net" net="{{vpcNet}}" min-mask="{{vpcM' +
                    'ask}}" max-mask="{{subnet.subnetMask}}" b-model="subnet.subnetNet"></span> /    ' +
                    '                                    <select class="tc-15-select vpc-common-field' +
                    '" data-name="subnetMask"                                                style="w' +
                    'idth: 50px;min-width: inherit;font-size:12px; line-height: 22px; height: 24px;pa' +
                    'dding:0;margin:0;"                                                b-model="subne' +
                    't.subnetMask">                                            <option b-repeat="opti' +
                    'on in subnetMasks" value="{{option}}">{{option}}</option>                       ' +
                    '                 </select>                                    </span>           ' +
                    '                     </td>                                <td>                  ' +
                    '                  <span>                                        <div class="tc-1' +
                    '5-select-wrap m">                                            <select class="tc-1' +
                    '5-select" data-name="zoneId" b-model="subnet.zoneId" style="width: 140px;">     ' +
                    '                                           <option b-repeat="zone in zoneList" v' +
                    'alue="{{zone.zoneId}}">{{zone.zoneName}}</option>                               ' +
                    '             </select>                                        </div>            ' +
                    '                        </span>                                </td>            ' +
                    '                    <td>                                    <span>              ' +
                    '                          <div class="tc-15-select-wrap m">                     ' +
                    '                       <select class="tc-15-select" style="width: 140px;" data-n' +
                    'ame="unRouteTableId" b-model="subnet.unRouteTableId">                           ' +
                    '                     <option b-repeat="option in rtbList" value="{{option.unRout' +
                    'eTableId}}">{{option.rtbName}}</option>                                         ' +
                    '   </select>                                        </div>                      ' +
                    '              </span>                                </td>                      ' +
                    '          <td>                                    <span b-if="$index<=0">-</span' +
                    '>                                    <a b-if="$index>0" class="ico-remove" href=' +
                    '"javascript:;" data-title="删除" b-on-click="subnetList.$remove($index)"></a>     ' +
                    '                           </td>                            </tr>               ' +
                    '         </tbody>                    </table>                </div>             ' +
                    '   <div class="btn-add-cont" style="cursor:default;">                    <a href' +
                    '="javascript:;" data-z-index="9999"                       b-on-click="add()"    ' +
                    '                   class="{{ subnetList.length>=maxNewSubnet ? \' disabled\' : ' +
                    '\'\'}}"                       data-title="{{subnetList.length>=maxNewSubnet ? \'' +
                    '私有网络创建子网数已达上限\' : \'\'}}">+ 新增一行</a></div>            </div>        </div>    </' +
                    'div>',$mixins:[p({maxSubnet:s.VPC_LIMIT.SUBNET_PER_VPC_LIMIT_TYPE})],$beforeInit' +
                    ':function(){n.prototype.$beforeInit.apply(this,arguments),this.subnetList||this.' +
                    '$set("subnetList",[]),r.beeBind(this,this,"maxSubnet - (vpcData && vpcData.subne' +
                    'tList && vpcData.subnetList.length || 0)","maxNewSubnet")},$afterInit:function()' +
                    '{var e=this;n.prototype.$afterInit.apply(e,arguments);var t=function(){var t,i,n' +
                    ',s,d=e.regionId,o=e.unVpcId;d&&o&&$.when(a.getZoneList(d).done(function(e){i=e})' +
                    ',a.getVpc(d,o).done(function(e){t=m(e)}),l.request(d,"DescribeSubnetEx",{offset:' +
                    '0,limit:999,vpcId:o},"加载子网列表失败！").done(function(e){n=l.translate(e.data,l.transl' +
                    'ateMap.subnet)}),l.request(d,"DescribeRouteTable",{offset:0,limit:999,vpcId:o},"' +
                    '加载路由表列表失败！").done(function(e){s=l.translate(e.data,l.translateMap.routeTable)}))' +
                    '.done(function(){t.subnetList=n;var a={subnetNet:"0.0.0.0",subnetMask:Math.max(t' +
                    '.mask,24)};e.$set({zoneList:i,vpcData:t,vpcId:t.vpcId,vpcNet:t.net,vpcMask:t.mas' +
                    'k,subnetMasks:t.subnetMasks,rtbList:s,defaultRow:a,subnetList:[c.cloneObject(a)]' +
                    '})})},i=function(){a.getZoneList(e.regionId).done(function(t){e.$set("zoneList",' +
                    't)})};e.$watch("regionId",t),e.$watch("unVpcId",t),t(),e.$watch("regionId",i,!0)' +
                    '},add:function(){var e=this,t=e.vpcData,i=e.subnetList;if(t&&!(t.subnetList.leng' +
                    'th+i.length>=e.maxSubnet)){i.push(c.cloneObject(e.defaultRow));var n=$(e.$el).fi' +
                    'nd(".def-scroll"),a=n[0];a.scrollTop=a.scrollHeight}},validate:function(){var e,' +
                    't=this,i=t.subnetList,n=t.vpcData,a=u.getBtn(0),s=!0;if(!n)return v.error("VPC数据' +
                    '正在加载中"),!1;if(i.length+n.subnetList.length>t.maxSubnet)return v.error("私有网络子网创建数' +
                    '达到上限"),!1;var c=new o([r.getIpZone(n.net,n.mask)]),l={};return n.subnetList.forE' +
                    'ach(function(e){var t=e.subnetCIDR,i=t.split("/");c.sub(r.getIpZone(i[0],i[1])),' +
                    'l[e.subnetName]=!0}),e=$(t.$el).find("table tbody tr"),i.forEach(function(t,i){i' +
                    'f(s){t.cidr=t.subnetNet+"/"+t.subnetMask;var n=d.multiValidate(t,["subnetName","' +
                    'cidr","zoneId","unRouteTableId"],{cidr:c,subnetName:l},e[i],{$btn:a,scrollIntoVi' +
                    'ew:!0});n&&(s=!1)}}),s}},{defaults:$.extend({},n.prototype.defaults,{maxSubnet:s' +
                    '.VPC_SUBNET_MAX_NUM||999,vpcNet:"10.0.0.0",vpcMask:16,subnetMasks:[],rtbList:[]}' +
                    ')});i.exports=h}),define("modules/vpc/subnet/ChangeAclBubble",function(e,t,i){va' +
                    'r n=e("../Bubble"),a=e("tips"),s=e("models/acl"),d=e("../acl/AclSelector"),o=n.e' +
                    'xtend({$afterInit:function(){n.prototype.$afterInit.apply(this,arguments),this.o' +
                    'ldValue=this.value},content:'<div class="tc-15-input-text-wrap m" style="overflo' +
                    'w: hidden"><acl-selector b-ref="field" region-id="{{regionId}}" cls="m" vpc-id="' +
                    '{{vpcId}}" b-model="value"></acl-selector></div>',doChange:function(){var e,t=th' +
                    'is,i=t.value,n=t.oldValue,d=(t.regionId,$.Deferred());return i===n?d.resolve():t' +
                    'his.$refs.field.invalid?(a.error(invalid),!1):(e=n&&0!=n?new Promise(function(e,' +
                    'i){s.request({name:"delSubnetAclRule",data:{vpcId:t.vpcId,aclId:n,subnetIds:[t.s' +
                    'ubnetId]},success:e,fail:i})}):Promise.resolve(),e.then(function(){s.request({na' +
                    'me:"addSubnetAclRule",data:{vpcId:t.vpcId,aclId:i,subnetIds:[t.subnetId]},succes' +
                    's:d.resolve,fail:d.reject})},d.reject),d.promise())}});o.tag("acl-selector",d),i' +
                    '.exports=o}),define("modules/vpc/subnet/ChangeRtbBubble",function(e,t,i){var n=e' +
                    '("../Bubble"),a=e("tips"),s=e("../cacher"),d=e("../cApi");e("../route/DropDown")' +
                    ',i.exports=n.extend({$afterInit:function(){n.prototype.$afterInit.apply(this,arg' +
                    'uments),this.oldValue=this.value},content:'<div class="tc-15-input-text-wrap m">' +
                    '<span b-tag="vpc-rtb-select" b-ref="field" region-id="{{regionId}}" cls="m" use-' +
                    'un-id="1" un-vpc-id="{{item.unVpcId}}" b-model="value"></span></div>',doChange:f' +
                    'unction(){var e,t=this,i=t.value,n=t.oldValue,o=t.item,c=t.regionId,r=o.unVpcId,' +
                    'l=o.unSubnetId,p=$.Deferred();return t.text=t.$refs.field.getText(),i===n?p.reso' +
                    'lve():e?(a.error(e),!1):d.request(c,"AssociateRouteTable",{vpcId:r,subnetId:l,ro' +
                    'uteTableId:i},"修改子网关联路由表失败！").done(function(){s.resetNamespace("subnet",c,r,l),s' +
                    '.resetNamespace("subnet",c,o.vpcId,o.subnetId)})}})}),define("modules/vpc/subnet' +
                    '/Detail",function(e,t,i){var n=e("../BeeBase"),a=(e("appUtil"),e("router")),s=e(' +
                    '"../cApi"),d=e("../cacher"),o=e("../ResourceGrid"),c=e("./RenameBubble"),r=e("co' +
                    'nstants"),l=e("reporter"),p=e("./subnetResource"),u=e("../Confirm"),v=e("modules' +
                    '/vpc/acl/detail/aclRules"),f=e("./ChangeRtbBubble"),m=e("./ChangeAclBubble"),h=e' +
                    '("modules/vpc/route/routeInfo"),b=e("models/acl"),g=n.extend({$tpl:'    <div cla' +
                    'ss="manage-area" b-style="{display: hidden ? \'none\': \'\'}" >        <div b-if' +
                    '="loading" style="text-align:center; padding:100px;">加载中...</div>        <div b-' +
                    'if="notFound" style="text-align:center; padding:100px;">未查询到指定子网, <a href="javas' +
                    'cript:;" b-on-click="$set(\'id\', \'\')">返回列表</a></div>        <div class="manag' +
                    'e-area-title secondary-title" b-style="{display:notFound||loading?\'none\':\'\'}' +
                    '">            <a class="back-link" b-on-click="back()" href="javascript:;">     ' +
                    '           <i class="btn-back-icon"></i><span>返回</span>            </a>         ' +
                    '   <span class="line-icon">|</span>            <h2>{{data && (data.subnetName + ' +
                    '" 详情")}}</h2>            <div class="manage-area-title-right">                <a' +
                    ' href="https://www.qcloud.com/doc/product/215/4927" target="_blank">            ' +
                    '        <b class="links-icon"></b> 私有网络与子网帮助文档                </a>            </' +
                    'div>        </div>        <div b-ref="main" class="manage-area-main secondary-ma' +
                    'in" b-style="{display:notFound||loading?\'none\':\'\'}">            <div class="' +
                    'fixed-tab tc-15-tab-alt">                <ul class="tc-15-tablist"  b-if="!notFo' +
                    'und">                    <li class="{{tab==0 ? \'tc-cur\': \'\'}}">             ' +
                    '           <a href="javascript:;" b-on-click="$set(\'tab\', 0)" title="基本信息" >基本' +
                    '信息</a>                    </li>                    <li class="{{tab==1 ? \'tc-cu' +
                    'r\': \'\'}}">                        <a href="javascript:;" b-on-click="$set(\'t' +
                    'ab\', 1)" title="IP地址分配" data-report="vpc.subnet.detail.ACL">ACL规则</a>          ' +
                    '          </li>                </ul>                <div b-style="{display : not' +
                    'Found || tab != 0 ? \'none\' : \'\'}">                    <div class="param-box"' +
                    '>                        <div class="param-hd">                            <h3>基' +
                    '本信息</h3>                        </div>                        <div class="param-' +
                    'bd">                            <ul class="item-descr-list">                    ' +
                    '            <li>                                    <em class="item-descr-tit">子' +
                    '网名称</em><span class="item-descr-txt"><i b-if="data.renaming" class="n-loading-ic' +
                    'on"></i>{{data.subnetName}}                                        <i class="pen' +
                    'cil-icon hover-icon" b-on="{click: rename(data)}" data-title="编辑"></i></span>   ' +
                    '                             </li>                                <li><em class=' +
                    '"item-descr-tit">子网ID</em><span class="item-descr-txt">{{data.unSubnetId}}</span' +
                    '>                                </li>                                <li>      ' +
                    '                              <em class="item-descr-tit">子网CIDR</em>            ' +
                    '                        <span class="item-descr-txt">{{data.cidrBlock||data.subn' +
                    'etCIDR}}</span>                                </li>                            ' +
                    '    <li>                                    <em class="item-descr-tit">所属网络</em>' +
                    '                                    <span class="item-descr-txt">               ' +
                    '                         <a data-event="nav" href="/vpc/vpc?rid={{regionId}}&unV' +
                    'pcId={{data.unVpcId}}">{{data.unVpcId}}</a>                                     ' +
                    '   ({{data.vpcName}} | {{data.vpcCidrBlock}})                                   ' +
                    ' </span>                                </li>                                <li' +
                    '><em class="item-descr-tit">地域</em><span class="item-descr-txt">{{regionName}}</' +
                    'span>                                </li>                                <li>  ' +
                    '                                  <em class="item-descr-tit">可用区</em>           ' +
                    '                         <span class="item-descr-txt">{{data._zoneName || data.z' +
                    'oneId}}</span>                                </li>                             ' +
                    '   <li>                                    <em class="item-descr-tit">已关联ACL</em' +
                    '>                                    <span class="item-descr-txt">              ' +
                    '                          <span b-if="data.networkAclId == 0">                  ' +
                    '                          无                                        </span>      ' +
                    '                                  <span b-if="data.networkAclId != 0">          ' +
                    '                                  <a data-event="nav" href="/vpc/acl/detail/{{da' +
                    'ta.vpcId}}/{{data.networkAclId}}?rid={{regionId}}">{{data.networkAclId}}</a>    ' +
                    '                                        <a style="margin-left: 0.5em" href="java' +
                    'script:;" b-on="{click: unBindAcl}">解绑</a>                                      ' +
                    '  </span>                                        <span class="{{disabledChangeAc' +
                    'l ? \'disable-link\': \'\'}}">                                            <a dat' +
                    'a-title?="disabledChangeAcl" href="javascript:;" b-on="{click: changeAcl}">     ' +
                    '                                           <span b-if="data.networkAclId == 0">绑' +
                    '定</span>                                                <span b-if="data.network' +
                    'AclId != 0">更换</span>                                            </a>           ' +
                    '                             </span>                                    </span> ' +
                    '                               </li>                                <li>        ' +
                    '                            <em class="item-descr-tit">创建时间</em>                ' +
                    '                    <span class="item-descr-txt">{{data.subnetCreateTime || data' +
                    '.createTime}}</span>                                </li>                       ' +
                    '     </ul>                        </div>                    </div>              ' +
                    '      <div class="param-box">                        <div class="param-hd">     ' +
                    '                       <h3>包含资源</h3>                            <resource-grid b' +
                    '-if="!notFound" region-id="{{regionId}}" item="{{data}}"></resource-grid>       ' +
                    '                 </div>                    </div>                    <div class=' +
                    '"param-box">                        <div class="param-hd">                      ' +
                    '      <h3>路由策略</h3>                        </div>                        <div cl' +
                    'ass="param-bd">                            <ul class="item-descr-list">         ' +
                    '                       <li>                                    <em class="item-d' +
                    'escr-tit">已绑定路由表</em>                                    <span class="item-descr' +
                    '-txt" b-if="!data.unRouteTableId">                                       无      ' +
                    '                              </span><span class="item-descr-txt" b-if="data.unR' +
                    'outeTableId">{{data.routeTableName}}                                        (<a ' +
                    'data-event="nav" href="/vpc/route?rid={{regionId}}&rtbUnVpcId={{data.unVpcId}}&u' +
                    'nRtbId={{data.unRouteTableId}}">{{data.unRouteTableId}}</a>)</span>             ' +
                    '                       <span class="{{data._disabledChangeRtb ? \'disable-link\'' +
                    ' : \'\'}}">                                        <a style="margin-left:0.5em;"' +
                    ' href="javascript:;" data-title?="data._disabledChangeRtb" b-on="{click: changeR' +
                    'tb}">更换路由表</a>                                    </span>                       ' +
                    '         </li>                            </ul>                        </div>   ' +
                    '                     <div b-if="data.unRouteTableId" style="width: 940px;" class' +
                    '="tc-15-table-panel">                           <route-info region-id="{{regionI' +
                    'd}}" un-vpc-id="{{data.unVpcId}}" un-route-table-id="{{data.unRouteTableId}}"></' +
                    'route-info>                        </div>                    </div>             ' +
                    '   </div>                <div class="param-box" b-if="!notFound && tab == 1">   ' +
                    '                 <div class="param-bd">                        <ul class="item-d' +
                    'escr-list">                            <li>                                <em c' +
                    'lass="item-descr-tit">已关联ACL</em><span class="item-descr-txt">                  ' +
                    '                      <span b-if="data.networkAclId == 0">                      ' +
                    '                      无                                        </span>          ' +
                    '                              <span b-if="data.networkAclId != 0">              ' +
                    '                              <a data-event="nav" href="/vpc/acl/detail/{{data.v' +
                    'pcId}}/{{data.networkAclId}}?rid={{regionId}}">{{data.networkAclId}}</a>        ' +
                    '                                    <a style="margin-left: 0.5em" href="javascri' +
                    'pt:;" b-on="{click: unBindAcl}">解绑</a>                                        </' +
                    'span>                                        <span class="{{disabledChangeAcl ? ' +
                    '\'disable-link\': \'\'}}">                                            <a data-ti' +
                    'tle?="disabledChangeAcl" href="javascript:;" b-on="{click: changeAcl}">         ' +
                    '                                       <span b-if="data.networkAclId == 0">绑定</s' +
                    'pan>                                                <span b-if="data.networkAclI' +
                    'd != 0">更换</span>                                            </a>               ' +
                    '                         </span></span>                            </li>        ' +
                    '                </ul>                    </div>                    <qc-tabs b-if' +
                    '="aclId && aclId!=0" list="{{aclRules}}" class-name=\'s\' vpc-id="{{vpcId}}" acl' +
                    '-id="{{aclId}}"></qc-tabs>                </div>            </div>        </div>' +
                    '    </div>',
$valuekey:"id",watchFields:["regionId","id"],backUrl:"/vpc/subnet",' +
                    'back:function(){history.length>1?history.back():this.backUrl&&a.navigate(this.ba' +
                    'ckUrl)},updateCallback:function(){},$afterInit:function(){var e=this;g.__super__' +
                    '.$afterInit.call(this),e.$watch("["+e.watchFields.join(",")+"]",function(){e.loa' +
                    'dData()}),e.bindDetail&&e.bindDetail(e)},loadData:function(e){var t=this;return ' +
                    't.regionId&&(t.id||e)?t.loading?void t.$set({loading:!1}):(t.$set("loading",!0),' +
                    't.$set({loading:!e,notFound:!1}),void $.when(s.request(t.regionId,"DescribeSubne' +
                    'tEx",{subnetId:t.id,getAclIdFlag:1}),d.getZoneList(t.regionId)).then(function(e,' +
                    'i){var n;e=e.data[0],e||(e={},n=!0),e.subnetId&&e.vpcId&&(e.vpcId=e.vpcId.replac' +
                    'e(/^.+?(\d+)$/,"$1"),e.subnetId=e.subnetId.replace(/^.+?(\d+)$/,"$1")),e.zoneId&' +
                    '&i&&i.some&&i.some(function(t){t.zoneId==e.zoneId&&(e._zoneName=t.zoneName)}),0=' +
                    '=e.rtbNum?e._disabledChangeRtb="无可用路由表":1==e.rtbNum&&(e._disabledChangeRtb="无法更换' +
                    '路由表, 该私有网络下只有一个路由表"),t.$set({loading:!1,notFound:n,data:e,vpcId:e.vpcId,aclId:e.' +
                    'networkAclId,regionName:r.REGIONNAMES[t.regionId]}),b.request({name:"getAclList"' +
                    ',data:{vpcId:e.vpcId,offset:0,limit:20},success:function(i){var n=!1;0==i.totalN' +
                    'um?n="无可用网络 ACL":1==i.totalNum&&e.networkAclId&&(n="无法更换网络 ACL, 该私有网络下只有一个网络 ACL' +
                    '"),t.$set("disabledChangeAcl",n)}})},function(){t.$set({loading:!1})})):void t.$' +
                    'set({loading:!1,notFound:!1})},rename:function(e){return function(e){var t=this;' +
                    'new c({regionId:t.regionId,item:t.data,value:t.data.subnetName,target:$(e.curren' +
                    'tTarget).parent(),onSave:function(e,i,n){t.$set({renaming:!0}),e.done(function()' +
                    '{t.$set({renaming:!1}),t.$set("data.subnetName",i),t.updateCallback&&t.updateCal' +
                    'lback(t.data)}),e.fail(function(){t.$set({renaming:!1}),t.$set("data.subnetName"' +
                    ',n)})}})}},refreshRtbList:function(){this.loadData(!0)},changeRtb:function(e){va' +
                    'r t=this,i=t.data;i._disabledChangeRtb||new f({regionId:t.regionId,item:i,value:' +
                    'i.unRouteTableId,target:$(e.currentTarget).parent(),attachEl:t.$refs.main,onYes:' +
                    'function(){var e=this.doChange();return e===!1?e:(e.done(function(){t.refreshRtb' +
                    'List(),t.updateCallback&&t.updateCallback()}),void l.click("vpc.subnet.detail.ch' +
                    'angeRoute"))}})},refreshAclList:function(){this.loadData(!0)},changeAcl:function' +
                    '(e){var t=this,i=t.data;t.disabledChangeAcl===!1&&new m({regionId:t.regionId,vpc' +
                    'Id:t.vpcId,subnetId:i.subnetId,item:i,value:i.networkAclId,target:$(e.currentTar' +
                    'get).parent(),attachEl:t.$refs.main,onYes:function(){var e=this.doChange();retur' +
                    'n e===!1?e:(e.done(function(e){e&&t.refreshAclList()}),void l.click("vpc.subnet.' +
                    'detail.changeACL"))}})},unBindAcl:function(e){var t=this,i=t.data;new u({target:' +
                    '$(e.currentTarget),box:this.$gridBody,$data:{title:"确定解绑该网络 ACL？"},onConfirm:fun' +
                    'ction(){b.request({regionId:t.regionId,name:"delSubnetAclRule",data:{vpcId:t.vpc' +
                    'Id,aclId:i.networkAclId,subnetIds:[i.subnetId]},success:function(){t.refreshAclL' +
                    'ist()}})}})}},{defaults:{loading:!0,aclRules:[{label:"入站规则",content:'<acl-rules ' +
                    'dir="1" hide-header="{{true}}" vpc-id="{{vpcId}}" acl-id="{{aclId}}"></acl-rules' +
                    '>'},{label:"出站规则",content:'<acl-rules dir="0" hide-header="{{true}}" vpc-id="{{v' +
                    'pcId}}" acl-id="{{aclId}}"></acl-rules>'}]}});g.tag("acl-rules",v),g.tag("resour' +
                    'ce-grid",o.extend({},{defaults:{RESOURCE:p}})),g.tag("route-info",h),i.exports=g' +
                    '}),define("modules/vpc/subnet/DropDown",function(e,t,i){var n=e("qccomponent"),a' +
                    '=e("../DropDown"),s=(e("../cApi"),e("../cacher")),d=a.extend({watchFields:["regi' +
                    'onId","vpcId","unVpcId"],getData:function(e){var t=this;s.getVpcSubnetList(t.reg' +
                    'ionId,t.unVpcId||t.vpcId).done(function(i){var n=+t.zoneId;e(i.filter(function(e' +
                    '){return!n||e.zoneId===n}).map(function(e){return{text:e.subnetCIDR,value:t.useU' +
                    'nId?e.unSubnetId:e.subnetId}}))})}});n.tag("vpc-subnet-select",d),i.exports=d}),' +
                    'function(e,t){define("modules/vpc/subnet/Ranges",t)}("Ranges",function(){functio' +
                    'n e(e,t){this.ranges=e instanceof Array?e:[[e,t]],this._connectRange()}var t={BE' +
                    'FORE:-1,IN:1,AFTER:2},i={BEFORE:-3,C_BEFORE:-2,CONTAIN:-1,IN:1,C_AFTER:2,AFTER:3' +
                    '},n={},a=n[t.BEFORE]={};a[t.BEFORE]=i.BEFORE,a[t.IN]=i.C_BEFORE,a[t.AFTER]=i.CON' +
                    'TAIN,a=n[t.IN]={},a[t.IN]=i.IN,a[t.AFTER]=i.C_AFTER,a=n[t.AFTER]={},a[t.AFTER]=i' +
                    '.AFTER;var s=function(e,i){return e<i[0]?t.BEFORE:e<=i[1]?t.IN:t.AFTER},d=functi' +
                    'on(e,t){return n[s(e[0],t)][s(e[1],t)]},o=function(e){return Number(e)===e&&e%1=' +
                    '==0},c=function(e,t){var i=e[1],n=t[0];return o(i)&&o(n)?i+1===n:!1},r=function(' +
                    'e,t){switch(d(e,t)){case i.BEFORE:return c(e,t)?[[e[0],t[1]]]:[e,t];case i.IN:re' +
                    'turn[t];case i.AFTER:return c(t,e)?[[t[0],e[1]]]:[t,e];case i.C_BEFORE:return[[e' +
                    '[0],t[1]]];case i.C_AFTER:return[[t[0],e[1]]];case i.CONTAIN:return[e]}},l=funct' +
                    'ion(e,t){switch(d(e,t)){case i.BEFORE:return[e];case i.IN:return[];case i.AFTER:' +
                    'return[e];case i.C_BEFORE:return[[e[0],t[0]-1]];case i.C_AFTER:return[[t[1]+1,e[' +
                    '1]]];case i.CONTAIN:return[[e[0],t[0]-1],[t[1]+1,e[1]]]}};return e.prototype={_c' +
                    'onnectRange:function(){var e=this.ranges;e.sort(d);var t,i,n,a=[],s=e[0];for(t=1' +
                    ';t<e.length;t++)i=e[t],n=r(s,i),a=a.concat(n),s=a.pop();return a.push(s),this.ra' +
                    'nges=a},add:function(e){return this.ranges.push(e),this._connectRange(),this},su' +
                    'b:function(e){if(!o(e[0]))throw new Error("Not support non integer range");var t' +
                    ',i,n,a=this.ranges,s=[];for(t=0;t<a.length;t++)n=a[t],i=l(n,e),s=s.concat(i);ret' +
                    'urn this.ranges=s,this},isConflict:function(e){var t,n=this.ranges;for(t=0;t<n.l' +
                    'ength;t++)switch(d(n[t],e)){case i.IN:case i.C_BEFORE:case i.C_AFTER:case i.CONT' +
                    'AIN:return!0}return!1},isContain:function(e){var n,a,c=this.ranges;if(o(e)){for(' +
                    'n=0;n<c.length;n++)if(s(e,c[n])===t.IN)return!0}else for(a=e,n=0;n<c.length;n++)' +
                    'if(d(a,c[n])===i.IN)return!0;return!1}},e}),define("modules/vpc/subnet/RenameBub' +
                    'ble",function(e,t,i){var n=e("../RenameBubble"),a=e("../cApi"),s=e("../cacher"),' +
                    'd=e("constants"),o=e("appUtil");i.exports=n.extend({validate:function(e){return ' +
                    'e.length>25?"子网名称不能超过25个字符":o.isValidName(e)?void 0:"子网"+d.INVALID_NAME_TIPS},do' +
                    'Save:function(e){var t=this,i=t.item,n=t.regionId,d=(i.vpcId,i.unVpcId),o=i.unSu' +
                    'bnetId;return a.request(n,"ModifySubnetAttribute",{vpcId:d,subnetId:o,subnetName' +
                    ':e},"修改子网名失败！").done(function(){s.resetNamespace("subnet",n,d,o),s.resetNamespac' +
                    'e("subnet",n,i.vpcId,i.subnetId)})}})}),define("modules/vpc/subnet/subnet",funct' +
                    'ion(e,t,i){e("widget/region/regionSelector");var n=e("../BeeBase"),a=e("../vpc/S' +
                    'elector"),s=e("../moduleBase"),d=e("../cacher"),o=e("../util"),c=e("../cApi"),r=' +
                    'e("../Grid"),l=e("./RenameBubble"),p=e("./ChangeRtbBubble"),u=e("./Add"),v=e("re' +
                    'porter"),f=e("./Detail"),m=e("../Confirm"),h=e("../createLimitsMixin"),b=(e("man' +
                    'ager"),e("constants")),g=e("dialog"),I=e("tips"),w=e("appUtil"),y="grid",x="regi' +
                    'onSelector",C="vpcSelector",T="addDialog",N=n.extend({$tpl:'<div><div b-style="{' +
                    'display: subnetId ? \'none\' : \'block\'}"><div class="manage-area-title"><h2>子网' +
                    '</h2><span b-tag="region-selector" b-ref="regionSelect" b-model="regionId" b-wit' +
                    'h="{getWhiteList:getWhiteList}"></span><span b-tag="vpc-select" b-ref="vpcSelect' +
                    '" use-un-id="1" region-id="{{regionId}}" b-model="unVpcId"></span><div class="ma' +
                    'nage-area-title-right"><a href="https://www.qcloud.com/doc/product/215/4927" tar' +
                    'get="_blank"><b class="links-icon"></b> 私有网络与子网帮助文档</a></div></div><div b-tag="q' +
                    'c-action-panel" b-ref="toolbar" b-with="{widgets:toolbar}"></div><div data-mod="' +
                    'grid"></div><div data-mod="pagination"></div></div><div b-style="{display: subne' +
                    'tId ? \'block\' : \'none\'}"><detail region-id="{{regionId}}"update-callback="{{' +
                    'detailUpdate}}"bind-detail="{{bindDetail.bind(this)}}"tab="{{tab}}" b-model="sub' +
                    'netId" data="{{detailData}}"></detail></div></div>',$mixins:[h({MAX_SUBNET_PER_V' +
                    'PC_NUM:b.VPC_LIMIT.SUBNET_PER_VPC_LIMIT_TYPE})],bindDetail:function(e){w.beeBind' +
                    'Both(e,this,"tab")}},{defaults:{MAX_SUBNET_PER_VPC_NUM:b.VPC_SUBNET_MAX_NUM}});N' +
                    '.tag("vpc-select",a),N.tag("detail",f),i.exports=s.extend({routeBase:"/vpc/subne' +
                    't",params:$.extend({},s.params,{zoneIds:{defaults:"",route:!1},keyword:"",subnet' +
                    'Id:{defaults:"",history:!0},tab:{defaults:"0"}}),Bee:N,getBeeConfig:function(){v' +
                    'ar e=this,t=s.getBeeConfig.apply(this,arguments),i=function(){e.handleAction(thi' +
                    's.name,this)};return $.extend(t.$data,{actionHandler:i,toolbar:[{type:"action",n' +
                    'ame:"add",label:"+新建",disabled:!0,attr:{},action:i},{type:"filter-expander",labe' +
                    'l:"筛选"},{type:"action",name:"columns",className:"weak setting","float":"right",a' +
                    'ction:i},{type:"search",name:"search","float":"right",placeholder:"搜索子网的关键词",mul' +
                    'tiline:!1,search:i},{type:"filter-panel",name:"filter","float":"none",initChange' +
                    ':!1,params:[{key:"zone",label:"区域",options:[]}],change:function(){e.handleAction' +
                    '("filter",this)}},{type:"filter-result-tag-list",name:"filter-result","float":"n' +
                    'one"}]}),$.extend(t,{getWhiteList:function(){return d.getRegionIdList()}})},onRe' +
                    'nder:function(){var t=this,i=t.$el,n=t.bee;e("../adTip").init(t.$el);var a=n.$re' +
                    'fs,s=a.toolbar,u=a.vpcSelect,f=s.findWidgetByName("search"),h=s.findWidgetByName' +
                    '("filter"),b=s.findWidgetByName("filter-result"),g=function(){t[y]&&t[y].bufferR' +
                    'eload()},I=!0;t.vpcId2UnId().always(function(){I=!1,g()});var x,C=function(e){x=' +
                    '{},e.forEach(function(e){x[e.zoneId]=e.zoneName});var i=e.map(function(e){return' +
                    '{label:e.zoneName,value:e.zoneId}});h.params.$set(0,{all:!1,options:i}),b.$set({' +
                    'result:[],visible:!1}),t.setParam("zoneIds",""),h.lastState=JSON.parse(JSON.stri' +
                    'ngify(h.params))},T=d.getZoneList(n.regionId).done(C);n.$watch("regionId",functi' +
                    'on(){t.setParam({unVpcId:void 0}),T=d.getZoneList(n.regionId).done(C),g()}),["un' +
                    'VpcId","zoneIds","keyword","page","count"].forEach(function(e){n.$watch(e,g)}),[' +
                    '"unVpcId","zoneIds","keyword","count"].forEach(function(e){n.$watch(e,function()' +
                    '{n.$set("page",1)})}),o.beeBind(t.bee,f,"keyword");var N=t[y]=new r({$el:t.getMo' +
                    'dDom(i,y),$data:{count:n.count,page:n.page,cacheKey:"vpc_subnet_columns_v2015112' +
                    '4",colums:[{key:"subnetName",name:"ID/名称",require:!0,order:!0,tdTpl:'<p><a href=' +
                    '"javascript:;" data-role="detail"><span class="text-overflow">{{item.unSubnetId}' +
                    '}</span></a></p><span class="text-overflow m-width"><i b-if="item.renaming" clas' +
                    's="n-loading-icon"></i>{{item.subnetName}}</span>{{{renameIconHtml}}}'},{key:"un' +
                    'VpcId",name:"所属网络",tdTpl:o.vpcTdTpl},{key:"subnetCIDR",name:"CIDR"},{key:"zoneId' +
                    '",name:"可用区"},{key:"unRouteTable",name:"关联路由表",tdTpl:'<p><a data-role="detail" d' +
                    'ata-event="nav" data-report="vpc.subnet.routeTable" href="/vpc/route?rid={{regio' +
                    'nId}}&rtbUnVpcId={{item.unVpcId}}&unRtbId={{item.unRouteTableId}}"><span class="' +
                    'text-overflow">{{item.unRouteTableId}}</span></a></p><span class="text-overflow ' +
                    'm-width"><i b-if="item.rtbEditing" class="n-loading-icon"></i>{{item.rtbName}}</' +
                    'span>'},{key:"vpcDevices",name:"云主机",width:100},{key:"availableIP",name:"可用IP",w' +
                    'idth:100},{key:"createTime",name:"创建时间",hide:!0},{key:"_action",name:"操作",minWid' +
                    'th:100,required:!0}]},getCellContent:function(e,t,i){var n;switch(i.key){case"zo' +
                    'neId":n='<span class="text-overflow">'+(x&&x[e]||e)+"</span>";break;case"vpcDevi' +
                    'ces":n=void 0===e?"-":(0>=e?'<span class="text-overflow">'+e+"个</span>":'<span c' +
                    'lass="text-overflow"><a data-event="nav" href="/cvm?rid={{regionId}}&vpcId={{ite' +
                    'm.vpcId}}&subnetIds={{item.subnetId}}">'+e+"个</a></span>")+' <a data-role="addCv' +
                    'm" data-report="vpc.subnet.addCVM" data-title="添加云主机" href="#"><i class="gateway' +
                    '-icon"></i></a>';break;case"availableIP":n=void 0===e?"-":e+"个";break;case"_acti' +
                    'on":n=this.getActionsCell([{text:"删除",act:"remove",disabled:t.vpcDevices>0,title' +
                    ':t.vpcDevices>0?"无法删除含有云服务器的子网":""},{text:"更换路由表",act:"EditRtb",disabled:t.rtbNu' +
                    'm<2,title:t.rtbNum<2?t.rtbNum?"无法更换路由表, 该私有网络下只有一个路由表":"无可用路由表":""}])}return n},' +
                    'getData:function(e,t){if(!I){var i,a,s,d=this,o=e.count,r=e.page,l=o*(r-1),p=o,u' +
                    '=n.keyword;d.showLoading("加载中..."),i=c.request(n.regionId,"DescribeSubnetEx",{of' +
                    'fset:l,limit:p,vpcId:n.unVpcId||void 0,getAclIdFlag:1,subnetName:u,zoneIds:n.zon' +
                    'eIds?n.zoneIds.split("|"):void 0},"加载子网列表失败").translate({totalCount:"totalNum",d' +
                    'ata:{key:"detail",map:"subnet"}}),i.done(function(e){a=e}),s=$.when(i,T),s.done(' +
                    'function(){var t=a.detail;t=d.localSort(t,e.orderField,e.order),d.setData({total' +
                    'Num:a.totalNum,searchKey:u,list:t})}),s.always(function(){d.hideLoading()})}},on' +
                    'RowRename:function(e,t,i){var a=this;new l({regionId:n.regionId,item:e,value:e.s' +
                    'ubnetName,target:$(i.currentTarget).parent(),onSave:function(e,i,n){a.list.$set(' +
                    't,{subnetName:i,renaming:!0}),e.done(function(){a.list.$set(t,{renaming:!1})}),e' +
                    '.fail(function(){a.list.$set(t,{subnetName:n,renaming:!1})})}})},onRowEditRtb:fu' +
                    'nction(e,t,i){var a=this;new p({regionId:n.regionId,item:e,value:e.unRouteTableI' +
                    'd,target:$(i.currentTarget).parent(),offset:{left:-100,top:0},onYes:function(){v' +
                    'ar i=this.doChange();if(i===!1)return i;var n=this.value,s=this.oldValue,d=e.rtb' +
                    'Name;a.list.$set(t,{unRouteTableId:n,rtbName:this.text,rtbEditing:!0}),i.done(fu' +
                    'nction(){a.list.$set(t,{rtbEditing:!1})}),i.fail(function(){a.list.$set(t,{unRou' +
                    'teTableId:s,rtbName:d,rtbEditing:!1})}),v.click("vpc.subnet.changeRoute")}})},on' +
                    'RowAddCvm:function(e,t,i){window.open("https://buy.qcloud.com/cvm?"+$.param({reg' +
                    'ionId:n.regionId,vpcId:e.vpcId,subnetId:e.subnetId,zoneId:e.zoneId}))},onRowRemo' +
                    've:function(e,t,i){var a=this;new m({target:$(i.currentTarget),box:this.$gridBod' +
                    'y,$data:{title:"确定删除该子网？"},onConfirm:function(){var t=n.regionId,i=e.unVpcId,s=e' +
                    '.unSubnetId;c.request(t,"DeleteSubnet",{vpcId:i,subnetId:s},"删除子网失败！").done(func' +
                    'tion(){d.resetNamespace("subnet",t,i,s),a.bufferReload()})}})},onRowDetail:funct' +
                    'ion(e,t,i){var a=this;n.$set({tab:"0",detailData:e,subnetId:e.subnetId,detailUpd' +
                    'ate:function(){a.bufferReload()}})},backList:function(){this.$set("searchKey",""' +
                    '),n.$set("keyword",void 0)}});o.beeBind(n,N,"regionId"),w.beeBind(u,n,"list.leng' +
                    'th<=1","noVpc"),w.beeBind(t[y],n,"totalNum","totalNum"),w.beeBind(n,n,"keyword |' +
                    '| !unVpcId ? 0 : totalNum","knownCount"),w.beeBind(n,n,"knowCount>=MAX_SUBNET_PE' +
                    'R_VPC_NUM","reachMax"),w.beeBind(n,n,"noVpc || reachMax","addDisabled"),w.beeBin' +
                    'd(n,n,'noVpc && "无可用私有网络" || reachMax && "私有网络子网创建数达到上限" || ""',"addDisabledText' +
                    '");var k=s.findWidgetByName("add");n.$watch("[addDisabled, addDisabledText]",fun' +
                    'ction(){k.$set("disabled",n.addDisabled),k.$set("attr",{"data-title":n.addDisabl' +
                    'edText})},!0),g()},getOneVpcId:function(){var e=this,t=e.bee,i=t.$refs.vpcSelect' +
                    ';return t.unVpcId||i.list&&i.list.length&&i.list[1].unVpcId},onActionAdd:functio' +
                    'n(){var e,t=this,i=t.bee,n=i.regionId;e=new u({regionId:n,unVpcId:i.unVpcId}),g.' +
                    'create(e.$el,"940","",{title:"创建子网",preventResubmit:!0,button:{"创建":function(i){' +
                    'if(!e.validate())return void i.removeClass("btn_unclick");var n=e.subnetList.map' +
                    '(function(e){return{subnetName:e.subnetName,cidrBlock:e.subnetNet+"/"+e.subnetMa' +
                    'sk,zoneId:e.zoneId,routeTableId:e.unRouteTableId}}),a=e.regionId,s=e.unVpcId;g.f' +
                    'reeze(!0).toggleBtnLoading(!0),c.request(a,"CreateSubnet",{vpcId:s,subnetSet:n},' +
                    '"创建子网失败！").then(function(i){I.success("创建子网成功"),g.hide(),d.resetNamespace("subne' +
                    't",a,s),d.resetNamespace("subnet",a,e.vpcId),t[y].bufferReload()},function(){g.f' +
                    'reeze(!1).toggleBtnLoading(!1),i.removeClass("btn_unclick")})}}})},onActionFilte' +
                    'r:function(e){var t=e.result,i=t.zone,n=i.selected.join("|");this.setParam("zone' +
                    'Ids",n)},onActionColumns:function(){this[y].showColumnsWindow(),v.click("vpc.sub' +
                    'net.configure")},onActionSearch:function(e){this.setParam("keyword",e.keyword)},' +
                    'onDestroy:function(){this._destroy(y,x,C,T)}})}),define("modules/vpc/subnet/subn' +
                    'etResource",function(e,t,i){var n=e("models/api"),a=(e("manager"),e("appUtil"),f' +
                    'unction(e){return n.request({serviceType:"cvm",cmd:"DescribeInstanceMainInfo",da' +
                    'ta:{vpcId:e.vpcId,subnetIds:[e.subnetId]}}).then(function(e){return e.data.total' +
                    'Num})}),s=function(e){return n.request({serviceType:"cdb",cmd:"GetCdbInstanceNum' +
                    'ByVpcSubnetId",data:{vpcId:e.vpcId,subnetIds:[e.subnetId]}}).then(function(e){re' +
                    'turn e.data.total})},d=function(e){return n.request({serviceType:"cmem",cmd:"Des' +
                    'cribeCmem",data:{vpcId:e.vpcId,subnetId:e.subnetId}}).then(function(e){return e.' +
                    'totalCount})},o=function(e){return n.request({serviceType:"tdsql",cmd:"CdbTdsqlG' +
                    'etInstanceCount",data:{vpcId:e.vpcId}}).then(function(e){return e.data.totalCoun' +
                    't})},c=function(e){return n.request({serviceType:"sqlserver",cmd:"SqlserverGetIn' +
                    'stanceList",data:{vpcId:e.vpcId}}).then(function(e){return e.data.totalCount||0}' +
                    ')},r=function(e){return n.request({serviceType:"postgres",cmd:"PostgresGetInstan' +
                    'ceList",data:{vpcId:e.vpcId}}).then(function(e){return e.data.totalCount})},l=[{' +
                    'label:"基础云资源",list:[{label:"云主机",link:"/cvm?rid={{regionId}}&vpcId={{item.vpcId}' +
                    '}&subnetIds={{item.subnetId}}",add:"https://buy.qcloud.com/cvm?regionId={{region' +
                    'Id}}&vpcId={{item.vpcId}}&subnetId={{item.subnetId}}&zoneId={{item.zoneId}}",fet' +
                    'ch:a}]},{label:"数据库",list:[{label:"MySQL",fetch:s,link:"/cdb?regionId={{regionId' +
                    '}}&vpcId={{item.vpcId}}&unVpcId={{item.unVpcId}}&subnetId={{item.subnetId}}&zone' +
                    'Id={{item.zoneId}}",add:"//buy.qcloud.com/cdb?regionId={{regionId}}&vpcId={{item' +
                    '.vpcId}}&unVpcId={{item.unVpcId}}&subnetId={{item.subnetId}}&zoneId={{item.zoneI' +
                    'd}}"},{label:"SQL Server",fetch:c,link:"/sqlserver?regionId={{regionId}}&vpcId={' +
                    '{item.vpcId}}&subnetId={{item.subnetId}}&zoneId={{item.zoneId}}",add:"https://bu' +
                    'y.qcloud.com/sqlserver?regionId={{regionId}}&vpcId={{item.vpcId}}&unVpcId={{item' +
                    '.unVpcId}}&subnetId={{item.subnetId}}&zoneId={{item.zoneId}}"},{label:"TDSQL",fe' +
                    'tch:o,link:"/tdsql?regionId={{regionId}}&vpcId={{item.vpcId}}&subnetId={{item.su' +
                    'bnetId}}&zoneId={{item.zoneId}}",add:"https://buy.qcloud.com/tdsql?regionId={{re' +
                    'gionId}}&vpcId={{item.vpcId}}&unVpcId={{item.unVpcId}}&subnetId={{item.subnetId}' +
                    '}&zoneId={{item.zoneId}}"},{label:"PostgreSQL",fetch:r,link:"/pgsql?regionId={{r' +
                    'egionId}}&vpcId={{item.vpcId}}&subnetId={{item.subnetId}}&zoneId={{item.zoneId}}' +
                    '",add:"/pgsql?regionId={{regionId}}&vpcId={{item.vpcId}}&subnetId={{item.subnetI' +
                    'd}}&zoneId={{item.zoneId}}"},{label:"云缓存Memcached",fetch:d,link:"/cmem?regionId=' +
                    '{{regionId}}&vpcId={{item.vpcId}}",add:"http://manage.qcloud.com/shoppingcart/sh' +
                    'op.php?tab=cmem&regionId={{regionId}}&vpcId={{item.vpcId}}&unVpcId={{item.unVpcI' +
                    'd}}&subnetId={{item.subnetId}}&zoneId={{item.zoneId}}"}]}];i.exports=l}),define(' +
                    '"modules/vpc/subnet/validator",function(e,t,i){var n=e("../validator"),a=e("appU' +
                    'til");i.exports=n.extend({subnetName:function(e,t){if(t){if(e in t)return"子网名称重复' +
                    '";t[e]=!0}return this._validName(e,"子网名称")},zoneId:function(e){return e?void 0:"' +
                    '必须选择一个可用区"},cidr:function(e,t){if(!e)return"子网网段不合法";var i=t,n=e.split("/");if(!' +
                    'a.isValidIp(n[0])||!a.isValidCIDR(n[0],n[1]))return"子网网段不合法";if(i){var s=a.getIp' +
                    'Zone(n[0],n[1]);return i.isContain(s)?void i.sub(s):"子网网段有重叠或者超出VPC网段"}},subnetN' +
                    'et:function(e,t,i){return a.isValidIp(e)&&a.isValidCIDR(e,i.subnetMask)?void 0:"' +
                    '子网网段不合法"},subnetMask:function(e,t,i){return e=parseInt(e,10)||0,16>e||e>28?"子网网段' +
                    '掩码必须在16和28之间":e<i.vpcMask?"子网网段掩码必须大于所属私有网络网段掩码":void 0}})}),define("modules/vpc' +
                    '/topo/GridTip",function(e,t,i){var n=e("./Tip"),a=e("../Grid"),s=e("appUtil"),d=' +
                    '"_dismissTimer",o="_loading",c=n.extend({$tpl:'    <div class="tc-15-bubble tc-1' +
                    '5-bubble-top routing-layer" style="position: absolute;"         b-on="{mouseente' +
                    'r:mouseenter, mouseleave: mouseleave}">        <div class="tc-15-bubble-inner" s' +
                    'tyle="left:10%">            <div class="routing-head">                <strong><s' +
                    'pan b-content="title"></span></strong>                <div class="fr">          ' +
                    '          <span b-if="showTime">更新时间：{{formatTime(time)}}</span>                ' +
                    '    <a b-if="refreshable" class="btn-refresh"  href="#" b-on="{ click : doRefres' +
                    'h }"></a>                </div>            </div>            <div class="routing' +
                    '-cont" b-style="{width:width}">                <span b-tag="grid" b-ref="grid"  ' +
                    '                    b-with="{ colums: columns, list: items, loadData : loadGridD' +
                    'ata, getCellContent : getCellContent , meta:meta}"                        ></spa' +
                    'n>            </div>        </div>    </div>',$beforeInit:function(){var e=this;' +
                    'e.loadGridData=function(){var t=$.Deferred();return e.loadData(this.forceReload)' +
                    '.done(function(i){e.$set("time",i.time||(new Date).getTime()),t.resolve(e.pickDa' +
                    'ta(i))}).fail(t.reject),t},n.prototype.$beforeInit.apply(e,arguments)},formatTim' +
                    'e:function(e){return e?s.formatDate(new Date(e),"yyyy-MM-dd hh:mm:ss"):""},doRef' +
                    'resh:function(e){e.preventDefault(),this.bufferReload(!0)},bufferReload:function' +
                    '(e){var t=this.$refs.grid;t.$set("forceReload",!!e),t.bufferReload(null,100)},pr' +
                    'eLoadData:function(){this.$refs.grid.getData()},clearDismiss:function(){var e=th' +
                    'is;e[d]&&(clearTimeout(e[d]),e[d]=null)},delayDismiss:function(){var e=this;e.cl' +
                    'earDismiss(),e[d]=setTimeout(function(){e[d]=null,e.dismiss()},e.dismissDelay)},' +
                    'dismiss:function(){var e=this;e.focused||$(e.$el).hide()},loadData:function(){re' +
                    'turn $.Deferred().resolve([])}}),r=a.extend({getData:function(){var e,t=this,i=!' +
                    '0,n="_dismissed",a=t[o];a&&(a[n]=!0),e=t[o]=t.loadData(),e.done(function(a){i=!1' +
                    ',e[n]||($.isArray(a)&&(a={list:a}),a.forceReload=!1,t.setData(a))}),i&&(t.showLo' +
                    'ading("加载中..."),e.always(function(){e[n]||t.hideLoading()}))}},{defaults:$.exten' +
                    'd({},a.prototype.defaults,{showPagination:!1,showState:!1,minHeight:150,height:1' +
                    '50})});c.tag("grid",r),i.exports=c}),define("modules/vpc/topo/renderer",function' +
                    '(e,t,i){function n(e,t){var i,n,a,s,d=this,o=e.refs||{},c=e.x||0,l=e.y||0,u=e.ty' +
                    'pe||"lb",v=e.count;i=t?o.rect:o.rect=d.rect(c,l,p.WIDTH,p.HEIGHT,p.RADIUS).attr(' +
                    '{stroke:r.STROKE,fill:r.FILL,"stroke-width":r.STROKE_WIDTH});var f=p.IMG,m=r.IMG' +
                    '_URL+u+".png",h=r.IMG_URL+u+"_on.png";n=t?o.icon:o.icon=d.image(m,c+f.X,l+f.Y,f.' +
                    'WIDTH,f.HEIGHT);var b=p.CROSS;a=t?o.cross:o.cross=d.path(["M",c+b.X,l+b.Y,"l",b.' +
                    'WIDTH,b.HEIGHT,"m",0,-b.HEIGHT,"l",-b.WIDTH,b.HEIGHT].join(",")).attr({stroke:b.' +
                    'STROKE,"stroke-width":b.STROKE_WIDTH});var g=p.TEXT,I="number"!=typeof v?"-":""+' +
                    'v;t?(s=o.text,s.attr("text",I)):s=o.text=d.text(c+g.X,l+g.Y,I),s.attr({"font-siz' +
                    'e":"14px","font-family":r.FONT_FAMILY,fill:v?g.FILL:g.FILL_NULL,"font-weight":v?' +
                    '"bold":"normal"});var w=d.set(i,n,a,s).attr({cursor:v?"pointer":"default"});retu' +
                    'rn s.data("count",v),t||w.hover(function(){var e=s.data("count");e>0&&(i.attr({s' +
                    'troke:r.HOVER_STROKE,fill:r.HOVER_FILL}),a.attr({stroke:r.HOVER_STROKE}),n.attr(' +
                    '"src",h))},function(){var e=o.text.data("count");e>0&&(i.attr({stroke:r.STROKE,f' +
                    'ill:r.FILL}),a.attr({stroke:b.STROKE}),n.attr("src",m))}),w}function a(e){var t,' +
                    'i,n,a,s=this,d=e.x,o=e.y,c=e.element,l=e.noDot,p=e.side||"tb",u=c.getBBox(),f=l?' +
                    '0:v.SPACING,m=u.x+u.width/2,h=u.y+u.height/2;"tb"===p?(n="V",a="H",t=m,i=h>o?u.y' +
                    '-f:u.y2+f):(n="H",a="V",t=d>m?u.x2+f:u.x-f,i=h),s.path(["M",t,i,n,"H"===n?d:o,a,' +
                    '"H"===a?d:o]).attr({stroke:r.LINE_STROKE,"stroke-width":r.LINE_STROKE_WIDTH}),l|' +
                    '|s.circle(t,i,v.RADIUS).attr({"stroke-width":0,fill:r.LINE_STROKE})}function s(e' +
                    ',t,i){var n=this,s=e.getBBox(),d=t.getBBox(),o=s.x+s.width/2,c=s.y+s.height/2,r=' +
                    'd.x+d.width/2,l=d.y+d.height/2,p=(o+r)/2,u=(c+l)/2;a.call(n,{element:e,x:p,y:u,s' +
                    'ide:i}),a.call(n,{element:t,x:p,y:u,side:i})}function d(e){var t=this,i=e.x,n=e.' +
                    'y,a=e.solid,s=e.noTip,d=e.type,o=e.title,c=u.SPLITTER,l=u.IMG,p=u.TEXT,v=u.RADIU' +
                    'S,f=u.WIDTH,m=u.HEIGHT,h=c.V_INDENT,b=t.rect(i,n,f,m,v).attr({stroke:r.STROKE,fi' +
                    'll:a?l.FILL:u.FILL,"stroke-width":r.STROKE_WIDTH}),g=r.STROKE_WIDTH/2,I=c.STROKE' +
                    '_WIDTH/2,w=a?null:t.path(["M",i+g,n+h-I,"v",-(h-v-g),"a",v-g,v-g,0,0,1,v-g,-(v-g' +
                    '),"h",f-2*v,"a",v-g,v-g,0,0,1,v-g,v-g,"v",h-v-I,"z"].join(",")).attr({fill:l.FIL' +
                    'L,"stroke-width":0}),y=r.IMG_URL+"entry_"+d+".png",x=r.IMG_URL+"entry_"+d+"_on.p' +
                    'ng",$=t.image(y,i+l.INDENT,n+l.V_INDENT,l.WIDTH,l.HEIGHT),C=a?null:t.path(["M",i' +
                    ',n+h,"h",u.WIDTH].join(",")).attr({stroke:r.STROKE,"stroke-width":c.STROKE_WIDTH' +
                    '}),T=t.set(b,C),N=t.text(i+p.INDENT,n+(a?p.V_INDENT_SOLID:p.V_INDENT),o).attr({"' +
                    'font-size":p.FONT_SIZE,"font-family":r.FONT_FAMILY,fill:a?p.FONT_FILL_SOLID:p.FO' +
                    'NT_FILL,"font-weight":"bold"}),k=t.set(b,w,$,C,N);return a||s||k.attr({cursor:"p' +
                    'ointer"}).hover(function(){T.attr("stroke",r.HOVER_STROKE),$.attr("src",x),w.att' +
                    'r("fill",l.HOVER_FILL)},function(){T.attr("stroke",r.STROKE),$.attr("src",y),w.a' +
                    'ttr("fill",l.FILL)}),k}function o(e){var t=$();return e.relateDom=function(e){t.' +
                    'add(e)},e.destroy=function(){return t.remove(),e.remove()},e}var c=e("lib/raphae' +
                    'l"),r={IMG_URL:"//imgcache.qq.com/qcloud/app/resource/images/vpctopo/",STROKE:"#' +
                    'c7cfdc",STROKE_WIDTH:2,LINE_STROKE:"#ffc633",LINE_STROKE_WIDTH:2,HOVER_STROKE:"#' +
                    '0070cc",FILL:"#fafbff",HOVER_FILL:"#fff",FONT_FILL:"#0071cc",FONT_FAMILY:'"Hirag' +
                    'ino Sans GB","Tahoma","microsoft yahei ui","microsoft yahei","simsun"',CONNECTOR' +
                    ':{RADIUS:2,SPACING:7},ROUTER:{V_INDENT:120,WIDTH:40,HEIGHT:40,IMG_SRC:"router.pn' +
                    'g"},VPN_CONN:{WIDTH:29,HEIGHT:29},OUTER:{WIDTH:200},ENTRY:{INDENT:20,V_INDENT:20' +
                    ',WIDTH:100,HEIGHT:100,SPACING:65,V_SPACING:20,RADIUS:5,FILL:"#fff",IMG:{WIDTH:40' +
                    ',HEIGHT:40,INDENT:29,V_INDENT:18,FILL:"#fafbff",HOVER_FILL:"#fff"},SPLITTER:{V_I' +
                    'NDENT:73.5,STROKE_WIDTH:1},TEXT:{INDENT:50,V_INDENT:87,V_INDENT_SOLID:80,FONT_SI' +
                    'ZE:"14px",FONT_FILL:"#0071cc",FONT_FILL_SOLID:"#727f9e"}},SUBNET:{HEIGHT:120,IND' +
                    'ENT:0,V_INDENT:0,LINE:{V_INDENT:50},TEXT:{INDENT:100,V_INDENT:35,SIZE:"12px",FIL' +
                    'L:"#000",CIDR_FILL:"#888"},RESOURCE:{INDENT:100,SPACING:30,V_INDENT:80,WIDTH:80,' +
                    'HEIGHT:30,RADIUS:5,TEXT:{FILL:"#0070cc",FILL_NULL:"#a2a2a2",X:60,Y:14},CROSS:{WI' +
                    'DTH:7,HEIGHT:8,STROKE:"#727f9e",STROKE_WIDTH:1.5,X:37,Y:11},IMG:{WIDTH:15,HEIGHT' +
                    ':15,X:14,Y:7}},APPENDIX:{WIDTH:30,HEIGHT:30,INDENT:10,SPACING:10}}},l=r.SUBNET,p' +
                    '=l.RESOURCE,u=r.ENTRY,v=r.CONNECTOR,f=r.ROUTER,m=r.VPN_CONN,h={cdb:"云数据库",cmem:"' +
                    'NoSQL",lb:"内网负载均衡",cvm:"云主机"};l.INDENT=u.WIDTH/2+u.SPACING+f.WIDTH+u.SPACING/2-2' +
                    ';var b=function(){var e={cvm:"vpcDevices",lb:"lbNum",cdb:"cdbNum",cmem:"cmemNum"' +
                    '},t=Object.keys(e),i=r.SUBNET,s=i.APPENDIX,d=i.LINE,o=i.TEXT;return function(i,c' +
                    '){var l=this,u=i.refs||{},v=i.x,f=i.y,m=i.data,b=i.regHover;if(t.forEach(functio' +
                    'n(t,i){var s=v+p.INDENT+(p.WIDTH+p.SPACING)*i,o=f+p.V_INDENT;c||(u[t]={});var r=' +
                    'n.call(l,{x:s,y:o,type:t,count:m[e[t]],refs:u[t]},c);c||(b(r,{isTitle:!0,title:h' +
                    '[t]}),a.call(l,{x:v,y:f+d.V_INDENT,element:r,side:"tb"}))}),!c){var g=$("<span><' +
                    '/span>"),I=$("<span></span>"),w=$("<div></div>").append(g,I);g.text(m.subnetName' +
                    '),I.text("("+m.subnetCIDR+")").css("color",o.CIDR_FILL),w.css({"font-size":o.SIZ' +
                    'E,position:"absolute",left:v+o.INDENT,top:f+o.V_INDENT-parseInt(o.SIZE,10)/2}).a' +
                    'ppendTo($(l.canvas).parent()),l.relateDom(w);var y=[];m.rtbId&&y.push({type:"rou' +
                    'teTable",rtbId:m.rtbId,rtbName:m.rtbName,unRouteTableId:m.uniqRtbId}),y.forEach(' +
                    'function(e,t){var i=v+s.INDENT+(s.SPACING+s.WIDTH)*t,n=f+d.V_INDENT-s.HEIGHT/2,a' +
                    '=l.image(r.IMG_URL+e.type+".png",i,n,s.WIDTH,s.HEIGHT);b(a,e)})}}}(),g=function(' +
                    'e){return Math.max(e,1)*l.HEIGHT};i.exports={renderOuter:function(e,t,i){var n,p' +
                    '=0,v=0,h=p+u.INDENT+2*(u.WIDTH+u.SPACING)+f.WIDTH+u.SPACING/2,b=v+u.V_INDENT+4*(' +
                    'u.HEIGHT+u.V_SPACING),I=o(new c(e[0],h,b)),w=(t.subnetList||[]).length,y=l.V_IND' +
                    'ENT+g(w),x={},$=[{type:"gw",dataKey:"gwList",title:"公网网关"},{type:"lb",dataKey:"l' +
                    'bList",title:"公网LB"},{type:"vpnGw",dataKey:"vpnGwList",title:"VPN网关"},{type:"pri' +
                    'vateGw",dataKey:"privateGwList",title:"专线网关"}];$=$.filter(function(e){var i=t[e.' +
                    'dataKey];return i&&i.length}),n=2*u.V_INDENT+$.length*(u.HEIGHT+u.V_SPACING)-u.V' +
                    '_SPACING;var C=p+(u.INDENT+2*(u.WIDTH+u.SPACING)),T=v+Math.min($.length?n/2:200,' +
                    'y-20),N=(p+u.INDENT+2*u.WIDTH+u.SPACING+C)/2,k=T,P=x.router=I.image(r.IMG_URL+"r' +
                    'outer.png",C,T-f.HEIGHT/2,f.WIDTH,f.HEIGHT);if(a.call(I,{element:P,x:C+f.WIDTH+u' +
                    '.SPACING/2,y:T,side:"lr",noDot:!0}),$.length&&a.call(I,{element:P,x:N,y:k,side:"' +
                    'lr",noDot:!0}),$.forEach(function(e,t){var n=p+u.INDENT+u.WIDTH+u.SPACING,s=v+u.' +
                    'V_INDENT+(u.HEIGHT+u.V_SPACING)*t,o=x[e.type]=d.call(I,{x:n,y:s,type:e.type,noTi' +
                    'p:"lb"===e.type,title:e.title});i(o,{type:e.type}),a.call(I,{element:o,x:N,y:k,s' +
                    'ide:"lr"})}),x.gw||x.lb){var D=I.set(x.gw,x.lb).getBBox(),_=p+u.INDENT+u.WIDTH+u' +
                    '.SPACING/2,E=D.y+D.height/2,R=p+u.INDENT,V=E-u.HEIGHT/2,S=d.call(I,{x:R,y:V,soli' +
                    'd:!0,type:"internet",title:"INTERNET"});a.call(I,{element:S,x:_,y:E,side:"lr"}),' +
                    '[x.gw,x.lb].forEach(function(e){e&&a.call(I,{element:e,x:_,y:E,side:"lr"})})}if(' +
                    'x.vpnGw&&t.vpnConnList&&t.vpnConnList.length){var A=x.vpnGw.getBBox(),L=p+u.INDE' +
                    'NT,G=A.y,M=x.userGw=d.call(I,{x:L,y:G,type:"userGw",title:"对端网关"});s.call(I,x.us' +
                    'erGw,x.vpnGw,"lr");var B=I.image(r.IMG_URL+"vpnConn.png",Math.round((L+A.x2-m.WI' +
                    'DTH)/2),Math.round(G+u.HEIGHT/2-m.HEIGHT/2),m.WIDTH,m.HEIGHT);i(B,{type:"vpnConn' +
                    '"}),i(M,{type:"userGw",dummy:{type:"vpnConn",st:B}})}if(x.privateGw){var q=x.pri' +
                    'vateGw.getBBox(),U=p+u.INDENT,F=q.y;x.idc=d.call(I,{x:U,y:F,type:"idc",title:"ID' +
                    'C机房",regHover:i}),s.call(I,x.idc,x.privateGw,"lr")}return I},renderInner:functio' +
                    'n(e,t,i,n){var a=0,s=0,d="topo-inner",u=t.subnetList.length,v=l.HEIGHT,f=g(u),m=' +
                    'a+l.INDENT+p.INDENT+4*(p.WIDTH+p.SPACING),h=s+l.V_INDENT+f,I=n?e.data(d):o(new c' +
                    '(e[0],m,h)),w=I.refs||(I.refs={});return e.data(d,I),n||(w.mainLine=I.path(["M",' +
                    'a+l.INDENT+r.LINE_STROKE_WIDTH/2,s+l.LINE.V_INDENT,"v",f].join(",")).attr({strok' +
                    'e:r.LINE_STROKE,"stroke-width":r.LINE_STROKE_WIDTH})),w.subnetList=w.subnetList|' +
                    '|{},(t.subnetList||[]).forEach(function(e,t){var d=a+l.INDENT,o=s+v*t;n||(w.subn' +
                    'etList[t]={}),b.call(I,{x:d,y:o,data:e,regHover:i,refs:w.subnetList[t]},n)}),I}}' +
                    '}),define("modules/vpc/topo/Tip",function(e,t,i){var n=e("../BeeBase"),a="_dismi' +
                    'ssTimer",s=n.extend({$tpl:'    <div class="tc-15-bubble" b-on="{mouseenter:mouse' +
                    'enter, mouseleave: mouseleave}" style="overflow: hidden; max-width: 360px; z-ind' +
                    'ex: 9999;display:none;" b-style="{width:width}">        <div class="tc-15-bubble' +
                    '-inner">{{title}}</div>    </div>',mouseenter:function(){var e=this;e.focused=!0' +
                    ',e.clearDismiss()},mouseleave:function(){this.focused=!1,this.delayDismiss()},cl' +
                    'earDismiss:function(){var e=this;e[a]&&(clearTimeout(e[a]),e[a]=null)},delayDism' +
                    'iss:function(){var e=this;e.clearDismiss(),e[a]=setTimeout(function(){e[a]=null,' +
                    'e.dismiss()},e.dismissDelay)},dismiss:function(){var e=this;e.focused||$(e.$el).' +
                    'hide()}},{defaults:$.extend({},n.prototype.defaults,{dismissDelay:500})});i.expo' +
                    'rts=s}),define("modules/vpc/topo/tipsConfig",function(e,t,i){var n=e("../cacher"' +
                    '),a=e("constants"),s=e("../route/util"),d=function(e){return"number"==typeof e&&' +
                    'isFinite(e)},o=function(e){return d(e)?{text:""+Math.round(e)+"%",cls:e>=80?"war' +
                    'ning":""}:"-"},c=function(e){return""+e+"Mbps"};i.exports={routeTable:{title:"关联' +
                    '路由表：{{meta.rtbName}}",showTime:!1,refreshable:!1,columns:[{key:"dest",name:"目的端"' +
                    '},{key:"nextType",name:"下一跳类型",renderer:function(e){return s.typeMap[e].name}},{' +
                    'key:"nextHop",name:"下一跳",tdTpl:'<vpc-route-rule-value types-data="{{typesData}}"' +
                    '	next-type="{{item.nextType}}" b-model="item.nextHop"	options="{{meta}}"	editing' +
                    '=""></vpc-route-rule-value>'}],loadData:function(e){var t,i,a=this.meta,d=$.Defe' +
                    'rred();return $.when(s.getExtraData(a.regionId,a.vpcId,a.unRouteTableId).done(fu' +
                    'nction(e){t=e}),n[(e?"getFresh":"get")+"VPCRouteTable"](a.regionId,a.vpcId,0,a.r' +
                    'tbId).done(function(e){i=e.routeList.map(function(e){return{dest:e.dest,nextType' +
                    ':e.nextType,nextHop:e.uniqNextHop||e.nextHop}})})).then(function(){t.list=i,d.re' +
                    'solve(t)},d.reject),d}},gw:{title:"公网网关",columns:[{key:"alias",name:"主机名"},{key:' +
                    '"wanIp",name:"公网IP"},{key:"percent",name:"网络使用率",width:100,renderer:o},{key:"ban' +
                    'dwidth",name:"带宽上限",width:80,minWidth:80,renderer:c}],loadData:function(e){var t' +
                    '=this.meta;return n[(e?"getFresh":"get")+"GwStatusList"](t.regionId,t.vpcId)},pi' +
                    'ckData:function(e){return e.list}},vpnGw:{title:"VPN网关信息",columns:[{key:"vpnGwNa' +
                    'me",name:"VPN网关"},{key:"percent",name:"网络使用率",width:100,renderer:o},{key:"bandwi' +
                    'dth",name:"带宽上限",width:80,minWidth:80,renderer:c}],loadData:function(e){var t=th' +
                    'is.meta;return n[(e?"getFresh":"get")+"VpnGwStatusList"](t.regionId,t.vpcId)},pi' +
                    'ckData:function(e){return e.list}},vpnConn:{title:"VPN通道信息",width:550,columns:[{' +
                    'key:"usrGwName",name:"对端网关",width:150},{key:"vpnConnId",name:"通道ID",width:80,min' +
                    'Width:80},{key:"netStatus",name:"通道状态",width:80,minWidth:80,renderer:function(e,' +
                    't){var i,n;if(t.state===a.CONNECT_STATUS.RUNNING)i="available"===e?"已联通":"未联通","' +
                    'available"===e?(i="已联通",n="succeed"):(i="未联通",n="warning");else switch(t.state){' +
                    'case a.VPN_STATUS.WAIT_PAY:i="创建中";break;case a.VPN_STATUS.PAY_ERROR:i="创建出错",n=' +
                    '"error";break;case a.VPN_STATUS.DELIVER_ING:i="更新中";break;case a.VPN_STATUS.DELI' +
                    'VER_ERROR:i="更新出错",
n="error";break;case a.VPN_STATUS.DESTROY_ING:i="销毁中";break;' +
                    'case a.VPN_STATUS.DESTROY_ERROR:i="销毁错误",n="error";break;case a.VPN_STATUS.RUNNI' +
                    'NG:i="运行中",n="succeed";break;default:i="未知"}return{text:i,cls:n}}},{key:"outTraf' +
                    'fic",name:"出带宽",width:80,minWidth:80,renderer:function(e,t){var i,n;return e>=0?' +
                    '(i=parseFloat((e/1024/1024).toFixed(1))+"Mbps",t.percent>=80&&(n="warning")):i="' +
                    '-",{text:i,cls:n}}}],loadData:function(e){var t=this.meta;return n[(e?"getFresh"' +
                    ':"get")+"VpnConnStatusList"](t.regionId,t.vpcId)},pickData:function(e){return e.' +
                    'list}}}}),define("modules/vpc/topo/topo",function(e,t,i){e("widget/region/region' +
                    'Selector");var n=e("../BeeBase"),a=e("../vpc/Selector"),s=e("../moduleBase"),d=e' +
                    '("../cacher"),o=e("util"),c=e("manager"),r=e("./renderer"),l=e("./GridTip"),p=e(' +
                    '"./Tip"),u="_tip",v="_titleTip",f="_hoverCacher",m="_inner",h="_outer",b="_resiz' +
                    'eHandler",g={1:"广州",4:"上海",6:"北美",8:"北京"},I=n.extend({$tpl:'    <div class="vpc-' +
                    'topo {{orphan ? \'orphan\' : \'\'}}">        <div class="manage-area-title">    ' +
                    '        <h2>网络拓扑图</h2>            <span b-tag="region-selector" b-ref="regionSel' +
                    'ect" b-model="regionId" b-with="{getWhiteList:getWhiteList}"></span>            ' +
                    '<span b-tag="vpc-select" b-ref="vpcSelect" region-id="{{regionId}}" has-all="" b' +
                    '-model="vpcId"></span>        </div>        <div class="network-topology-cont"> ' +
                    '           <div class="subnet-box" b-style="{display: vpc ? \'\' : \'none\', hei' +
                    'ght: boxHeight ? boxHeight+\'px\' : \'\', minHeight: boxHeight ? \'500px\' : \'' +
                    '\'}">                <div class="subnet-box-head">                    <span clas' +
                    's="tit"><strong>{{vpc && vpc.vpcName}}</strong></span>                    <i cla' +
                    'ss="separate"></i>                    <span class="tit"><em>CIDR</em><strong>{{v' +
                    'pc && vpc.vpcCIDR}}</strong></span>                    <span class="tit sum">共<s' +
                    'trong>{{vpc && vpc.subnetList.length}}</strong>个子网</span>                </div> ' +
                    '               <div class="subnet-box-cont" data-mod="inner">                </d' +
                    'iv>            </div>            <div data-mod="outer" class="vpc-topo-outer" b-' +
                    'style="{display: vpc ? \'\' : \'none\'}"></div>        </div>    </div>'});I.tag' +
                    '("vpc-select",a),o.insertStyle("    .vpc-topo{        position: relative;    }  ' +
                    '  .vpc-topo .network-topology-cont{    position: relative;    }    .vpc-topo .su' +
                    'bnet-box{    margin-left: 235px;    position: relative;    }    .vpc-topo .vpc-t' +
                    'opo-outer{    position: absolute;    top: 64px;    }    .vpc-topo .subnet-box .s' +
                    'ubnet-box-cont{    position: absolute;    top: 34px;    left: 0;    bottom: 0;  ' +
                    '  right: 0;    overflow-y: auto;    overflow-x: hidden;    }    .vpc-topo.orphan' +
                    ' .subnet-box{    }    .vpc-topo.orphan .vpc-topo-outer{    }");var w=function(e,' +
                    't){var i,n,a,s=this,d=function(){a&&clearTimeout(a),setTimeout(function(){n!==i&' +
                    '&(i&&s.leave(i),n&&s.hover(n),i=n)},100)};s.bufferHover=function(e){n=e,d()},s.b' +
                    'ufferLeave=function(e){n=null,d()},s.destroy=function(){a&&clearTimeout(a),s.hov' +
                    'er=s.leave=$.noop},s.hover=e,s.leave=t};i.exports=s.extend({routeBase:"/vpc/topo' +
                    '",params:$.extend({keyword:""},s.params),Bee:I,getBeeConfig:function(){var e=thi' +
                    's,t=s.getBeeConfig.apply(this,arguments),i=function(){e.handleAction(this.name,t' +
                    'his)};return $.extend(t.$data,{actionHandler:i}),$.extend(t,{addDisabled:!0,getW' +
                    'hiteList:function(){return d.getRegionIdList()}})},onRender:function(){var t=thi' +
                    's,i=t.bee,n=t.$el,a=n.find(".subnet-box"),s=n.find(".manage-area-title"),d=n.fin' +
                    'd(".network-topology-cont");e("../adTip").init(t.$el),t.$inner=n.find('[data-mod' +
                    '="inner"]'),t.$outer=n.find('[data-mod="outer"]');var o=function(){i.$set("vpc",' +
                    'null),t.loadData()};i.$watch("regionId",function(){t.setParam({vpcId:void 0}),o(' +
                    ')}),["vpcId"].forEach(function(e){i.$watch(e,o)});var c=s.outerHeight(!0),r=d.ou' +
                    'terHeight(!0)-d.height(),l=a.outerHeight(!0)-a.height(),p=function(){var e=n.par' +
                    'ent().height();i.$set("boxHeight",e-c-r-l)};t[b]={destroy:function(){$(window).o' +
                    'ff("resize",p)}},$(window).on("resize",p),p(),o()},loadData:function(){var e=thi' +
                    's,t=e.bee,i=t.regionId,n=t.vpcId,a=$.Deferred();n&&i&&!e.loading&&(e.loading=a,a' +
                    '.always(function(){e.loading=null}),c.getVpcTopo(i,n,a.resolve,a.reject),a.done(' +
                    'function(a){var s=g[i];t.$set("vpc",a),e.lastData=a;var d;e._destroy(f,m,h),d=e[' +
                    'f]=new w(function(t){var i=t.st,n=t.meta,a=t.type,s=e.getTip(),d=e.getTitleTip()' +
                    ',o=n.isTitle,c=o?d:s,r=o?{title:n.title}:$.extend({meta:n},e.hoverTipDefaults,e.' +
                    'hoverTips[a]);c.$set(r),e.showTip(i,o)},function(){e.getTip().delayDismiss(),e.g' +
                    'etTitleTip().delayDismiss()});var o=function(t,s){var o=s.type,c=t,r=s.dummy;if(' +
                    'r&&(o=r.type,c=r.st),s.isTitle||e.hoverTips.hasOwnProperty(o)){s.vpc=a,s.regionI' +
                    'd=i,s.vpcId=n,s.unVpcId=a.unVpcId;var l={type:o,st:c,meta:s};t.hover(function(){' +
                    'd.bufferHover(l)},function(){d.bufferLeave(l)})}};e[m]=r.renderInner(e.$inner.ht' +
                    'ml(""),a,o),e[h]=r.renderOuter(e.$outer.html(""),a,o),s&&c.getVpcTopoExternal(i,' +
                    'n,function(t){a===e.lastData&&(a.subnetList.forEach(function(e){e.cmemNum=t[e.su' +
                    'bnetId]||0}),r.renderInner(e.$inner,a,o,!0))})}))},hoverTipDefaults:{width:420,t' +
                    'itle:"",time:"",showTime:!0,refreshable:!0,columns:[],loading:!0,items:[],loadDa' +
                    'ta:function(){return $.Deferred().resolve([])},pickData:function(e){return e},ge' +
                    'tCellContent:function(e,t,i){var n;return i.renderer&&(n=i.renderer.call(this,e,' +
                    't,i),"string"==typeof n&&(n={text:n}),n)?'<span class="text-overflow '+(n.cls||"' +
                    '")+'">'+n.text+"</span>":void 0}},hoverTips:e("./tipsConfig"),_getTip:function(e' +
                    ',t){var i=this,n=i[e];return n||(n=i[e]=new t,$(n.$el).appendTo(i.$el),i.$inner.' +
                    'on("scroll",function(){n.dismiss()})),n},getTip:function(){return this._getTip(u' +
                    ',l)},getTitleTip:function(){return this._getTip(v,p)},showTip:function(e,t){var ' +
                    'i=this,n=!t,a=e.paper,s=$(a.canvas).parent(),d=e.getBBox(),o=i.getTip(),c=i.getT' +
                    'itleTip(),r=t?c:o,l=r.width||100,p=$(r.$el),u=s.offset(),v={width:d.width,height' +
                    ':d.height,x:u.left+d.x,y:u.top+d.y-s.scrollTop()},f={x:t?0:-.1*l,y:t?1:7};n&&r.p' +
                    'reLoadData(),p.show().offset({left:v.x+(n?v.width/2:0)+f.x,top:v.y+v.height+f.y+' +
                    '10}),n?(o.clearDismiss(),c.dismiss()):(c.clearDismiss(),o.dismiss())},hideTip:fu' +
                    'nction(){this.getTip().dismiss(),this.getTitleTip().dismiss()},onDestroy:functio' +
                    'n(){this._destroy(u,v,f,m,h,b)}})}),define("modules/vpc/userGw/Add",function(e,t' +
                    ',i){var n=e("../BeeBase"),a=e("../vpnGw/DropDown");e("../userGw/DropDown");e("..' +
                    '/Checkbox");var s=n.extend({$tpl:'    <div class="tc-15-list-wrap form" style="p' +
                    'adding:0;">        <form>            <div class="tc-15-list-content">           ' +
                    '     <ul>                    <li class="even"><em class="tc-15-list-tit" >名称</em' +
                    '>                        <span class="tc-15-list-det">                          ' +
                    '  <div class="tc-15-input-text-wrap m">                                <input ty' +
                    'pe="text" class="tc-15-input-text" data-name="userGwName" b-model="userGwName"> ' +
                    '                               <help-tips>自定义的IDC名称</help-tips>                 ' +
                    '           </div>                        </span>                    </li>       ' +
                    '             <li><em class="tc-15-list-tit" >公网IP</em>                        <s' +
                    'pan b-tag="vpc-ip-input" name="userGwAddr" b-ref="userGwAddr" type="ip" b-model=' +
                    '"userGwAddr"></span>                        <help-tips>对端IDC的公网IP</help-tips>   ' +
                    '                 </li>                </ul>            </div>        </form>    ' +
                    '</div>'});s.tag("vpc-vpn-gw-dropdown",a),i.exports=s}),define("modules/vpc/userG' +
                    'w/DropDown",function(e,t,i){var n=e("qccomponent"),a=e("../DropDown"),s=(e("mana' +
                    'ger"),e("../cApi")),d=a.extend({watchFields:["regionId","vpcId","unVpcId","vpnGw' +
                    'Id"],getData:function(e,t){if(!this.regionId||!this.unVpcId)return e([]);var i,n' +
                    ',a,d,o,c=this;i=s.request(c.regionId,"DescribeUserGw",{vpcId:c.unVpcId,offset:0,' +
                    'limit:999},"加载对端网关列表失败！").done(function(e){a=s.translate(e.data,"userGw")}),n=s.' +
                    'request(c.regionId,"DescribeVpnConn",{vpcId:c.unVpcId,offset:0,limit:999},"加载VPN' +
                    '通道列表失败！").done(function(e){d=s.translate(e.data,"vpnConn")});var r=s.DescribeVpc' +
                    'Limit(c.regionId,{type:[7]}).done(function(e){e=e.data,o=e.limit[7]});$.when(i,n' +
                    ',r).done(function(){var t=!1,i={};d.forEach(function(e){i[e.unUserGwId]||(i[e.un' +
                    'UserGwId]={num:0}),i[e.unUserGwId].disabled=e.unVpnGwId==c.vpnGwId,i[e.unUserGwI' +
                    'd].num++});var n=a.map(function(e){var n=i[e.unUserGwId]&&i[e.unUserGwId].num>=o' +
                    ',a="",s=n;return i[e.unUserGwId]&&i[e.unUserGwId].disabled?(a="(通道已建立)",s=!0):n?' +
                    'a="(通道已达到上限)":t=!0,$.extend(e,{text:e.userGwName+a,value:e.unUserGwId,disabled:c' +
                    '.disableLimit?s:!1})});t||n.unshift({text:"",value:""}),e(n)})}},{defaults:{disa' +
                    'bleLimit:!1}});n.tag("vpc-user-gw-select",d),i.exports=d}),define("modules/vpc/u' +
                    'serGw/userGw",function(e,t,i){e("widget/region/regionSelector"),e("../IPInput");' +
                    'var n=e("../BeeBase"),a=e("../moduleBase"),s=e("../cacher"),d=e("../util"),o=e("' +
                    '../cApi"),c=e("appUtil"),r=e("dialog"),l=e("../Grid"),p=e("../RenameBubble"),u=e' +
                    '("../Confirm"),v=e("./Add"),f=e("./validator"),m=e("../createLimitsMixin"),h=e("' +
                    'constants"),b=(e("manager"),"grid"),g="regionSelector",I="addDialog",w=(h.GW_MAX' +
                    '_NUM,n.extend({$tpl:'<div><div><div class="manage-area-title"><h2>对端网关</h2><span' +
                    ' b-tag="region-selector" b-ref="regionSelect" b-model="regionId" b-with="{getWhi' +
                    'teList:getRegionWhiteList}"></span><div class="manage-area-title-right"><a href=' +
                    '"https://www.qcloud.com/doc/product/215/4956" target="_blank"><b class="links-ic' +
                    'on"></b> VPN连接帮助文档</a></div></div><div class="tc-15-action-panel"><div b-tag="qc' +
                    '-action-button" b-ref="create" name="add" label="+新建" attr="{{ {\'data-title\':a' +
                    'ddDisabledText} }}" b-with="{action:actionHandler, disabled:addDisabled}"></div>' +
                    '<div b-tag="qc-action-button" float="right" name="columns" class-name="weak sett' +
                    'ing" b-with="{action:actionHandler}"></div><div b-tag="qc-search" b-ref="search"' +
                    ' float="right" name="search" placeholder="搜索对端网关的关键词" multiline="false" b-with="' +
                    '{search:actionHandler}" keyword="{{keyword}}"></div></div><div data-mod="grid"><' +
                    '/div></div></div>',getRegionWhiteList:function(){return $.Deferred().resolve(h.U' +
                    'SER_GW_REGION)},$mixins:[m({maxUserGwCount:h.VPC_LIMIT.USRGW_PER_OWNER_LIMIT_TYP' +
                    'E})]},{defaults:{maxUserGwCount:h.GW_MAX_NUM}}));i.exports=a.extend({routeBase:"' +
                    '/vpc/userGw",params:$.extend({keyword:""},a.params,{vpcId:"",unVpcId:""}),Bee:w,' +
                    'getBeeConfig:function(){var e=this,t=a.getBeeConfig.apply(this,arguments),i=func' +
                    'tion(){e.handleAction(this.name,this)};return $.extend(t.$data,{actionHandler:i}' +
                    '),$.extend(t,{addDisabled:!0,getWhiteList:function(){return s.getRegionIdList()}' +
                    '})},onRender:function(){var t=this,i=t.$el,n=t.bee;e("../adTip").init(t.$el);var' +
                    ' a=t.bee.$refs,r=a.search,v=function(){t[b]&&t[b].bufferReload()};n.$watch("regi' +
                    'onId",function(){t.setParam({keyword:void 0}),v()}),["keyword","page","count"].f' +
                    'orEach(function(e){n.$watch(e,v)}),d.beeBind(n,r,"keyword"),t[b]=new l({$el:t.ge' +
                    'tModDom(i,b),$data:{count:n.count,page:n.page,cacheKey:"vpc_vpn_usergw_columns_v' +
                    '20151124",colums:[{key:"userGwName",name:"ID/名称",required:!0,tdTpl:'<p><span cla' +
                    'ss="text-overflow fake-link">{{item.unUserGwId}}</span></p><span class="text-ove' +
                    'rflow m-width"><i b-if="item.renaming" class="n-loading-icon"></i>{{item.userGwN' +
                    'ame || "未命名"}}</span>{{{renameIconHtml}}}'},{key:"userGwAddress",name:"公网IP"},{k' +
                    'ey:"vpnConnNum",name:"通道个数",width:100},{key:"_action",name:"操作",minWidth:100,req' +
                    'uired:!0}]},getCellContent:function(e,t,i){var n;switch(i.key){case"_action":n=t' +
                    'his.getActionsCell([{text:"删除",act:"remove",disabled:t.vpnConnNum>0,title:t.vpnC' +
                    'onnNum>0?"无法删除有通道连接的对端网关":""}])}return n},getData:function(e){var t,i=this,a=e.c' +
                    'ount,s=e.page,d=a*(s-1),c=a,r=n.keyword;i.showLoading("加载中..."),t=o.request(n.re' +
                    'gionId,"DescribeUserGw",{offset:d,limit:c,userGwName:r},"加载对端网关列表失败").translate(' +
                    '{totalCount:"totalNum",data:{key:"detail",map:o.translateMap.userGw}}),t.done(fu' +
                    'nction(t){var n=t.detail;n=i.localSort(n,e.orderField,e.order),i.setData({totalN' +
                    'um:t.totalNum,searchKey:r,list:n})}),t.always(function(){i.hideLoading()})},onRo' +
                    'wRename:function(e,t,i){var a=this,d=e.userGwName;new p({value:d,target:$(i.curr' +
                    'entTarget).parent(),validate:function(e){return e.length>25?"对端网关名称不能超过25个字符":c.' +
                    'isValidName(e)?void 0:"对端网关"+h.INVALID_NAME_TIPS},doSave:function(t){return o.re' +
                    'quest(n.regionId,"ModifyUserGw",{userGwId:e.unUserGwId,userGwName:t},"修改对端网关失败！"' +
                    ').done(function(){s.resetNamespace("userGw",n.regionId,e.unUserGwId),s.resetName' +
                    'space("userGw",n.regionId,e.userGwId)})},onSave:function(e,i,n){a.list.$set(t,{u' +
                    'serGwName:i,renaming:!0}),e.done(function(){a.list.$set(t,{renaming:!1})}),e.fai' +
                    'l(function(){a.list.$set(t,{userGwName:n,renaming:!1})})}})},onRowRemove:functio' +
                    'n(e,t,i){var a=this;new u({target:i.currentTarget,box:a.$gridBody,$data:{title:"' +
                    '确定删除该对端网关？"},onConfirm:function(){o.request(n.regionId,"DeleteUserGw",{userGwId:' +
                    'e.unUserGwId},"删除对端网关失败！").done(function(){s.resetNamespace("userGw",n.regionId,' +
                    'e.unUserGwId),s.resetNamespace("userGw",n.regionId,e.userGwId),a.bufferReload()}' +
                    ')}})},backList:function(){this.$set("searchKey",""),n.$set("keyword",void 0)}}),' +
                    'c.beeBind(t[b],n,"totalNum","totalNum"),c.beeBind(n,n,"keyword ? 0 : totalNum","' +
                    'knownCount"),c.beeBind(n,n,"knownCount>=maxUserGwCount","reachMax"),c.beeBind(n,' +
                    'n,"reachMax","addDisabled"),c.beeBind(n,n,'reachMax ? "对端网关创建数达到上限" : ""',"addDi' +
                    'sabledText"),v()},onActionAdd:function(){var e,t=this,i=t.bee,n=i.regionId;e=new' +
                    ' v({regionId:n}),r.create(e.$el,"600","",{"class":"dialog_layer_v2 tc-15-rich-di' +
                    'alog new-gateway",title:"新建对端网关",preventResubmit:!0,button:{"创建":function(i){var' +
                    ' a,d=["userGwName","userGwAddr"];if(e.$refs.userGwAddr.getValue(),a=f.multiValid' +
                    'ate(e,d,{},e.$el,{$btn:i}))return void i.removeClass("btn_unclick");r.freeze(!0)' +
                    '.toggleBtnLoading(!0);var c=$.Deferred().done(function(){r.hide(),s.resetNamespa' +
                    'ce("vpnConn",n,e.unVpcId),s.resetNamespace("vpnConn",n,e.vpcId),t[b].bufferReloa' +
                    'd()}).fail(function(){i.removeClass("btn_unclick")}).always(function(){r.freeze(' +
                    '!1).toggleBtnLoading(!1)});o.request(n,"AddUserGw",{userGwName:e.userGwName,user' +
                    'GwAddr:e.userGwAddr},"创建对端网关失败！").then(function(e){s.resetNamespace("userGw",n),' +
                    'c.resolve()},c.reject)}}})},onActionColumns:function(){this[b].showColumnsWindow' +
                    '()},onActionSearch:function(e){this.bee.$set("keyword",e.keyword)},onDestroy:fun' +
                    'ction(){this._destroy(b,g,I)}})}),define("modules/vpc/userGw/validator",function' +
                    '(e,t,i){var n=e("../vpnConn/validator");i.exports=n.extend({userGwName:function(' +
                    'e){return this._validName(e,"对端网关名称")},userGwAddr:function(e){return this._valid' +
                    'Ip(e,"公网IP")}})}),define("modules/vpc/util",function(e,t,i){var n=e("manager"),a' +
                    '=e("appUtil"),s=e("util"),d=e("constants"),o=d.REGIONMAP,c=e("./cApi"),r=functio' +
                    'n(e,t,i){return function(s){var d=$.Deferred(),o=[].slice.call(arguments);"objec' +
                    't"==typeof s&&(s=s.regionId),s=s||a.getRegionId();var c=function(e){d.notify(e);' +
                    'var t=e.data&&e.data.taskId||e.taskId,i=function(){n.getFlowCscResult({taskId:t,' +
                    'regionId:s},function(e){0===e.status?d.resolve(e.output):1===e.status?d.reject()' +
                    ':setTimeout(i,1e3)},d.reject,"vpc")};i()};return i?e[t].apply(e,o).done(c).fail(' +
                    'd.reject):(o.push(c),o.push(d.reject),e[t].apply(e,o)),d}};i.exports={beeBind:a.' +
                    'beeBind,beeBindBoth:a.beeBindBoth,capitalize:a.capitalize,toDayStart:a.toDayStar' +
                    't,toDayEnd:a.toDayEnd,splitUserGwSubnetList:function(e){return(e||"").split(/[\r' +
                    '\n,，]+/).map(function(e){return $.trim(e)}).filter(function(e){return e})},getLo' +
                    'calSlaIpMeta:function(e){return{net:d.SLANETMAP[e],minMask:24}},getRemoteSlaIpMe' +
                    'ta:function(e){return{net:"169.254.96.0",minMask:19}},makeAsyncTaskToPromise:r,u' +
                    'pdateCvm:r(n,"updateCvm"),addVpnConnEx:r(c,"addVpnConn",!0),modifyVpnConnEx:r(c,' +
                    '"modifyVpnConn",!0),delVpnConnEx:r(c,"delVpnConn",!0),addVpnConn:r(n,"addVpnConn' +
                    '"),modifyVpnConn:r(n,"modifyVpnConn"),delVpnConn:r(n,"delVpnConn"),hasInWhiteLis' +
                    't:function(){var e=$.Deferred();return e.resolve(),e},switchedToOld:function(e){' +
                    'var t="vpc_has_switched_to_old";if(window.localStorage){if(arguments.length<=0)r' +
                    'eturn localStorage[t];localStorage[t]=e?"1":""}return!1},getRegionId:function(){' +
                    'return a.getRegionId()},getVpcId:function(e){return window.localStorage?window.l' +
                    'ocalStorage[[s.getUin(),e,"vpcId"].join("_")]||"":""},setVpcId:function(e,t){win' +
                    'dow.localStorage&&(window.localStorage[[s.getUin(),e,"vpcId"].join("_")]=t)},get' +
                    'UnVpcId:function(e){return window.localStorage?window.localStorage[[s.getUin(),e' +
                    ',"unVpcId"].join("_")]||"":""},setUnVpcId:function(e,t){window.localStorage&&(wi' +
                    'ndow.localStorage[[s.getUin(),e,"unVpcId"].join("_")]=t)},getProperRegionId:func' +
                    'tion(e,t){for(var i=t||a.getRegionId(),n=!1,s=0;s<e.length;s++)if(e[s]===i){n=!0' +
                    ';break}return!n&&e.length>0&&(i=e[0],a.setRegionId(i)),i},getZoneList:function(e' +
                    ',t,i){n.queryZone(e,function(e){for(var a=[],s=0;s<e.length;s++)a.push(e[s].zone' +
                    'Id);var d=e;n.getVpcZoneWhiteList(a,function(e){var i,n={};for(i=0;i<e.length;i+' +
                    '+)n[e[i]]=!0;var a=[];for(i=0;i<d.length;i++)n[d[i].zoneId]&&a.push(d[i]);t(a)},' +
                    'i)},i)},getLocalId:function(e,t,i){return[o[t]||t,e,i].join("_")},getVpcShowId:f' +
                    'unction(e,t){return this.getLocalId("vpc",e,t)},getRtbShowId:function(e,t){retur' +
                    'n this.getLocalId("rtb",e,t)},getRtbShowType:function(e){return""+e=="1"?"默认路由表"' +
                    ':"自定义表"},getVpnShowId:function(e){return"VPN-"+e},getUserGwShowId:function(e){re' +
                    'turn"GW-"+e},vpcTdTpl:'    <p>        <a data-event="nav" href="/vpc/vpc?rid={{r' +
                    'egionId}}&unVpcId={{item.unVpcId || item.uniqVpcId}}">            <span class="t' +
                    'ext-overflow">{{item.unVpcId || item.uniqVpcId}}</span>        </a>    </p>    <' +
                    'span class="text-overflow" title="{{item.vpcName}} ({{item.vpcCIDR || item.vpcCi' +
                    'drBlock}})">{{item.vpcName}}</span>'}}),define("modules/vpc/validator",function(' +
                    'e,t,i){var n=e("widget/validator/util"),a=e("appUtil");i.exports=n.extend({_vali' +
                    'dName:function(e,t){return e=e||"",t=t||"",e?e.length>25?t+"不能超过25个字符":a.isValid' +
                    'Name(e)?void 0:t+'仅支持中文、英文、数字、下划线、分隔符"-"、小数点':t+"不能为空"},_validIp:function(e,t){r' +
                    'eturn e=e||"",t=t||"",e?a.isValidIp(e)?void 0:t+"不是有效的IP":t+"不能为空"},_notEmptyVal' +
                    'idate:function(e){return function(t){return!t||$.trim(t).length<=0?e+"不能为空":void' +
                    ' 0}}})}),define("modules/vpc/vpc/Add",function(e,t,i){var n=e("../BeeBase"),a=fu' +
                    'nction(e,t){var i,n=[];for(i=e;t>=i;i++)n.push(""+i);return n},s={10:{net:"10.0.' +
                    '0.0",mask:8,part2Dropdown:null},172:{net:"172.16.0.0",mask:12,part2Dropdown:null' +
                    '},192:{net:"192.168.0.0",mask:16,part2Dropdown:null}},d=s[10],o=n.extend({$tpl:'' +
                    '    <div class="tc-15-list-wrap form" style="padding:0;">        <form>         ' +
                    '   <div class="tc-15-list-legend border-top-none">私有网络信息</div>            <div c' +
                    'lass="tc-15-list-content">                <ul>                    <li>          ' +
                    '              <em class="tc-15-list-tit" >所属地域</em>                        <span' +
                    ' class="tc-15-list-det">{{regionName}}</span>                    </li>          ' +
                    '          <li>                        <em class="tc-15-list-tit" >名称</em>       ' +
                    '                 <span class="tc-15-list-det">                            <input' +
                    ' type="text" class="tc-15-input-text" style="width: 356px" data-name="vpcName" b' +
                    '-model="vpcName">                        </span>                    </li>       ' +
                    '             <li>                        <em class="tc-15-list-tit" >CIDR</em>  ' +
                    '                      <span class="tc-15-list-det">                            <' +
                    'span b-tag="vpc-ip-input" b-ref="vpcNet" name="vpcNet" b-with="vpcCidrInput"    ' +
                    '                            type="net" net="{{curPrivateNet}}" min-mask="{{curPr' +
                    'ivateMask}}" max-mask="{{vpcMask}}" b-model="vpcNet"></span> /                  ' +
                    '          <select class="tc-15-select" data-name="vpcMask" style="width: 50px;mi' +
                    'n-width: inherit;font-size:12px; height: 24px;padding:0;margin:0;" b-model="vpcM' +
                    'ask">                                <option b-repeat="option in vpcMasks" value' +
                    '="{{option}}">{{option}}</option>                            </select>          ' +
                    '                  <help-tips width="310">                                VPC网段，创' +
                    '建后不可修改，需创建对等连接的私有网络之间CIDR不可重叠，<a style="margin:0" target="_blank" href="//www.qc' +
                    'loud.com/doc/product/215/4927#cidr">查看CIDR配置约束详情</a>。                           ' +
                    ' </help-tips>                        </span>                    </li>           ' +
                    '     </ul>            </div>            <div class="tc-15-list-legend">初始子网信息</d' +
                    'iv>            <div class="tc-15-list-content">                <ul>             ' +
                    '       <li>                        <em class="tc-15-list-tit" >子网名称</em>        ' +
                    '                <span class="tc-15-list-det">                            <input ' +
                    'type="text" class="tc-15-input-text" style="width: 356px" data-name="subnetName"' +
                    ' b-model="subnetName">                        </span>                    </li>  ' +
                    '                  <li>                        <em class="tc-15-list-tit" >CIDR</' +
                    'em>                        <span class="tc-15-list-det">                        ' +
                    '    <div class="num">                                <span b-tag="vpc-ip-input" ' +
                    'b-with="subnetCidrInput" name="subnetNet" type="net" net="{{vpcNet}}" min-mask="' +
                    '{{vpcMask}}" max-mask="{{subnetMask}}" b-model="subnetNet"></span> /            ' +
                    '                    <select class="tc-15-select" data-name="subnetMask" style="w' +
                    'idth: 50px;min-width: inherit;font-size:12px; height: 24px;padding:0; margin:0;"' +
                    ' b-model="subnetMask">                                    <option b-repeat="opti' +
                    'on in subnetMasks" value="{{option}}">{{option}}</option>                       ' +
                    '         </select>                            </div>                        </sp' +
                    'an>                    </li>                    <li>                        <em ' +
                    'class="tc-15-list-tit" >可用区</em>                        <span class="tc-15-list-' +
                    'det">                            <select class="tc-15-select m" style="width: 15' +
                    '0px;font-size:12px;" data-name="zoneId" b-model="zoneId">                       ' +
                    '         <option b-repeat="option in zoneList" value="{{option.zoneId}}">{{optio' +
                    'n.zoneName}}</option>                            </select>                      ' +
                    '      <help-tips>                                同一地域下电力和网络互相独立的物理机房，目的是隔离故障，<a ' +
                    'style="margin:0" target="_blank" href="https://www.qcloud.com/doc/product/215/49' +
                    '27#.E5.8F.AF.E7.94.A8.E5.8C.BA.EF.BC.88zone.EF.BC.89">点击查看可用区容灾实践指南</a>。        ' +
                    '                    </help-tips>                        </span>                 ' +
                    '   </li>                    <li>                        <em class="tc-15-list-ti' +
                    't" >关联路由表</em>                        <span class="tc-15-list-det">             ' +
                    '               默认                        </span>                        <help-ti' +
                    'ps content="子网须关联一个路由表进行流量转发控制。"></help-tips>                    </li>          ' +
                    '      </ul>            </div>        </form>    </div>',$afterInit:function(){va' +
                    'r e=this,t=e.$refs,i=t.vpcNet;n.prototype.$afterInit.apply(e,arguments),i.$watch' +
                    '("part1",function(t){var i,n,d=s[t];d&&(this.$set("part2Cfg.dropdown",n=d.part2D' +
                    'ropdown),n&&n.indexOf(""+this.part2)<0&&this.$set("part2",n[0]),i=a(Math.max(16,' +
                    'd.mask),28),e.$set({curPrivateNet:d.net,curPrivateMask:d.mask,vpcMasks:i,vpcMask' +
                    ':i[0]}))},!0),e.$watch("vpcMask",function(t){var i=a(+t,28);e.$set("subnetMasks"' +
                    ',i),i&&i.indexOf(""+e.subnetMask)<0&&e.$set("subnetMask",i[0])},!0)}},{defaults:' +
                    '$.extend({},n.prototype.defaults,{curPrivateNet:d.net,curPrivateMask:d.mask,vpcN' +
                    'et:d.net,vpcMask:"16",vpcMasks:a(16,28),vpcCidrInput:{part1Cfg:{dropdown:Object.' +
                    'keys(s)},part2Cfg:{dropdown:d.part2Dropdown}},subnetNet:"10.0.0.0",subnetMask:"2' +
                    '4",subnetMasks:a(16,28)})});i.exports=o}),define("modules/vpc/vpc/addClassicLink' +
                    '",function(e,t){var i=e("models/api"),n=e("manager"),a=e("appUtil"),s=e("../BeeB' +
                    'ase"),d=e("dialog");t.render=function(e){return new Promise(function(t,o){var c=' +
                    's.getComponent("device-selector"),r=new c({$data:{initGetData:!0,scrollPager:!0,' +
                    'alias:{id:"uInstanceId",name:"alias",ip:"deviceLanIp",network:"uInstanceId"}},_g' +
                    'etLimit:function(){var e;return function(){return e||(e=i.req({cmd:"DescribeVpcL' +
                    'imit",serviceType:"vpc",data:{type:[22]}}).then(function(e){return e.data.limit[' +
                    '22]})),e}}(),listTpl:'  <input type="checkbox" disabled?="item.disabled" class="' +
                    'tc-15-checkbox" b-model="item.selected">    <span class="opt-txt" data-title?="i' +
                    'tem.tips">      <span class="opt-txt-inner">        {{> itemTpl}}      </span>  ' +
                    '  </span>',getData:function(e,t){var i={count:e.count,offset:(e.page-1)*e.count}' +
                    ',s=e.searchKey;this._getLimit(),s&&(s=s.trim(),a.isValidIp(s)?i.vagueIp=s:/^ins-' +
                    '/.test(s)?i.uInstanceIdList=[s]:i.alias=s),n.getCvmList($.extend(i,{allType:1,vp' +
                    'cId:0}),function(e){e.deviceList.forEach(function(e){3==e.runflag?(e.disabled=!0' +
                    ',e.tips="该主机还未创建好"):e.hypervisor?(e.disabled=!0,e.tips="旧架构主机不支持关联到私有网络"):e.rela' +
                    'tedVpcId&&(e.disabled=!0,e.tips="该主机已经和私有网络互通")}),t(null,{size:e.totalNum,list:e' +
                    '.deviceList})})}});d.create(r.$el,750,"",{title:"关联云主机",preventResubmit:!0,butto' +
                    'n:{"确定":function(){var a=r.selected;a.length?i.req({serviceType:"vpc",cmd:"Attac' +
                    'hClassicLinkVpc",data:{vpcId:e,instanceIds:a.map(function(e){return e.uInstanceI' +
                    'd})}}).then(function(e){d.hide(),n.checkTask({taskId:e.taskId}).then(function(){' +
                    '},function(){}).then(t)},o):(o(),d.hide())}}})}).then(function(){d.hide()})}}),d' +
                    'efine("modules/vpc/vpc/ClassicLinkGrid",function(e,t,i){var n=e("qccomponent"),a' +
                    '=e("../Grid"),s=e("$"),d=e("appUtil"),o=e("models/api"),c=e("manager");i.exports' +
                    '=a.extend({$afterInit:function(){a.prototype.$afterInit.call(this);var e=this;e.' +
                    '$watch("unVpcId",function(t){t&&e.bufferReload()},!0),this.$set("regionId",d.get' +
                    'RegionId())},getData:function(e,t){var i=this;e=s.extend({},e),e.limit=e.count||' +
                    'i.count,e.offset=((e.page||i.page)-1)*e.limit,t=t||function(e,t){i.setData(t)};v' +
                    'ar n={vpcId:e.unVpcId||i.unVpcId,offset:e.offset,limit:e.limit};return e.searchK' +
                    'ey&&(n.lanIp=e.searchKey),o.req({serviceType:"vpc",cmd:"DescribeVpcClassicLink",' +
                    'data:n}).then(function(n){t(null,{totalNum:n.totalCount,list:n.data||[],page:e.p' +
                    'age||i.page,searchKey:e.searchKey})})},unBind:function(e){var t=this;return o.re' +
                    'q({serviceType:"vpc",cmd:"DetachClassicLinkVpc",data:{vpcId:this.unVpcId,instanc' +
                    'eIds:e.map(function(e){return e.instanceId})}}).then(function(i){e.forEach(funct' +
                    'ion(e){t.updateItem(t.list.indexOf(e),{$loading:!0,$selected:!1})}),c.checkTask(' +
                    '{taskId:i.taskId}).then(function(){},function(){}).then(function(){t.getData({pa' +
                    'ge:1}).then(function(){t.$emit("update")})})})},del:function(e){var t=this;retur' +
                    'n function(i){if(!e.$disable){var a=new(n.getComponent("popup-confirm"))({$data:' +
                    '{trigger:"",title:"确认对该基础网络云主机解关联? ",content:"解除关联后, 该云主机将无法与该私有网络互通",style:"wid' +
                    'th: 350px;",hideDestroy:!0},handler:i.target,onConfirm:function(){t.unBind([e])}' +
                    '});a.show()}}}},{defaults:{hasFirst:!0,colums:[{name:"云主机ID",key:"instanceId",td' +
                    'Tpl:'<span class="text-overflow"><a href="/cvm/detail/{{regionId}}/{{item.instan' +
                    'ceId}}" data-event="nav">{{item.instanceId}}</a></span>'},{name:"名称",key:"instan' +
                    'ceName"},{name:"内网IP",key:"lanIp"},{name:"操作",key:"_action",tdTpl:'<a b-on="{cli' +
                    'ck: del(item)}" href="javascript:;">解关联</a>'}]}})}),define("modules/vpc/vpc/Deta' +
                    'il",function(e,t,i){e("../IPInput");var n,a=(e("../Grid"),e("dialog")),s=e("../B' +
                    'eeBase"),d=e("./RenameBubble"),o=e("./ClassicLinkGrid"),c=(e("../Confirm"),e("ma' +
                    'nager")),r=(e("constants"),e("../cacher")),l=e("../util"),p="regionMapTask",u=(e' +
                    '("./validator"),e("models/api"),e("./addClassicLink"));e("../subnet/DropDown");v' +
                    'ar v=e("../ResourceGrid"),f=e("./vpcResource");n=s.extend({$valuekey:"unVpcId",t' +
                    'ab:0,$beforeInit:function(){var e=this;e.actionHandler=function(){e.handleAction' +
                    '(this.name,this)};var t=e[p]=$.Deferred();c.queryRegion(t.resolve,t.reject),t.do' +
                    'ne(function(t){e.$set("regionMap",t)}),s.prototype.$beforeInit.apply(this,argume' +
                    'nts)},$afterInit:function(){s.prototype.$afterInit.apply(this,arguments);var e=t' +
                    'his,t=function(){if(this.regionId&&this.unVpcId){e.$set("notFound",!1);var t=thi' +
                    's.vpc;t&&t.unVpcId===this.unVpcId||(e.$set({vpc:null,hasNext:$.noop,next:$.noop,' +
                    'hasPrev:$.noop,prev:$.noop}),e.getVpcData())}};this.$watch("unVpcId",t,!0);var i' +
                    '=!1;this.$watch("vpc.classicLinkNum",function(){this.$refs.classicLinkGrid&&!i&&' +
                    '(i=!0,this.$refs.classicLinkGrid.$watch("selectedNum",function(){var t=this.getS' +
                    'elected();e.$set("disabledUnBindBtn",!t.length)},!0),this.$refs.classicLinkGrid.' +
                    '$on("update",function(){e.$set("event_update",{classicLinkNum:e.$refs.classicLin' +
                    'kGrid.list.length})}))},!0)},getVpcShowId:$.proxy(l.getVpcShowId,l),$tpl:'<div b' +
                    '-on="events" data-vpc-detail class="manage-area" b-style="{display: unVpcId ? \'' +
                    'block\' : \'none\'}" ><div b-if="notFound" style="text-align:center; padding:100' +
                    'px;">未查询到指定私有网络, <a href="javascript:;" b-on-click="$set(\'unVpcId\', \'\')">返回列' +
                    '表</a></div><div class="manage-area-title secondary-title" b-style="{display:notF' +
                    'ound?\'none\':\'\'}"><a class="back-link" data-action="return" href="javascript:' +
                    ';"><i class="btn-back-icon"></i><span>返回</span></a><span class="line-icon">|</sp' +
                    'an><h2>{{ifLoading(vpc && (vpc.vpcName + " 详情"))}}</h2><div class="manage-area-t' +
                    'itle-right"><a b-if="tab != 1" href="https://www.qcloud.com/doc/product/215/4927' +
                    '" target="_blank"><b class="links-icon"></b> 私有网络与子网帮助文档</a><a b-if="tab == 1" h' +
                    'ref="https://www.qcloud.com/doc/product/215/5002" target="_blank"><b class="link' +
                    's-icon"></b> 基础网络互通帮助文档</a></div></div><div class="manage-area-main secondary-ma' +
                    'in" b-style="{display:notFound?\'none\':\'\'}"><div class="fixed-tab tc-15-tab-a' +
                    'lt"><ul class="tc-15-tablist"  b-if="vpc && !notFound"><li class="{{tab==0 ? \'t' +
                    'c-cur\': \'\'}}"><a href="javascript:;" b-on-click="$set(\'tab\', 0)" data-repor' +
                    't="vpc.vpc.detail" title="基本信息" >基本信息</a></li><li class="{{tab==1 ? \'tc-cur\': ' +
                    '\'\'}}"><a href="javascript:;" b-on-click="$set(\'tab\', 1)" title="基础网络互通">基础网络' +
                    '互通</a></li></ul><div b-style="{display : !vpc || notFound || tab != 0 ? \'none\'' +
                    ' : \'\'}"><div class="param-box"><div class="param-hd"><h3>基本信息</h3></div><div c' +
                    'lass="param-bd"><ul class="item-descr-list"><li><em class="item-descr-tit">私有网络名' +
                    '称</em><span class="item-descr-txt"><i b-if="vpc.renaming" class="n-loading-icon"' +
                    '></i>{{ifLoading(vpc && vpc.vpcName)}} <a b-if="vpc && !vpc.renaming" data-actio' +
                    'n="rename" href="javascript:;">修改</a></span></li><li><em class="item-descr-tit">' +
                    '私有网络ID</em><span class="item-descr-txt">{{unVpcId}}</span></li><li><em class="it' +
                    'em-descr-tit">地域</em><span class="item-descr-txt">{{regionMap && regionMap[regio' +
                    'nId]}}</span></li><li><em class="item-descr-tit">CIDR</em><span class="item-desc' +
                    'r-txt">{{ifLoading(vpc && vpc.vpcCIDR)}}</span></li><li><em class="item-descr-ti' +
                    't">创建时间</em><span class="item-descr-txt">{{ifLoading(vpc && vpc.createTime)}}</s' +
                    'pan></li></ul></div></div><div class="param-box"><div class="param-hd"><h3>包含资源<' +
                    '/h3><resource-grid region-id="{{regionId}}" item="{{ifLoading(vpc)}}"></resource' +
                    '-grid></div></div></div><div class="param-box" b-style="{display : !vpc || notFo' +
                    'und || tab != 1 ? \'none\' : \'\'}"><div style="padding: 100px; text-align: cent' +
                    'er" b-if="vpc.classicLinkNum < 0"><div class="tc-15-list-legend" style="border: ' +
                    'none">该私有网络不支持与基础网络互通</div><div class="tc-15-list-content">基础网络互通仅支持网段为 10.0~47.' +
                    '0.0/16（含子集）的私有网络。<!-- <a href="#" target="_blank">更多帮助</a> --></div></div><div b' +
                    '-if="vpc.classicLinkNum >= 0"><div class="param-hd"><h3>已关联云主机</h3></div><div><b' +
                    'utton class="tc-15-btn m btn-add"data-action="addClassicLink">+关联云主机</button><bu' +
                    'tton class="tc-15-btn weak m {{disabledUnBindBtn ? \'disabled\' : \'\'}}" data-a' +
                    'ction="unBindClassicLink">解除关联</button> <qc-search placeholder="输入内网 IP 进行搜索" mu' +
                    'ltiline="false" on-search="{{onSearchClsLink.bind(this)}}"></qc-search></div><cl' +
                    'assic-link-grid un-vpc-id="{{vpc.unVpcId}}" b-ref="classicLinkGrid"></classic-li' +
                    'nk-grid></div></div></div></div></div>',events:{"click [data-action]":function(e' +
                    '){var t=$(e.currentTarget);this.handleAction(t.attr("data-action"),null,e),e.pre' +
                    'ventDefault()}},ifLoading:function(e){return this.vpc?e:"加载中..."},handleAction:f' +
                    'unction(e,t,i){var n=this;switch(e){case"return":history.back();break;case"renam' +
                    'e":new d({regionId:this.regionId,unVpcId:this.unVpcId,vpcId:this.vpcId,value:thi' +
                    's.vpc.vpcName,target:$(i.currentTarget).closest("span"),onSave:function(e,t,i){n' +
                    '.$set("vpc.vpcName",t),n.$set("vpc.renaming",!0),e.always(function(){n.$set("vpc' +
                    '.renaming",!1)}),e.fail(function(){n.$set("vpc.vpcName",i)}),e.done(function(){n' +
                    '.$set("event_update",{vpcName:t})})}});break;case"addClassicLink":this.addClassi' +
                    'cLink().then(function(){n.$refs.classicLinkGrid.refresh(function(){n.$refs.class' +
                    'icLinkGrid.$emit("update");
})});break;case"unBindClassicLink":if($(i.currentTar' +
                    'get).hasClass("disabled"))return;a.create("解除关联后, 这些云主机将无法与该私有网络互通",480,"",{titl' +
                    'e:"批量解关联",preventResubmit:!0,button:{"确定":function(){var e=n.$refs.classicLinkGr' +
                    'id;e.unBind(e.getSelected()).then(function(){a.hide()})}}})}},addClassicLink:fun' +
                    'ction(){return u.render(this.unVpcId)},getVpcData:function(){var e,t=this,i=t.re' +
                    'gionId,n=t.unVpcId;$.when(t[p],r.getVpc(i,n).done(function(t){e=t})).done(functi' +
                    'on(){t.$replace("vpc",e)}).fail(function(){t.$replace("vpc",{}),t.$set("notFound' +
                    '",!0)})},onSearchClsLink:function(e){var t=this,i=t.$refs.classicLinkGrid;i.getD' +
                    'ata({searchKey:e,page:1})}}),e("util").insertStyle(".vpc-cvm-grid-editing a[data' +
                    '-role]{ display:none; }"),n.tag("classic-link-grid",o),n.tag("resource-grid",v.e' +
                    'xtend({},{defaults:{RESOURCE:f}})),i.exports=n}),define("modules/vpc/vpc/DropDow' +
                    'n",function(e,t,i){var n=e("qccomponent"),a=e("../DropDown"),s=e("../cacher"),d=' +
                    'a.extend({watchFields:["regionId"],getData:function(e,t){var i=this,n=i.textFiel' +
                    'ds,a=i.useUnId?"unVpcId":"vpcId";s.getVpcListEx(this.regionId).done(function(t){' +
                    'var s=t.vpcList;s=i.vpcFilterList?s.filter(function(e){return-1==i.vpcFilterList' +
                    '.indexOf(e[a])}):s,e(s.map(function(e){var t,s;return n?(s=n.map(function(t){ret' +
                    'urn e[t]}),t=s[0]+(s.length>1?" ("+s.slice(1).join(" | ")+")":"")):t=e.vpcName+(' +
                    'i.showCidr?" ("+e.vpcCIDR+")":""),{text:t,value:e[a],vpcId:e.vpcId,unVpcId:e.unV' +
                    'pcId,vpcName:e.vpcName,vpcCIDR:e.vpcCIDR}}))}).fail(t)}},{defaults:{textFields:n' +
                    'ull,showCidr:!1,useUnId:!1}});n.tag("vpc-vpc-select",d),i.exports=d}),define("mo' +
                    'dules/vpc/vpc/RenameBubble",function(e,t,i){var n=e("../RenameBubble"),a=e("tips' +
                    '"),s=e("appUtil"),d=e("constants"),o=e("../cApi"),c=e("../cacher");i.exports=n.e' +
                    'xtend({validate:function(e){return e.length>25?"私有网络名称不能超过25个字符":s.isValidName(e' +
                    ')?void 0:"私有网络"+d.INVALID_NAME_TIPS},doSave:function(e){var t=this,i=t.regionId,' +
                    'n=t.unVpcId;return o.request(i,"ModifyVpcAttribute",{vpcId:n,vpcName:e}).done(fu' +
                    'nction(){c.resetVpc(i,n),c.resetVpc(i,t.vpcId)}).fail(function(e){e.data&&28029=' +
                    '==e.data.cgwerrorCode?a.error("修改vpc网络名称已存在"):a.error("修改vpc名称失败!")})}})}),defin' +
                    'e("modules/vpc/vpc/Selector",function(e,t,i){var n=e("qccomponent"),a=n.getCompo' +
                    'nent("qc-select"),s=e("../util"),d=e("../cacher"),o=a.extend({$valuekey:"vpcId",' +
                    '$beforeInit:function(){var e=this;e.useUnId&&(e.$valuekey="unVpcId"),a.prototype' +
                    '.$beforeInit.apply(e,arguments)},$afterInit:function(){var e=this,t=e.$valuekey,' +
                    'i=e.useUnId;a.prototype.$afterInit.apply(e,arguments);var n=function(){var i,n=e' +
                    '.selected,a=e[t]||"",s=e.list||[],d="resolved"===e.loadTask.state(),o=!1;s.forEa' +
                    'ch(function(e){""+a==""+(e[t]||"")&&(i=e)}),!i&&d&&s.length&&(i=s[0],o=!0),i&&(!' +
                    'i||!o&&n&&n[t]===i[t]||e.$set("selected",i))};e.$watch("regionId",function(){e.$' +
                    'replace("selected",null),e.loadTask=d.getVpcListEx(e.regionId).done(function(t){' +
                    'var i=t.vpcList,n={label:"全部私有网络"},a=[];e.hasAll?a.push(n):i.length||a.push({lab' +
                    'el:"无可用私有网络"}),e.$set("list",a.concat(i.map(function(e){return{label:e.vpcName,v' +
                    'pcId:e.vpcId,unVpcId:e.unVpcId}})))})},!0),e.$watch("vpcId",n),e.$watch("unVpcId' +
                    '",n),e.$watch("list",n),n(),e.$watch("selected",function(n){if(n){var a=n[t]||""' +
                    ',d=e[t]||"",o=e.regionId;a!==d&&(e.$set(t,a),i&&s.setUnVpcId(o,a),s.setVpcId(o,n' +
                    '.vpcId))}})}},{defaults:$.extend({},a.prototype.defaults,{label:"加载中...",selecte' +
                    'd:{},list:[],useUnId:!1,hasAll:!0,simulateSelect:!0})});i.exports=o}),define("mo' +
                    'dules/vpc/vpc/validator",function(e,t,i){var n=e("../validator"),a=e("appUtil");' +
                    'i.exports=n.extend({vpcName:function(e){return this._validName(e,"私有网络名称")},subn' +
                    'etName:function(e){return this._validName(e,"子网名称")},zoneId:function(e){return e' +
                    '?void 0:"必须选择一个可用区"},vpcNet:function(e,t,i){return a.isValidIp(e)&&a.isValidCIDR' +
                    '(e,i.vpcMask)?void 0:"私有网络网段不合法"},vpcMask:function(e,t,i){return e=parseInt(e,10' +
                    ')||0,16>e||e>28?"私有网络网段掩码必须在16和28之间":void 0},subnetNet:function(e,t,i){return a.' +
                    'isValidIp(e)&&a.isValidCIDR(e,i.subnetMask)?void 0:"子网网段不合法"},subnetMask:functio' +
                    'n(e,t,i){return e=parseInt(e,10)||0,16>e||e>28?"子网网段掩码必须在16和28之间":e<i.vpcMask?"子' +
                    '网网段掩码必须大于所属私有网络网段掩码":void 0},subnetId:function(e){return e?void 0:"所属子网不能为空"},de' +
                    'viceLanIp:function(e){return this._validIp(e,"内网IP")}})}),define("modules/vpc/vp' +
                    'c/vpc",function(e,t,i){e("widget/region/regionSelector");var n=e("../Grid"),a=e(' +
                    '"pageManager"),s=(e("manager"),e("constants")),d=e("appUtil"),o=e("../util"),c=e' +
                    '("../cApi"),r=e("../cacher"),l=e("../Confirm"),p=e("./RenameBubble"),u=e("../mod' +
                    'uleBase"),v=e("./Detail"),f=e("../BeeBase"),m=e("reporter"),h="grid",b="regionSe' +
                    'lector",g="loadToken",I=s.VPC_MAX_NUM,w=e("./Add"),y=e("dialog"),x=e("./validato' +
                    'r"),C=e("../createLimitsMixin"),T=e("tips"),N=f.extend({$tpl:'<div><div b-style=' +
                    '"{display: unVpcId ? \'none\' : \'block\'}" ><div class="manage-area-title"><h2>' +
                    '私有网络</h2><span b-tag="region-selector" b-ref="regionSelect" b-model="regionId" b' +
                    '-with="{getWhiteList:getWhiteList}"></span>                <!-- <a data-name="sw' +
                    'itchToOld" href="javascript:void(0);" style="float:right;font-size:14px;" b-on="' +
                    '{click:actionHandler}">切换到旧版</a> --><div class="manage-area-title-right"><a href' +
                    '="https://www.qcloud.com/doc/product/215/4927" target="_blank"><b class="links-i' +
                    'con"></b> 私有网络与子网帮助文档</a></div></div><div class="tc-15-action-panel"><div b-tag=' +
                    '"qc-action-button" b-ref="create" disabled="true" name="add" label="+新建" attr="{' +
                    '{ {\'data-title\':addDisabledText} }}" b-with="{action:actionHandler, disabled:a' +
                    'ddDisabled}"></div><div b-tag="qc-action-button" float="right" name="columns" cl' +
                    'ass-name="weak setting" b-with="{action:actionHandler}"></div><div b-tag="qc-sea' +
                    'rch" b-ref="search" float="right" name="search" placeholder="搜索私有网络的关键词" multili' +
                    'ne="false" b-with="{search:actionHandler}" keyword="{{keyword}}"></div></div><di' +
                    'v data-mod="grid"></div></div><div b-tag="vpc-detail" b-ref="detail" region-id="' +
                    '{{regionId}}" tab="{{tab}}" b-model="unVpcId" vpc-id="oldVpcId" vpc="{{pseudoVpc' +
                    '}}"></div></div>',$mixins:[C({MAX_VPC_NUM:s.VPC_LIMIT.VPC_PER_OWNER_LIMIT_TYPE})' +
                    ']},{defaults:{MAX_VPC_NUM:I}});N.tag("vpc-detail",v),i.exports=u.extend({title:"' +
                    '私有网络-控制台",routeBase:"/vpc/vpc",params:$.extend({},u.params,{unVpcId:{initial:"",' +
                    'defaults:"",history:!0,formula:function(e){return e||""}},vpc:{route:!1},tab:"",' +
                    'keyword:""}),Bee:N,onParamRegionId:function(e){e&&r.getZoneList(e)},getBeeConfig' +
                    ':function(){var e=this,t=u.getBeeConfig.apply(this,arguments),i=function(t){var ' +
                    'i=this.name||t&&$(t.currentTarget).attr("data-name");e.handleAction(i,this)};ret' +
                    'urn $.extend(t.$data,{actionHandler:i}),$.extend(t,{getWhiteList:function(){retu' +
                    'rn r.getRegionIdList()}})},onRender:function(){var t=this,i=t.bee;e("../adTip").' +
                    'init(t.$el),t.vpcId2UnId();var n=function(){t[h]&&t[h].bufferReload()},a=functio' +
                    'n(e){t.zoneId=e&&e[0]&&e[0].zoneId};t.zoneTask=r.getZoneList(i.regionId).done(a)' +
                    ',i.$watch("regionId",function(){t.zoneTask=r.getZoneList(i.regionId).done(a),n()' +
                    '}),["keyword","page","count"].forEach(function(e){i.$watch(e,n)}),t.renderList()' +
                    ',d.beeBind(t[h],i,"totalNum","totalNum"),d.beeBind(i,i,"keyword ? 0 : totalNum",' +
                    '"knownCount"),d.beeBind(i,i,"knownCount>=MAX_VPC_NUM","addDisabled"),d.beeBind(i' +
                    ',i,'knownCount>=MAX_VPC_NUM ? "私有网络创建数达到上限" : ""',"addDisabledText"),i.$refs.det' +
                    'ail.$watch("event_update",function(e){var n=t[h];i.unVpcId&&n&&n.updateRowData({' +
                    'unVpcId:i.unVpcId},e)}),n()},renderList:function(){var e=this,t=e.bee,i=a.appAre' +
                    'a;e[h]=new n({$el:e.getModDom(i,h),onRowRename:function(e,i,n){var a=this;new p(' +
                    '{regionId:t.regionId,unVpcId:e.unVpcId,vpcId:e.vpcId,value:e.vpcName,target:$(n.' +
                    'currentTarget).parent(),onSave:function(e,t,n){a.list.$set(i,{vpcName:t,renaming' +
                    ':!0}),e.done(function(){a.list.$set(i,{renaming:!1})}),e.fail(function(){a.list.' +
                    '$set(i,{vpcName:n,renaming:!1})})}})},onRowRemove:function(e,i,n){var a=this;new' +
                    ' l({target:$(n.currentTarget),box:a.$gridBody,$data:{title:"确定删除该私有网络？"},onConfi' +
                    'rm:function(){var i=t.regionId;c.request(i,"DeleteVpc",{vpcId:e.unVpcId},"删除私有网络' +
                    '失败").then(function(){r.resetVpc(i),a.bufferReload()})}})},onRowDetail:function(e' +
                    ',i,n){t.$set("unVpcId",e.unVpcId),t.$set("oldVpcId",e.vpcId)},onRowAddCvm:functi' +
                    'on(t,i,n){window.open("https://buy.qcloud.com/cvm?"+$.param({regionId:this.regio' +
                    'nId,zoneId:e.zoneId,vpcId:t.vpcId}))},onRowAddCdb:function(t,i,n){window.open("h' +
                    'ttp://manage.qcloud.com/shoppingcart/shop.php?"+$.param({tab:"cdb",regionId:this' +
                    '.regionId,zoneId:e.zoneId,vpcId:t.vpcId}))},onRowAddLb:function(t,i,n){window.op' +
                    'en("http://manage.qcloud.com/shoppingcart/shop.php?"+$.param({tab:"lb",regionId:' +
                    'this.regionId,zoneId:e.zoneId,vpcId:t.vpcId}))},onRowAddCmem:function(t,i,n){win' +
                    'dow.open("http://manage.qcloud.com/shoppingcart/shop.php?"+$.param({tab:"cmem",r' +
                    'egionId:this.regionId,zoneId:e.zoneId,vpcId:t.vpcId}))},$data:{count:t.count,pag' +
                    'e:t.page,cacheKey:"vpc_list_columns_v20151124",colums:[{key:"vpcName",name:"ID/名' +
                    '称",required:!0,order:!0,tdTpl:'<p><a data-role="detail" href="javascript:;" clas' +
                    's="text-overflow"><span class="text-overflow">{{item.unVpcId}}</span></a></p><sp' +
                    'an class="text-overflow m-width"><i b-if="item.renaming" class="n-loading-icon">' +
                    '</i>{{item.vpcName}}</span>{{{renameIconHtml}}}'},{key:"vpcCIDR",name:"CIDR"},{k' +
                    'ey:"subnetNum",name:"子网",width:100},{key:"rtbNum",name:"路由表",width:100},{key:"na' +
                    'tNum",name:"Nat 网关",width:100,hide:!0},{key:"vpnGwNum",name:"VPN网关",width:100,hi' +
                    'de:!0},{key:"vpcPeerNum",name:"对等连接",width:100,hide:!0},{key:"vpcDevices",name:"' +
                    '云主机",width:100},{key:"classicLinkNum",name:"基础网络互通",width:120,hide:!0},{key:"vpg' +
                    'Num",name:"专线网关",width:100,hide:!0},{key:"_action",name:"操作",minWidth:100,requir' +
                    'ed:!0}]},getCellContent:function(e,t,i){var n,a,s,d=void 0===e;switch(i.key){cas' +
                    'e"subnetNum":n=d?"-":0>=e?e+"个":'<a data-report="vpc.vpc.subnet" data-event="nav' +
                    '" href="/vpc/subnet?unVpcId='+t.unVpcId+'">'+e+"个</a>";break;case"rtbNum":n=d?"-' +
                    '":0>=e?e+"个":'<a data-report="vpc.vpc.route" data-event="nav" href="/vpc/route?u' +
                    'nVpcId='+t.unVpcId+'">'+e+"个</a>";break;case"vpnGwNum":n=d?"-":0>=e?e+"个":'<a da' +
                    'ta-event="nav" href="/vpc/vpnGw?unVpcId='+t.unVpcId+'">'+e+"个</a>";break;case"vp' +
                    'cPeerNum":n=d?"-":0>=e?e+"个":'<a data-event="nav" href="/vpc/conn?unVpcId={{item' +
                    '.unVpcId}}">'+e+"个</a>";break;case"vpcDevices":n=d?"-":(0>=e?e+"个":'<a data-even' +
                    't="nav" data-report="vpc.vpc.cvm" href="/cvm?vpcId={{item.vpcId}}" style="margin' +
                    ':0">'+e+"个</a>")+' <a data-role="addCvm"  data-report="vpc.vpc.cvm_add" data-tit' +
                    'le="添加云主机" href="#"><i class="gateway-icon"></i></a>';break;case"classicLinkNum"' +
                    ':n=0>e?"不支持":0==e?e+"个":'<a data-event="nav" href="/vpc/vpc?rid={{regionId}}&unV' +
                    'pcId={{item.unVpcId}}&tab=1">'+e+"个</a>";break;case"vpgNum":n=d?"-":0>=e?e+"个":'' +
                    '<a data-event="nav" data-report="vpc.vpc.dcGw" href="/vpc/dcGw?rid={{regionId}}&' +
                    'unVpcId={{item.unVpcId}}" style="margin:0">'+e+"个</a>";break;case"natNum":n=d?"-' +
                    '":0>=e?e+"个":'<a data-event="nav" data-report="vpc.vpc.nat" href="/vpc/nat?rid={' +
                    '{regionId}}&unVpcId={{item.unVpcId}}" style="margin:0">'+e+"个</a>";break;case"_a' +
                    'ction":t.vpnGwNum>0?a="VPN网关":t.dcGwNum>0?a="专线网关":t.lbNum>0?a="负载均衡":t.subnetNu' +
                    'm>0?a="子网":t.vpcPeerNum>0?a="对等连接":t.classicLinkNum>0?(a="基础网络互通",s="无法删除已与基础网络互' +
                    '通的私有网络"):t.vpgNum>0&&(a="专线网关"),n=this.getActionsCell([{text:"删除",act:"remove",d' +
                    'isabled:!!a,title:s?s:a?"无法删除含有"+a+"的私有网络":""}])}return n},getData:function(e,i)' +
                    '{var n,a=this,s=a[g]={},d=e.count,o=e.page,r=d*(o-1),l=d,p=t.keyword;a.showLoadi' +
                    'ng("加载中..."),n=c.request(t.regionId,"DescribeVpcEx",{offset:r,limit:l,vpcName:p}' +
                    ',"加载VPC列表失败").translate({totalCount:"totalNum",data:{key:"detail",map:"vpc"}});v' +
                    'ar u;n.done(function(e){u=e}),$.when(a.zoneTask,n).done(function(){if(s===a[g]){' +
                    'var i=u.detail;i=a.localSort(i,e.orderField,e.order),a.regionId=t.regionId,a.set' +
                    'Data({totalNum:u.totalNum,searchKey:p,list:i})}}).always(function(){a.hideLoadin' +
                    'g()})},backList:function(){this.$set("searchKey",""),e.setParam("keyword",void 0' +
                    ')}})},onActionAdd:function(){var e,t,i=this,n=i.bee,a=n.regionId,s=r.getRegionMa' +
                    'p(),d=r.getZoneList(a);d.done(function(e){t=e}),s.done(function(t){e=t[a]}),$.wh' +
                    'en(d,s).done(function(){var n=new w({regionId:a,regionName:e,zoneList:t});y.crea' +
                    'te(n.$el,"600","",{title:"新建VPC","class":"dialog_layer_v2 tc-15-rich-dialog new-' +
                    'gateway",preventResubmit:!0,button:{"创建":function(e){var t=x.multiValidate(n,["v' +
                    'pcName","vpcNet","vpcMask","subnetName","subnetNet","subnetMask","zoneId"],null,' +
                    'n.$el,{$btn:e});if(t)return void e.removeClass("btn_unclick");var a=[{zoneId:par' +
                    'seInt(n.zoneId,10),subnetName:n.subnetName,cidrBlock:n.subnetNet+"/"+n.subnetMas' +
                    'k}];y.freeze(!0).toggleBtnLoading(!0);var s=n.regionId;c.request(s,"CreateVpc",{' +
                    'vpcName:n.vpcName,cidrBlock:n.vpcNet+"/"+n.vpcMask,subnetSet:a}).then(function()' +
                    '{y.hide(),r.resetNamespace("vpc",s),i[h].bufferReload(),0==i[h].list.length&&y.c' +
                    'reate('VPC创建成功，<a href="https://www.qcloud.com/doc/product/215/5168" target="_bl' +
                    'ank">点击查看</a>VPC快速入门向导。',500,"",{title:"VPC 创建成功",button:{"确定":function(){y.hide' +
                    '()}}})},function(t){t.data&&28029===t.data.cgwerrorCode?(T.error("创建vpc网络名称已存在")' +
                    ',x._toggleError($(n.$el).find('[data-name="vpcName"]'),"vpc网络名称已存在")):T.error("创' +
                    '建vpc网络失败!"),e.removeClass("btn_unclick"),y.freeze(!1).toggleBtnLoading(!1)})}}})' +
                    '})},onActionColumns:function(){this[h].showColumnsWindow(),m.click("vpc.vpc.conf' +
                    'igure")},onActionSearch:function(e){this.setParam("keyword",e.keyword),m.click("' +
                    'vpc.vpc.search")},onActionSwitchToOld:function(){o.switchedToOld(!0),a.displaySi' +
                    'debar(!1),e("router").navigate("/vpc")},onDestroy:function(){this._destroy(h,b,g' +
                    ')}})}),define("modules/vpc/vpc/vpcResource",function(e,t,i){var n=e("models/api"' +
                    '),a=e("manager"),s=e("appUtil"),d=function(e,t){return t=this,new Promise(functi' +
                    'on(i){i(e[t._key])})},o=function(e){return new Promise(function(t,i){a.getVpcExt' +
                    'ernalStatistic(s.getRegionId(),[e.vpcId],["lbNum"],function(i){t(i[e.vpcId].lbNu' +
                    'm)},i)})},c=function(e){return new Promise(function(t){var i=e.classicLinkNum;t(' +
                    '0>i?"不支持":i)})},r=function(e,t){return e.classicLinkNum>=0},l=function(e){return' +
                    ' n.request({serviceType:"cdb",cmd:"GetCdbInstanceNumByVpcSubnetId",data:{vpcId:e' +
                    '.vpcId}}).then(function(e){return e.data.total})},p=function(e){return n.request' +
                    '({serviceType:"cmem",cmd:"DescribeCmem",data:{vpcId:e.vpcId}}).then(function(e){' +
                    'return e.totalCount})},u=function(e){return new Promise(function(t,i){a.getSslVp' +
                    'nStatisticListEx(s.getRegionId(),e.unVpcId,"","",0,20,function(e){t(e.totalCount' +
                    ')},i)})},v=function(e){return n.request({serviceType:"vpc",cmd:"DescribeNatGatew' +
                    'ay",data:{vpcId:e.unVpcId}}).then(function(e){return e.totalCount})},f=function(' +
                    'e){return n.request({serviceType:"tdsql",cmd:"CdbTdsqlGetInstanceCount",data:{co' +
                    'untByVpcId:1,vpcIds:[e.vpcId]}}).then(function(e){return e.data.vpcDetail[0].cou' +
                    'nt})},m=function(e){return n.request({serviceType:"sqlserver",cmd:"SqlserverGetI' +
                    'nstanceList",data:{vpcId:e.vpcId}}).then(function(e){return e.data.totalCount||0' +
                    '})},h=function(e){return n.request({serviceType:"postgres",cmd:"PostgresGetInsta' +
                    'nceList",data:{vpcId:e.vpcId}}).then(function(e){return e.data.totalCount})},b=n' +
                    'ew Promise(function(e,t){var i="LB_SZJR_WHITELIST";a.queryWhiteList({whiteKey:[i' +
                    ']},function(n){var a=n[i];return a&&a.length?void e():t()},t)}).then(function(){' +
                    'return"https://buy.qcloud.com/lb?rid={{regionId}}&vpcId={{item.vpcId}}"},functio' +
                    'n(){return"http://manage.qcloud.com/shoppingcart/shop.php?tab=lb&regionId={{regi' +
                    'onId}}&vpcId={{item.vpcId}}"}),g=[{label:"基础云资源",list:[{label:"云主机",fetch:d,link' +
                    ':"/cvm?rid={{regionId}}&vpcId={{item.vpcId}}",add:"https://buy.qcloud.com/cvm?re' +
                    'gionId={{regionId}}&vpcId={{item.vpcId}}",_key:"vpcDevices"},{label:"负载均衡",fetch' +
                    ':o,link:"/loadbalance?rid={{regionId}}&vpcId={{item.vpcId}}&unVpcId={{item.unVpc' +
                    'Id}}",checkAdd:function(e,t){return b.then(function(e){t.add=e}),Promise.resolve' +
                    '()},add:"http://manage.qcloud.com/shoppingcart/shop.php?tab=lb&regionId={{region' +
                    'Id}}&vpcId={{item.vpcId}}"}]},{label:"数据库",list:[{label:"MySQL",link:"/cdb?regio' +
                    'nId={{regionId}}&vpcId={{item.vpcId}}&unVpcId={{item.unVpcId}}",add:"https://buy' +
                    '.qcloud.com/cdb?regionId={{regionId}}&vpcId={{item.vpcId}}&unVpcId={{item.unVpcI' +
                    'd}}",fetch:l},{label:"SQL Server",fetch:m,link:"/sqlserver?regionId={{regionId}}' +
                    '&vpcId={{item.vpcId}}",add:"https://buy.qcloud.com/sqlserver?regionId={{regionId' +
                    '}}&vpcId={{item.vpcId}}&unVpcId={{item.unVpcId}}"},{label:"TDSQL",fetch:f,link:"' +
                    '/tdsql?regionId={{regionId}}&vpcId={{item.vpcId}}",add:"https://buy.qcloud.com/t' +
                    'dsql?regionId={{regionId}}&vpcId={{item.vpcId}}&unVpcId={{item.unVpcId}}"},{labe' +
                    'l:"PostgreSQL",fetch:h,link:"/pgsql?regionId={{regionId}}&vpcId={{item.vpcId}}",' +
                    'add:"/pgsql?regionId={{regionId}}&vpcId={{item.vpcId}}"},{label:"云缓存Memcached",f' +
                    'etch:p,link:"/cmem?regionId={{regionId}}&vpcId={{item.vpcId}}",add:"http://manag' +
                    'e.qcloud.com/shoppingcart/shop.php?tab=cmem&regionId={{regionId}}&vpcId={{item.v' +
                    'pcId}}"}]},{label:"网络资源",list:[{label:"子网",fetch:d,_key:"subnetNum",link:"/vpc/s' +
                    'ubnet?unVpcId={{item.unVpcId}}&rid={{regionId}}",add:"/vpc/subnet?unVpcId={{item' +
                    '.unVpcId}}&rid={{regionId}}"},{label:"路由表",fetch:d,_key:"rtbNum",link:"/vpc/rout' +
                    'e?unVpcId={{item.unVpcId}}&rid={{regionId}}",add:"/vpc/route?unVpcId={{item.unVp' +
                    'cId}}&rid={{regionId}}"},{label:"对等连接",fetch:d,_key:"vpcPeerNum",link:"/vpc/conn' +
                    '?rid={{regionId}}&unVpcId={{item.unVpcId}}",add:"/vpc/conn?rid={{regionId}}&unVp' +
                    'cId={{item.unVpcId}}"},{label:"基础网络互通",fetch:c,_key:"classicLinkNum",link:"/vpc/' +
                    'vpc?rid={{regionId}}&unVpcId={{item.unVpcId}}&tab=2",add:"/vpc/vpc?rid={{regionI' +
                    'd}}&unVpcId={{item.unVpcId}}&tab=2",checkAdd:r},{label:"NAT网关",fetch:v,link:"/vp' +
                    'c/nat?rid={{regionId}}&unVpcId={{item.unVpcId}}",add:"/vpc/nat?rid={{regionId}}&' +
                    'unVpcId={{item.unVpcId}}"},{label:"VPN网关",fetch:d,_key:"vpnGwNum",link:"/vpc/vpn' +
                    'Gw?unVpcId={{item.unVpcId}}&rid={{regionId}}",add:"/vpc/vpnGw?unVpcId={{item.unV' +
                    'pcId}}&rid={{regionId}}"},{label:"VPN运维网关",fetch:u,link:"/sslvpn/vpn?rid={{regio' +
                    'nId}}&unVpcId={{item.unVpcId}}",add:"/sslvpn/vpn?rid={{regionId}}&unVpcId={{item' +
                    '.unVpcId}}"},{label:"专线网关",fetch:d,_key:"vpgNum",link:"/vpc/dcGw?rid={{regionId}' +
                    '}&unVpcId={{item.unVpcId}}",add:"/vpc/dcGw?rid={{regionId}}&unVpcId={{item.unVpc' +
                    'Id}}"}]}];i.exports=g}),define("modules/vpc/vpnConn/AddVpnConn",function(e,t,i){' +
                    'e("../IPInput");var n,a=e("$"),s=(e("../BeeBase"),e("widget/detail/detail")),d=e' +
                    '("appUtil"),o=e("dialog"),c=e("../simple-vpc-selector"),r=e("widget/region/regio' +
                    'nPills"),l=e("../vpnGw/DropDown"),p=e("../userGw/DropDown"),u=e("./SPDEditor"),v' +
                    '=e("widget/baseInfo/validator"),f=e("./validator"),m=e("../cacher"),h=e("../util' +
                    '"),b=(e("constants"),e("./options")),g=e("../DropDown"),I=e("widget/step/steps")' +
                    ',w=e("widget/popover/HelpTips"),y=function(e,t){return e?(t=t||255,e.length>t?"长' +
                    '度不能超过"+t+"字符":/[\?\"\\ ]/.test(e)?'不能含有空格、“?”、“"”、“\\”等特殊字符':/^[\x20-\x7e]+$/.te' +
                    'st(e)?void 0:"只能包含ASCII可见字符"):"不能为空"},x=s.extend({$afterInit:function(){x.__supe' +
                    'r__.$afterInit.call(this);var e=this.$refs.steps;e.$watch("conn.userGwId",functi' +
                    'on(){var t=e.$refs.steps[0].$refs.userGw.getOption();t&&e.$set("userGwAddress",t' +
                    '.userGwAddress)}),e.$watch("[userGwAddress, conn.userGwAddr, createUserGw]",func' +
                    'tion(){e.$set("conn.remoteAddress",e.createUserGw?e.conn.userGwAddr:e.userGwAddr' +
                    'ess)}),e.$watch("conn.vpnGwId",function(){var t=e.$refs.steps[0].$refs.vpnGw.get' +
                    'Option();t&&(e.$set({vpcCidr:t.vpcCIDR}),e.$set("conn.localAddress",t.vpnGwWanIp' +
                    '))}),e.$watch("curStep",function(t){e.$set({showBtns:t<e.steps.length-1,nextBtn:' +
                    '3==t?"完成":"下一步"})});var t={};e.$watch("[regionId, conn.vpcId]",function(){t={},t' +
                    'his.regionId&&this.conn&&this.conn.vpcId&&m.getVpnConnList(this.regionId,this.co' +
                    'nn.vpcId).done(function(e){e.forEach(function(e){t[e.vpnConnName]=1})})},!0);var' +
                    ' i;e.$watch("regionId",function(e){i={},e&&m.getUserGwList(e).done(function(e){e' +
                    '.forEach(function(e){i[e.userGwAddress]=1})})},!0),n=new v({errClass:"is-error",' +
                    'rules:{vpnGwId:f.vpnGwId,vpnConnName:function(e){return t&&e in t?"通道名重复":f.vpnC' +
                    'onnName(e)},preSharedKey:f.preSharedKey,address:function(e){return e?d.isValidIp' +
                    '(e)?void 0:"标识地址不合法":"标识不能为空"},fqdnName:function(e){if(!e)return"标识不能为空";var t=y' +
                    '(e);return t?"标识"+t:void 0},userGwAddr:function(e){return i&&e in i?"此IP已经在其它对端网' +
                    '关中存在":f.userGwAddr(e)},ikeSaLifetimeSeconds:f.ikeSaLifetimeSeconds.bind(f),ipsec' +
                    'SaLifetimeSeconds:f.ipsecSaLifetimeSeconds.bind(f),ipsecSaLifetimeTraffic:f.ipse' +
                    'cSaLifetimeTraffic.bind(f)}}),n.btn=e.$refs.next},back:function(e){var t=this;e?' +
                    '(t.$refs.steps.$set({conn:{},curStep:0}),history.back()):o.confirm("配置内容尚未完成，你要离' +
                    '开此页吗？",function(){t.$refs.steps.$set({conn:{},curStep:0}),history.back()},functi' +
                    'on(){},"配置未完成","","离开此页")},cancel:function(){var e=this;e.back()},confirm:functi' +
                    'on(){var e=this,t=this.$refs.steps.$data,i=a.extend({},t.conn,{spdAcl:JSON.strin' +
                    'gify(t.conn.spdAcl)});return new Promise(function(n,s){t.createUserGw?delete i.u' +
                    'serGwId:(delete i.userGwName,delete i.userGwAddr),"fqdn"===i.localIdentity?delet' +
                    'e i.localAddress:"address"===i.localIdentity&&delete i.localFqdnName,"fqdn"===i.' +
                    'remoteIdentity?delete i.remoteAddress:"address"===i.remoteIdentity&&delete i.rem' +
                    'oteFqdnName,i.ikeSaLifetimeSeconds||delete i.ikeSaLifetimeSeconds,i.ipsecSaLifet' +
                    'imeSeconds||delete i.ipsecSaLifetimeSeconds,i.ipsecSaLifetimeTraffic||delete i.i' +
                    'psecSaLifetimeTraffic,h.addVpnConnEx(t.regionId,i,"创建VPN通道失败！").then(function(s)' +
                    '{e.$set("item",a.extend({},t.conn,{unVpnConnId:s.uniqVpnconnId,unVpnGwId:i.vpnGw' +
                    'Id,unUserGwId:i.userGwId,unVpcId:i.vpcId})),n(s),e.onAdd&&e.onAdd(),t.createUser' +
                    'Gw&&m.resetNamespace("userGw",t.regionId)},function(e){s(e)})})},_downloadCfg:fu' +
                    'nction(){this.downloadCfg&&this.downloadCfg(this.item)},steps:[{title:"基本配置",con' +
                    'tent:'    <div class="tc-15-list-wrap vpn-gallery-form">        <ul class="form-' +
                    'list">            <li>                <div class="form-label required">         ' +
                    '           <label for="vpnConnName">通道名称</label>                </div>          ' +
                    '      <div class="form-input">                    <!--                        出错' +
                    '时候填写的class .is-error                        成功时候填写的class .is-success            ' +
                    '         -->                    <div class="form-unit" style="position:static"> ' +
                    '                       <input type="text" id="vpnConnName" name="vpnConnName"   ' +
                    '                            b-model="conn.vpnConnName"                          ' +
                    '     data-validate="vpnConnName" maxlength="25"                               cl' +
                    'ass="tc-15-input-text m" placeholder="请输入通道名称">                        <b class=' +
                    '"icon-valid-flag"></b>                        <p class="form-input-help">还能输入{{2' +
                    '5-(conn.vpnConnName.length||0)}}个字符，英文、汉字、数字、连接线"-"或下划线"_"</p>                  ' +
                    '  </div>                </div>            </li>            <li>                <' +
                    'div class="form-label">                    <label for="">地域</label>             ' +
                    '   </div>                <div class="form-input">                    <region-pil' +
                    'ls b-model="regionId"></region-pills>                </div>            </li>    ' +
                    '        <li>                <div class="form-label required">                   ' +
                    ' <label for="">私有网络</label>                </div>                <div class="for' +
                    'm-input">                    <div class="form-unit"  data-vpcselector>          ' +
                    '              <vpc-selector region-id="{{regionId}}" un-vpc-id="{{vpcId}}" $valu' +
                    'ekey="unVpcId" b-model="conn.vpcId"></vpc-selector>                        <b cl' +
                    'ass="icon-valid-flag"></b>                    </div>                </div>      ' +
                    '      </li>            <li>                <div class="form-label required">    ' +
                    '                <label for="">VPN网关</label>                </div>               ' +
                    ' <div class="form-input">                    <div class="form-unit"  data-vpnsel' +
                    'ector>                        <vpn-selector region-id="{{regionId}}" use-un-id="' +
                    '1" b-model="conn.vpnGwId"                                      select-attr="{{{' +
                    '\'data-validate\':\'vpnGwId\'}}}"                                     b-ref="vpn' +
                    'Gw" vpc-id="{{conn.vpcId}}" cls="m"></vpn-selector>                    </div>   ' +
                    '             </div>            </li>            <li>                <div class="' +
                    'form-label required">                    <label for="">对端网关</label>             ' +
                    '   </div>                <div class="form-input">                    <div class=' +
                    '"form-unit">                        <label class="form-ctrl-label"><input type="' +
                    'radio" name="vpc-conn-targetType" value="" b-model="createUserGw" class="tc-15-r' +
                    'adio">选择已有</label>                        <label class="form-ctrl-label"><input ' +
                    'type="radio" name="vpc-conn-targetType" value="1" b-model="createUserGw" class="' +
                    'tc-15-radio">新建</label>                    </div>                    <div class=' +
                    '"form-unit" b-if="!createUserGw">                        <usergw-selector cls="m' +
                    '" region-id="{{regionId}}" vpn-gw-id="{{conn.vpnGwId}}" un-vpc-id="{{conn.vpcId}' +
                    '}" use-un-id="1"                                         select-attr="{{{\'data-' +
                    'usergw\':1}}}"                                         b-ref="userGw" disable-li' +
                    'mit="true" b-model="conn.userGwId"></usergw-selector>                        <b ' +
                    'class="icon-valid-flag"></b>                    </div>                </div>    ' +
                    '        </li>            <li>                <div class="form-label {{createUser' +
                    'Gw ? \'required\' : \'\'}}">                    <label for="">对端网关IP</label>    ' +
                    '            </div>                <div class="form-input" b-if="!createUserGw"> ' +
                    '                   <p>{{userGwAddress || \'-\'}}</p>                </div>      ' +
                    '          <div class="form-input" b-if="createUserGw">                    <vpc-i' +
                    'p-input name="userGwAddr" attr="{{{\'data-usergwaddr\':\'1\'}}}" type="ip" b-mod' +
                    'el="conn.userGwAddr"></vpc-ip-input>                    <b class="icon-valid-fla' +
                    'g"></b>                </div>            </li>            <li>                <d' +
                    'iv class="form-label">                    <label for="">协议类型</label>            ' +
                    '    </div>                <div class="form-input">                    <p>IKE/IPs' +
                    'ec</p>                </div>            </li>            <li>                <di' +
                    'v class="form-label required">                    <label for="">预共享密钥</label>   ' +
                    '             </div>                <div class="form-input">                    <' +
                    '!--                        出错时候填写的class .is-error                        成功时候填写的' +
                    'class .is-success                     -->                    <div class="form-un' +
                    'it">                        <input type="text" data-validate="preSharedKey"     ' +
                    '                          b-model="conn.preSharedKey"                           ' +
                    '    class="tc-15-input-text m" placeholder="请输入预共享密钥">                        <h' +
                    'elp-tips>Unicode 字符串，本端和对端须使用相同的预共享密钥。</help-tips>                        <b cla' +
                    'ss="icon-valid-flag"></b>                    </div>                </div>       ' +
                    '     </li>        </ul>    </div>',init:function(){},confirm:function(e,t){var i' +
                    ',s,d=n.validateAll(this.$refs.steps[t].$el);return this.createUserGw?(i=n.rules.' +
                    'userGwAddr(this.conn.userGwAddr),s=a(this.$el).find("[data-usergwaddr]"),s.toggl' +
                    'eClass(n.errClass+" vpc-input-error",!!i),d&&i&&n.showErr(s,i)):(i=f.userGwId(th' +
                    'is.conn.userGwId),s=a(this.$el).find("[data-usergw]"),s.toggleClass(n.errClass+"' +
                    ' vpc-input-error",!!i),d&&i&&n.showErr(s,i)),d=i?!1:d,this.conn.vpnGwId?a(this.$' +
                    'el).find("[data-vpnselector]").removeClass(n.errClass):(d=!1,a(this.$el).find("[' +
                    'data-vpnselector]").addClass(n.errClass)),this.conn.vpcId?a(this.$el).find("[dat' +
                    'a-vpcselector]").removeClass(n.errClass):(d=!1,a(this.$el).find("[data-vpcselect' +
                    'or]").addClass(n.errClass)),d}},{title:"SPD 策略",content:'    <div class="param-b' +
                    'd">        <div class="tc-15-msg">            <div class="tip-info">SPD策略：用于指定 V' +
                    'PC 内哪些网段可以和 IDC 中哪些网段通信。</div>        </div>        <ul class="item-descr-list" ' +
                    'style="margin:20px;">            <li>                <div class="item-descr-tit ' +
                    'required">                    <label for=""><strong style="color: black">本端私有网络<' +
                    '/strong></label>                </div>                <div class="item-descr-txt' +
                    '">                    <p>{{vpcCidr}}</p>                </div>            </li> ' +
                    '       </ul>        <div>            <spd-editor b-ref="spdEditor" vpc-cidr="{{v' +
                    'pcCidr}}"></spd-editor>        </div>    </div>',confirm:function(e,t){var i,n=t' +
                    'his.$refs.steps[t],a=n.$refs.spdEditor,s=a.formatData();return a.validator.btn=t' +
                    'his.$refs.next,i=a.validate(),this.$set("conn.spdAcl",s),i}},{title:"IKE 配置（选填）"' +
                    ',content:'        <div class="tc-15-list-wrap vpn-gallery-form">            <div' +
                    ' class="head-top-title"><h3>IKE 配置</h3></div>            <ul class="form-list"> ' +
                    '               <li>                    <div class="form-label">                 ' +
                    '       <label for="">版本</label>                    </div>                    <di' +
                    'v class="form-input">                        <p>IKE V1</p>                    </' +
                    'div>                </li>                <li>                    <div class="for' +
                    'm-label">                        <label for="">身份认证方法</label>                   ' +
                    ' </div>                    <div class="form-input">                        <p>预共' +
                    '享密钥</p>                    </div>                </li>                <li>      ' +
                    '              <div class="form-label">                        <label for="">加密算法' +
                    '</label>                    </div>                    <div class="form-input">  ' +
                    '                      <div class="form-unit">                            <drop-d' +
                    'own cls="m" b-model="conn.propoEncryAlgorithm"                                  ' +
                    'b-with="{options:options.encryptAlgorithmOptions}"></drop-down>                 ' +
                    '           <b class="icon-valid-flag"></b>                        </div>        ' +
                    '            </div>                </li>                <li>                    <' +
                    'div class="form-label">                        <label for="">认证算法</label>       ' +
                    '             </div>                    <div class="form-input">                 ' +
                    '       <div class="form-unit">                            <drop-down cls="m" b-m' +
                    'odel="conn.propoAuthenAlgorithm"                                       b-with="{' +
                    'options:options.ikeHashAlgorithmOptions}"></drop-down>                          ' +
                    '  <b class="icon-valid-flag"></b>                        </div>                 ' +
                    '   </div>                </li>                <li>                    <div class' +
                    '="form-label">                        <label for="">协商模式</label>                ' +
                    '    </div>                    <div class="form-input">                        <d' +
                    'iv class="form-unit">                            <drop-down cls="m" b-model="con' +
                    'n.exchangeMode"                                  b-with="{options:options.exchan' +
                    'geModeOptions}"></drop-down>                            <b class="icon-valid-fla' +
                    'g"></b>                        </div>                    </div>                <' +
                    '/li>                <li>                    <div class="form-label">            ' +
                    '            <label for="">本端标识</label>                    </div>                ' +
                    '    <div class="form-input">                        <div class="form-unit txt-ip' +
                    '-site">                            <drop-down cls="m" b-model="conn.localIdentit' +
                    'y"                                  b-with="{options:options.identityOptions}"><' +
                    '/drop-down>                            <input class="tc-15-input-text m" b-model' +
                    '="conn.localAddress"                                   data-validate="address"  ' +
                    '                                 b-if="conn.localIdentity===\'address\'" disable' +
                    'd="disabled">                            <input class="tc-15-input-text m" b-mod' +
                    'el="conn.localFqdnName"                                   data-validate="fqdnNam' +
                    'e"                                   b-if="conn.localIdentity===\'fqdn\'">      ' +
                    '                      <help-tips b-if="conn.localIdentity===\'fqdn\'">本端全称域名，例如：' +
                    'www.qcloud.com</help-tips>                        </div>                    </di' +
                    'v>                </li>                <li>                    <div class="form-' +
                    'label">                        <label for="">远端标识</label>                    </d' +
                    'iv>                    <div class="form-input">                        <div clas' +
                    's="form-unit txt-ip-site">                            <drop-down b-model="conn.r' +
                    'emoteIdentity" cls="m"                                  b-with="{options:options' +
                    '.identityOptions}"></drop-down>                            <input class="tc-15-i' +
                    'nput-text m" b-model="conn.remoteAddress"                                   data' +
                    '-validate="address"                                   b-if="conn.remoteIdentity=' +
                    '==\'address\'" disabled="disabled">                            <input class="tc-' +
                    '15-input-text m" b-model="conn.remoteFqdnName"                                  ' +
                    ' data-validate="fqdnName"                                   b-if="conn.remoteIde' +
                    'ntity===\'fqdn\'">                            <help-tips b-if="conn.remoteIdenti' +
                    'ty===\'fqdn\'">对端全称域名，例如：www.company.com</help-tips>                        </di' +
                    'v>                    </div>                </li>                <li>           ' +
                    '         <div class="form-label">                        <label for="">DH group<' +
                    '/label>                    </div>                    <div class="form-input">   ' +
                    '                     <div class="form-unit">                            <drop-do' +
                    'wn cls="m" b-model="conn.dhGroupName"                                       b-wi' +
                    'th="{options:options.dhGroupOptions}"></drop-down>                            <h' +
                    'elp-tips>密钥交换的安全性随着 DH 组的扩大而增大，但交换的时间也会增加</help-tips>                           ' +
                    ' <b class="icon-valid-flag"></b>                        </div>                  ' +
                    '  </div>                </li>                <li>                    <div class=' +
                    '"form-label">                        <label for="">IKE SA Lifetime</label>      ' +
                    '              </div>                    <div class="form-unit is-success">      ' +
                    '                  <input class="tc-15-input-text m" placeholder="86400"         ' +
                    '                      data-validate="ikeSaLifetimeSeconds" b-model="conn.ikeSaLi' +
                    'fetimeSeconds">                        <span class="units">s</span>             ' +
                    '       </div>                </li>            </ul>        </div>',
confirm:func' +
                    'tion(e,t){var i=this.$refs.steps[t],a=n.validateAll(i.$el);return a}},{title:"IP' +
                    'sec 配置（选填）",content:'    <div class="tc-15-list-wrap vpn-gallery-form">        <' +
                    'div class="head-top-title"><h3>IPsec 信息</h3></div>        <ul class="form-list">' +
                    '            <li>                <div class="form-label">                    <lab' +
                    'el for="">加密算法</label>                </div>                <div class="form-inp' +
                    'ut">                    <div class="form-unit">                        <drop-dow' +
                    'n cls="m" b-model="conn.encryptAlgorithm"                                   b-wi' +
                    'th="{options:options.encryptAlgorithmOptions}" ></drop-down>                    ' +
                    '    <b class="icon-valid-flag"></b>                    </div>                </d' +
                    'iv>            </li>            <li>                <div class="form-label">    ' +
                    '                <label for="">认证算法</label>                </div>                ' +
                    '<div class="form-input">                    <div class="form-unit">             ' +
                    '           <drop-down cls="m" b-model="conn.integrityAlgorith"                  ' +
                    '                 b-with="{options:options.ipsecHashAlgorithmOptions}" ></drop-do' +
                    'wn>                        <b class="icon-valid-flag"></b>                    </' +
                    'div>                </div>            </li>            <li>                <div ' +
                    'class="form-label">                    <label for="">报文封装模式</label>             ' +
                    '   </div>                <div class="form-input">                    <p>Tunnel</' +
                    'p>                </div>            </li>            <li>                <div cl' +
                    'ass="form-label">                    <label for="">安全协议</label>                <' +
                    '/div>                <div class="form-input">                    <p>ESP</p>     ' +
                    '           </div>            </li>            <li>                <div class="fo' +
                    'rm-label">                    <label for="">PFS</label>                </div>   ' +
                    '             <div class="form-input">                    <div class="form-unit">' +
                    '                        <drop-down cls="m" b-model="conn.pfsDhGroup"            ' +
                    '                       b-with="{options:options.pfsOptions}" ></drop-down>      ' +
                    '              </div>                </div>            </li>            <li>     ' +
                    '           <div class="form-label">                    <label for="">IPsec sa Li' +
                    'fetime(s)</label>                </div>                <div class="form-unit is-' +
                    'success">                    <input class="tc-15-input-text m" data-validate="ip' +
                    'secSaLifetimeSeconds"                           b-model="conn.ipsecSaLifetimeSec' +
                    'onds" placeholder="3600">                    <span class="units">s</span>       ' +
                    '         </div>            </li>            <li>                <div class="form' +
                    '-label">                    <label for="">IPsec sa Lifetime(KB)</label>         ' +
                    '       </div>                <div class="form-unit is-success">                 ' +
                    '   <input class="tc-15-input-text m" placeholder="1843200"                      ' +
                    '     data-validate="ipsecSaLifetimeTraffic" b-model="conn.ipsecSaLifetimeTraffic' +
                    '">                    <span class="units">KB</span>                </div>       ' +
                    '     </li>        </ul>    </div>',confirm:function(e,t){var i=this.$refs.steps[' +
                    't],a=n.validateAll(i.$el);return a&&this.confirm()}},{title:"完成配置",content:'    ' +
                    '<div class="tc-15-list-wrap ">        <div class="operation-tips-box">          ' +
                    '  <i class="b-success-icon"></i>            <div class="operation-tip-text">    ' +
                    '            <strong class="operating-tips">创建成功</strong>                <p class' +
                    '="operating-txt">您的VPN通道已创建成功，接下来您需要下载配置文件完成最后的配置，使VPN连接连通。<br>                 ' +
                    ' <!-- <a href="javascript:;">更多帮助</a> -->                </p>            </div> ' +
                    '       </div>        <div class="operation-btn-wrap">            <button type="b' +
                    'utton" b-on-click="_downloadCfg()" class="tc-15-btn">下载配置文件</button>            ' +
                    '<a class="return-btn" b-on-click="back(true)" href="javascript:;">返回VPN通道</a>   ' +
                    '     </div>    </div>'}]},{defaults:{title:"新建 VPN 通道",content:'<add-steps regio' +
                    'n-id="{{regionId}}" vpc-id="{{vpcId}}" options="{{options}}" steps="{{steps}}" b' +
                    '-ref="steps" back="{{back.bind(this)}}"confirm="{{confirm.bind(this)}}" _downloa' +
                    'd-cfg="{{_downloadCfg.bind(this)}}" cancel="{{cancel.bind(this)}}"></add-steps>'' +
                    ',headerTpl:'    <div class="return-title-panel bottom-border secondary-title" st' +
                    'yle="border-bottom:none">        <style>          .tc-15-step.col6 li {         ' +
                    '   min-width: 185px;          }        </style>        <a href="javascript:;" b-' +
                    'on-click="back()" class="btn-return">            <i class="btn-back-icon"></i>  ' +
                    '          <span> 返回</span>        </a>        <i class="line"></i>        <h2>{{' +
                    'title}}</h2>    </div>',options:b}});x.tag("add-steps",I),x.tag("vpc-selector",c' +
                    '),x.tag("region-pills",r.extend({getWhiteList:function(){return m.getRegionIdLis' +
                    't()}})),x.tag("vpn-selector",l),x.tag("usergw-selector",p),x.tag("spd-editor",u.' +
                    'extend({getData:function(e,t){var i=this;(t=function(e,t){i.setData({list:[{}],t' +
                    'otalNum:0})})(null)}})),x.tag("drop-down",g),x.tag("help-tips",w),i.exports=x}),' +
                    'define("modules/vpc/vpnConn/Chart",function(e,t,i){e("Highcharts");var n,a=e("..' +
                    '/BeeBase"),s=e("manager"),d=e("appUtil"),o=e("../util"),c="vpnConnId",r=864e5,l=' +
                    '{inChart:{viewName:"vpn_tunnel",type:"vpninbps",unit:"Mbps",isMbps:!0},outChart:' +
                    '{viewName:"vpn_tunnel",type:"vpnoutbps",unit:"Mbps",isMbps:!0}},p={chart:{zoomTy' +
                    'pe:"x"},title:{text:""},yAxis:{title:{text:""},lineWidth:1,tickPixelInterval:45,' +
                    'min:0,labels:{style:{fontSize:"12px"}}},plotOptions:{line:{marker:{enabled:!0,li' +
                    'neWidth:2,lineColor:"#ff7a22",fillColor:"#FFFFFF",radius:3,states:{hover:{enable' +
                    'd:!0}}},lineWidth:1,color:"#ff7a22"},spline:{marker:{enabled:!1,radius:3,states:' +
                    '{hover:{enabled:!0}}},lineWidth:1,color:"#ff7a22"}},credits:{enabled:!1},legend:' +
                    '{enabled:!1},tooltip:{}},u=function(e){var t="0",i=e.getFullYear(),n=e.getMonth(' +
                    ')+1,a=e.getDate();return[i,(10>n?t:"")+n,(10>a?t:"")+a].join("-")},v=function(e)' +
                    '{var t=e.split("-"),i=new Date(0);return i.setFullYear(+t[0],+t[1]-1,+t[2]),i};n' +
                    '=a.extend({$valuekey:c,$beforeInit:function(){var e=this;e.actionHandler=functio' +
                    'n(){e.handleAction(this.name,this)};var t=new Date,i=new Date(t.getTime()-r),n=n' +
                    'ew Date(t.getTime()-7*r),s=new Date(t.getTime()-15*r),d=new Date(t.getTime()-30*' +
                    'r);e.datePickerConfig={showCalendar:!0,lang:"zh-cn",tabs:[{label:"实时",from:u(t),' +
                    'to:u(t)},{label:"近7天",from:u(n),to:u(i)},{label:"近15天",from:u(s),to:u(i)},{label' +
                    ':"近30天",from:u(d),to:u(i)}],range:{min:u(d),max:u(t)},selected:{from:u(t),to:u(t' +
                    ')}},a.prototype.$beforeInit.apply(this,arguments)},$afterInit:function(){a.proto' +
                    'type.$afterInit.apply(this,arguments);var e=this,t=function(){e.regionId&&e[c]&&' +
                    'e.loadData()},i=e.$refs.datePicker;i.setSelectedRange(i.selected.from,i.selected' +
                    '.to),i.$on("datepick",t),e.$watch(c,t,!0)},$tpl:'<div class="tc-15-details-panel' +
                    ' vpn-flow-wrap" b-on="events" b-style="{display: !vpnConnId ? \'none\' : \'block' +
                    '\'}">        <div class="tc-15-details-panel-inner">            <div class="tc-1' +
                    '5-details-panel-hd">                <h3 class="run-in">{{data && data.vpnConnNam' +
                    'e}}监控</h3>                <a href="#" data-action="return" role="button" class="' +
                    'btn-details-panel-dismiss" data-title="关闭">关闭</a>            </div>            <' +
                    'div class="tc-15-details-panel-bd">                <!-- 滚动条区域 开始 -->            ' +
                    '    <div class="scroll-area">                    <div class="param-box">        ' +
                    '                <div class="param-bd">                        <span b-tag="qc-da' +
                    'te-picker" b-ref="datePicker" b-with="datePickerConfig"></span>                 ' +
                    '           <div class="tc-15-data-graph" style="position:absolute;top:115px;bott' +
                    'om:0px;left:10px;right:10px;padding:0 20px;overflow-x:hidden;overflow-y:auto;"> ' +
                    '                               <ul>                                    <li class' +
                    '="line-partition">                                        <div class="tc-15-data' +
                    '-graph-title"><strong>总出带宽</strong><em>(单位：Mbps)</em></div>                     ' +
                    '                   <div class="tc-15-data-graph-info" b-tag="chart" day-view="{{' +
                    'dayView}}" b-model="outChart"></div>                                    </li>   ' +
                    '                                 <li class="line-partition">                    ' +
                    '                    <div class="tc-15-data-graph-title"><strong>总入带宽</strong><em' +
                    '>(单位：Mbps)</em></div>                                        <div class="tc-15-d' +
                    'ata-graph-info" b-tag="chart" day-view="{{dayView}}" b-model="inChart"></div>   ' +
                    '                                 </li>                                    <!--<l' +
                    'i class="line-partition">-->                                        <!--<div cla' +
                    'ss="tc-15-data-graph-title"><strong>延时</strong><em>(单位：ms)</em></div>-->        ' +
                    '                                <!--<div class="tc-15-data-graph-info" b-tag="ch' +
                    'art" day-view="{{dayView}}" b-model="delayChart"></div>-->                      ' +
                    '              <!--</li>-->                                    <!--<li>-->       ' +
                    '                                 <!--<div class="tc-15-data-graph-title"><strong' +
                    '>丢包率</strong><em>(单位：%)</em></div>-->                                        <!-' +
                    '-<div class="tc-15-data-graph-info" b-tag="chart" day-view="{{dayView}}" b-model' +
                    '="dropChart"></div>-->                                    <!--</li>-->          ' +
                    '                      </ul>                            </div>                   ' +
                    '     </div>                    </div>                </div>                <!-- ' +
                    '滚动条区域 结束 -->            </div>        </div>    </div>',events:{"click [data-act' +
                    'ion]":function(e){var t=$(e.currentTarget);this.handleAction(t.attr("data-action' +
                    '"),null,e),e.preventDefault()}},setRange:function(e,t){var i=this,n=i.data,a=o.t' +
                    'oDayStart(v(e)),d=o.toDayEnd(v(t)),p=d.getTime()-a.getTime()>r;i.$set({inChart:"' +
                    '",outChart:"",dayView:p}),Object.keys(l).forEach(function(e){var t=l[e];s.loadVp' +
                    'nChartData({regionId:i.regionId,vpcId:n.vpcId,vpnGwId:n.vpnGwId,vpnConnId:i[c],v' +
                    'iewName:t.viewName,fromTime:Math.round(a.getTime()/1e3),toTime:Math.round(d.getT' +
                    'ime()/1e3),ioType:t.type},function(n){for(var a=!1,s=0,d=n.y.length;d>s;s++)n.y[' +
                    's]&&t.isMbps&&(n.y[s]=1*(n.y[s]/1e3).toFixed(3)),(n.y[s]||0===n.y[s])&&(a=!0);a|' +
                    '|(n.y=[]),n.unit=t.unit,i.$set(e,n)},function(){i.$set(e,{y:[]})})})},handleActi' +
                    'on:function(e,t,i){var n=this;switch(e){case"edit":break;case"return":n.$set(c,"' +
                    '")}},loadData:function(){var e=this.$refs.datePicker.selected;this.setRange(e.fr' +
                    'om,e.to)}});var f=a.extend({$tpl:'<div class="tc-15-data-graph-info" style="posi' +
                    'tion:relative;"><div data-mod="chart"></div><span b-if="!data || noData" style="' +
                    'position:absolute;top:50%;left:50%;margin-top:-12px;margin-left:-24px;">        ' +
                    '    {{data ? \'没有数据\' : \'加载中...\'}}        </span></div>',$valuekey:"data",$aft' +
                    'erInit:function(){a.prototype.$afterInit.apply(this,arguments);var e=this,t=func' +
                    'tion(){if(!e.data){var t=e.$chart.highcharts();return void(t&&t.destroy())}e.sho' +
                    'wChart()};e.$chart=$(e.$el).find('[data-mod="chart"]'),e.$watch("data",t,!0)},sh' +
                    'owChart:function(){var e,t=this,i=t.data,n=!!t.dayView,a=!0,s=t.$chart,o={xAxis:' +
                    '{type:"datetime",dateTimeLabelFormats:{hour:"%H:%M"},tickPixelInterval:80,title:' +
                    '{text:""}}};i.y.length&&($.extend(o,p),e=d.cgwTimeParse(i.x[0]),o.series=[{point' +
                    'Start:e.getTime()-6e4*e.getTimezoneOffset(),data:i.y}],n?(o.series[0].pointInter' +
                    'val=864e5,o.xAxis.dateTimeLabelFormats.day="%m-%d",o.xAxis.dateTimeLabelFormats.' +
                    'week="%m-%d",o.tooltip.formatter=function(){var e=new Date(this.x);return e=new ' +
                    'Date(e.getTime()+6e4*e.getTimezoneOffset()),[d.formatDate(e,"MM-dd")," ","<b>",t' +
                    'his.y,i.unit,"</b>"].join("")}):(o.series[0].pointInterval=3e5,o.series[0].type=' +
                    '"spline",o.xAxis.dateTimeLabelFormats.day="%H:%M",o.xAxis.maxZoom=36e5,o.tooltip' +
                    '.formatter=function(){var e=new Date(this.x);return e=new Date(e.getTime()+6e4*e' +
                    '.getTimezoneOffset()),[d.formatDate(e,"hh:mm")," ","<b>",this.y,i.unit,"</b>"].j' +
                    'oin("")}),s.highcharts(o),a=!1),this.$set("noData",a)}});n.tag("chart",f),i.expo' +
                    'rts=n}),define("modules/vpc/vpnConn/Detail",function(e,t,i){e("Highcharts");var ' +
                    'n,a=e("../BeeBase"),s=e("../DropDown"),d=e("appUtil"),o=e("../cacher"),c=e("../c' +
                    'Api"),r=e("../util"),l=e("./validator"),p=e("dialog"),u="unVpnConnId",v=e("./opt' +
                    'ions"),f=e("./SPDEditor"),m=v.encryptAlgorithmOptions,h=v.encryptAlgorithmOption' +
                    'sMap,b=v.ikeHashAlgorithmOptions,g=v.ikeHashAlgorithmOptionsMap,I=v.ipsecHashAlg' +
                    'orithmOptions,w=v.ipsecHashAlgorithmOptionsMap,y=v.dhGroupOptions,x=v.dhGroupOpt' +
                    'ionsMap,C=v.pfsOptions,T=v.pfsOptionsMap,N=v.exchangeModeOptions,k=v.exchangeMod' +
                    'eOptionsMap,P=v.identityOptions,D=v.identityOptionsMap;n=a.extend({$valuekey:u,o' +
                    'ptions:{ike:{propoEncryAlgorithm:m,propoAuthenAlgorithm:b,exchangeMode:N,identit' +
                    'y:P,dhGroupName:y},ipsec:{encryptAlgorithm:m,integrityAlgorith:I,pfsDhGroup:C}},' +
                    'optionsMap:{ike:{propoEncryAlgorithm:h,propoAuthenAlgorithm:g,exchangeMode:k,ide' +
                    'ntity:D,dhGroupName:x},ipsec:{encryptAlgorithm:h,integrityAlgorith:w,pfsDhGroup:' +
                    'T}},optionText:function(e,t){return t in e?e[t]:t},$beforeInit:function(){var e=' +
                    'this;e.actionHandler=function(){e.handleAction(this.name,this)},a.prototype.$bef' +
                    'oreInit.apply(this,arguments)},$afterInit:function(){a.prototype.$afterInit.appl' +
                    'y(this,arguments);var e=this,t=function(){return e.regionId&&e[u]?(e.$set("remot' +
                    'eSlaIpMeta",r.getRemoteSlaIpMeta(e.regionId)),e.$set("notFound",!1),void e.loadD' +
                    'ata()):void e.$set({basicEditing:!1,ikeEditing:!1,ipsecEditing:!1})};e.$watch(u,' +
                    't,!0),e.$watch("event_update",function(){e.changeName||p.create("  VPN通道配置修改后，请重' +
                    '新下载配置文件并更新本地数据中心VPN通道配置策略，通道参数不一致可能会使您的VPN通道不稳定或无法连通。",560,"",{title:"VPN通道配置修改完' +
                    '成",button:{"下载配置文件":function(){e.downloadCfg(e.data||e.event_update)}}})})},$tpl' +
                    ':'    <div b-on="events" style="position:absolute;left:0;top:0;height:100%;width' +
                    ':100%;"         b-style="{display: !unVpnConnId ? \'none\' : \'block\'}" class="' +
                    'vpn-connect-wrap">        <div b-if="!notFound" class="return-title-panel bottom' +
                    '-border">            <a class="btn-return" data-action="return" href="#"><i clas' +
                    's="btn-back-icon"></i><span>返回</span></a>            <i class="line"></i>       ' +
                    '     <h2>{{data&&data.vpnConnName}}详情</h2>            <div class="manage-area-ti' +
                    'tle-right">                <a href="https://www.qcloud.com/doc/product/215/4956"' +
                    ' target="_blank">                    <b class="links-icon"></b> VPN连接帮助文档       ' +
                    '         </a>            </div>        </div>        <div class="tab-switch">   ' +
                    '         <a href="javascript:;" b-on-click="$set(\'tab\', 0)" title="基本信息" class' +
                    '="{{tab==0 ? \'cur\': \'\'}}">基本信息</a>            <a href="javascript:;" b-on-cl' +
                    'ick="$set(\'tab\', 1)" title="高级配置" class="{{tab==1 ? \'cur\': \'\'}}">高级配置</a> ' +
                    '       </div>        <div b-if="loading">加载中...</div>        <div b-if="notFound' +
                    '" style="text-align:center; padding:100px;">未查询到指定VPN通道, <a href="javascript:;" ' +
                    'b-on-click="$set(\'unVpnConnId\', \'\')">返回列表</a></div>        <div b-style="{di' +
                    'splay : loading || notFound || tab != 0 ? \'none\' : \'\'}">        <div class="' +
                    'tc-15-list-wrap private-network-inf">            <div class="tc-15-list-legend">' +
                    '基本信息                <span class="title-edit" b-if="!basicEditing" data-action="b' +
                    'asicEdit"><i                        class="pencil-icon hover-icon"></i><a       ' +
                    '                 href="javascript:;">编辑</a></span>            </div>            ' +
                    '<div class="tc-15-list-content" data-form="basic">                <ul>          ' +
                    '          <li>                        <em class="tc-15-list-tit">VPN通道名称</em>   ' +
                    '                     <span class="tc-15-list-det" b-if="!basicEditing">{{data.vp' +
                    'nConnName}}</span>                        <span class="tc-15-list-det" b-if="bas' +
                    'icEditing">                            <input type="text" class="tc-15-input-tex' +
                    't s" data-name="vpnConnName" b-model="basicForm.vpnConnName">                   ' +
                    '     </span>                    </li>                    <li>                   ' +
                    '     <em class="tc-15-list-tit">VPN通道ID</em>                        <span class=' +
                    '"tc-15-list-det">{{data.unVpnConnId}}</span>                    </li>           ' +
                    '         <li>                        <em class="tc-15-list-tit">协议类型</em>       ' +
                    '                 <span class="tc-15-list-det">{{data.vpnProto}}</span>          ' +
                    '          </li>                    <li>                        <em class="tc-15-' +
                    'list-tit">VPN网关</em>                        <span class="tc-15-list-det">{{data.' +
                    'vpnGwName}}</span>                    </li>                    <li>             ' +
                    '           <em class="tc-15-list-tit">所属网络</em>                        <span cla' +
                    'ss="tc-15-list-det">{{data.vpcName}}({{data.vpcCIDR}})</span>                   ' +
                    ' </li>                    <li>                        <em class="tc-15-list-tit"' +
                    '>预共享密钥</em>                        <span class="tc-15-list-det" b-if="!basicEdit' +
                    'ing">{{data.preSharedKey}}</span>                        <span class="tc-15-list' +
                    '-det" b-if="basicEditing"><input type="text" class="tc-15-input-text s"         ' +
                    '                                                                       data-name' +
                    '="preSharedKey"                                                                 ' +
                    '               b-model="basicForm.preSharedKey"></span>                    </li>' +
                    '                    <!--<li>-->                        <!--<em class="tc-15-list' +
                    '-tit">本端网段</em>-->                        <!--<span class="tc-15-list-det" style' +
                    '="min-width:600px;width:60%;overflow:hidden;">{{data.snet}}</span>-->           ' +
                    '         <!--</li>-->                    <li>                        <em class="' +
                    'tc-15-list-tit">对端网关</em>                        <span class="tc-15-list-det">{{' +
                    'data.usrGwName}}</span>                    </li>                    <li>        ' +
                    '                <em class="tc-15-list-tit">创建时间</em>                        <spa' +
                    'n class="tc-15-list-det">{{data.createTime}}</span>                    </li>    ' +
                    '                <!--<li>-->                        <!--<em class="tc-15-list-tit' +
                    '">对端SLA IP</em>-->                        <!--<span class="tc-15-list-det" b-if=' +
                    '"!basicEditing">{{data.remoteSlaIp}}</span>-->                        <!--<span ' +
                    'class="tc-15-list-det" b-if="basicEditing"><span b-tag="vpc-ip-input"-->        ' +
                    '                                               <!--name="remoteSlaIp"-->        ' +
                    '                                               <!--b-ref="remoteSlaIp"-->       ' +
                    '                                                <!--net="{{remoteSlaIpMeta.net}}' +
                    '" min-mask="{{remoteSlaIpMeta.minMask}}"-->                                     ' +
                    '                  <!--b-model="basicForm.remoteSlaIp"></span></span>-->         ' +
                    '           <!--</li>-->                    <!--<li>-->                        <!' +
                    '--<em class="tc-15-list-tit">对端网段</em>-->                        <!--<span class' +
                    '="tc-15-list-det" style="min-width:600px;width:60%;overflow:hidden;" b-if="!basi' +
                    'cEditing">{{data.dnet}}</span>-->                        <!--<span class="tc-15-' +
                    'list-det" b-if="basicEditing"><textarea class="textarea" placeholder=""-->      ' +
                    '                                                                             <!-' +
                    '-data-name="userGwSubnetList"-->                                                ' +
                    '                                   <!--b-model="basicForm.dnet"></textarea></spa' +
                    'n>-->                    <!--</li>-->                    <li b-if="basicEditing"' +
                    '>                        <em class="tc-15-list-tit">&nbsp;</em>                 ' +
                    '       <button class="tc-15-btn {{spdSubmitting || basicSubmitting || ikeSubmitt' +
                    'ing || ipsecSubmitting ? \'disabled\' : \'\'}}" data-action="basicSave">        ' +
                    '                    <i b-if="basicSubmitting" class="n-loading-icon"></i>保存</but' +
                    'ton>                        <button class="tc-15-btn weak {{basicSubmitting ? \'' +
                    'disabled\' : \'\'}}" data-action="basicCancel">取消</button>                    </' +
                    'li>                </ul>            </div>        </div>            <div class="' +
                    'tc-15-list-wrap private-network-inf">                <div class="tc-15-list-lege' +
                    'nd">SPD策略                <span class="title-edit" b-if="!spdEditing" data-action' +
                    '="spdEdit"><i                        class="pencil-icon hover-icon"></i><a      ' +
                    '                  href="javascript:;">编辑</a></span>                    <spd-grid' +
                    ' b-if="!spdEditing && data.spdAcl" has-first="{{false}}"                        ' +
                    '      total-num="{{data.spdAcl.length}}" list="{{data.spdAcl.slice()}}"></spd-gr' +
                    'id>                    <spd-editor b-if="spdEditing" vpc-cidr="{{data.vpcCIDR}}"' +
                    ' b-ref="spdAclEditor"                                total-num="{{data._spdAcl.l' +
                    'ength}}" list="{{data._spdAcl.slice()}}"></spd-editor>                </div>    ' +
                    '        </div>            <div style="margin-bottom: 1em;">                <ul> ' +
                    '                   <li b-if="spdEditing">                        <em class="tc-1' +
                    '5-list-tit">&nbsp;</em>                        <button class="tc-15-btn {{spdSub' +
                    'mitting || basicSubmitting || ikeSubmitting || ipsecSubmitting ? \'disabled\' : ' +
                    '\'\'}}"                                data-action="spdSave">                   ' +
                    '         <i b-if="spdSubmitting" class="n-loading-icon"></i>保存</button>         ' +
                    '               <button class="tc-15-btn weak {{spdSubmitting ? \'disabled\' : \'' +
                    '\'}}" data-action="spdCancel">取消</button>                    </li>              ' +
                    '  </ul>            </div>        </div>        <div b-style="{display : loading ' +
                    '|| notFound || tab != 1 ? \'none\' : \'\'}">        <div class="tc-15-list-wrap ' +
                    'private-network-inf">            <div class="tc-15-list-legend">IKE配置           ' +
                    '     <span class="title-edit" b-if="!ikeEditing" data-action="ikeEdit"><i class=' +
                    '"pencil-icon hover-icon"></i><a                        href="">编辑</a></span>    ' +
                    '        </div>            <div class="tc-15-list-content" data-form="ike">      ' +
                    '          <ul>                    <li>                        <em class="tc-15-l' +
                    'ist-tit">版本</em>                        <span class="tc-15-list-det">{{data.IKEA' +
                    'rr.IKEversion || \'IKEv1\'}}</span>                    </li>                    ' +
                    '<li>                        <em class="tc-15-list-tit">身份认证法</em>               ' +
                    '         <span class="tc-15-list-det">{{data.IKEArr.propoAuthenMethod || \'预共享密钥' +
                    '\'}}</span>                    </li>                    <li>                    ' +
                    '    <em class="tc-15-list-tit">加密算法</em>                        <span class="tc-' +
                    '15-list-det" b-if="!ikeEditing">{{optionText(optionsMap.ike.propoEncryAlgorithm,' +
                    ' data.IKEArr.propoEncryAlgorithm)}}</span>                        <span class="t' +
                    'c-15-list-det" b-if="ikeEditing">                            <span b-tag="vpc-dr' +
                    'opdown" b-model="ikeForm.propoEncryAlgorithm"                                  b' +
                    '-with="{options:options.ike.propoEncryAlgorithm}"></span>                       ' +
                    ' </span>                    </li>                    <li><em class="tc-15-list-t' +
                    'it">认证算法</em>                        <span class="tc-15-list-det" b-if="!ikeEdit' +
                    'ing">{{optionText(optionsMap.ike.propoAuthenAlgorithm, data.IKEArr.propoAuthenAl' +
                    'gorithm)}}</span>                        <span class="tc-15-list-det" b-if="ikeE' +
                    'diting">                            <span b-tag="vpc-dropdown" b-model="ikeForm.' +
                    'propoAuthenAlgorithm"                                  b-with="{options:options.' +
                    'ike.propoAuthenAlgorithm}"></span>                        </span>               ' +
                    '     </li>                    <li>                        <em class="tc-15-list-' +
                    'tit">协商模式</em>                        <span class="tc-15-list-det" b-if="!ikeEdi' +
                    'ting">{{optionText(optionsMap.ike.exchangeMode, data.IKEArr.exchangeMode)}}</spa' +
                    'n>                        <span class="tc-15-list-det" b-if="ikeEditing">       ' +
                    '                     <span b-tag="vpc-dropdown" b-model="ikeForm.exchangeMode"  ' +
                    '                                b-with="{options:options.ike.exchangeMode}"></sp' +
                    'an>                        </span>                    </li>                    <' +
                    'li>                        <em class="tc-15-list-tit">本端标识</em>                 ' +
                    '       <span class="tc-15-list-det" b-if="!ikeEditing && data.IKEArr.localIdenti' +
                    'ty===\'fqdn\'">{{data.IKEArr.localFqdnName}}</span>                        <span' +
                    ' class="tc-15-list-det" b-if="!ikeEditing && data.IKEArr.localIdentity===\'addre' +
                    'ss\'">{{data.IKEArr.localAddress}}</span>                        <span class="tc' +
                    '-15-list-det" b-if="ikeEditing">                            <span b-tag="vpc-dro' +
                    'pdown" b-model="ikeForm.localIdentity"                                  b-with="' +
                    '{options:options.ike.identity}"></span>                            <input class=' +
                    '"tc-15-input-text s" b-model="ikeForm.localAddress"                             ' +
                    '      data-name="localAddress"                                   b-if="ikeForm.l' +
                    'ocalIdentity===\'address\'" disabled="disabled">                            <inp' +
                    'ut class="tc-15-input-text s" b-model="ikeForm.localFqdnName"                   ' +
                    '                data-name="localFqdnName"                                   b-if' +
                    '="ikeForm.localIdentity===\'fqdn\'">                        </span>             ' +
                    '       </li>                    <li>                        <em class="tc-15-lis' +
                    't-tit">远端标识</em>                        <span class="tc-15-list-det" b-if="!ikeE' +
                    'diting && data.IKEArr.remoteIdentity===\'fqdn\'">{{data.IKEArr.remoteFqdnName}}<' +
                    '/span>                        <span class="tc-15-list-det" b-if="!ikeEditing && ' +
                    'data.IKEArr.remoteIdentity===\'address\'">{{data.IKEArr.remoteAddress}}</span>  ' +
                    '                      <span class="tc-15-list-det" b-if="ikeEditing">           ' +
                    '                 <span b-tag="vpc-dropdown" b-model="ikeForm.remoteIdentity"    ' +
                    '                              b-with="{options:options.ike.identity}"></span>   ' +
                    '                         <input class="tc-15-input-text s" b-model="ikeForm.remo' +
                    'teAddress"                                   data-name="remoteAddress"          ' +
                    '                         b-if="ikeForm.remoteIdentity===\'address\'" disabled="d' +
                    'isabled">                            <input class="tc-15-input-text s" b-model="' +
                    'ikeForm.remoteFqdnName"                                   data-name="remoteFqdnN' +
                    'ame"                                   b-if="ikeForm.remoteIdentity===\'fqdn\'">' +
                    '                        </span>                    </li>                    <li>' +
                    '                        <em class="tc-15-list-tit">DH group</em>                ' +
                    '        <span class="tc-15-list-det" b-if="!ikeEditing">{{optionText(optionsMap.' +
                    'ike.dhGroupName, data.IKEArr.dhGroupName)}}</span>                        <span ' +
                    'class="tc-15-list-det" b-if="ikeEditing">                            <span b-tag' +
                    '="vpc-dropdown" b-model="ikeForm.dhGroupName"                                  b' +
                    '-with="{options:options.ike.dhGroupName}"></span>                        </span>' +
                    '                    </li>                    <li>                        <em cla' +
                    'ss="tc-15-list-tit">IKE sa lifetime(s)</em>                        <span class="' +
                    'tc-15-list-det" b-if="!ikeEditing">{{data.IKEArr.ikeSaLifetimeSeconds}} s</span>' +
                    '                        <span class="tc-15-list-det" b-if="ikeEditing">         ' +
                    '                   <input class="tc-15-input-text s" data-name="ikeSaLifetimeSec' +
                    'onds" b-model="ikeForm.ikeSaLifetimeSeconds">                        </span>    ' +
                    '                </li>                    <li b-if="ikeEditing">                 ' +
                    '       <em class="tc-15-list-tit">&nbsp;</em>                        <button cla' +
                    'ss="tc-15-btn {{spdSubmitting || basicSubmitting || ikeSubmitting || ipsecSubmit' +
                    'ting ? \'disabled\' : \'\'}}" data-action="ikeSave">                            ' +
                    '<i b-if="ikeSubmitting" class="n-loading-icon"></i>保存</button>                  ' +
                    '      <button class="tc-15-btn weak {{ikeSubmitting ? \'disabled\' : \'\'}}" dat' +
                    'a-action="ikeCancel">取消</button>                    </li>                </ul>  ' +
                    '          </div>        </div>        <div class="tc-15-list-wrap private-networ' +
                    'k-inf" b-style="{display : loading || notFound ? \'none\' : \'\'}">            <' +
                    'div class="tc-15-list-legend">IPsec配置                <span class="title-edit" b-' +
                    'if="!ipsecEditing" data-action="ipsecEdit"><i                        class="penc' +
                    'il-icon hover-icon"></i><a                        href="">编辑</a></span>         ' +
                    '   </div>            <div class="tc-15-list-content" data-form="ipsec">         ' +
                    '       <ul>                    <li>                        <em class="tc-15-list' +
                    '-tit">加密算法</em>                        <span class="tc-15-list-det" b-if="!ipsec' +
                    'Editing">{{optionText(optionsMap.ipsec.encryptAlgorithm, data.IPSECArr.encryptAl' +
                    'gorithm)}}</span>                        <span class="tc-15-list-det" b-if="ipse' +
                    'cEditing">                            <span b-tag="vpc-dropdown" b-model="ipsecF' +
                    'orm.encryptAlgorithm" b-with="{options:options.ipsec.encryptAlgorithm}" ></span>' +
                    '                        </span>                    </li>                    <li>' +
                    '                        <em class="tc-15-list-tit">认证算法</em>                    ' +
                    '    <span class="tc-15-list-det" b-if="!ipsecEditing">{{optionText(optionsMap.ip' +
                    'sec.integrityAlgorith, data.IPSECArr.integrityAlgorith)}}</span>                ' +
                    '        <span class="tc-15-list-det" b-if="ipsecEditing">                       ' +
                    '     <span b-tag="vpc-dropdown" b-model="ipsecForm.integrityAlgorith" b-with="{o' +
                    'ptions:options.ipsec.integrityAlgorith}" ></span>                        </span>' +
                    '                    </li>                    <li>                        <em cla' +
                    'ss="tc-15-list-tit">报文封装模式</em>                        <span class="tc-15-list-d' +
                    'et">{{data.IPSECArr.encapMode}}</span>                    </li>                 ' +
                    '   <li>                        <em class="tc-15-list-tit">安全协议</em>             ' +
                    '           <span class="tc-15-list-det">{{data.IPSECArr.securityProto}}</span>  ' +
                    '                  </li>                    <li>                        <em class' +
                    '="tc-15-list-tit">PFS</em>                        <span class="tc-15-list-det" b' +
                    '-if="!ipsecEditing">{{optionText(optionsMap.ipsec.pfsDhGroup, data.IPSECArr.pfsD' +
                    'hGroup)}}</span>                        <span class="tc-15-list-det" b-if="ipsec' +
                    'Editing">                            <span b-tag="vpc-dropdown" b-model="ipsecFo' +
                    'rm.pfsDhGroup" b-with="{options:options.ipsec.pfsDhGroup}" ></span>             ' +
                    '           </span>                    </li>                    <li>             ' +
                    '           <em class="tc-15-list-tit">IPSec SA生存周期(s)</em>                      ' +
                    '  <span class="tc-15-list-det" b-if="!ipsecEditing">{{data.IPSECArr.ipsecSaLifet' +
                    'imeSeconds}} s</span>                        <span class="tc-15-list-det" b-if="' +
                    'ipsecEditing">                            <input class="tc-15-input-text s" data' +
                    '-name="ipsecSaLifetimeSeconds" b-model="ipsecForm.ipsecSaLifetimeSeconds"> s    ' +
                    '                    </span>                    </li>                    <li>    ' +
                    '                    <em class="tc-15-list-tit">IPSec SA生存周期(KB)</em>            ' +
                    '            <span class="tc-15-list-det" b-if="!ipsecEditing">{{data.IPSECArr.ip' +
                    'secSaLifetimeTraffic}} KB</span>                        <span class="tc-15-list-' +
                    'det" b-if="ipsecEditing">                            <input class="tc-15-input-t' +
                    'ext s" data-name="ipsecSaLifetimeTraffic" b-model="ipsecForm.ipsecSaLifetimeTraf' +
                    'fic"> KB                        </span>                    </li>                ' +
                    '    <li b-if="ipsecEditing">                        <em class="tc-15-list-tit">&' +
                    'nbsp;</em>                        <button class="tc-15-btn {{spdSubmitting || ba' +
                    'sicSubmitting || ikeSubmitting || ipsecSubmitting ? \'disabled\' : \'\'}}" data-' +
                    'action="ipsecSave">                            <i b-if="ipsecSubmitting" class="' +
                    'n-loading-icon"></i>保存</button>                        <button class="tc-15-btn ' +
                    'weak {{ipsecSubmitting ? \'disabled\' : \'\'}}" data-action="ipsecCancel">取消</bu' +
                    'tton>                    </li>                </ul>            </div>        </d' +
                    'iv>        </div>    </div>',events:{"click [data-action]:not(.disabled)":functi' +
                    'on(e){var t=$(e.currentTarget);this.handleAction(t.attr("data-action"),null,e),e' +
                    '.preventDefault()}},_getIndexData:function(){var e=this,t=e.data;return{vpcId:t.' +
                    'unVpcId,vpnGwId:t.unVpnGwId,vpnConnId:t.unVpnConnId}},handleAction:function(e,t,' +
                    'i){var n,a,s,c=this,u=$(i.currentTarget).closest("[data-form]"),v=c.data;switch(' +
                    'e){case"basicEdit":c.$set("basicForm",d.clone(v)),c.$set("basicEditing",!0);brea' +
                    'k;case"basicSave":if(c.basicForm.vpnConnName==v.vpnConnName&&c.basicForm.preShar' +
                    'edKey==v.preSharedKey){c.$set("basicEditing",!1);break}if(n=l.multiValidate({vpn' +
                    'ConnName:c.basicForm.vpnConnName,preSharedKey:c.basicForm.preSharedKey
},null,{u' +
                    'serGwSubnetList:c.basicForm.snet},c.$el,{$btn:$(i.currentTarget)}))return;c.$set' +
                    '("basicSubmitting",!0),s=$.Deferred(),p.freeze(!0,u),s.always(function(){c.$set(' +
                    '"basicSubmitting",!1),p.freeze(!1,u)}),r.modifyVpnConnEx(c.regionId,$.extend(c._' +
                    'getIndexData(),{vpnConnName:c.basicForm.vpnConnName,preSharedKey:c.basicForm.pre' +
                    'SharedKey,userGwCidrBlock:r.splitUserGwSubnetList(c.basicForm.dnet)})).then(s.re' +
                    'solve,s.reject),s.done(function(){o.resetNamespace("vpnConn",c.regionId,v.unVpcI' +
                    'd,v.unVpnConnId),o.resetNamespace("vpnConn",c.regionId,v.vpcId,v.vpnConnId),c.$s' +
                    'et("basicEditing",!1),c.$set("data",c.basicForm),c.$set({event_update:c.basicFor' +
                    'm,changeName:c.basicForm.preSharedKey===v.preSharedKey}),c.$set("changeName",!1)' +
                    '});break;case"basicCancel":c.$set("basicEditing",!1);break;case"spdEdit":c.$set(' +
                    '"data._spdAcl",d.clone(c.data.spdAcl.slice())),c.$set("spdEditing",!0);break;cas' +
                    'e"spdCancel":c.$set("spdEditing",!1);break;case"spdSave":var f=c.$refs.spdAclEdi' +
                    'tor;f.removePreRemove();var m=JSON.stringify(f.formatData());if(f.validator.btn=' +
                    'i.target,!f.validate())return;c.$set("spdSubmitting",!0),s=$.Deferred(),r.modify' +
                    'VpnConnEx(c.regionId,$.extend(c._getIndexData(),{spdAcl:m})).then(s.resolve,s.re' +
                    'ject),s.always(function(){c.$set("spdSubmitting",!1)}),s.done(function(){var e=f' +
                    '.list.map(function(e,t){return{_rule:"规则 "+(t+1),vpnGwCidr:e.vpnGwCidr,userGwCid' +
                    'r:e.userGwCidr,_userGwCidr:e.userGwCidr.split(/\n/).join(",")}});c.$set("spdEdit' +
                    'ing",!1),c.$set("data.spdAcl",e),c.$set("event_update",{spdAcl:e})});break;case"' +
                    'ikeEdit":c.$set("ikeEditing",!0),a=d.clone(v.IKEArr),a.remoteAddress=a.remoteAdd' +
                    'ress||v.userGwAddress,a.localAddress=a.localAddress||v.vpnGwAddress,c.$set("ikeF' +
                    'orm",a);break;case"ikeSave":if(n=l.multiValidate(c.ikeForm,null,null,c.$el,{$btn' +
                    ':$(i.currentTarget)}))return;c.$set("ikeSubmitting",!0),s=$.Deferred(),p.freeze(' +
                    '!0,u),s.always(function(){c.$set("ikeSubmitting",!1),p.freeze(!1,u)}),a=c._getIn' +
                    'dexData(),["propoEncryAlgorithm","propoAuthenAlgorithm","exchangeMode","localIde' +
                    'ntity","remoteIdentity","dhGroupName","ikeSaLifetimeSeconds"].forEach(function(e' +
                    '){a[e]=c.ikeForm[e]}),"fqdn"===a.localIdentity?a.localFqdnName=c.ikeForm.localFq' +
                    'dnName:"address"===a.localIdentity&&(a.localAddress=c.ikeForm.localAddress),"fqd' +
                    'n"===a.remoteIdentity?a.remoteFqdnName=c.ikeForm.remoteFqdnName:"address"===a.re' +
                    'moteIdentity&&(a.remoteAddress=c.ikeForm.remoteAddress),r.modifyVpnConnEx(c.regi' +
                    'onId,a).then(s.resolve,s.reject),s.done(function(){c.$set("ikeEditing",!1),c.$se' +
                    't("data.IKEArr",c.ikeForm),c.$set("event_update",{IKEArr:c.ikeForm})});break;cas' +
                    'e"ikeCancel":c.$set("ikeEditing",!1);break;case"ipsecEdit":c.$set("ipsecEditing"' +
                    ',!0),c.$set("ipsecForm",d.clone(v.IPSECArr));break;case"ipsecSave":if(n=l.multiV' +
                    'alidate(c.ipsecForm,null,null,c.$el,{$btn:$(i.currentTarget)}))return;c.$set("ip' +
                    'secSubmitting",!0),s=$.Deferred(),p.freeze(!0,u),s.always(function(){c.$set("ips' +
                    'ecSubmitting",!1),p.freeze(!1,u)}),a=c._getIndexData(),["encryptAlgorithm","inte' +
                    'grityAlgorith","pfsDhGroup","ipsecSaLifetimeSeconds","ipsecSaLifetimeTraffic"].f' +
                    'orEach(function(e){a[e]=c.ipsecForm[e]}),r.modifyVpnConnEx(c.regionId,a).then(s.' +
                    'resolve,s.reject),s.done(function(){c.$set("ipsecEditing",!1),c.$set("data.IPSEC' +
                    'Arr",c.ipsecForm),c.$set("event_update",{IPSECArr:c.ipsecForm})});break;case"ips' +
                    'ecCancel":c.$set("ipsecEditing",!1);break;case"return":history.back()}},loadData' +
                    ':function(){var e=this,t=e,i=t.data;!t[u]||i&&t[u]===i[u]||(t.$set({loading:!0,n' +
                    'otFound:!1,data:{}}),c.request(t.regionId,"DescribeVpnConn",{offset:0,limit:1,vp' +
                    'nConnId:t[u]}).done(function(e){var i=e.data[0],n=!0;i&&(n=!1,i=c.translate(i,"v' +
                    'pnConn")),t.$set({notFound:n,data:i||{}})}).always(function(){t.$set("loading",!' +
                    '1)}))}},{basicForm:{},ikeForm:{},ipsecForm:{},defaults:{tab:0}}),n.tag("vpc-drop' +
                    'down",s),n.tag("spd-editor",f),n.tag("spd-grid",n.getComponent("grid-view").exte' +
                    'nd({},{defaults:{initGetData:!1,autoMaxHeight:!1,showState:!1,showPagination:!1,' +
                    'minHeight:"auto",colums:[{name:"规则",width:200,key:"_rule"},{name:"本端网段",key:"vpn' +
                    'GwCidr"},{name:"对端网段",key:"_userGwCidr"}]}})),i.exports=n}),define("modules/vpc/' +
                    'vpnConn/options",function(e,t,i){var n=function(e){var t={};return e.forEach(fun' +
                    'ction(e){t[e.value]=e.text}),t};t.encryptAlgorithmOptions=[{value:"3des-cbc",tex' +
                    't:"3DES",desc:"3DES-CBC"},{value:"aes-cbc-128",text:"AES-128",desc:"AES-CBC-128"' +
                    '},{value:"aes-cbc-192",text:"AES-192",desc:"AES-CBC-192"},{value:"aes-cbc-256",t' +
                    'ext:"AES-256",desc:"AES-CBC-256"},{value:"des-cbc",text:"DES",desc:"DES-CBC"}],t' +
                    '.encryptAlgorithmOptionsMap=n(t.encryptAlgorithmOptions),t.ikeHashAlgorithmOptio' +
                    'ns=[{value:"md5",text:"MD5"},{value:"sha",text:"SHA1"}],t.ikeHashAlgorithmOption' +
                    'sMap=n(t.ikeHashAlgorithmOptions),t.ipsecHashAlgorithmOptions=[{value:"md5",text' +
                    ':"MD5"},{value:"sha1",text:"SHA1"}],t.ipsecHashAlgorithmOptionsMap=n(t.ipsecHash' +
                    'AlgorithmOptions),t.dhGroupOptions=[{value:"group1",text:"DH1"},{value:"group2",' +
                    'text:"DH2"},{value:"group5",text:"DH5"},{value:"group14",text:"DH14"},{value:"gr' +
                    'oup24",text:"DH24"}],t.dhGroupOptionsMap=n(t.dhGroupOptions),t.pfsOptions=[{valu' +
                    'e:"null",text:"disable"},{value:"dh-group1",text:"dh-group1"},{value:"dh-group2"' +
                    ',text:"dh-group2"},{value:"dh-group5",text:"dh-group5"},{value:"dh-group14",text' +
                    ':"dh-group14"},{value:"dh-group24",text:"dh-group24"}],t.pfsOptionsMap=n(t.pfsOp' +
                    'tions),t.exchangeModeOptions=[{value:"main",text:"main"},{value:"aggressive",tex' +
                    't:"aggressive"}],t.exchangeModeOptionsMap=n(t.exchangeModeOptions),t.identityOpt' +
                    'ions=[{value:"address",text:"IP Address"},{value:"fqdn",text:"FQDN"}],t.identity' +
                    'OptionsMap=n(t.identityOptions)}),define("modules/vpc/vpnConn/SPDEditor",functio' +
                    'n(e,t,i){var n=e("qccomponent"),a=n.getComponent("grid-editor"),s=e("widget/base' +
                    'Info/validator"),d=e("constants"),o=e("../util"),c=e("appUtil"),r=d.VPC_CONN_USE' +
                    'R_GW_SUBNET_MAX,l=new s({errClass:"is-error",rules:{vpnGwCidr:function(e,t){var ' +
                    'i=e.split("/"),n=i[0],a=+i[1],s=this._vpcCidr,d=$(t).closest("tr").attr("data-in' +
                    'dex");return e?n&&c.isValidIp(n)&&!isNaN(a)?this._spdAcl.some(function(t,i){retu' +
                    'rn i==d||t._remove?!1:c.isCIDRIntersect(e,t.vpnGwCidr)})?"本端网段不能重叠":c.isCIDRSubs' +
                    'et(s,e)?void 0:"本端网段需要在 "+s+"之内":"本端端网段 ("+e+"）不合法":"本端网段不能为空"},userGwCidr:funct' +
                    'ion(e){var t=o.splitUserGwSubnetList(e),i=this._vpcCidr;if(!t.length)return"对端网段' +
                    '不能为空";if(t.length>r)return"对端网段不能超过"+r+"条";for(var n,a,s,d,l=0,p=t.length;p>l;l+' +
                    '+){if(n=t[l],a=n.split("/"),s=a[0],d=+a[1],!s||!c.isValidIp(s))return"对端网段 ("+n+' +
                    '"）不合法";if(!(d&&d>=8&&32>=d))return"对端网段 ("+n+"）掩码必须在8-32之间";if(i&&c.isCIDRInters' +
                    'ect(i,n))return"对端网段 ("+n+"）与私有网络 CIDR ("+i+") 冲突";for(var u=l+1;p>u;u++)if(cidr' +
                    '_next=t[u],c.isCIDRIntersect(n,cidr_next))return"对端网段之间不能重叠"}}}}),p=a.extend({va' +
                    'lidator:l,validate:function(){return l._vpcCidr=this.vpcCidr,l._spdAcl=this.list' +
                    ',l.validateAll(this.$el)},formatData:function(){var e={};return this.list.forEac' +
                    'h(function(t){t.vpnGwCidr&&t.userGwCidr&&!t._remove&&(e[t.vpnGwCidr.trim()]=o.sp' +
                    'litUserGwSubnetList(t.userGwCidr))}),e}},{defaults:{dragable:!1,insertBtn:!1,can' +
                    'ResizeColum:!1,bottomInsertBtn:!0,minSize:1,maxSize:10,newData:{vpnGwCidr:"",use' +
                    'rGwCidr:""},colums:[{name:"规则",tdTpl:"规则{{$parent.$index + 1}}",thTpl:'规则<help-t' +
                    'ips position="right">本端网段和对端网段不能重叠。每个本端网段的多条对端网段不可重叠，<a href="https://www.qcloud' +
                    '.com/doc/product/215/4956#spd.EF.BC.88security-policy-database.EF.BC.89.E7.AD.96' +
                    '.E7.95.A5" target="_blank">点击查看详情</a>。</help-tips>',width:100},{name:"本端网段",tdTp' +
                    'l:'    <p>        <input data-validate="vpnGwCidr" type="text" b-model="item.vpn' +
                    'GwCidr" style="width: 200px" disabled?="item._remove"              class="tc-15-' +
                    'input-text m" data-vpccidr="{{vpcCidr}}" placeholder="{{vpcCidr ? (\'网段需要在\' + v' +
                    'pcCidr + \'之内\') : \'请输入本端网段\'}}">    </p>',thTpl:"本端网段<help-tips>所有规则中本端网段之间不可重' +
                    '叠。</help-tips>"},{name:"对端网段",tdTpl:'    <p>        <textarea data-validate="use' +
                    'rGwCidr" class="tc-15-input-textarea"  disabled?="item._remove"                 ' +
                    ' b-model="item.userGwCidr" placeholder="多个网段换行隔离，网段掩码在/8~/32之间"></textarea></p>'' +
                    ',thTpl:"对端网段<help-tips>对端网段不可以与私有网络网段重叠</help-tips>"},{name:"操作",key:"_action"}]' +
                    '}});i.exports=p}),define("modules/vpc/vpnConn/validator",function(e,t,i){var n=e' +
                    '("../validator"),a=e("appUtil"),s=e("../util"),d=e("constants"),o=d.VPC_CONN_USE' +
                    'R_GW_SUBNET_MAX,c=function(e,t){return e?(t=t||255,e.length>t?"长度不能超过"+t+"字符":/[' +
                    '\?\"\\ ]/.test(e)?'不能含有空格、“?”、“"”、“\\”等特殊字符':/^[\x20-\x7e]+$/.test(e)?void 0:"只能' +
                    '包含ASCII可见字符"):"不能为空"};i.exports=n.extend({_validInt:function(e,t){if(e=$.trim(""' +
                    '+e)){var i=parseInt(e,10);return isNaN(i)?t+"必须是数字":/^[0-9]+$/.test(e)?void 0:t+' +
                    '"必须是整数"}},userGwName:function(e){return this._validName(e,"对端网关名称")},userGwAddr:' +
                    'function(e){return this._validIp(e,"公网IP")},vpnConnName:function(e){return this.' +
                    '_validName(e,"VPN通道名称")},vpnGwId:function(e){return e?void 0:"VPN网关不能为空"},userGw' +
                    'Id:function(e){return e?void 0:"对端网关不能为空"},unVpnGwId:function(e){return e?void 0' +
                    ':"VPN网关不能为空"},unUserGwId:function(e){return e?void 0:"对端网关不能为空"},preSharedKey:fu' +
                    'nction(e){var t=c(e,128);return t?"预共享密钥"+t:void 0},remoteSlaIp:function(e){retu' +
                    'rn this._validIp(e,"对端SLA IP")},userGwSubnetList:function(e,t){var i=s.splitUser' +
                    'GwSubnetList(e);if(!i.length)return"对端网段不能为空";if(i.length>o)return"对端网段不能超过"+o+"' +
                    '条";for(var n,d,c,r,l=0,p=i.length;p>l;l++){if(n=i[l],d=n.split("/"),c=d[0],r=+d[' +
                    '1],!c||!a.isValidIp(c))return"对端网段 ("+n+"）不合法";if(!(r&&r>=8&&28>=r))return"对端网段 ' +
                    '("+n+"）掩码必须在8-28之间";if(t&&a.isCIDRIntersect(t,n))return"对端网段 ("+n+"）与本端网段冲突"}},p' +
                    'ropoEncryAlgorithm:n._notEmptyValidate("IKE加密算法"),propoAuthenAlgorithm:n._notEmp' +
                    'tyValidate("IKE认证算法"),exchangeMode:n._notEmptyValidate("IKE协商模式"),localIdentity:' +
                    'n._notEmptyValidate("IKE本端标识"),localAddress:function(e,t,i){return"fqdn"!==i.loc' +
                    'alIdentity?(e=$.trim(e||""),e?a.isValidIp(e)?void 0:"本端标识地址不合法":"本端标识不能为空"):void' +
                    ' 0},localFqdnName:function(e,t,i){if("address"!==i.localIdentity){if(!e)return"本' +
                    '端标识不能为空";var n=c(e);return n?"本端标识"+n:void 0}},remoteIdentity:n._notEmptyValidat' +
                    'e("IKE远端标识"),remoteAddress:function(e,t,i){return"fqdn"!==i.remoteIdentity?(e=$.' +
                    'trim(e||""),e?a.isValidIp(e)?void 0:"远端标识地址不合法":"远端标识不能为空"):void 0},remoteFqdnNa' +
                    'me:function(e,t,i){if("address"!==i.remoteIdentity){if(!e)return"远端标识不能为空";var n' +
                    '=c(e);return n?"远端标识"+n:void 0}},dhGroupName:n._notEmptyValidate("IKE DH group")' +
                    ',ikeSaLifetimeSeconds:function(e){var t=this._validInt(e,"IKE sa lifetime");if(t' +
                    ')return t;var i=parseInt(e,10);return 60>i||i>604800||isNaN(i)&&e?"IKE sa lifeti' +
                    'me取值必须在60~604800之间":void 0},encryptAlgorithm:n._notEmptyValidate("IPsec加密算法"),in' +
                    'tegrityAlgorith:n._notEmptyValidate("认证算法"),pfsDhGroup:n._notEmptyValidate("PFS"' +
                    '),ipsecSaLifetimeSeconds:function(e){var t=this._validInt(e,"IPSec SA生存周期");if(t' +
                    ')return t;var i=parseInt(e,10);return 180>i||i>604800||isNaN(i)&&e?"IPSec SA生存周期' +
                    '取值必须在180~604800之间":void 0},ipsecSaLifetimeTraffic:function(e){var t=this._validI' +
                    'nt(e,"IPSec SA生存周期");if(t)return t;var i=parseInt(e,10);return 2560>i||i>4294967' +
                    '295||isNaN(i)&&e?"IPSec SA生存周期取值必须在2560~4294967295之间":void 0}})}),define("module' +
                    's/vpc/vpnConn/vpnConn",function(e,t,i){e("widget/region/regionSelector"),e("../I' +
                    'PInput");var n=e("../BeeBase"),a=e("../vpc/Selector"),s=e("../moduleBase"),d=e("' +
                    '../cacher"),o=e("../util"),c=e("../cApi"),r=e("util"),l=e("dialog"),p=e("tips"),' +
                    'u=e("../Grid"),v=e("reporter"),f=e("../RenameBubble"),m=e("../Bubble"),h=e("../C' +
                    'onfirm"),b=e("./Detail"),g=e("./Chart"),I=e("./validator"),w=e("constants"),y="g' +
                    'rid",x="regionSelector",C="addDialog",T=e("./AddVpnConn"),N=e("appUtil"),k={down' +
                    'load:'    <form class="download-config" action="javascript:;">    <div class="tc' +
                    '-15-list-wrap form" style="padding:0;"><div class="tc-15-list-content">        <' +
                    'p class="cont_tip">选择对端网关参数以下载相应的配置文件</p>        <ul>            <li>           ' +
                    '     <em class="tc-15-list-tit">类型</em>                <span class="tc-15-list-d' +
                    'et">                    <div class="tc-15-select-wrap m">                       ' +
                    ' <select class="tc-15-select" name="vendorname" id="vendorname" b-model="vendorn' +
                    'ame">                            <% for (var vendor in configs) { %>            ' +
                    '                <option value="<%= vendor %>"><%= vendor %></option>            ' +
                    '                <% } %>                        </select>                    </di' +
                    'v>                </span>            </li>            <li>                <em cl' +
                    'ass="tc-15-list-tit">平台</em>                <span class="tc-15-list-det">       ' +
                    '             <div class="tc-15-select-wrap m">                        <select cl' +
                    'ass="tc-15-select" name="platform" id="platform"></select>                    </' +
                    'div>                </span>            </li>            <li>                <em ' +
                    'class="tc-15-list-tit">版本</em>                <span class="tc-15-list-det">     ' +
                    '               <div class="tc-15-select-wrap m">                        <select ' +
                    'class="tc-15-select" name="software" id="software"></select>                    ' +
                    '</div>                </span>            </li>            <li>                <e' +
                    'm class="tc-15-list-tit">接口名称</em>                <span class="tc-15-list-det"> ' +
                    '                   <div class="tc-15-input-text-wrap m">                        ' +
                    '<input type="text" class="tc-15-input-text" style="vertical-align: middle;" id="' +
                    'ifName" disabled?="vendorname===\'其它\'" name="ifName">                        <a' +
                    ' href="javascript:void(0);" class="ico-qa" style="margin-left: 5px;" data-z-inde' +
                    'x="9999" data-title="VPN通道对端接入设备物理接口名称"></a>                    </div>          ' +
                    '      </span>            </li>        </ul>    </div></div>    </form>',option:'' +
                    '    <% for (var opt in sets) { %>    <option value="<%= opt %>"><%= opt %></opti' +
                    'on>    <% } %>'},P=n.extend({$tpl:'<div>            <div b-style="{display: unVp' +
                    'nConnId || add ? \'none\' : \'block\'}">                <div class="manage-area-' +
                    'title">                    <h2>VPN通道</h2>                    <span b-tag="region' +
                    '-selector" b-ref="regionSelect" b-model="regionId" b-with="{getWhiteList:getRegi' +
                    'onWhiteList}"></span>                    <span b-tag="vpc-select" b-ref="vpcSele' +
                    'ct" region-id="{{regionId}}" use-un-id="1" b-model="unVpcId"></span>            ' +
                    '        <div class="manage-area-title-right">                        <a href="ht' +
                    'tps://www.qcloud.com/doc/product/215/4956" target="_blank">                     ' +
                    '       <b class="links-icon"></b> VPN连接帮助文档                        </a>         ' +
                    '           </div>                </div>                <div class="tc-15-action-' +
                    'panel">                    <div b-tag="qc-action-button" name="add" label="+新建" ' +
                    'attr="{{ {\'data-title\':addDisabledText} }}" b-with="{action:actionHandler, dis' +
                    'abled:addDisabled}"></div>                    <div b-tag="qc-action-button" floa' +
                    't="right" name="columns" class-name="weak setting" b-with="{action:actionHandler' +
                    '}"></div>                    <div b-tag="qc-search" b-ref="search" float="right"' +
                    ' name="search" placeholder="搜索VPN通道的关键词" multiline="false" b-with="{search:actio' +
                    'nHandler}" keyword="{{keyword}}"></div>                </div>                <di' +
                    'v data-mod="grid"></div>            </div>            <div b-style="{display: ad' +
                    'd ? \'none\' : \'block\'}">                <div b-tag="vpc-vpn-gateway-chart" b-' +
                    'ref="chart" region-id="{{regionId}}" b-model="chartVpnConnId"></div>            ' +
                    '    <div b-tag="vpc-vpn-gateway-detail" download-cfg="{{downloadCfg}}" b-ref="de' +
                    'tail" region-id="{{regionId}}" b-model="unVpnConnId"></div>            </div>   ' +
                    '     <div b-ref="add" b-if="add"></div></div>',getRegionWhiteList:function(){ret' +
                    'urn $.Deferred().resolve(w.VPN_CONN_REGION)},downloadCfg:function(e){var t=this;' +
                    'd.getUserGwVendor().done(function(i){var a,s,d,o,c={};if(i.push({vendorname:"其它"' +
                    ',platform:"其它",software:"其它"}),i&&i.length){for(var u,f,m,h=0;u=i[h];h++)c[u.ven' +
                    'dorname]||(c[u.vendorname]={}),f=c[u.vendorname],f[u.platform]||(f[u.platform]={' +
                    '}),m=f[u.platform],m[u.software]||(m[u.software]=u);var b=new n({$tpl:r.tmpl(k.d' +
                    'ownload,{configs:c})});d=l.create(b.$el,480,"",{title:"下载配置文件",button:{"下载":func' +
                    'tion(){var n;window.downloadIframe?n=$("#downloadIframe")[0]:(n=window.downloadI' +
                    'frame=document.createElement("iframe"),n.id="downloadIframe",n.style.display="no' +
                    'ne",document.body.appendChild(n)),a=c[o.vendorname.value][o.platform.value][o.so' +
                    'ftware.value],"其它"===a.vendorname&&(a=i[0]),s={t:(new Date).getTime(),uin:r.getU' +
                    'in(),csrfCode:r.getACSRFToken(),unVpcId:e.unVpcId,unVpnConnId:e.unVpnConnId,vend' +
                    'orname:a.vendorname,platform:a.platform,software:a.software,ifName:o.ifName.valu' +
                    'e,regionId:t.regionId,unVpnGwId:e.unVpnGwId,unUserGwId:e.unUserGwId},n.src="/cgi' +
                    '/vpc?action=DescribeVpnConnConfig&"+$.param(s),l.hide()}}}),o=d.find("form")[0],' +
                    '$(o.platform).html(r.tmpl(k.option,{sets:c[o.vendorname.value]})),$(o.software).' +
                    'html(r.tmpl(k.option,{sets:c[o.vendorname.value][o.platform.value]})),d.on("chan' +
                    'ge","select",function(e){o=d.find("form")[0];var t=e.currentTarget;"vendorname"=' +
                    '==t.id&&$(o.platform).html(r.tmpl(k.option,{sets:c[o.vendorname.value]})),("vend' +
                    'orname"===t.id||"platform"===t.id)&&$(o.software).html(r.tmpl(k.option,{sets:c[o' +
                    '.vendorname.value][o.platform.value]}))}),v.click("vpc.vpnTunnel.download.Conf")' +
                    '}else p.error("下载配置文件失败")})}});P.tag("vpc-select",a),P.tag("vpc-vpn-gateway-deta' +
                    'il",b),P.tag("vpc-vpn-gateway-chart",g);var D={},_={},E=w.CONNECT_STATUS;[E.UPDA' +
                    'TE_ERROR,E.RUNNING].forEach(function(e){_[e]=1}),[E.CREATE_ERROR,E.UPDATE_ERROR,' +
                    'E.DESTROY_ERROR,E.RUNNING].forEach(function(e){D[e]=1}),i.exports=s.extend({rout' +
                    'eBase:"/vpc/vpnConn",params:$.extend({},s.params,{keyword:"",unVpnConnId:{histor' +
                    'y:!0,defaults:""},add:{history:!0,defaults:""}}),Bee:P,getBeeConfig:function(){v' +
                    'ar e=this,t=s.getBeeConfig.apply(this,arguments),i=function(){e.handleAction(thi' +
                    's.name,this)};return $.extend(t.$data,{actionHandler:i}),$.extend(t,{addDisabled' +
                    ':!0,getWhiteList:function(){return d.getRegionIdList()}})},onRender:function(){v' +
                    'ar t=this,i=t.$el,n=t.bee,a=!0;e("../adTip").init(t.$el);var s=t.bee.$refs,r=s.s' +
                    'earch,p=s.vpcSelect,b=s.chart,g=s.detail,x=function(){t[y]&&t[y].bufferReload()}' +
                    ';n.$watch("regionId",function(){t.setParam({unVpcId:void 0,keyword:void 0}),x()}' +
                    '),["unVpcId","keyword","page","count"].forEach(function(e){n.$watch(e,x)}),t.vpc' +
                    'Id2UnId().always(function(){a=!1,x()}),p.$watch("list",function(e){n.$set("addDi' +
                    'sabled",e.length<=1),n.$set("addDisabledText",e.length<=1?"无可用私有网络":"")},!0),o.b' +
                    'eeBind(n,r,"keyword"),o.beeBind(n,b,"vpnConnDetail","data"),o.beeBind(n,g,"vpnCo' +
                    'nnDetail","data"),n.$refs.detail.$watch("event_update",function(e){var i=t[y];n.' +
                    'unVpnConnId&&i&&i.updateRowData({unVpcId:n.editingUnVpcId,unVpnConnId:n.unVpnCon' +
                    'nId},e)}),t.bee.$watch("add",function(e){var i;e&&(i=new T({onAdd:function(){t[y' +
                    '].bufferReload()},downloadCfg:t.bee.downloadCfg.bind(t.bee),$data:{regionId:N.ge' +
                    'tRegionId(),vpcId:n.unVpcId||void 0}}),$(t.bee.$refs.add).html(i.$el))},!0),t[y]' +
                    '=new u({$el:t.getModDom(i,y),$data:{count:n.count,page:n.page,minHeight:450,cach' +
                    'eKey:"vpc_vpn_conn_columns_v20151124",colums:[{key:"vpnConnName",name:"ID/名称",re' +
                    'quired:!0,tdTpl:'<p><a data-role="detail" href="javascript:;" class="text-overfl' +
                    'ow"><span class="text-overflow">{{item.unVpnConnId}}</span></a></p><span class="' +
                    'text-overflow m-width"><i b-if="item.renaming" class="n-loading-icon"></i>{{item' +
                    '.vpnConnName || "未命名"}}</span>{{{item.$editable ? renameIconHtml : ""}}}'},{name' +
                    ':"监控",key:"_monitor",width:50,tdTpl:'<i b-if="item.$editable" class="dosage-icon' +
                    '" data-role="monitor" data-title="查看监控"></i>{{item.$editable ? "":"-"}}'},{key:"' +
                    'netStatus",name:"状态",width:80},{key:"vpnProto",name:"协议类型",width:80,hide:!0},{ke' +
                    'y:"vpnGwName",name:"VPN网关",width:120,hide:!0},{key:"usrGwName",name:"对端网关",tdTpl' +
                    ':'<p><span class="text-overflow fake-link">{{item.unUserGwId}}</span></p><span c' +
                    'lass="text-overflow" title="{{item.usrGwName}}({{item.userGwAddress}})">{{item.u' +
                    'srGwName}}{{item.userGwAddress ? "("+item.userGwAddress+")" : ""}}</span>'},{key' +
                    ':"unVpcId",name:"所属网络",tdTpl:o.vpcTdTpl},{key:"preSharedKey",name:"预共享密钥"},{key:' +
                    '"_action",name:"操作",minWidth:160,required:!0}]},getHeadContent:function(e){retur' +
                    'n"remoteSlaIp"===e.key?e.name+'<div class="tc-15-bubble-icon tc-15-triangle-alig' +
                    'n-center"><i class="tc-icon icon-what"></i><div class="tc-15-bubble tc-15-bubble' +
                    '-top"><div class="tc-15-bubble-inner">对端SLA IP用于VPN通道监控，非特殊情况无需更改</div></div></d' +
                    'iv>':void 0},getCellContent:function(e,t,i){var n,a;switch(i.key){case"netStatus' +
                    '":if(t.state===w.CONNECT_STATUS.RUNNING)n="available"===e?"已联通":"未联通","available' +
                    '"===e?(n="已联通",a="succeed"):(n="未联通",a="warning");else switch(t.state){case w.VP' +
                    'N_STATUS.WAIT_PAY:n="创建中";break;case w.VPN_STATUS.PAY_ERROR:n="创建出错",a="error";b' +
                    'reak;case w.VPN_STATUS.DELIVER_ING:n="更新中";break;case w.VPN_STATUS.DELIVER_ERROR' +
                    ':n="更新出错",a="error";break;case w.VPN_STATUS.DESTROY_ING:n="销毁中";break;case w.VPN' +
                    '_STATUS.DESTROY_ERROR:n="销毁错误",a="error";break;case w.VPN_STATUS.RUNNING:n="运行中"' +
                    ',a="succeed";break;default:n="未知"}"reseting"==t._state?n="重置中":"recreating"==t._' +
                    'state&&(n="重新创建中"),n='<span class="text-overflow '+(a||"")+'">'+n+"</span>";brea' +
                    'k;case"preSharedKey":n='<span class="text-overflow m-width">'+(t.sharedKeyChangi' +
                    'ng?'<i class="n-loading-icon"></i>':"")+e+"</span>"+(t.$editable?' <i class="pen' +
                    'cil-icon hover-icon" data-role="changeSharedKey" data-title="编辑"></i>':"");break' +
                    ';case"snet":n='<span class="text-overflow">'+(e&&e.join&&e.join(",")||e)+"</span' +
                    '>";break;case"dnet":n='<span class="text-overflow m-width">'+(t.dnetChanging?'<i' +
                    ' class="n-loading-icon"></i>':"")+(e&&e.join&&e.join(",")||e)+"</span>"+(t.$edit' +
                    'able?' <i class="pencil-icon hover-icon" data-role="changeDNet" data-title="编辑">' +
                    '</i>':"");break;case"remoteSlaIp":n='<span class="text-overflow m-width">'+(t.sl' +
                    'aIpChanging?'<i class="n-loading-icon"></i>':"")+e+"</span>"+(t.$editable?' <i c' +
                    'lass="pencil-icon hover-icon" data-role="changeSlaIp" data-title="编辑"></i>':"");' +
                    'break;case"_action":n=this.getActionsCell([{text:"重新创建",act:"recreate",hide:t.st' +
                    'ate!=w.VPN_STATUS.PAY_ERROR},{text:"删除",act:"remove",disabled:!t.$removable},{te' +
                    'xt:"下载配置文件",act:"downloadConfig",disabled:t.connectInvalid,hide:t.state==w.VPN_S' +
                    'TATUS.PAY_ERROR},{text:"重置",act:"reset",disabled:t.connectInvalid||"reseting"==t' +
                    '._state,hide:t.state==w.VPN_STATUS.PAY_ERROR}])}return n},getData:function(e){if' +
                    '(!a){var t,i=this,s=e.count,d=e.page,o=s*(d-1),r=s,l=n.keyword;i.showLoading("加载' +
                    '中..."),t=c.request(n.regionId,"DescribeVpnConn",{offset:o,limit:r,isNeedConnNet:' +
                    '1,vpcId:n.unVpcId||void 0,vpnConnName:l},"加载VPN通道列表失败").translate({totalCount:"t' +
                    'otalNum",data:{key:"detail",map:"vpnConn"}}),t.done(function(t){var n=t.detail;n' +
                    '.forEach(function(e){e.connectInvalid=e.state!==w.CONNECT_STATUS.RUNNING,e.vpnPr' +
                    'oto="ip-sec"===e.vpnProto?"IKE/IPsec":e.vpnProto,e.$editable=e.state in _,e.$rem' +
                    'ovable=e.state in D}),n=i.localSort(n,e.orderField,e.order),i.setData({totalNum:' +
                    't.totalNum,searchKey:l,list:n})}),t.always(function(){i.hideLoading()})}},_updat' +
                    'eItemState:function(e){var t=this,i=t.list,n=i[e],a=n.renaming||n.sharedKeyChang' +
                    'ing||n.dnetChanging||n.slaIpChanging;i.$set(e,{$editable:n.state in _&&!a,$remov' +
                    'able:n.state in D&&!a})},onRowMonitor:function(e){e&&e.$editable&&(n.$set({vpnCo' +
                    'nnDetail:e,unVpnConnId:""}),n.$set("chartVpnConnId",e.vpnConnId))},onRowDetail:f' +
                    'unction(e){e&&e.$editable&&(n.$set({vpnConnDetail:e,editingUnVpcId:e.unVpcId,cha' +
                    'rtVpnConnId:""}),n.$set("unVpnConnId",e.unVpnConnId),v.click("vpc.vpnTunnel.deta' +
                    'il"))},onRowRename:function(e,t,i){var a=this,s=e.vpnConnName;new f({value:s,tar' +
                    'get:$(i.currentTarget).parent(),validate:function(e){return I.vpnConnName(e)},do' +
                    'Save:function(t){return o.modifyVpnConnEx(n.regionId,{vpcId:e.unVpcId,vpnGwId:e.' +
                    'unVpnGwId,vpnConnId:e.unVpnConnId,vpnConnName:t}).done(function(){d.resetNamespa' +
                    'ce("vpnConn",n.regionId,e.unVpcId,e.unVpnConnId),d.resetNamespace("vpnConn",n.re' +
                    'gionId,e.vpcId,e.vpnConnId)})},onSave:function(e,i,n){a.list.$set(t,{vpnConnName' +
                    ':i,renaming:!0}),a._updateItemState(t),e.done(function(){a.list.$set(t,{renaming' +
                    ':!1}),a._updateItemState(t)}),e.fail(function(){a.list.$set(t,{vpnConnName:n,ren' +
                    'aming:!1}),a._updateItemState(t)})}})},onRowChangeSharedKey:function(e,t,i){var ' +
                    'a=this,s=e.preSharedKey;new f({value:s,target:$(i.currentTarget).parent(),valida' +
                    'te:function(e){return I.preSharedKey(e)},doSave:function(t){return o.modifyVpnCo' +
                    'nnEx(n.regionId,{vpcId:e.unVpcId,vpnGwId:e.unVpnGwId,vpnConnId:e.unVpnConnId,pre' +
                    'SharedKey:t})},onSave:function(e,i,s){a.list.$set(t,{preSharedKey:i,sharedKeyCha' +
                    'nging:!0}),a._updateItemState(t),e.done(function(){a.list.$set(t,{sharedKeyChang' +
                    'ing:!1}),n.$refs.detail.$set("event_update",a.list[t]),a._updateItemState(t)}),e' +
                    '.fail(function(){a.list.$set(t,{preSharedKey:s,sharedKeyChanging:!1}),a._updateI' +
                    'temState(t)})}})},onRowChangeDNet:function(e,t,i){var a=this,s=e.dnet;new m({con' +
                    'tent:'<div class="tc-15-input-text-wrap m {{invalid ? \'error\' : \'\'}}"><div c' +
                    'lass="clr"><textarea b-model="value" style="width:200px;height:80px;"></textarea' +
                    '></div><div class="tc-15-input-tips">{{invalid || tips}}</div></div>',value:s,ta' +
                    'rget:$(i.currentTarget).parent(),onYes:function(){var i=this.value;if(i!==s){var' +
                    ' d=I.userGwSubnetList(i,e.snet);if(this.$set("invalid",d),d)return!1;a.list.$set' +
                    '(t,{dnet:i,dnetChanging:!0}),a._updateItemState(t);var c=o.modifyVpnConnEx(n.reg' +
                    'ionId,{vpcId:e.unVpcId,vpnGwId:e.unVpnGwId,vpnConnId:e.unVpnConnId,userGwCidrBlo' +
                    'ck:o.splitUserGwSubnetList(i)});c.done(function(){a.list.$set(t,{dnetChanging:!1' +
                    '}),a._updateItemState(t)}),c.fail(function(){a.list.$set(t,{dnet:s,dnetChanging:' +
                    '!1}),a._updateItemState(t)})}}})},onRowChangeSlaIp:function(e,t,i){var a=this,s=' +
                    'e.remoteSlaIp,d=o.getRemoteSlaIpMeta(n.regionId);new m({content:'<div class="tc-' +
                    '15-input-text-wrap m {{invalid ? \'error\' : \'\'}}" style="font-size: 14px;"><d' +
                    'iv class="clr"><div b-tag="vpc-ip-input" b-ref="ipInput" b-model="value" net="'+' +
                    'd.net+'" min-mask="'+d.minMask+'"></div></div><div class="tc-15-input-tips">{{in' +
                    'valid || tips}}</div></div>',value:s,target:$(i.currentTarget).parent(),onYes:fu' +
                    'nction(){var i=this.$refs.ipInput.getValue();if(i!==s){var d=I.remoteSlaIp(i);if' +
                    '(this.$set("invalid",d),d)return!1;a.list.$set(t,{remoteSlaIp:i,slaIpChanging:!0' +
                    '}),a._updateItemState(t);var c=o.modifyVpnConnEx(n.regionId,{vpcId:e.unVpcId,vpn' +
                    'GwId:e.unVpnGwId,vpnConnId:e.unVpnConnId,remoteSlaIp:i});c.done(function(){a.lis' +
                    't.$set(t,{slaIpChanging:!1}),a._updateItemState(t)}),c.fail(function(){a.list.$s' +
                    'et(t,{remoteSlaIp:s,slaIpChanging:!1}),a._updateItemState(t)})}}})},onRowRemove:' +
                    'function(e,t,i){var a=this;new h({target:i.currentTarget,box:a.$gridBody,$data:{' +
                    'title:"确定删除该VPN通道？"},onConfirm:function(){o.delVpnConnEx(n.regionId,{vpcId:e.unV' +
                    'pcId,vpnGwId:e.unVpnGwId,vpnConnId:e.unVpnConnId}).done(function(){d.resetNamesp' +
                    'ace("vpnConn",n.regionId,e.unVpcId,e.unVpnConnId),d.resetNamespace("vpnConn",n.r' +
                    'egionId,e.vpcId,e.vpnConnId),a.bufferReload()})}})},onRowDownloadConfig:function' +
                    '(e,t,i){n.downloadCfg(e)},onRowReset:function(e,t,i){var a=this;new h({target:i.' +
                    'currentTarget,box:a.$gridBody,$data:{confirmKey:"重置",title:"确认重置此VPN通道？",style:"' +
                    'width: 600px",content:"重置操作会中断现有vpn通道数据传输并重新建立连接，请提前做好网络变更准备。"},onConfirm:functi' +
                    'on(){c.request(n.regionId,"ResetVpnConnSA",{vpnGwId:e.unVpnGwId,vpnConnId:e.unVp' +
                    'nConnId,vpcId:e.unVpcId}).done(function(){a.updateItem(t,{_state:"reset"}),l.hid' +
                    'e(),setTimeout(function(){a.bufferReload()},6e3)}).fail(function(){l.hide(),a.bu' +
                    'fferReload()}),v.click("vpc.vpnTunnel.reset")}})},onRowRecreate:function(e,t){va' +
                    'r i=this,a={vpcId:e.unVpcId,vpnGwId:e.unVpnGwId,vpcGwId:e.unVpcId,userGwId:e.unU' +
                    'serGwId,vpnConnName:e.vpnConnName,preSharedKey:e.preSharedKey};e._origin_spdAcl&' +
                    '&(a.spdAcl=JSON.stringify(e._origin_spdAcl)),$.extend(a,e.IKEArr,e.IPSECArr),"fq' +
                    'dn"===a.localIdentity?delete a.localAddress:"address"===a.localIdentity&&delete ' +
                    'a.localFqdnName,"fqdn"===a.remoteIdentity?delete a.remoteAddress:"address"===a.r' +
                    'emoteIdentity&&delete a.remoteFqdnName,a.ikeSaLifetimeSeconds||delete a.ikeSaLif' +
                    'etimeSeconds,a.ipsecSaLifetimeSeconds||delete a.ipsecSaLifetimeSeconds,a.ipsecSa' +
                    'LifetimeTraffic||delete a.ipsecSaLifetimeTraffic,i.updateItem(t,{_state:"recreat' +
                    'ing"}),o.addVpnConnEx(n.regionId,a,"重新创建VPN通道失败！").then(function(){i.bufferReloa' +
                    'd()},function(){i.bufferReload()}),v.click("vpc.vpnTunnel.recreate")},backList:f' +
                    'unction(){this.$set("searchKey",""),n.$set("keyword",void 0)}}),o.beeBind(n,t[y]' +
                    ',"regionId"),x()},onActionAdd:function(){var e=this,t=e.bee;t.$set("add","true")' +
                    '},onActionColumns:function(){this[y].showColumnsWindow()},onActionSearch:functio' +
                    'n(e){this.bee.$set("keyword",e.keyword)},onDestroy:function(){this._destroy(y,x,' +
                    'C)}})}),define("modules/vpc/vpnGw/Chart",function(e,t,i){var n=e("widget/chart/R' +
                    'ecent"),a=(e("widget/chart/baradUtil"),e("widget/cacher/Cacher")),s=e("manager")' +
                    ',d={getChartData:function(e,t,i,n,a,d,o){var c=$.Deferred();return s.loadVpnChar' +
                    'tData({regionId:e,vpcId:t,vpnGwId:i,viewName:"lb_vpc_rs",fromTime:a,toTime:d,ioT' +
                    'ype:n,rsip:o},c.resolve,c.reject),c}},o=new a([{host:d,fnName:"getChartData",pro' +
                    'mise:!0}]),c=function(){var e=$.Deferred(),t=this,i=t.composedId;return o.getFre' +
                    'shChartData(i.regionId,i.vpcId,i.vpnGwId,t.metric,t.fromTime,t.toTime,i.hotHaIp)' +
                    '.done(function(t){for(var i=t&&t.y||[],n=!1,a=0,s=i.length;s>a;a++)i[a]&&(i[a]=1' +
                    '*(i[a]/1e3/1e3).toFixed(3)),(i[a]||0===i[a])&&(n=!0);n||(i=[]),e.resolve({y:i})}' +
                    ').fail(function(){e.resolve({y:[]})}),e};i.exports=n.extend({isComposedIdAvailab' +
                    'le:function(e){return e&&e.regionId&&e.vpcId&&e.vpnGwId}},{defaults:{composedIdE' +
                    'xpression:"{regionId:regionId, vpcId:data.vpcId, vpnGwId : data.vpnGwId, hotHaIp' +
                    ': data.hotHaIp}",charts:[{title:"公网总出带宽",unit:"Mbps",metric:"outtraffic",toMbps:' +
                    '!0,loadData:c},{title:"公网总入带宽",unit:"Mbps",metric:"intraffic",toMbps:!0,loadData' +
                    ':c}]}})}),define("modules/vpc/vpnGw/dealvpn",function(e,t,i){function n(e,t){thi' +
                    's.quotaData=[],a=[{name:"1",fullName:"1个月",value:1},{name:"2",fullName:"2个月",val' +
                    'ue:2},{name:"3",fullName:"3个月",value:3},{name:"4",fullName:"4个月",value:4,hide:!0' +
                    '},{name:"5",fullName:"5个月",value:5,hide:!0},{name:"半年",value:6,expandName:"6",ex' +
                    'pandFullName:"6个月"},{name:"7",fullName:"7个月",value:7,hide:!0},{name:"8",fullName' +
                    ':"8个月",value:8,hide:!0},{name:"9",fullName:"9个月",value:9,hide:!0},{name:"1年",val' +
                    'ue:12},{name:"2年",value:24},{name:"3年",value:36}],this.vpn=e,this.tpl=b.add,this' +
                    '.currentQuota=0,this.callback=t||function(){},this.vpcTask=s.Deferred()}var a,s=' +
                    'e("$"),d=e("manager"),o=e("util"),c=e("appUtil"),r=e("event"),l=e("widget/pills/' +
                    'pills"),p=e("lib/Class"),u=e("dialog"),v=e("../cacher"),f=e("../cApi"),m=e("../v' +
                    'pc/DropDown"),h=e("./validator"),b={add:'    <div class="tc-15-list-wrap form" s' +
                    'tyle="padding:0;">        <form>            <div class="tc-15-list-content">    ' +
                    '            <ul>                    <li>                        <em class="tc-15' +
                    '-list-tit" >所属网络</em>                        <span class="tc-15-list-det">      ' +
                    '                      <span data-mod="vpcSelect"></span>                        ' +
                    '</span>                    </li>                    <li>                        ' +
                    '<em class="tc-15-list-tit" >名称</em>                        <span class="tc-15-li' +
                    'st-det">                            <div class="tc-15-input-text-wrap">         ' +
                    '                       <input type="text" class="tc-15-input-text" name="vpnGwNa' +
                    'me" data-name="vpnGwName">                            </div>                    ' +
                    '    </span>                    </li>                    <li class=\'vpn-spec\'> ' +
                    '                       <em class="tc-15-list-tit" >规格</em>                      ' +
                    '  <input type="hidden" name="vpnGwQuota" />                    </li>            ' +
                    '        <li class="even">                        <em class="tc-15-list-tit" >单价<' +
                    '/em>                        <span class="tc-15-list-det" id="vpn_price">10.00 元/' +
                    '月</span>                    </li>                    <li>                       ' +
                    ' <em class="tc-15-list-tit" >时长</em>                        <span class="vpn-dur' +
                    'ation"></span>                        <input type="hidden" name="costTime" />   ' +
                    '                     <a class="link j-cdb-dur-type1" href="javascript:;" data-ev' +
                    'ent="all_costTime" id="cdb-duration-change">其他时长</a>                    </li>   ' +
                    '                 <li><em class="tc-15-list-tit" >总价</em>                        ' +
                    '<span class="loader" style="display: inline-block;vertical-align: middle;"><span' +
                    ' class="ico ico_loading"></span></span>                        <span class="mone' +
                    'y-tip" id="vpn_total_price" style="vertical-align: middle;display: none;"> - 元</' +
                    'span>                        <label class="txt-tip" style="display:block;"><inpu' +
                    't type="checkbox" class="tc-15-checkbox" name="isAutoRenewals" checked=""> 帐户余额足' +
                    '够时，到期后自动续费1个月</label>                    </li>                </ul>            <' +
                    '/div>        </form>    </div>',
renew:'    <div class="tc-15-list-wrap form" st' +
                    'yle="padding:0;">        <form>            <div class="tc-15-list-legend border-' +
                    'top-none">VPN信息</div>            <div class="tc-15-list-content">               ' +
                    ' <ul>                    <li>                        <em class="tc-15-list-tit" ' +
                    '>名称</em>                        <span class="tc-15-list-det"><%= vpnGwName %></s' +
                    'pan>                    </li>                    <li>                        <em' +
                    ' class="tc-15-list-tit" >公网IP</em>                        <span class="tc-15-lis' +
                    't-det"><%= vpnGwWanIp %></span>                    </li>                    <li>' +
                    '                        <em class="tc-15-list-tit" >规格</em>                     ' +
                    '   <span class="tc-15-list-det"><%= vpnGwQuotaBand %>Mbps</span>                ' +
                    '    </li>                    <li>                        <em class="tc-15-list-t' +
                    'it" >到期时间</em>                        <span class="tc-15-list-det"><%= expireTim' +
                    'e %></span>                    </li>                </ul>            </div>     ' +
                    '       <div class="tc-15-list-legend border-top-none">续费</div>            <div c' +
                    'lass="tc-15-list-content">                <ul>                    <li class=\'vp' +
                    'n-spec\' style="display: none;">                        <em class="tc-15-list-ti' +
                    't" >新规格</em>                        <input type="hidden" name="vpnGwQuota" />   ' +
                    '                 </li>                    <li>                        <em class=' +
                    '"tc-15-list-tit" >续费时长</em>                        <span class="vpn-duration"></' +
                    'span>                        <input type="hidden" name="costTime" />            ' +
                    '            <a class="link j-cdb-dur-type1" href="javascript:;" data-event="all_' +
                    'costTime" id="cdb-duration-change">其他时长</a>                    </li>            ' +
                    '        <li>                        <em class="tc-15-list-tit" >新到期时间</em>      ' +
                    '                  <span class="tc-15-list-det expireDate"></span>               ' +
                    '     </li>                    <li><em class="tc-15-list-tit" >总价</em>           ' +
                    '             <span class="loader" style="display: inline-block;vertical-align: m' +
                    'iddle;"><span class="ico ico_loading"></span></span>                        <spa' +
                    'n class="money-tip" id="vpn_total_price" style="vertical-align: middle;display: ' +
                    'none;"> - 元</span>                    </li>                </ul>            </di' +
                    'v>        </form>    </div>',update:'    <div class="tc-15-list-wrap form" style' +
                    '="padding:0;">        <form>            <div class="tc-15-list-legend border-top' +
                    '-none">VPN信息</div>            <div class="tc-15-list-content">                <u' +
                    'l>                    <li>                        <em class="tc-15-list-tit" >名称' +
                    '</em>                        <span class="tc-15-list-det"><%= vpnGwName %></span' +
                    '>                    </li>                    <li>                        <em cl' +
                    'ass="tc-15-list-tit" >公网IP</em>                        <span class="tc-15-list-d' +
                    'et"><%= vpnGwWanIp %></span>                    </li>                    <li>   ' +
                    '                     <em class="tc-15-list-tit" >规格</em>                        ' +
                    '<span class="tc-15-list-det"><%= vpnGwQuotaBand %>Mbps</span>                   ' +
                    ' </li>                    <li>                        <em class="tc-15-list-tit"' +
                    ' >到期时间</em>                        <span class="tc-15-list-det"><%= expireTime %' +
                    '></span>                    </li>                </ul>            </div>        ' +
                    '    <div class="tc-15-list-legend border-top-none">升级</div>            <div clas' +
                    's="tc-15-list-content">                <ul>                    <li class=\'vpn-s' +
                    'pec\'>                        <em class="tc-15-list-tit" >新规格</em>              ' +
                    '          <input type="hidden" name="vpnGwQuota" />                    </li>    ' +
                    '                <li style="display: none;">                        <em class="tc' +
                    '-15-list-tit" >续费时长</em>                        <span class="vpn-duration"></spa' +
                    'n>                        <input type="hidden" name="costTime" />               ' +
                    '         <a class="link j-cdb-dur-type1" href="javascript:;" data-event="all_cos' +
                    'tTime" id="cdb-duration-change">其他时长</a>                    </li>               ' +
                    '     <li><em class="tc-15-list-tit" >总价</em>                        <span class=' +
                    '"loader" style="display: inline-block;vertical-align: middle;"><span class="ico ' +
                    'ico_loading"></span></span>                        <span class="money-tip" id="v' +
                    'pn_total_price" style="vertical-align: middle;display: none;"> - 元</span>       ' +
                    '             </li>                </ul>            </div>        </form>    </di' +
                    'v>',confirm:'    <div class="coin-alert">        <table class="ui-popmsg"><tbody' +
                    '><tr>            <td height="72" class="i">                <i class="ico ico-con' +
                    'firm mr10"></i>            </td>            <td height="72" class="t">          ' +
                    '      <span class="info">                    <span class="tit">下单成功, 是否立即去支付?</s' +
                    'pan><br>                </span>            </td>        </tr></tbody></table>   ' +
                    ' </div>'};o.insertStyle(".vpn-duration .ui_block_46 .b_item{ width: 36px; } .vpn' +
                    '-duration .ui_block_46 .b_item.b_selected{ width: 46px; }"),s.extend(n,p),n.prot' +
                    'otype={constructor:n,priceType:"newVPN",title:"新建VPN网关",yesText:"立即购买",show:func' +
                    'tion(){var e=this;v.getVpnGwQuota().done(function(t){t.sort(function(e,t){return' +
                    ' e.bandwidth-t.bandwidth});for(var i,n,r,p=0,h=t.length;h>p;p++)n=t[p].bandwidth' +
                    ',e.quotaData.push({name:n+"M",value:t[p].name}),void 0===i&&e.currentQuota===t[p' +
                    '].name&&(i=Math.min(p+1,h-1),r=n);void 0===i&&(i=0),e.$el=s(o.tmpl(e.tpl,s.exten' +
                    'd({vpnGwQuotaBand:r},e.vpn)));var b,g=e.$el.find('[data-mod="vpcSelect"]')[0],I=' +
                    'e.vpcTask,w=e.vpn&&e.vpn.specifyVpcId;g?(b=e.beeVpcSelect=new m({$el:g,name:"vpc' +
                    'Id",$data:{value:w,regionId:c.getRegionId()},getData:function(t,i){var n,a,o,r=c' +
                    '.getRegionId(),l=s.Deferred();d.getVpcVpnGwStatisticListEx(r,"","","",0,999,func' +
                    'tion(e){l.resolve(f.translate(e.data,"vpnGw"))},l.reject),s.when(l.done(function' +
                    '(e){n=e}),v.getVpcListEx(r).done(function(e){o=e.vpcList}),f.DescribeVpcLimit(r,' +
                    '{type:[4]}).done(function(e){e=e.data,a=e.limit[4]})).done(function(){var i={};n' +
                    '.forEach(function(e){e.invalidDeal||(i[e.vpcId]=i[e.vpcId]||0,i[e.vpcId]=i[e.vpc' +
                    'Id]+1)});var s=!1,d=o.length,c=o.map(function(e){var t=i[e.vpcId]>=a;return t||(' +
                    's=!0),{text:e.vpcName+(t?"(无配额)":""),value:e.vpcId,disabled:t}}).sort(function(e' +
                    ',t){return e=e.disabled?1:0,t=t.disabled?1:0,e-t});s||(u.toggleBtnDisable(!0,0),' +
                    'c=[{text:"",value:""}].concat(c)),t(c),b.$set("invalid",s?"":d?"没有可创建VPN网关的私有网络"' +
                    ':"无可用私有网络"),e.setTotalPrice()},i)}}),b.loadTask.done(I.resolve).fail(I.reject),b' +
                    '.$watch("value",function(t,i){t&&i&&e.setTotalPrice()})):I.resolve(),e.form=e.$e' +
                    'l.find("form").get(0),e.quotaPill=new l({renderTo:e.$el.find(".vpn-spec"),data:e' +
                    '.quotaData,change:function(t){e.form.vpnGwQuota.value=t.attr("data-value"),e.set' +
                    'TotalPrice(),this.$el.find("[data-value]").each(function(){var e=s(this);e.text(' +
                    'e.text())})}}),e.quotaPill.$el.children("a").each(function(e){i>e&&s(this).addCl' +
                    'ass("b_disable").attr("disabled","")}),e.costTimePill=new l({renderTo:e.$el.find' +
                    '(".vpn-duration"),data:a,change:function(t){e.form.costTime.value=t.attr("data-v' +
                    'alue"),e.setTotalPrice(),this.options.updateFullText.call(this)},updateFullText:' +
                    'function(){var e={},t=this.val();this.options.data.forEach(function(t){e[t.value' +
                    ']=t}),this.$el.find("[data-value]").each(function(){var i=s(this),n=i.attr("data' +
                    '-value");i.text(n===t?e[n].fullName||e[n].name:e[n].name)})}});var y={};y[e.yesT' +
                    'ext]=s.proxy(e.onYes,e),u.create('<div style="height:300px;"></div>',"650","",{t' +
                    'itle:e.title,"class":"dialog_layer_v2 tc-15-rich-dialog",button:y}),u.contentEl.' +
                    'html("").append(e.$el),e.quotaPill.$el.children("a").eq(i).click(),e.costTimePil' +
                    'l.$el.children("a").eq(0).click(),e.$el.find("input:text[autofocus]").focus(),e.' +
                    'bindEvent()})},getPriceParam:function(){var e=this;return{regionId:c.getRegionId' +
                    '(),vpnGwQuota:e.form.vpnGwQuota.value,newQuta:"",timeSpan:e.form.costTime.value,' +
                    'priceType:e.priceType,vpcId:e.beeVpcSelect.value,vpnGwId:e.vpnGwId||0}},getPrice' +
                    ':function(){var e=this,t=e.form,i=t.vpnGwQuota.value,n=t.costTime.value;if(i&&n&' +
                    '&(!e.beeVpcSelect||parseInt(e.beeVpcSelect.value,10))){var a=s.Deferred();return' +
                    ' e.vpcTask.done(function(){d.getVpnPrice(e.getPriceParam(),function(e){a.resolve' +
                    '(e)},a.reject)}),a.promise()}},getDealParam:function(){var e=this.form,t=this;re' +
                    'turn{regionId:c.getRegionId(),vpnGwQuota:e.vpnGwQuota.value,vpnGwName:e.vpnGwNam' +
                    'e.value,newQuta:e.vpnGwQuota.value,timeSpan:e.costTime.value,priceType:t.priceTy' +
                    'pe,vpcId:t.beeVpcSelect.value,isAutoRenewals:e.isAutoRenewals.checked,vpnGwId:t.' +
                    'vpnGwId||0}},getDeal:function(){var e=s.Deferred(),t=this;return t.vpcTask.done(' +
                    'function(){d.payByCart(t.getDealParam(),function(i){var a=i.dealIds[0],s=t.vpn.d' +
                    'ealId;d.getDealsByCond({dealIds:[1*a],regionId:c.getRegionId()},function(i){var ' +
                    'd=i.bigDealIds[0];t.constructor===n?t.initVpn(a,s).done(function(i){e.resolve(d)' +
                    ',t.callback("init",{vpnGwId:i.vpnGwId,vpcId:t.vpn.vpcId,vpnGwName:t.form.vpnGwNa' +
                    'me.value,vpnGwQuota:t.form.vpnGwQuota.value})}).fail(e.reject):e.resolve(d)},e.r' +
                    'eject)},e.reject)}),e.promise()},hide:function(){u.hide(),this.unbindEvent()},on' +
                    'Yes:function(e){var t=this,i=t.form;t.constructor===n&&h.multiValidate({vpnGwNam' +
                    'e:i.vpnGwName.value,vpcId:t.beeVpcSelect.value},null,null,i,{$btn:e})||e.hasClas' +
                    's("btn_unclick")||(e.addClass("btn_unclick"),u.freeze(!0).toggleBtnLoading(!0),t' +
                    '.getDeal().done(function(e){t.doneAction(e)}).always(function(){u.freeze(!1).tog' +
                    'gleBtnLoading(!1)}))},bindEvent:function(){var e=this;r.bindCommonEvent(this.$el' +
                    '[0],"click",{all_costTime:function(){for(var t=1*e.form.costTime.value,i=0,n=a.l' +
                    'ength;n>i;i++)delete a[i].hide,delete a[i].active,a[i].name=a[i].expandName||a[i' +
                    '].name,a[i].fullName=a[i].expandFullName||a[i].fullName,t===a[i].value&&(a[i].ac' +
                    'tive=!0);var d=e.costTimePill;d.$el.remove();var o=e.costTimePill=new l({classNa' +
                    'me:"ui_block_46",renderTo:e.$el.find(".vpn-duration"),data:a,change:d.options.ch' +
                    'ange,updateFullText:d.options.updateFullText});o.$el.removeClass("ui_block_55"),' +
                    'o.options.updateFullText.call(o),s(this).remove()}})},unbindEvent:function(){},d' +
                    'oneAction:function(e){var t=this;u.create(b.confirm,480,"",{title:"创建 VPN 网关",bu' +
                    'tton:{"去付款":function(){window.open("http://manage.qcloud.com/deal/dealsConfirm.p' +
                    'hp?payMode=1&dealIds[]="+e),u.hide(),t.hide()}},defaultCancelCb:function(){t.hid' +
                    'e()}})},setTotalPrice:function(){var e=this,t=e.getPrice(),i=e.form.costTime.val' +
                    'ue,n=0,a=0,d=e.$el.find(".loader"),o=s("#vpn_total_price");return t?(d.fadeIn(),' +
                    'o.hide(),void t.done(function(t){var r=(+t[0].realCostMonth/100).toFixed(2),l=(+' +
                    't[0].realTotalCost/100).toFixed(2);if(s("#vpn_price").text(r+" 元/月"),o.text(l+" ' +
                    '元"),e.vpn.expireTime){var p=c.cgwTimeParse(e.vpn.expireTime);a=i%12,n=Math.floor' +
                    '(i/12),p.setFullYear(p.getFullYear()+n),p.setMonth(p.getMonth()+a),e.$el.find(".' +
                    'expireDate").text(c.formatDate(p,"yyyy-MM-dd hh:mm:ss"))}o.show(),d.hide()})):vo' +
                    'id("pending"!==e.vpcTask.state()&&(d.hide(),o.text("- 元").show()))},initVpn:func' +
                    'tion(e,t){var i=s.Deferred(),n=this.beeVpcSelect;return d.initVpnGw({vpcId:n.val' +
                    'ue,vpnGwName:this.form.vpnGwName.value,dealId:e,oldDealId:t||e},function(e){v.re' +
                    'setNamespace("vpnGw",c.getRegionId(),n.selected&&n.selected.unVpcId),v.resetName' +
                    'space("vpnGw",c.getRegionId(),n.value),i.resolve(e)},i.reject),i.promise()}};var' +
                    ' g=n.extend({title:"VPN续费",yesText:"立即续费",constructor:function(e){n.apply(this,a' +
                    'rguments),this.tpl=b.renew,this.currentQuota=e.vpnGwQuota,this.vpnGwId=e.vpnGwId' +
                    '},priceType:"renewVPN",getPriceParam:function(){var e=this;return{regionId:c.get' +
                    'RegionId(),vpnGwQuota:e.vpn.vpnGwQuota,newQuta:e.vpn.vpnGwQuota,timeSpan:e.form.' +
                    'costTime.value,priceType:e.priceType,vpcId:e.vpn.vpcId,vpnGwId:e.vpnGwId,expireD' +
                    'ate:e.vpn.expireTime}},getDealParam:function(){var e=this.form,t=this;return{reg' +
                    'ionId:c.getRegionId(),vpnGwQuota:t.vpn.vpnGwQuota,vpnGwName:t.vpn.vpnGwName,newQ' +
                    'uta:t.vpn.vpnGwQuota,timeSpan:e.costTime.value,priceType:t.priceType,vpcId:t.vpn' +
                    '.vpcId,isAutoRenewals:t.vpn.isAutoRenewals,vpnGwId:t.vpnGwId||0,unVpnGwId:t.vpn.' +
                    'unVpnGwId}},doneAction:function(e){location.href="http://manage.qcloud.com/deal/' +
                    'dealsConfirm.php?payMode=1&dealIds[]="+e}}),I=n.extend({title:"VPN升级",yesText:"升' +
                    '级",constructor:function(e){n.apply(this,arguments),this.tpl=b.update,this.curren' +
                    'tQuota=e.vpnGwQuota,this.vpnGwId=e.vpnGwId},priceType:"upgradeVPN",getPriceParam' +
                    ':function(){var e=this;return{regionId:c.getRegionId(),vpnGwQuota:e.vpn.vpnGwQuo' +
                    'ta,newQuta:e.form.vpnGwQuota.value,priceType:e.priceType,vpcId:e.vpn.vpcId,vpnGw' +
                    'Id:e.vpnGwId,expireDate:e.vpn.expireTime}},getDealParam:function(){var e=this.fo' +
                    'rm,t=this;return{regionId:c.getRegionId(),vpnGwQuota:t.vpn.vpnGwQuota,vpnGwName:' +
                    't.vpn.vpnGwName,newQuta:e.vpnGwQuota.value,timeSpan:e.costTime.value,priceType:t' +
                    '.priceType,vpcId:t.vpn.vpcId,isAutoRenewals:t.vpn.isAutoRenewals,expireDate:t.vp' +
                    'n.expireTime,vpnGwId:t.vpnGwId||0,unVpnGwId:t.vpn.unVpnGwId}},doneAction:functio' +
                    'n(e){location.href="http://manage.qcloud.com/deal/dealsConfirm.php?payMode=1&dea' +
                    'lIds[]="+e}});i.exports.addVpn=function(e,t){var i=new n(e,t);return i.show(),i}' +
                    ',i.exports.renewVpn=function(e,t){var i=new g(e,t);return i.show(),i},i.exports.' +
                    'updateVpn=function(e,t){var i=new I(e,t);return i.show(),i}}),define("modules/vp' +
                    'c/vpnGw/Detail",function(e,t,i){var n=e("widget/page/DetailPanel"),a=e("manager"' +
                    '),s=e("./util"),d=e("../cApi"),o=e("widget/popover/TextEditor"),c=e("./RenameBub' +
                    'ble"),r=e("../cacher"),l=i.exports=n.extend({$tpl:'    <div>        <div class="' +
                    'return-title-panel bottom-border" b-style="{display:notFound||loading?\'none\':' +
                    '\'\'}">            <a href="javascript:;" b-on-click="back()" class="btn-return"' +
                    '><i class="btn-back-icon"></i><span>返回</span></a>            <i class="line"></i' +
                    '>            <h2>{{data&&data.vpnGwName}} 详情</h2>            <div class="manage-' +
                    'area-title-right">                <a href="https://www.qcloud.com/doc/product/21' +
                    '5/4956" target="_blank">                    <b class="links-icon"></b> VPN连接帮助文档' +
                    '                </a>            </div>        </div>        <div b-if="loading" ' +
                    'style="text-align:center; padding:100px;">加载中...</div>        <div b-if="notFoun' +
                    'd" style="text-align:center; padding:100px;">未查询到指定VPN网关, <a href="javascript:;"' +
                    ' b-on-click="$set(\'id\', \'\')">返回列表</a></div>        <div class="charts-panel"' +
                    ' b-if="!loading && !notFound">            <div class="param-box nat-form">      ' +
                    '          <div class="param-hd">                    <h3>基本信息</h3>               ' +
                    ' </div>                <div class="param-bd">                    <ul class="item' +
                    '-descr-list">                        <li>                            <span class' +
                    '="item-descr-tit">网关名称</span>                        <span class="item-descr-txt' +
                    '">{{data.vpnGwName}}                            <a href="javascript:;" b-if="!re' +
                    'naming && !data.$loading && data.$editable" b-on="{click:rename}">更改</a>        ' +
                    '                    <i b-if="renaming" class="n-loading-icon"></i>              ' +
                    '          </span>                        </li>                        <li>      ' +
                    '                      <span class="item-descr-tit">网关ID</span>                  ' +
                    '          <span class="item-descr-txt">{{data.unVpnGwId}}</span>                ' +
                    '        </li>                        <li>                            <span class' +
                    '="item-descr-tit">公网IP</span>                            <span class="item-descr' +
                    '-txt">{{data.vpnGwWanIp || \'-\'}}</span>                        </li>          ' +
                    '              <li>                            <span class="item-descr-tit">规格</s' +
                    'pan>                            <span class="item-descr-txt">{{data.bandwidth}} ' +
                    'Mbps</span>                        </li>                        <li>            ' +
                    '                <span class="item-descr-tit">所在地域</span>                        ' +
                    '    <span class="tc-15-list-det">{{REGION_MAP && REGION_MAP[regionId]}}</span>  ' +
                    '                      </li>                        <li>                         ' +
                    '   <span class="item-descr-tit">所属网络</span>                            <span cla' +
                    'ss="item-descr-txt">                                <a data-event="nav" href="/v' +
                    'pc/vpc?rid={{regionId}}&unVpcId={{data.unVpcId}}">{{data.unVpcId}}</a>          ' +
                    '                      ({{data.vpcName}} | {{data.vpcCIDR || data.vpcCidrBlock}})' +
                    '                            </span>                        </li>                ' +
                    '        <li>                            <span class="item-descr-tit">创建时间</span>' +
                    '                            <span class="item-descr-txt">{{data.createTime}}</sp' +
                    'an>                        </li>                        <li>                    ' +
                    '        <span class="item-descr-tit">自动续费</span>                            <spa' +
                    'n class="item-descr-txt">                                <label title="{{data.au' +
                    'toRenewalsChanging ? (data.isAutoRenewals ? \'正在开启\' : \'正在关闭\') : (data.isAutoR' +
                    'enewals ? \'已开启\' : \'已关闭\')}}"                                       b-on="{cli' +
                    'ck:changeRenewal}" class="tc-15-switch {{data.isAutoRenewals ? \'tc-15-switch-ch' +
                    'ecked\' : \'\'}} {{data.autoRenewalsChanging ? \'indeterminate\' : \'\'}}">     ' +
                    '                               <input type="checkbox" class="tc-15-switch-input"' +
                    ' disabled?="!data.$editable" b-model="data.isAutoRenewals">                     ' +
                    '               <span class="tc-15-switch-helper">{{data.isAutoRenewals ? \'已开启\'' +
                    ' : \'已关闭\'}}</span>                                </label>                     ' +
                    '       </span>                        </li>                        <li>         ' +
                    '                   <span class="item-descr-tit">到期时间</span>                     ' +
                    '       <span class="item-descr-txt">{{data.expireTime}}                         ' +
                    '       <span b-if="data.isAutoRenewals">到期后自动续费1个月</span>                       ' +
                    '     </span>                        </li>                    </ul>              ' +
                    '  </div>            </div>            <div class="param-box nat-form">          ' +
                    '      <div class="param-hd">                    <h3>相关路由策略</h3>                <' +
                    '/div>                <route-info region-id="{{regionId}}" un-vpc-id="{{data && d' +
                    'ata.unVpcId}}" id="{{data && data.unVpnGwId}}"></route-info>            </div>  ' +
                    '      </div>    </div>',back:function(){history.back()},$beforeInit:function(){v' +
                    'ar e=this;l.__super__.$beforeInit.apply(e,arguments),r.getRegionMap().done(funct' +
                    'ion(t){e.$set("REGION_MAP",t)})},loadData:function(e){var t=$.Deferred(),i=$.Def' +
                    'erred();return a.getVpcVpnGwStatisticListEx(e.regionId,"",e.id,"",0,1,function(e' +
                    '){i.resolve(d.translate(e,{totalCount:"totalNum",data:{key:"detail",map:"vpnGw"}' +
                    '}))},i.reject),$.when(i,s.getItemProcessorTask()).then(function(e,i){var n=e&&e.' +
                    'detail||[],a=n&&n[0];t.resolve(a&&i(a))},t.reject),t},rename:function(e){var t=t' +
                    'his,i=t,n=t.data;new c({regionId:i.regionId,unVpcId:n.unVpcId,vpcId:n.vpcId,unVp' +
                    'nGwId:n.unVpnGwId,vpnGwId:n.vpnGwId,value:n.vpnGwName,item:n,target:$(e.currentT' +
                    'arget).parent(),onSave:function(e,n,a){this.handleSimpleEdit({def:e,host:i,key:"' +
                    'data.vpnGwName",loadingKey:"renaming",newValue:n,oldValue:a}),e.done(function(){' +
                    't.update({vpnGwName:n})})}})},changeRenewal:function(e){e.preventDefault();var t' +
                    '=this,i=t,n=t.data;if(n.$editable&&!n.autoRenewalsChanging){var a=n.isAutoRenewa' +
                    'ls,s=!a,c=d.request(i.regionId,"ModifyVpnGw",{vpcId:n.unVpcId,vpnGwId:n.unVpnGwI' +
                    'd,vpnGwName:n.vpnGwName,isAutoRenewals:s?1:0},"修改VPN网关失败！");o.prototype.handleSi' +
                    'mpleEdit({def:c,host:i,key:"data.isAutoRenewals",loadingKey:"data.autoRenewalsCh' +
                    'anging",newValue:s,oldValue:a}),c.done(function(){t.update({isAutoRenewals:s})})' +
                    '}}},{defaults:{compositeIdExpression:"{regionId:regionId, id:id}",detailId:"unVp' +
                    'nGwId"}});l.tag("route-info",e("../RouteInfo"))}),define("modules/vpc/vpnGw/Drop' +
                    'Down",function(e,t,i){var n=e("qccomponent"),a=e("../DropDown"),s=e("../cApi"),d' +
                    '=e("constants"),o=a.extend({watchFields:["regionId","vpcId","unVpcId"],getData:f' +
                    'unction(e,t){var i=this;s.request(i.regionId,"DescribeVpnGw",{vpcId:i.unVpcId||i' +
                    '.vpcId,offset:0,limit:999},"加载VPN网关列表失败！").done(function(t){var n=s.translate(t.' +
                    'data,s.translateMap.vpnGw),a=!1;e(n.map(function(e){var t=e.vpnGwStatus===d.VPN_' +
                    'STATUS.RUNNING;return t&&(a=!0),$.extend(e,{text:e.vpnGwName+"("+e.vpcName+")",v' +
                    'alue:i.useUnId?e.unVpnGwId:e.vpnGwId,vpcId:e.vpcId,unVpcId:e.unVpcId,vpcCIDR:e.v' +
                    'pcCIDR,disabled:!t})}).filter(function(e){return!e.disabled})),i.$set("invalid",' +
                    'a?"":"无可用VPN网关")})}});n.tag("vpc-vpn-gw-select",o),i.exports=o}),define("modules' +
                    '/vpc/vpnGw/RenameBubble",function(e,t,i){var n=e("../RenameBubble"),a=e("../cApi' +
                    '"),s=e("../cacher"),d=e("constants"),o=e("appUtil");i.exports=n.extend({validate' +
                    ':function(e){return e.length>25?"VPN网关名称不能超过25个字符":o.isValidName(e)?void 0:"VPN网' +
                    '关"+d.INVALID_NAME_TIPS},doSave:function(e){var t=this,i=t.item;return a.request(' +
                    't.regionId,"ModifyVpnGw",{vpcId:t.unVpcId,vpnGwId:t.unVpnGwId,vpnGwName:e,isAuto' +
                    'Renewals:i.isAutoRenewals?1:0},"修改VPN网关失败！").done(function(){s.resetNamespace("v' +
                    'pnGw",t.regionId,t.unVpcId,t.unVpnGwId),s.resetNamespace("vpnGw",t.regionId,t.vp' +
                    'cId,t.vpnGwId)})}})}),define("modules/vpc/vpnGw/util",function(e,t,i){var n=e(".' +
                    './cacher"),a=e("constants"),s=86400;i.exports={REGION_WHITELIST:a.VPN_GW_REGION,' +
                    'getItemProcessorTask:function(){var e=$.Deferred();return n.getVpnGwQuota().then' +
                    '(function(t){var i,n=0,d={},o={};t.forEach(function(e){var t=e.bandwidth;d[e.nam' +
                    'e]=t,o[t]=e.name,t>n&&(n=t,i=e.name)}),e.resolve(function(e){e.vpnGwQuota=o[e.ba' +
                    'ndwidth],e.$editable=e.vpnGwStatus===a.VPN_STATUS.RUNNING,e.vpnGwQuota===i&&(e.r' +
                    'eachMaxQuota=!0);var t,n;if(e.invalidDeal?(t="订单失效",n="error"):e.bigDealId&&(t="' +
                    '未支付",n="warning"),!t)switch(e.vpnGwStatus){case a.VPN_STATUS.WAIT_PAY:t="创建中";br' +
                    'eak;case a.VPN_STATUS.PAY_ERROR:t="创建中";break;case a.VPN_STATUS.DELIVER_ING:case' +
                    ' a.VPN_STATUS.DELIVER_ERROR:t="发货中";break;case a.VPN_STATUS.DESTROY_ING:case a.V' +
                    'PN_STATUS.DESTROY_ERROR:t="销毁中";break;case a.VPN_STATUS.RUNNING:t="运行中",n="succe' +
                    'ed";break;default:t="未知"}e.statusText=t,e.statusCls=n;var d,c=e.expireTime,r="";' +
                    'return c&&e.remainSeconds<7*s?(d=""+Math.max(0,Math.floor(e.remainSeconds/s))+"天' +
                    '后到期",r="warning"):d=c||"-",e.expireText=d,e.expireCls=r,e})},e.reject),e}}}),def' +
                    'ine("modules/vpc/vpnGw/validator",function(e,t,i){var n=e("../validator");i.expo' +
                    'rts=n.extend({vpnGwName:function(e){return this._validName(e,"VPN网关名称")},vpcId:f' +
                    'unction(e){return e?void 0:"请选择私有网络"},localSlaIp:function(e){return this._validI' +
                    'p(e,"SLA IP")}})}),define("modules/vpc/vpnGw/vpnGw",function(e,t,i){e("widget/re' +
                    'gion/regionSelector"),e("../IPInput");var n=e("../BeeBase"),a=e("../vpc/Selector' +
                    '"),s=e("../gridPage"),d=e("../cacher"),o=e("manager"),c=e("../util"),r=e("./util' +
                    '"),l=e("../cApi"),p=e("../Grid"),u=e("./RenameBubble"),v=e("reporter"),f=e("widg' +
                    'et/popover/TextEditor"),m=e("./dealvpn"),h=(e("./Detail"),e("../Confirm")),b=e("' +
                    'widget/fieldsManager/gridView"),g=(e("constants"),e("dialog")),I=e("./validator"' +
                    '),w="_vpcIdTranslating",y=n.extend({$tpl:'<div><div b-style="{display:detailId ?' +
                    ' \'none\' : \'\'}"><div class="manage-area-title"><h2>VPN网关</h2><span b-tag="reg' +
                    'ion-selector" b-ref="regionSelect" b-model="regionId" b-with="{getWhiteList:getR' +
                    'egionWhiteList}"></span><span b-tag="vpc-select" b-ref="vpcSelect" region-id="{{' +
                    'regionId}}" use-un-id="1" b-model="unVpcId" vpc-id="{{vpcId}}"></span><div class' +
                    '="manage-area-title-right"><a href="https://www.qcloud.com/doc/product/215/4956"' +
                    ' target="_blank"><b class="links-icon"></b> VPN连接帮助文档</a></div></div><div class=' +
                    '"tc-15-action-panel"><qc-action-button b-ref="create" name="add" label="+新建" att' +
                    'r="{{ {\'data-title\':addDisabledText} }}" b-with="{action:actionHandler, disabl' +
                    'ed:addDisabled}"></qc-action-button><qc-action-button float="right" name="column' +
                    's" class-name="weak setting" b-with="{action:actionHandler}"></qc-action-button>' +
                    '<qc-search float="right" name="search" placeholder="搜索VPN网关的关键词" multiline="fals' +
                    'e" b-with="{search:actionHandler}" keyword="{{keyword}}"></qc-search></div><grid' +
                    ' b-ref="grid" b-with="gridCfg"></grid></div><!--详情面板--><div b-if="detailId"><det' +
                    'ail region-id="{{regionId}}"update-callback="{{detailUpdate}}"b-model="detailId"' +
                    ' data="{{detailData}}"></detail></div><!--图表浮层--><div class="sidebar-panel" b-if' +
                    '="chartId && !detailId"><a class="btn-close" href="javascript:;" b-on-click="$se' +
                    't(\'chartId\', \'\')">关闭</a><div class="sidebar-panel-container nat-sidebar"><di' +
                    'v class="sidebar-panel-hd"><h3>{{chartId}}(<span class="entry-name-txt">{{chartN' +
                    'ame}}</span>)监控</h3></div><div class="sidebar-panel-bd"><chart region-id="{{regi' +
                    'onId}}" data="{{chartData}}"></chart></div></div></div></div>',getRegionWhiteLis' +
                    't:function(){return $.Deferred().resolve(r.REGION_WHITELIST)}});y.tag("vpc-selec' +
                    't",a),y.tag("chart",e("./Chart")),y.tag("detail",e("./Detail")),y.tag("grid",p.e' +
                    'xtend({$mixins:[b("vpc_vpn_gw_columns_v20151124")],getHeadContent:function(e){re' +
                    'turn"localSlaIp"===e.key?e.name+'<div class="tc-15-bubble-icon tc-15-triangle-al' +
                    'ign-center"><i class="tc-icon icon-what"></i><div class="tc-15-bubble tc-15-bubb' +
                    'le-top"><div class="tc-15-bubble-inner">SLA IP用于VPN通道监控，非特殊情况无需更改</div></div></d' +
                    'iv>':void 0},getActions:function(e){var t=[];return e.invalidDeal?t.push({act:"r' +
                    'eCreate",text:"重新创建"}):e.bigDealId?(t.push({act:"continuePay",text:"继续支付",disabl' +
                    'ed:e.agentPay,title:e.agentPay&&"该VPN网关处于等待代理商付款状态，请联系您的代理商及时付款。"||""}),t.push({' +
                    'act:"clear",text:"删除"})):(t.push({act:"renew",text:"续费",disabled:!e.$editable}),' +
                    't.push({act:"upgrade",text:"升级",disabled:!e.$editable||e.reachMaxQuota,title:e.r' +
                    'eachMaxQuota?"VPN网关规格已达上限":""})),t},getCellContent:function(e,t,i){var n;switch(' +
                    'i.key){case"vpnGwWanIp":n='<span class="text-overflow">'+(e||"-")+"</span>";brea' +
                    'k;case"localSlaIp":n='<span class="text-overflow m-width">'+(t.slaIpChanging?'<i' +
                    ' class="n-loading-icon"></i>':"")+e+"</span>"+(t.$editable?' <i class="pencil-ic' +
                    'on hover-icon" data-role="changeSlaIp" data-title="编辑"></i>':"");break;case"band' +
                    'width":n=e?'<span class="text-overflow">'+e+"Mbps</span>":"-"}return n}},{defaul' +
                    'ts:{colums:[{key:"vpnGwName",name:"ID/名称",required:!0,tdTpl:'<p><a data-role="{{' +
                    'item.$loading ? \'\' : \'detail\'}}" href="javascript:;" class="text-overflow"><' +
                    'span class="text-overflow">{{item.unVpnGwId}}</span></a></p><span class="text-ov' +
                    'erflow m-width"><i b-if="item.renaming" class="n-loading-icon"></i>{{item.vpnGwN' +
                    'ame || "未命名"}}</span>{{{item.$editable ? renameIconHtml : ""}}}'},{name:"监控",key' +
                    ':"_monitor",width:50,minWidth:50,tdTpl:'<i b-if="item.$editable" class="dosage-i' +
                    'con" data-role="chart" data-title="查看监控"></i>{{item.$editable ? "":"-"}}'},{key:' +
                    '"vpnGwStatus",name:"状态",tdTpl:'<span class="text-overflow {{item.statusCls}}">{{' +
                    'item.statusText}}</span>'},{key:"vpnGwWanIp",name:"公网IP"},{key:"unVpcId",name:"所' +
                    '属网络",tdTpl:c.vpcTdTpl},{key:"bandwidth",name:"网关规格",hide:!0},{key:"isAutoRenewal' +
                    's",name:"自动续费",tdTpl:"<label title=\"{{item.autoRenewalsChanging ? (item.isAutoR' +
                    'enewals ? '正在开启' : '正在关闭') : (item.isAutoRenewals ? '已开启' : '已关闭')}}\" data-role' +
                    '=\"changeAutoRenewals\" class=\"tc-15-switch {{item.isAutoRenewals ? 'tc-15-swit' +
                    'ch-checked' : ''}} {{item.autoRenewalsChanging ? 'indeterminate' : ''}}\"><input' +
                    ' type=\"checkbox\" class=\"tc-15-switch-input\" disabled?=\"!item.$editable\" b-' +
                    'model=\"item.isAutoRenewals\"><span class=\"tc-15-switch-helper\">{{item.isAutoR' +
                    'enewals ? '已开启' : '已关闭'}}</span></label>"},{key:"expireTime",name:"到期时间",tdTpl:'' +
                    '<p><span class="text-overflow">{{item.expireTime}}</span></p><span b-if="item.is' +
                    'AutoRenewals && !item.autoRenewalsChanging || !item.isAutoRenewals && item.autoR' +
                    'enewalsChanging" class="text-overflow">到期后自动续费1个月</span>'},{key:"_action",name:"' +
                    '操作",minWidth:100,required:!0,tdTpl:"{{>ACTION_TD}}"}],outerPaging:!0}})),e("../C' +
                    'heckbox");i.exports=s.extend().mixin(e("widget/page/mixins/grid/chart")).mixin({' +
                    'routeBase:"/vpc/vpnGw",params:$.extend({},s.params,{keyword:"",order:{defaults:0' +
                    ',formula:function(e){return"desc"===e?1:0}},orderField:"",detailId:{defaults:"",' +
                    'history:!0}}),Bee:y,watchFields:["regionId","unVpcId"],idProperty:"unVpnGwId",na' +
                    'meProperty:"vpnGwName",onRender:function(){var t=this,i=t.bee;e("../adTip").init' +
                    '(t.$el),t[w]=!0,t.vpcId2UnId(!0).always(function(){t[w]=!1,t.bufferReload()}),i.' +
                    '$refs.vpcSelect.$watch("list",function(e){i.$set("addDisabled",e.length<=1?1:"")' +
                    ',i.$set("addDisabledText",e.length<=1?"无可用私有网络":"")},!0)},getData:function(){var' +
                    ' e=this;if(!e[w]){var t=e.bee,i=t.count,n=t.page,a=i*(n-1),s=i,d=t.keyword,c=$.D' +
                    'eferred(),p=$.Deferred();return o.getVpcVpnGwStatisticListEx(t.regionId,t.unVpcI' +
                    'd,"",d,a,s,function(e){p.resolve(l.translate(e,{totalCount:"totalNum",data:{key:' +
                    '"detail",map:"vpnGw"}}))},p.reject),$.when(p,r.getItemProcessorTask()).then(func' +
                    'tion(e,t){var i=e&&e.detail||[];c.resolve({totalNum:e.totalNum,list:i.map(t)})})' +
                    ',c}},onActionAdd:function(){var e=this;o.isPrepaid().then(function(){m.addVpn({s' +
                    'pecifyVpcId:e.bee.oldVpcId},function(){e.bufferReload()})},function(){g.create('' +
                    '<p>当前为后付费月结的购买方式，需要切换到预付费购买方式才能购买VPN网关，查看<a target="_blank" href="https://www.qc' +
                    'loud.com/doc/product/213/5041">详细说明</a></p>',"500","",{title:"提示",button:{"切换到预付' +
                    '费":function(){location.href="http://manage.qcloud.com/shoppingcart/changeguide.p' +
                    'hp"}}})})},onGridDetail:function(e,t,i){var n=this,a=n.bee;v.click("vpc.vpnGatew' +
                    'ay.detail"),a.$set({detailVpcId:e.unVpcId,detailData:e,detailUpdate:function(e){' +
                    'n.grid.updateItem(t,e)},detailId:e.unVpnGwId})},onGridContinuePay:function(e){e.' +
                    'bigDealId&&window.open("http://manage.qcloud.com/deal/dealsConfirm.php?payMode=1' +
                    '&dealIds[]="+e.bigDealId)},onGridClear:function(e,t,i){var n=this,a=n.bee,s=n.gr' +
                    'id;new h({target:$(i.currentTarget),box:this.$gridBody,$data:{title:"确定删除该VPN网关？' +
                    '"},onConfirm:function(){s.updateItem(t,{$removing:!0}),o.clearVpnGw(a.regionId,e' +
                    '.vpcId,e.vpnGwId,function(){n.bufferReload(),d.resetNamespace("vpnGw",a.regionId' +
                    ',e.vpcId,e.vpnGwId),d.resetNamespace("vpnGw",a.regionId,e.unVpcId,e.unVpnGwId)},' +
                    'function(){s.updateItem(t,{$removing:!1})})}})},onGridReCreate:function(e){var t' +
                    '=e.vpcId;m.addVpn({specifyVpcId:t,dealId:e.dealId},function(){})},onGridRename:f' +
                    'unction(e,t,i){var n=this,a=n.grid,s=n.bee;new u({regionId:s.regionId,unVpcId:e.' +
                    'unVpcId,vpcId:e.vpcId,unVpnGwId:e.unVpnGwId,vpnGwId:e.vpnGwId,value:e.vpnGwName,' +
                    'item:e,target:$(i.currentTarget).parent(),onSave:function(e,i,n){this.handleSimp' +
                    'leEdit({def:e,host:a.list,index:t,key:"vpnGwName",loadingKey:"renaming",newValue' +
                    ':i,oldValue:n})}})},onGridChangeAutoRenewals:function(e,t,i){var n=this,a=n.bee,' +
                    's=n.grid,d=+e.isAutoRenewals>0;if(e.$editable&&!e.autoRenewalsChanging){var o=!d' +
                    ',c=l.request(a.regionId,"ModifyVpnGw",{vpcId:e.unVpcId,vpnGwId:e.unVpnGwId,vpnGw' +
                    'Name:e.vpnGwName,isAutoRenewals:o?1:0},"修改VPN网关失败！");f.prototype.handleSimpleEdi' +
                    't({def:c,host:s,method:"updateItem",index:t,key:"isAutoRenewals",loadingKey:"aut' +
                    'oRenewalsChanging",newValue:o,oldValue:d})}},onGridChangeSlaIp:function(e,t,i){v' +
                    'ar n=this,a=n.bee,s=n.grid,d=e.localSlaIp,o=c.getLocalSlaIpMeta(a.regionId);new ' +
                    'f({$data:{inputTpl:'<vpc-ip-input b-ref="ipInput" b-model="value" net="'+o.net+'' +
                    '" min-mask="'+o.minMask+'"></vpc-ip-input>',value:d},target:$(i.currentTarget).p' +
                    'arent(),onYes:function(){this.$refs.ipInput.getValue(),f.prototype.onYes.apply(t' +
                    'his,arguments)},validate:I.localSlaIp,doSave:function(t,i){return l.request(a.re' +
                    'gionId,"ModifyVpnGw",{vpcId:e.unVpcId,vpnGwId:e.unVpnGwId,vpnGwName:e.vpnGwName,' +
                    'isAutoRenewals:e.isAutoRenewals?1:0,localSlaIp:t},"修改VPN网关失败！")},onSave:function' +
                    '(e,i,n){this.handleSimpleEdit({def:e,host:s,method:"updateItem",index:t,key:"loc' +
                    'alSlaIp",loadingKey:"slaIpChanging",newValue:i,oldValue:n})}})},onGridRenew:func' +
                    'tion(e,t,i){var n=this;m.renewVpn(e,function(){n.bufferReload()})},onGridUpgrade' +
                    ':function(e,t,i){var n=this;m.updateVpn(e,function(){n.bufferReload()})}})});'