angular
    .module('baocloud.components.boxhvr',[])
    .directive('boxhvr', boxhvr)
    .controller('BoxhvrCtrl', BoxhvrCtrl);

function boxhvr() {
    var directive = {
        restrict: 'AE',
        transclude: true,
        scope: {
            model: '=',
            animat: '@',
            color: '@'
        },
        templateUrl: function (element, attrs) {
            return attrs.templateUrl || 'boxhvr/boxhvr.html';
        },
        controller: 'boxhvrCtrl'

    };
    return directive;
}

function BoxhvrCtrl($scope){
    console.log('BoxhvrCtrl-------->'+JSON.stringify($scope.animat));
}