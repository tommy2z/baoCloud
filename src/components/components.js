
var components = exports = module.exports = {};

components.modules = [
    'baocloud.components.sidebar',
    'baocloud.components.screenPanel',
    'baocloud.components.subnetView',
    'baocloud.components.boxhvr'
];

components.styles = [
    'sidebar/style',
    'screenPanel/style',
    'subnetView/style',
    'boxhvr/style'
];

components.templates = [
    './sidebar/sidebar.html',
    './screenPanel/screenPanel.html',
    './subnetView/subnetView.html',
    './boxhvr/boxhvr.html'
];
