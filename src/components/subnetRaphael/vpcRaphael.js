/**
 *
 */
angular
    .module('baocloud.components.screenPanel',[])
    .factory('screenPanelHandle',screenPanelHandle)
    .controller('ScreenPanelCtrl', ScreenPanelCtrl)
    .directive('screenPanel', screenPanel)

function screenPanelHandle() {
    return {
        resize: function(){}
    }
}

function screenPanel() {
    var directive = {
        restrict: 'AE',
        replace: true,
        transclude: true,
        scope: {
            model: '='
        },
        templateUrl: function (element, attrs) {
            return attrs.templateUrl || 'screenPanel/screenPanel.html';
        },
        link:link,
        controller: 'ScreenPanelCtrl',
        controllerAs: 'screenPanel'

    };
    return directive;
}

function link(scope, element, attr, ctrl){

}

function ScreenPanelCtrl($scope, $element, screenPanelHandle){
    //var model = this.model = $scope.model;
    console.log('----->ScreenPanelCtrl');


    screenPanelHandle.resize = function resize(e){
        setTimeout(function(){
            $element.toggleClass('full-screenPanel');
        },0);
    };



}
