/**
 *
 */
angular
    .module('baocloud.components.subnetView',[])
    .directive('subnetView', subnetView)
    .directive('onFinishRender', onViewFinishRender)
    .controller('SubnetViewCtlr', SubnetViewCtlr);


function subnetView() {
    var directive = {
        restrict: 'AE',
        replace: true,
        transclude: true,
        scope: {
            model: '='
        },
        templateUrl: function (element, attrs) {
            return attrs.templateUrl || 'subnetView/subnetView.html';
        },
        link:link,
        controller: 'SubnetViewCtlr',
        controllerAs: '$ctrl'

    };
    return directive;
}

function link(scope, element, attr, ctrl){

}

function SubnetViewCtlr($scope, $element){
    var model = this.model = $scope.model;
    var defaultHeight = model.height;
    var items = this.items = [];

    var itemStyle = this.itemStyle = {
        height: defaultHeight +'px'
    };

    this.onCollapsed = function(index) {
        var item = items[index];
        //item.height = item.hasMore ? item.height + defaultHeight : item.height - defaultHeight;
        item.height = item.hasMore ? item.height + defaultHeight : defaultHeight;
        item.hasMore = item.height < item.maxHeight;
        itemStyle.height = item.height + 'px';
        //console.log('collapsed---->',JSON.stringify(items));
    }

    $scope.$on('onViewFinish', function(event){
        $element.find('.subnet-group ul').each(function(index,item){
            items.push({
                height: defaultHeight, 
                maxHeight: item.clientHeight, 
                hasMore: defaultHeight < item.clientHeight,
                isMore: item.clientHeight - defaultHeight
            });
        });
        event.stopPropagation();
        console.log('--->',items);
    });

}


function onViewFinishRender($timeout) {
    var directive = {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('onViewFinish');
                },0);
            }
        }
    };
    return directive;
}