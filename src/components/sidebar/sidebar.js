/**
 *
 */
angular
    .module('baocloud.components.sidebar',[])
    .factory('sidebarHandle',sidebarHandle)
    .controller('SidebarCtrl', SidebarCtrl)
    .directive('sidebar', sidebar)
    .directive('onRepeatFinish', onRepeatFinish);

function sidebarHandle() {
    return {
        resize: function(){}
    }
}

function sidebar() {
    var directive = {
        restrict: 'AE',
        replace: true,
        transclude: true,
        scope: {
            model: '='
        },
        templateUrl: function (element, attrs) {
            return attrs.templateUrl || 'sidebar/sidebar.html';
        },
        link:link,
        controller: 'SidebarCtrl',
        controllerAs: 'sidebar'

    };
    return directive;
}

function link(scope, element, attr, ctrl){

}

function SidebarCtrl($scope, $element, $attrs, sidebarHandle){
    //var model = this.model = $scope.model;
    console.log('----->SidebarCtrl');
    $scope.$on('onViewFinish', function(event){
        event.stopPropagation();
        console.log('--->',items);
    });

    sidebarHandle.resize = function resize(e){
        setTimeout(function(){
            $element.toggleClass('aside-fullscreen');
        },0);

        //$attrs.class = $attrs.class + " aside-fullscreen";
        console.log('resize--->',$attrs);
    };



}


function onRepeatFinish($timeout) {
    var directive = {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('onViewFinish');
                },0);
            }
        }
    };
    return directive;
}