# baoCloud

## 项目构建
```
npm install
npm start
```
## 创建页面模块步骤:

1. `modules/` 目录下新增加页面模块目录new
2. `modules/new/` 新建index.js, 创建angular模块格式：
    ```javascript
    angular.module('baocloud.new', []);
    require('./NewCtlr');
    ```
3. 创建NewCtlr.js controller
    ```javascript
    angular
        .module('baocloud.new')
        .controller('NewCtlr',NewCtlr);

    function NewCtlr($scope){
        //...
    }
    ```
4. 创建模板文件new.html 和 样式表new.less
5. 注册new模块: 文件`config/app.js`
   ```javascript
   require('../modules/new');
   ```
6. 注册模板：文件`config/templates.js`
    ```javascript
    require('../modules/new/new.html');
    ```
7. 注册样式表: 文件`config/modules.less`
    ```less
    @import '@{modules}/new/new.less';
    ```
8. 配置模板路由: `config/routes.js`
    ```javascript
    .state('new',{
        url: '/new',
        title: 'new',
        templateUrl: 'new/new.html'
    })
    ```
9. 重启 `npm start`