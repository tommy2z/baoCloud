 var c = e("lib/raphael")
      , r = {
        IMG_URL: "//imgcache.qq.com/qcloud/app/resource/images/vpctopo/",
        STROKE: "#c7cfdc",
        STROKE_WIDTH: 2,
        LINE_STROKE: "#ffc633",
        LINE_STROKE_WIDTH: 2,
        HOVER_STROKE: "#0070cc",
        FILL: "#fafbff",
        HOVER_FILL: "#fff",
        FONT_FILL: "#0071cc",
        FONT_FAMILY: '"Hiragino Sans GB","Tahoma","microsoft yahei ui","microsoft yahei","simsun"',
        CONNECTOR: {
            RADIUS: 2,
            SPACING: 7
        },
        ROUTER: {
            V_INDENT: 120,
            WIDTH: 40,
            HEIGHT: 40,
            IMG_SRC: "router.png"
        },
        VPN_CONN: {
            WIDTH: 29,
            HEIGHT: 29
        },
        OUTER: {
            WIDTH: 200
        },
        ENTRY: {
            INDENT: 20,
            V_INDENT: 20,
            WIDTH: 100,
            HEIGHT: 100,
            SPACING: 65,
            V_SPACING: 20,
            RADIUS: 5,
            FILL: "#fff",
            IMG: {
                WIDTH: 40,
                HEIGHT: 40,
                INDENT: 29,
                V_INDENT: 18,
                FILL: "#fafbff",
                HOVER_FILL: "#fff"
            },
            SPLITTER: {
                V_INDENT: 73.5,
                STROKE_WIDTH: 1
            },
            TEXT: {
                INDENT: 50,
                V_INDENT: 87,
                V_INDENT_SOLID: 80,
                FONT_SIZE: "14px",
                FONT_FILL: "#0071cc",
                FONT_FILL_SOLID: "#727f9e"
            }
        },
        SUBNET: {
            HEIGHT: 120,
            INDENT: 0,
            V_INDENT: 0,
            LINE: {
                V_INDENT: 50
            },
            TEXT: {
                INDENT: 100,
                V_INDENT: 35,
                SIZE: "12px",
                FILL: "#000",
                CIDR_FILL: "#888"
            },
            RESOURCE: {
                INDENT: 100,
                SPACING: 30,
                V_INDENT: 80,
                WIDTH: 80,
                HEIGHT: 30,
                RADIUS: 5,
                TEXT: {
                    FILL: "#0070cc",
                    FILL_NULL: "#a2a2a2",
                    X: 60,
                    Y: 14
                },
                CROSS: {
                    WIDTH: 7,
                    HEIGHT: 8,
                    STROKE: "#727f9e",
                    STROKE_WIDTH: 1.5,
                    X: 37,
                    Y: 11
                },
                IMG: {
                    WIDTH: 15,
                    HEIGHT: 15,
                    X: 14,
                    Y: 7
                }
            },
            APPENDIX: {
                WIDTH: 30,
                HEIGHT: 30,
                INDENT: 10,
                SPACING: 10
            }
        }
    }
      , l = r.SUBNET
      , p = l.RESOURCE
      , u = r.ENTRY
      , v = r.CONNECTOR
      , f = r.ROUTER
      , m = r.VPN_CONN
      , h = {
        cdb: "云数据库",
        cmem: "NoSQL",
        lb: "内网负载均衡",
        cvm: "云主机"
    };
    l.INDENT = u.WIDTH / 2 + u.SPACING + f.WIDTH + u.SPACING / 2 - 2;
    var b = function() {
        var e = {
            cvm: "vpcDevices",
            lb: "lbNum",
            cdb: "cdbNum",
            cmem: "cmemNum"
        }
          , t = Object.keys(e)
          , i = r.SUBNET
          , s = i.APPENDIX
          , d = i.LINE
          , o = i.TEXT;
        return function(i, c) {
            var l = this
              , u = i.refs || {}
              , v = i.x
              , f = i.y
              , m = i.data
              , b = i.regHover;
            if (t.forEach(function(t, i) {
                var s = v + p.INDENT + (p.WIDTH + p.SPACING) * i
                  , o = f + p.V_INDENT;
                c || (u[t] = {});
                var r = n.call(l, {
                    x: s,
                    y: o,
                    type: t,
                    count: m[e[t]],
                    refs: u[t]
                }, c);
                c || (b(r, {
                    isTitle: !0,
                    title: h[t]
                }),
                a.call(l, {
                    x: v,
                    y: f + d.V_INDENT,
                    element: r,
                    side: "tb"
                }))
            }),
            !c) {
                var g = $("<span></span>")
                  , I = $("<span></span>")
                  , w = $("<div></div>").append(g, I);
                g.text(m.subnetName),
                I.text("(" + m.subnetCIDR + ")").css("color", o.CIDR_FILL),
                w.css({
                    "font-size": o.SIZE,
                    position: "absolute",
                    left: v + o.INDENT,
                    top: f + o.V_INDENT - parseInt(o.SIZE, 10) / 2
                }).appendTo($(l.canvas).parent()),
                l.relateDom(w);
                var y = [];
                m.rtbId && y.push({
                    type: "routeTable",
                    rtbId: m.rtbId,
                    rtbName: m.rtbName,
                    unRouteTableId: m.uniqRtbId
                }),
                y.forEach(function(e, t) {
                    var i = v + s.INDENT + (s.SPACING + s.WIDTH) * t
                      , n = f + d.V_INDENT - s.HEIGHT / 2
                      , a = l.image(r.IMG_URL + e.type + ".png", i, n, s.WIDTH, s.HEIGHT);
                    b(a, e)
                })
            }
        }
    }()
      , g = function(e) {
        return Math.max(e, 1) * l.HEIGHT
    }
    ;
    i.exports = {
        renderOuter: function(e, t, i) {
            var n, p = 0, v = 0, h = p + u.INDENT + 2 * (u.WIDTH + u.SPACING) + f.WIDTH + u.SPACING / 2, b = v + u.V_INDENT + 4 * (u.HEIGHT + u.V_SPACING), I = o(new c(e[0],h,b)), w = (t.subnetList || []).length, y = l.V_INDENT + g(w), x = {}, $ = [{
                type: "gw",
                dataKey: "gwList",
                title: "公网网关"
            }, {
                type: "lb",
                dataKey: "lbList",
                title: "公网LB"
            }, {
                type: "vpnGw",
                dataKey: "vpnGwList",
                title: "VPN网关"
            }, {
                type: "privateGw",
                dataKey: "privateGwList",
                title: "专线网关"
            }];
            $ = $.filter(function(e) {
                var i = t[e.dataKey];
                return i && i.length
            }),
            n = 2 * u.V_INDENT + $.length * (u.HEIGHT + u.V_SPACING) - u.V_SPACING;
            var C = p + (u.INDENT + 2 * (u.WIDTH + u.SPACING))
              , T = v + Math.min($.length ? n / 2 : 200, y - 20)
              , N = (p + u.INDENT + 2 * u.WIDTH + u.SPACING + C) / 2
              , k = T
              , P = x.router = I.image(r.IMG_URL + "router.png", C, T - f.HEIGHT / 2, f.WIDTH, f.HEIGHT);
            if (a.call(I, {
                element: P,
                x: C + f.WIDTH + u.SPACING / 2,
                y: T,
                side: "lr",
                noDot: !0
            }),
            $.length && a.call(I, {
                element: P,
                x: N,
                y: k,
                side: "lr",
                noDot: !0
            }),
            $.forEach(function(e, t) {
                var n = p + u.INDENT + u.WIDTH + u.SPACING
                  , s = v + u.V_INDENT + (u.HEIGHT + u.V_SPACING) * t
                  , o = x[e.type] = d.call(I, {
                    x: n,
                    y: s,
                    type: e.type,
                    noTip: "lb" === e.type,
                    title: e.title
                });
                i(o, {
                    type: e.type
                }),
                a.call(I, {
                    element: o,
                    x: N,
                    y: k,
                    side: "lr"
                })
            }),
            x.gw || x.lb) {
                var D = I.set(x.gw, x.lb).getBBox()
                  , _ = p + u.INDENT + u.WIDTH + u.SPACING / 2
                  , E = D.y + D.height / 2
                  , R = p + u.INDENT
                  , V = E - u.HEIGHT / 2
                  , S = d.call(I, {
                    x: R,
                    y: V,
                    solid: !0,
                    type: "internet",
                    title: "INTERNET"
                });
                a.call(I, {
                    element: S,
                    x: _,
                    y: E,
                    side: "lr"
                }),
                [x.gw, x.lb].forEach(function(e) {
                    e && a.call(I, {
                        element: e,
                        x: _,
                        y: E,
                        side: "lr"
                    })
                })
            }
            if (x.vpnGw && t.vpnConnList && t.vpnConnList.length) {
                var A = x.vpnGw.getBBox()
                  , L = p + u.INDENT
                  , G = A.y
                  , M = x.userGw = d.call(I, {
                    x: L,
                    y: G,
                    type: "userGw",
                    title: "对端网关"
                });
                s.call(I, x.userGw, x.vpnGw, "lr");
                var B = I.image(r.IMG_URL + "vpnConn.png", Math.round((L + A.x2 - m.WIDTH) / 2), Math.round(G + u.HEIGHT / 2 - m.HEIGHT / 2), m.WIDTH, m.HEIGHT);
                i(B, {
                    type: "vpnConn"
                }),
                i(M, {
                    type: "userGw",
                    dummy: {
                        type: "vpnConn",
                        st: B
                    }
                })
            }
            if (x.privateGw) {
                var q = x.privateGw.getBBox()
                  , U = p + u.INDENT
                  , F = q.y;
                x.idc = d.call(I, {
                    x: U,
                    y: F,
                    type: "idc",
                    title: "IDC机房",
                    regHover: i
                }),
                s.call(I, x.idc, x.privateGw, "lr")
            }
            return I
        },
        renderInner: function(e, t, i, n) {
            var a = 0
              , s = 0
              , d = "topo-inner"
              , u = t.subnetList.length
              , v = l.HEIGHT
              , f = g(u)
              , m = a + l.INDENT + p.INDENT + 4 * (p.WIDTH + p.SPACING)
              , h = s + l.V_INDENT + f
              , I = n ? e.data(d) : o(new c(e[0],m,h))
              , w = I.refs || (I.refs = {});
            return e.data(d, I),
            n || (w.mainLine = I.path(["M", a + l.INDENT + r.LINE_STROKE_WIDTH / 2, s + l.LINE.V_INDENT, "v", f].join(",")).attr({
                stroke: r.LINE_STROKE,
                "stroke-width": r.LINE_STROKE_WIDTH
            })),
            w.subnetList = w.subnetList || {},
            (t.subnetList || []).forEach(function(e, t) {
                var d = a + l.INDENT
                  , o = s + v * t;
                n || (w.subnetList[t] = {}),
                b.call(I, {
                    x: d,
                    y: o,
                    data: e,
                    regHover: i,
                    refs: w.subnetList[t]
                }, n)
            }),
            I
        }
    }
}),
define("modules/vpc/topo/Tip", function(e, t, i) {
    var n = e("../BeeBase")
      , a = "_dismissTimer"
      , s = n.extend({
        $tpl: '    <div class="tc-15-bubble" b-on="{mouseenter:mouseenter, mouseleave: mouseleave}" style="overflow: hidden; max-width: 360px; z-index: 9999;display:none;" b-style="{width:width}">        <div class="tc-15-bubble-inner">{{title}}</div>    </div>',
        mouseenter: function() {
            var e = this;
            e.focused = !0,
            e.clearDismiss()
        },
        mouseleave: function() {
            this.focused = !1,
            this.delayDismiss()
        },
        clearDismiss: function() {
            var e = this;
            e[a] && (clearTimeout(e[a]),
            e[a] = null )
        },
        delayDismiss: function() {
            var e = this;
            e.clearDismiss(),
            e[a] = setTimeout(function() {
                e[a] = null ,
                e.dismiss()
            }, e.dismissDelay)
        },
        dismiss: function() {
            var e = this;
            e.focused || $(e.$el).hide()
        }
    }, {
        defaults: $.extend({}, n.prototype.defaults, {
            dismissDelay: 500
        })
    });
    i.exports = s
}),
define("modules/vpc/topo/tipsConfig", function(e, t, i) {
    var n = e("../cacher")
      , a = e("constants")
      , s = e("../route/util")
      , d = function(e) {
        return "number" == typeof e && isFinite(e)
    }
      , o = function(e) {
        return d(e) ? {
            text: "" + Math.round(e) + "%",
            cls: e >= 80 ? "warning" : ""
        } : "-"
    }
      , c = function(e) {
        return "" + e + "Mbps"
    }
    ;
    i.exports = {
        routeTable: {
            title: "关联路由表：{{meta.rtbName}}",
            showTime: !1,
            refreshable: !1,
            columns: [{
                key: "dest",
                name: "目的端"
            }, {
                key: "nextType",
                name: "下一跳类型",
                renderer: function(e) {
                    return s.typeMap[e].name
                }
            }, {
                key: "nextHop",
                name: "下一跳",
                tdTpl: '<vpc-route-rule-value types-data="{{typesData}}"	next-type="{{item.nextType}}" b-model="item.nextHop"	options="{{meta}}"	editing=""></vpc-route-rule-value>'
            }],
            loadData: function(e) {
                var t, i, a = this.meta, d = $.Deferred();
                return $.when(s.getExtraData(a.regionId, a.vpcId, a.unRouteTableId).done(function(e) {
                    t = e
                }), n[(e ? "getFresh" : "get") + "VPCRouteTable"](a.regionId, a.vpcId, 0, a.rtbId).done(function(e) {
                    i = e.routeList.map(function(e) {
                        return {
                            dest: e.dest,
                            nextType: e.nextType,
                            nextHop: e.uniqNextHop || e.nextHop
                        }
                    })
                })).then(function() {
                    t.list = i,
                    d.resolve(t)
                }, d.reject),
                d
            }
        },
        gw: {
            title: "公网网关",
            columns: [{
                key: "alias",
                name: "主机名"
            }, {
                key: "wanIp",
                name: "公网IP"
            }, {
                key: "percent",
                name: "网络使用率",
                width: 100,
                renderer: o
            }, {
                key: "bandwidth",
                name: "带宽上限",
                width: 80,
                minWidth: 80,
                renderer: c
            }],
            loadData: function(e) {
                var t = this.meta;
                return n[(e ? "getFresh" : "get") + "GwStatusList"](t.regionId, t.vpcId)
            },
            pickData: function(e) {
                return e.list
            }
        },
        vpnGw: {
            title: "VPN网关信息",
            columns: [{
                key: "vpnGwName",
                name: "VPN网关"
            }, {
                key: "percent",
                name: "网络使用率",
                width: 100,
                renderer: o
            }, {
                key: "bandwidth",
                name: "带宽上限",
                width: 80,
                minWidth: 80,
                renderer: c
            }],
            loadData: function(e) {
                var t = this.meta;
                return n[(e ? "getFresh" : "get") + "VpnGwStatusList"](t.regionId, t.vpcId)
            },
            pickData: function(e) {
                return e.list
            }
        },
        vpnConn: {
            title: "VPN通道信息",
            width: 550,
            columns: [{
                key: "usrGwName",
                name: "对端网关",
                width: 150
            }, {
                key: "vpnConnId",
                name: "通道ID",
                width: 80,
                minWidth: 80
            }, {
                key: "netStatus",
                name: "通道状态",
                width: 80,
                minWidth: 80,
                renderer: function(e, t) {
                    var i, n;
                    if (t.state === a.CONNECT_STATUS.RUNNING)
                        i = "available" === e ? "已联通" : "未联通",
                        "available" === e ? (i = "已联通",
                        n = "succeed") : (i = "未联通",
                        n = "warning");
                    else
                        switch (t.state) {
                        case a.VPN_STATUS.WAIT_PAY:
                            i = "创建中";
                            break;
                        case a.VPN_STATUS.PAY_ERROR:
                            i = "创建出错",
                            n = "error";
                            break;
                        case a.VPN_STATUS.DELIVER_ING:
                            i = "更新中";
                            break;
                        case a.VPN_STATUS.DELIVER_ERROR:
                            i = "更新出错",
                            n = "error";
                            break;
                        case a.VPN_STATUS.DESTROY_ING:
                            i = "销毁中";
                            break;
                        case a.VPN_STATUS.DESTROY_ERROR:
                            i = "销毁错误",
                            n = "error";
                            break;
                        case a.VPN_STATUS.RUNNING:
                            i = "运行中",
                            n = "succeed";
                            break;
                        default:
                            i = "未知"
                        }
                    return {
                        text: i,
                        cls: n
                    }
                }
            }, {
                key: "outTraffic",
                name: "出带宽",
                width: 80,
                minWidth: 80,
                renderer: function(e, t) {
                    var i, n;
                    return e >= 0 ? (i = parseFloat((e / 1024 / 1024).toFixed(1)) + "Mbps",
                    t.percent >= 80 && (n = "warning")) : i = "-",
                    {
                        text: i,
                        cls: n
                    }
                }
            }],
            loadData: function(e) {
                var t = this.meta;
                return n[(e ? "getFresh" : "get") + "VpnConnStatusList"](t.regionId, t.vpcId)
            },
            pickData: function(e) {
                return e.list
            }
        }
    }
}),