var _ = require('underscore');
var fs = require('fs');
var ejs = require('ejs');
var path = require('path');
var components = require('../src/components/components');


var sourcePath = path.resolve(__dirname, './') + path.sep;
var targetPath = path.resolve(__dirname, '../src/components') + path.sep;
console.log('--->',sourcePath);

var cfile = fs.readFileSync(sourcePath + 'component.js.tpl');
cfile = ejs.render(cfile.toString(), {modules: components.modules});
fs.writeFileSync(targetPath.concat('index', ".js"), cfile);

var lfile = fs.readFileSync(sourcePath + 'component.less.tpl');
lfile = ejs.render(lfile.toString(), {styles: components.styles});
fs.writeFileSync(targetPath.concat('components', ".less"), lfile);

var tfile = fs.readFileSync(sourcePath + 'templates.js.tpl');
tfile = ejs.render(tfile.toString(), {templates: components.templates});
fs.writeFileSync(targetPath.concat('templates', ".js"), tfile);