var modules = require('./components').modules;

angular.module('baocloud.components',['baocloud.components.templates'].concat(modules));

<% modules.forEach(function(module){ %>
require('<%= module.replace(/baocloud.components./,'./') %>');
<% }); %>